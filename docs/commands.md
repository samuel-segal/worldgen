Commands
========

*WorldGen* supports a number of command line options for configuration and 
editing. These are mostly used for testing and development purposes at the
moment.

Run with the following:

```
java -jar worldgen.jar <command> <options...>
```

status
------

Returns the current status of the system.


server
------

Starts a web server.

sectors
-------

Lists all known sectors.

sector
------

``<x,y> <name> ...``

Creates one or more new sectors. The x,y coordinates specify the position
of the sector, and the name is the name to use. Multiple pairs of <x,y>
and <name> can be specified to create multiple sectors.

system
------
``<sector> <xxyy> [<name>|<Creator> <Method>]``

Creates a star system in the given sector at the given coordinates. Sector
can be specified as an id, a name or a coordinate.

If 'xx' is specified as the system coordinate, then a random coordinate within
the sector will be chosen.

Either a name can be given for the system, or a random name will be selected
if no name is given.

Instead of a name, you can specify a specific Creator and Method to use when
creating the system.

