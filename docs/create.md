Creating Star Systems
=====================

Simple
------

A simple star system designed for testing purposes. Not designed for creating
production systems due to lack of variance.

**Sol**

Creates a star system very similar to the Sol system. The number and 
type of planets is identical. Used for testing the common worlds types
to ensure 'normal' things come out as expected.


**SingleStar**

Creates a system with a single star similar to Sol, but with more
variance than the *Sol* option. Planets are similar in type, but not
exactly the same.


**RockyWorlds**

Creates a small system with a few rocky inner worlds.


**HotWorlds**

Creates a single hot star with a few rocky inner worlds for testing one
extreme of world creation.


Barren
------

A barren star system with no life bearing worlds. More varied than *Simple*
and designed for testing but also suitable for production systems.


BlueGiant
---------



BrownDwarf
----------
