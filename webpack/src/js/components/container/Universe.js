import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import GalacticMap from './GalacticMap';

/**
 * The header at the top of the page.
 *
 * @param props
 * @returns {*}
 * @constructor
 */
function Header(props) {
    return <h1 className="header">{props.name}</h1>;
}

function PrettyNumber(props) {
    return props.value.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
}

function Statistics(props) {
    return (
        <div class="header-stats">
            <dl class="statistics">
                <dt>Named Sectors</dt>
                <dd><PrettyNumber value={props.data.sectors}/></dd>
                <dt>Star Systems</dt>
                <dd><PrettyNumber value={props.data.systems}/></dd>
                <dt>Planets</dt>
                <dd><PrettyNumber value={props.data.planets}/></dd>
                <dt>Population</dt>
                <dd><PrettyNumber value={props.data.population}/></dd>
            </dl>
        </div>
    );
}


class Universe extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            universe: false,
            statistics: false
        }
    }

    componentDidMount() {
        var _self = this;
        fetch("/api/universe", { headers: { 'Content-Type': 'application.json'}})
            .then(function(response) {
                return response.json();
            })
            .then(function(data) {
                _self.setState({ universe: data });
            })
            .catch(function(error) {
                console.log(error);
            })
        fetch("/api/statistics", {headers: { 'Content-Type': 'application.json' }})
            .then(function(response) {
                return response.json();
            })
            .then(function(data) {
                _self.setState({ statistics: data });
            })
            .catch(function (error) {
                console.log(error);
            })
    }



    renderHeader(name) {
        return <Header name={name} />;
    }

    renderStatistics(statistics) {
        console.log(statistics.planets);
        return <Statistics data={statistics} />
    }

    render() {
        if (this.state.universe && this.state.statistics) {
            return (
                <div>
                    <div className="heading">
                        {this.renderHeader(this.state.universe.name)}
                        {this.renderStatistics(this.state.statistics)}
                    </div>
                    <div>
                        <GalacticMap universe={this.state.universe}/>
                    </div>
                </div>
            );
        } else {
            return false;
        }
    }
}

ReactDOM.render(
    <Universe />,
    document.getElementById('root')
);

