import React, { Component } from 'react';
import AppBar from '@material-ui/core/AppBar';
import Typography from '@material-ui/core/Typography';

export default class Header extends Component {

    componentDidMount() {
        var _self = this;
        fetch("/api/universe", { headers: { 'Content-Type': 'application.json'}})
            .then(function(response) {
                return response.json();
            })
            .then(function(data) {
                _self.setState({ universe: data });
            })
            .catch(function(error) {
                console.log(error);
            })
    }

    render() {
        return (
            <AppBar position={"static"}>
                <Typography variant="h2" align={"left"} component={"h1"}>
                    WorldGen Universe
                </Typography>
            </AppBar>
        );
    }
}
