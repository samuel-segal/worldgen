
import universeReducer from './universe';

import {combineReducers} from 'redux';

const allReducers = combineReducers({
    universe: universeReducer
});

export default allReducers;
