/*
 * Copyright (c) 2021, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */

package uk.org.glendale.worldgen.astro.systems.generators.deepnight;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.org.glendale.utils.rpg.Die;
import uk.org.glendale.utils.rpg.Roller;
import uk.org.glendale.worldgen.WorldGen;
import uk.org.glendale.worldgen.astro.Physics;
import uk.org.glendale.worldgen.astro.planets.Planet;
import uk.org.glendale.worldgen.astro.planets.PlanetFactory;
import uk.org.glendale.worldgen.astro.planets.codes.PlanetType;
import uk.org.glendale.worldgen.astro.planets.generators.belt.IceBelt;
import uk.org.glendale.worldgen.astro.sectors.Sector;
import uk.org.glendale.worldgen.astro.stars.*;
import uk.org.glendale.worldgen.astro.systems.*;
import uk.org.glendale.worldgen.exceptions.DuplicateObjectException;

import java.util.List;

/**
 * Generate a barren, type M system for the Deepnight campaign.
 */
public class DNBarrenK extends DNBarren {
    private static final Logger logger = LoggerFactory.getLogger(DNBarrenK.class);

    public DNBarrenK(WorldGen worldgen) {
        super(worldgen);
    }

    /**
     * Creates a barren star system. Such a system has one or more stars, with planets, but the
     * planets are barren with no civilisation or life of any kind. If the system has multiple stars,
     * the second star is much smaller and a long way away.
     *
     * @param sector    Sector to generate system in.
     * @param name      Name of the system.
     * @param x         X coordinate of the system in the sector (1-32).
     * @param y         Y coordinate of the system in the sector (1-40).
     *
     * @return          Created and persisted star system.
     * @throws DuplicateObjectException     If a duplicate system is created.
     */
    public StarSystem generate(Sector sector, String name, int x, int y) throws DuplicateObjectException {
        StarSystem system = createEmptySystem(sector, name, x, y);

        switch (Die.d6(2)) {
            case 2:
                // Just a single star by itself.
                createEmptySystem(system);
                break;
            case 3:
                createDustOnly(system);
                break;
            case 4: case 5:
                createAsteroidsOnly(system);
                break;
            case 6: case 7: case 8:
                // Barren system with some rock worlds.
                createRockWorlds(system);
                break;
            case 9: case 10:
                createColdGiants(system);
                break;
            case 11:
                createHotGiants(system);
                break;
            case 12:
                createProto(system);
                break;
        }

        colonise(system);

        updateStarSystem(system);

        return system;
    }

    /**
     * This system is empty, it only has a single red dwarf star and no planets.
     */
    public void createEmpty(StarSystem system) throws DuplicateObjectException {
        logger.info(String.format("Generating [BarrenK] [Empty] system [%s]", system.getName()));

        StarGenerator starGenerator = new StarGenerator(worldgen, system, false);
        // On average a cool main-sequence star will be generated.
        Star primary = starGenerator.generatePrimary(Luminosity.VI,
                SpectralType.K5.getSpectralType(Die.dieV(4)));
        system.addStar(primary);
        system.setType(StarSystemType.SINGLE);

        if (Die.d100() <= 30) {
            PlanetFactory planetFactory = worldgen.getPlanetFactory();
            long         distance = (100 + Die.dieV(20)) * Physics.AU;
            String       name = StarSystemFactory.getBeltName(primary, 1);
            PlanetType   type = PlanetType.OortCloud;
            List<Planet> planets = planetFactory.createPlanet(system, primary, name, type, distance);

            system.addPlanets(planets);
        }

        setDescription(system, null);
    }

    /**
     * Creates a simple system around an M class main sequence star. A simple system is one with a pretty traditional
     * layout, with rocky worlds in the inner system, and gassy worlds towards the outer edge.
     */
    public void createSimple(StarSystem system) throws DuplicateObjectException {
        logger.info(String.format("Generating [DNBarrenK] [Simple] system [%s]", system.getName()));

        StarGenerator starGenerator = new StarGenerator(worldgen, system, false);
        PlanetFactory planetFactory = worldgen.getPlanetFactory();

        // On average a cool main-sequence star will be generated.
        Star primary = starGenerator.generatePrimary(Luminosity.V,
                SpectralType.K5.getSpectralType(Die.dieV(6)));
        system.addStar(primary);
        system.setType(StarSystemType.SINGLE);

        // Start at a reasonably hot orbit, but not too hot. Add a bit of variety.
        int  orbit = 1;
        int  belt = 1;
        long distance = Physics.getOrbitWithTemperature(primary, 500);
        distance += Die.die(distance / 5, 2);

        // Create a random number of Dwarf Terrestrial worlds.
        int num = Die.d4() + 1;
        while (num-- > 0) {
            String      name = StarSystemFactory.getPlanetName(primary, orbit++);
            int         k = Physics.getTemperatureOfOrbit(primary, distance);
            PlanetType  type = getDwarfTerrestrial(CircumstellarZone.getZone(k));

            List<Planet> planets = planetFactory.createPlanet(system, primary, name, type, distance);
            system.addPlanets(planets);

            distance += Die.die(distance / 3, 2) + distance / 3;
        }

        if (Die.d6() == 1) {
            String      name = StarSystemFactory.getPlanetName(primary, belt++);
            int         k = Physics.getTemperatureOfOrbit(primary, distance);
            PlanetType  type = getBelt(CircumstellarZone.getZone(k));

            distance *= 1.2;
            List<Planet> planets = planetFactory.createPlanet(system, primary, name, type, distance);
            system.addPlanets(planets);
            distance *= 1.2;
        } else {
            distance += Die.die(distance) / 2;
        }

        num = Die.d3() - 1;
        while (num-- > 0) {
            String      name = StarSystemFactory.getPlanetName(primary, orbit++);
            int         k = Physics.getTemperatureOfOrbit(primary, distance);
            PlanetType  type = getJovian(CircumstellarZone.getZone(k));

            List<Planet> planets = planetFactory.createPlanet(system, primary, name, type, distance);
            system.addPlanets(planets);

            distance *= 1.5;
            distance += Die.die(distance) / 5;
        }

        if (Die.d3() == 1) {
            String      name = StarSystemFactory.getPlanetName(primary, belt++);
            int         k = Physics.getTemperatureOfOrbit(primary, distance);
            PlanetType  type = getBelt(CircumstellarZone.getZone(k));

            distance *= 1.2;
            List<Planet> planets = planetFactory.createPlanet(system, primary, name, type, distance);
            system.addPlanets(planets);
            distance *= 1.2;
        }

        // Oort Cloud
        if (Die.d100() <= 50) {
            String      name = StarSystemFactory.getPlanetName(primary, belt++);
            int         k = Physics.getTemperatureOfOrbit(primary, distance);
            PlanetType  type = PlanetType.OortCloud;

            distance += Physics.AU;
            distance *= (100 + Die.dieV(20));
            List<Planet> planets = planetFactory.createPlanet(system, primary, name, type, distance);
            system.addPlanets(planets);
        }

        // Good chance of it suffering from solar flares.
        if (Die.d3() != 1) {
            system.addTradeCode(StarSystemCode.Sf);
        }
        setDescription(system, null);
    }

    /**
     * Create a system with some (probably) rocky worlds. There is a chance of an asteroid/ice belt.
     *
     * @param system
     * @throws DuplicateObjectException
     */
    public void createRockWorlds(StarSystem system) throws DuplicateObjectException {
        logger.info(String.format("Generating [DNBarrenM] [RockWorlds] system [%s]", system.getName()));

        StarGenerator starGenerator = new StarGenerator(worldgen, system, false);
        PlanetFactory planetFactory = worldgen.getPlanetFactory();

        // On average a cool main-sequence star will be generated.
        Star primary = starGenerator.generatePrimary(new Roller<Luminosity>(Luminosity.VI, Luminosity.V).roll(),
                SpectralType.K5.getSpectralType(Die.dieV(6)));
        system.addStar(primary);
        system.setType(StarSystemType.SINGLE);

        // Start at a reasonably hot orbit, but not too hot. Add a bit of variety.
        long distance = Physics.getOrbitWithTemperature(primary, 500);
        distance += Die.die(distance / 5, 2);

        // Create a random number of Dwarf Terrestrial worlds.
        int numPlanets = Die.d4() + 1;
        for (int orbit = 1; orbit <= numPlanets; orbit++) {
            String      name = StarSystemFactory.getPlanetName(primary, orbit);
            int         k = Physics.getTemperatureOfOrbit(primary, distance);
            PlanetType  type = getDwarfTerrestrial(CircumstellarZone.getZone(k));

            logger.info(String.format("createRockWorlds: [%d] [%dK] [%s] [%s]",
                    orbit, k, CircumstellarZone.getZone(k), type));

            List<Planet> planets = planetFactory.createPlanet(system, primary, name, type, distance);
            system.addPlanets(planets);

            distance += Die.die(distance / 3, 2) + distance / 3;
        }

        // Potentially, add an asteroid belt in as well. Probably an icebelt by this point.
        if (Die.d2() == 1) {
            distance *= 2;
            String      name = StarSystemFactory.getBeltName(primary, 1);
            int k = Physics.getTemperatureOfOrbit(primary, distance);
            PlanetType type = getBelt(CircumstellarZone.getZone(k));
            List<Planet> planets = planetFactory.createPlanet(system, primary, name, type, distance);

            system.addPlanets(planets);
        }

        // Good chance of it suffering from solar flares.
        if (Die.d3() != 1) {
            system.addTradeCode(StarSystemCode.Sf);
        }
        setDescription(system, null);
    }


    /**
     * This type of star system only has a dust belt and a type M sub-dwarf. No major planets have formed.
     *
     * @param system        Star System to create belt in.
     */
    @SuppressWarnings("WeakerAccess")
    public void createDustOnly(StarSystem system) throws DuplicateObjectException {
        logger.info(String.format("Generating [BarrenMK] [DustOnly] system [%s]", system.getName()));

        StarGenerator starGenerator = new StarGenerator(worldgen, system, false);
        // On average a cool main-sequence star will be generated.
        Star primary = starGenerator.generatePrimary(Luminosity.VI,
                SpectralType.M3.getSpectralType(Die.dieV(6)));
        system.addStar(primary);
        system.setType(StarSystemType.SINGLE);

        // Place the belt around 1AU from the star.
        long distance = 75_000_000 + Die.dieV(25_000_000);
        String name = StarSystemFactory.getBeltName(primary, 1);

        List<Planet> allPlanets;

        allPlanets = worldgen.getPlanetFactory().createPlanet(system, system.getStars().get(0),
                name, PlanetType.DustDisc, distance);

        system.addTradeCode(StarSystemCode.Ba);
        if (Die.d4() == 1) {
            system.addTradeCode(StarSystemCode.Sf);
            system.setZone(Zone.AMBER);
        }

        system.setPlanets(allPlanets);
        setDescription(system, null);
    }

    public void createAsteroidsOnly(StarSystem system) throws DuplicateObjectException {
        logger.info(String.format("Generating [BarrenMK] [AsteroidsOnly] system [%s]", system.getName()));

        StarGenerator starGenerator = new StarGenerator(worldgen, system, false);
        // On average a cool main-sequence star will be generated.
        Star primary = starGenerator.generatePrimary(Luminosity.VI,
                SpectralType.M1.getSpectralType(Die.dieV(6)));
        system.addStar(primary);
        system.setType(StarSystemType.SINGLE);

        // Place the belt around 1AU from the star.
        long distance = 60_000_000 + Die.dieV(25_000_000);
        String name = StarSystemFactory.getBeltName(primary, 1);

        List<Planet> allPlanets;

        allPlanets = worldgen.getPlanetFactory().createPlanet(system, system.getStars().get(0),
                name, PlanetType.AsteroidBelt, distance);

        system.addTradeCode(StarSystemCode.Ba);
        if (Die.d4() == 1) {
            system.addTradeCode(StarSystemCode.Sf);
            system.setZone(Zone.AMBER);
        }

        system.setPlanets(allPlanets);
        setDescription(system, null);
    }


    public void createHotGiant(StarSystem system) throws DuplicateObjectException {
        logger.info(String.format("Generating [BarrenMK] [EpiStellarJovian.properties] system [%s]", system.getName()));

        StarGenerator starGenerator = new StarGenerator(worldgen, system, false);
        // On average a cool main-sequence star will be generated.
        Star primary = starGenerator.generatePrimary(Luminosity.VI,
                SpectralType.M1.getSpectralType(Die.dieV(6)));
        system.addStar(primary);
        system.setType(StarSystemType.SINGLE);

        // Place the belt around 1AU from the star.
        long distance = 1_500_000 + Die.dieV(500_000);
        String name = StarSystemFactory.getPlanetName(primary, 1);

        List<Planet> allPlanets;
        if (Die.d3() == 1) {
            allPlanets = worldgen.getPlanetFactory().createPlanet(system, system.getStars().get(0),
                    name, PlanetType.Sokarian, distance);
        } else {
            allPlanets = worldgen.getPlanetFactory().createPlanet(system, system.getStars().get(0),
                    name, PlanetType.Osirian, distance);
        }

        system.addTradeCode(StarSystemCode.Ba);
        if (Die.d4() == 1) {
            system.addTradeCode(StarSystemCode.Sf);
            system.setZone(Zone.AMBER);
        }

        system.setPlanets(allPlanets);
        setDescription(system, null);
    }

    /**
     * A star system where the main world is a GeoHelian world.
     */
    public void createHelian(StarSystem system) throws DuplicateObjectException {
        logger.info(String.format("Generating [BarrenMK] [Helian] system [%s]", system.getName()));

        StarGenerator starGenerator = new StarGenerator(worldgen, system, false);
        // On average a cool main-sequence star will be generated.
        Star primary = starGenerator.generatePrimary(Luminosity.VI,
                SpectralType.M1.getSpectralType(Die.dieV(6)));
        system.addStar(primary);
        system.setType(StarSystemType.SINGLE);

        // Place the belt around 1AU from the star.
        long distance = 2_500_000 + Die.die(500_000);
        int  numPlanets = Die.d4() + 1;
        String name = StarSystemFactory.getPlanetName(primary, 1);

        List<Planet> planets;
        if (Die.d3() == 1) {
            planets = worldgen.getPlanetFactory().createPlanet(system, system.getStars().get(0),
                    name, PlanetType.Halcyonic, distance);
        } else {
            distance *= 3;
            planets = worldgen.getPlanetFactory().createPlanet(system, system.getStars().get(0),
                    name, PlanetType.Hyperionic, distance);
        }
        system.addPlanets(planets);

        for (int i = 2; i <= numPlanets; i++) {
            distance *= 1.2;
            distance += Die.die(2_000_000, 5);

            CircumstellarZone zone = CircumstellarZone.getZone(Physics.getTemperatureOfOrbit(primary, distance));
            PlanetType type = getDwarfTerrestrial(zone);

            name = StarSystemFactory.getPlanetName(primary, i);
            planets = worldgen.getPlanetFactory().createPlanet(system, system.getStars().get(0),
                name, type, distance);
            system.addPlanets(planets);
        }

        system.addTradeCode(StarSystemCode.Ba);
        if (Die.d2() == 1) {
            system.addTradeCode(StarSystemCode.Sf);
            system.setZone(Zone.AMBER);
        }

        setDescription(system, null);
    }
    /**
     * Create a system with a single red dwarf star, with some worlds in the outer system. Most
     * worlds are small Jovians or icy worlds.
     *
     * @param system    Star system to create planets in.
     */
    public void createColdWorlds(StarSystem system) throws DuplicateStarException {
        system.setType(StarSystemType.SINGLE);

        // Create a cool red dwarf star.
        StarGenerator starGenerator = new StarGenerator(worldgen, system, false);
        Star          primary = starGenerator.generatePrimary(Luminosity.VI, SpectralType.M9.getSpectralType(Die.d6()));
        system.addStar(primary);

        PlanetFactory factory = worldgen.getPlanetFactory();

        long distance = (2 + Die.d4(2)) * Physics.AU + Die.die(Physics.AU);
        String name = StarSystemFactory.getPlanetName(primary, 1);
        List<Planet> planets = null;

        if (Die.d2() == 1) {
            // A small, cold, jovian world.
            planets = factory.createPlanet(system, primary, name, PlanetType.Neptunian, distance);
        } else {
            // A huge, icy, super-Earth.
            planets = factory.createPlanet(system, primary, name, PlanetType.Thean, distance);
        }
        system.addPlanets(planets);

        distance *= 2;
        name = StarSystemFactory.getBeltName(primary, 2);
        planets = factory.createPlanet(system, primary, name, PlanetType.IceBelt, distance,
                planets.get(planets.size()-1), IceBelt.IceBeltFeature.KuiperBelt);
        system.addPlanets(planets);

        setDescription(system, null);
    }

    public void createProto(StarSystem system) throws DuplicateStarException {
        system.setType(StarSystemType.SINGLE);

        // Create a cool red dwarf star.
        StarGenerator starGenerator = new StarGenerator(worldgen, system, false);
        Star          primary = starGenerator.generatePrimary(Luminosity.VI, SpectralType.M2.getSpectralType(Die.d6()));
        system.addStar(primary);

        PlanetFactory factory = worldgen.getPlanetFactory();

        long    distance = 5_000_000 + Die.die(5_000_000, 2);
        String  name;
        int     orbit = 1;

        name = StarSystemFactory.getPlanetName(primary, orbit++);
        system.addPlanets(factory.createPlanet(system, primary, name, PlanetType.ProtoFerrinian, distance));
        distance += 5_000_000;
        name = StarSystemFactory.getPlanetName(primary, orbit++);
        system.addPlanets(factory.createPlanet(system, primary, name, PlanetType.ProtoLithian, distance));
        distance += 5_000_000;
        name = StarSystemFactory.getPlanetName(primary, orbit++);
        system.addPlanets(factory.createPlanet(system, primary, name, PlanetType.ProtoCarbonian, distance));
        distance += 5_000_000;
        name = StarSystemFactory.getPlanetName(primary, orbit++);
        system.addPlanets(factory.createPlanet(system, primary, name, PlanetType.ProtoGelidian, distance));

        setDescription(system, null);
    }

    private void createHotGiants(StarSystem system) throws DuplicateObjectException {
        logger.info(String.format("Generating [DNBarrenM] [HotGiants] system [%s]", system.getName()));

        StarGenerator starGenerator = new StarGenerator(worldgen, system, false);
        PlanetFactory planetFactory = worldgen.getPlanetFactory();

        // On average a cool main-sequence star will be generated.
        Star primary = starGenerator.generatePrimary(Luminosity.V,
                SpectralType.M7.getSpectralType(Die.dieV(3)));
        system.addStar(primary);
        system.setType(StarSystemType.SINGLE);

        // Start at a reasonably hot orbit, but not too hot. Add a bit of variety.
        long distance = Physics.getOrbitWithTemperature(primary, 1500);
        distance += Die.dieV(distance / 5);

        // Create a random number of Dwarf Terrestrial worlds.
        int numPlanets = Die.d3();
        for (int orbit = 1; orbit <= numPlanets; orbit++) {
            String      name = StarSystemFactory.getPlanetName(primary, orbit);
            int         k = Physics.getTemperatureOfOrbit(primary, distance);
            PlanetType  type = getJovian(CircumstellarZone.getZone(k));

            List<Planet> planets = planetFactory.createPlanet(system, primary, name, type, distance);
            system.addPlanets(planets);

            distance += Die.die(distance / 3, 3);
        }

        // Good chance of it suffering from solar flares.
        if (Die.d4() != 1) {
            system.addTradeCode(StarSystemCode.Sf);
        }
        setDescription(system, null);
    }

    private void createColdGiants(StarSystem system) throws DuplicateObjectException {
        logger.info(String.format("Generating [DNBarrenM] [HotGiants] system [%s]", system.getName()));

        StarGenerator starGenerator = new StarGenerator(worldgen, system, false);
        PlanetFactory planetFactory = worldgen.getPlanetFactory();

        // On average a cool main-sequence star will be generated.
        Star primary = starGenerator.generatePrimary(Luminosity.V,
                SpectralType.M7.getSpectralType(Die.dieV(3)));
        system.addStar(primary);
        system.setType(StarSystemType.SINGLE);

        // Start at a reasonably hot orbit, but not too hot. Add a bit of variety.
        long distance = Physics.getOrbitWithTemperature(primary, 200);
        distance += Die.dieV(distance / 5);

        // Create a random number of Dwarf Terrestrial worlds.
        int numPlanets = Die.d3() + 1;
        for (int orbit = 1; orbit <= numPlanets; orbit++) {
            String      name = StarSystemFactory.getPlanetName(primary, orbit);
            int         k = Physics.getTemperatureOfOrbit(primary, distance);
            PlanetType  type = getJovian(CircumstellarZone.getZone(k));

            List<Planet> planets = planetFactory.createPlanet(system, primary, name, type, distance);
            system.addPlanets(planets);

            distance += Die.die(distance / 5, 3);
        }

        // Good chance of it suffering from solar flares.
        if (Die.d3() != 1) {
            system.addTradeCode(StarSystemCode.Sf);
        }
        setDescription(system, null);
    }
}
