/*
 * Copyright (c) 2021, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */

package uk.org.glendale.worldgen.astro.systems.generators.deepnight;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.org.glendale.utils.rpg.Die;
import uk.org.glendale.utils.rpg.Roller;
import uk.org.glendale.worldgen.WorldGen;
import uk.org.glendale.worldgen.astro.Physics;
import uk.org.glendale.worldgen.astro.planets.Planet;
import uk.org.glendale.worldgen.astro.planets.PlanetFactory;
import uk.org.glendale.worldgen.astro.planets.codes.PlanetType;
import uk.org.glendale.worldgen.astro.planets.generators.Belt;
import uk.org.glendale.worldgen.astro.planets.generators.belt.AsteroidBelt;
import uk.org.glendale.worldgen.astro.planets.generators.belt.IceBelt;
import uk.org.glendale.worldgen.astro.planets.generators.belt.OortCloud;
import uk.org.glendale.worldgen.astro.sectors.Sector;
import uk.org.glendale.worldgen.astro.stars.*;
import uk.org.glendale.worldgen.astro.systems.*;
import uk.org.glendale.worldgen.exceptions.DuplicateObjectException;

import java.util.List;

/**
 * Generate a barren, type M system for the Deepnight campaign.
 */
public class DNBarrenM extends DNBarren {
    private static final Logger logger = LoggerFactory.getLogger(DNBarrenM.class);

    public DNBarrenM(WorldGen worldgen) {
        super(worldgen);
    }

    /**
     * Creates a barren star system. Such a system has one or more stars, with planets, but the
     * planets are barren with no civilisation or life of any kind. If the system has multiple stars,
     * the second star is much smaller and a long way away.
     *
     * @param sector    Sector to generate system in.
     * @param name      Name of the system.
     * @param x         X coordinate of the system in the sector (1-32).
     * @param y         Y coordinate of the system in the sector (1-40).
     *
     * @return          Created and persisted star system.
     * @throws DuplicateObjectException     If a duplicate system is created.
     */
    public StarSystem generate(Sector sector, String name, int x, int y) throws DuplicateObjectException {
        StarSystem system = createEmptySystem(sector, name, x, y);

        switch (Die.d6(3)) {
            case 3:
                createEmpty(system);
                break;
            case 4:
                createDustOnly(system);
                break;
            case 6:
                createAsteroids(system);
                break;
           case 7: case 8:
                // Barren system with some rock worlds.
                createRockWorlds(system);
                break;
            case 9: case 10:
                createColdGiants(system);
                break;
            case 11:
                createHotGiants(system);
                break;
            case 12:
                createProto(system);
                break;
            case 16:
                createGiant(system);
                break;
            default:
                createSimple(system);
                break;
        }

        colonise(system);

        updateStarSystem(system);

        return system;
    }

    /**
     * This system is empty, it only has a single red dwarf star and no planets.
     */
    public void createEmpty(StarSystem system) throws DuplicateObjectException {
        logger.info(String.format("Generating [BarrenM] [Empty] system [%s]", system.getName()));

        StarGenerator starGenerator = new StarGenerator(worldgen, system, false);
        // On average a cool main-sequence star will be generated.
        Star primary = starGenerator.generatePrimary(Luminosity.VI,
                SpectralType.M5.getSpectralType(Die.dieV(4)));
        system.addStar(primary);
        system.setType(StarSystemType.SINGLE);

        // Oort Cloud
        if (Die.d100() <= 60) {
            PlanetFactory   planetFactory = worldgen.getPlanetFactory();
            String          name = StarSystemFactory.getOortCloudName(primary, 1);
            PlanetType      type = PlanetType.OortCloud;

            long distance = (long)(Physics.AU * 10_000 * primary.getMass());
            List<Planet> planets = planetFactory.createPlanet(system, primary, name, type, distance,
                    OortCloud.OortCloudFeatures.InnerOort);
            system.addPlanets(planets);

            if (Die.d100() <= 30) {
                name = StarSystemFactory.getOortCloudName(primary, 2);
                distance = (long)(Physics.AU * 40_000 * primary.getMass());
                planets = planetFactory.createPlanet(system, primary, name, type, distance,
                        OortCloud.OortCloudFeatures.OuterOort);
                system.addPlanets(planets);
            }
        }

        setDescription(system, null);
    }

    /**
     * Creates a simple system around an M class main sequence star. A simple system is one with a pretty traditional
     * layout, with rocky worlds in the inner system, and gassy worlds towards the outer edge.
     */
    public void createSimple(StarSystem system) throws DuplicateObjectException {
        logger.info(String.format("Generating [DNBarrenM] [Simple] system [%s]", system.getName()));

        StarGenerator starGenerator = new StarGenerator(worldgen, system, false);
        PlanetFactory planetFactory = worldgen.getPlanetFactory();

        // On average a cool main-sequence star will be generated.
        Star primary = starGenerator.generatePrimary(Luminosity.V,
                SpectralType.M5.getSpectralType(Die.dieV(6)));
        system.addStar(primary);
        system.setType(StarSystemType.SINGLE);

        // Start at a reasonably hot orbit, but not too hot. Add a bit of variety.
        int  orbit = 1;
        int  belt = 1;
        long distance = Physics.getOrbitWithTemperature(primary, 500);
        distance += Die.die(distance / 5, 2);

        // Create a random number of Dwarf Terrestrial worlds.
        int num = Die.d4() + 1;
        while (num-- > 0) {
            String      name = StarSystemFactory.getPlanetName(primary, orbit++);
            int         k = Physics.getTemperatureOfOrbit(primary, distance);
            PlanetType  type = getDwarfTerrestrial(CircumstellarZone.getZone(k));
            if (Die.d3() == 1) {
                type = getTerrestrial(CircumstellarZone.getZone(k));
            }

            List<Planet> planets = planetFactory.createPlanet(system, primary, name, type, distance);
            system.addPlanets(planets);

            distance += Die.die(distance / 3, 2) + distance / 3;
        }

        if (Die.d4() == 1) {
            String      name = StarSystemFactory.getPlanetName(primary, belt++);
            int         k = Physics.getTemperatureOfOrbit(primary, distance);
            PlanetType  type = getBelt(CircumstellarZone.getZone(k));

            distance *= 1.2;
            List<Planet> planets = planetFactory.createPlanet(system, primary, name, type, distance);
            system.addPlanets(planets);
            distance *= 1.2;
        } else {
            distance += Die.die(distance) / 2;
        }

        num = Die.d3();
        while (num-- > 0) {
            String      name = StarSystemFactory.getPlanetName(primary, orbit++);
            int         k = Physics.getTemperatureOfOrbit(primary, distance);
            PlanetType  type = getJovian(CircumstellarZone.getZone(k));

            List<Planet> planets = planetFactory.createPlanet(system, primary, name, type, distance);
            system.addPlanets(planets);

            distance *= 1.5;
            distance += Die.die(distance) / 5;
        }

        if (Die.d2() == 1) {
            String      name = StarSystemFactory.getPlanetName(primary, belt++);
            int         k = Physics.getTemperatureOfOrbit(primary, distance);
            PlanetType  type = getBelt(CircumstellarZone.getZone(k));

            distance *= 1.2;
            List<Planet> planets = planetFactory.createPlanet(system, primary, name, type, distance);
            system.addPlanets(planets);
            distance *= 1.2;
        }

        // Oort Clouds
        if (Die.d100() <= 50) {
            String      name = StarSystemFactory.getPlanetName(primary, belt++);
            PlanetType  type = PlanetType.OortCloud;

            distance += Physics.AU;
            distance *= (100 + Die.dieV(20));
            List<Planet> planets = planetFactory.createPlanet(system, primary, name, type, distance);
            system.addPlanets(planets);
        }

        // Good chance of it suffering from solar flares.
        if (Die.d3() != 1) {
            system.addTradeCode(StarSystemCode.Sf);
        }
        setDescription(system, null);
    }

    /**
     * Create a system with some (probably) rocky worlds. There is a chance of an asteroid/ice belt.
     *
     * @param system
     * @throws DuplicateObjectException
     */
    public void createRockWorlds(StarSystem system) throws DuplicateObjectException {
        logger.info(String.format("Generating [DNBarrenM] [RockWorlds] system [%s]", system.getName()));

        StarGenerator starGenerator = new StarGenerator(worldgen, system, false);
        PlanetFactory planetFactory = worldgen.getPlanetFactory();

        // On average a cool main-sequence star will be generated.
        Star primary = starGenerator.generatePrimary(Luminosity.VI,
                SpectralType.M3.getSpectralType(Die.dieV(6)));
        system.addStar(primary);
        system.setType(StarSystemType.SINGLE);

        // Start at a reasonably hot orbit, but not too hot. Add a bit of variety.
        long distance = Physics.getOrbitWithTemperature(primary, 500);
        distance += Die.die(distance / 5, 2);

        // Create a random number of Dwarf Terrestrial worlds.
        int numPlanets = Die.d4() + 1;
        for (int orbit = 1; orbit <= numPlanets; orbit++) {
            String      name = StarSystemFactory.getPlanetName(primary, orbit);
            int         k = Physics.getTemperatureOfOrbit(primary, distance);
            PlanetType  type = getDwarfTerrestrial(CircumstellarZone.getZone(k));

            logger.info(String.format("createRockWorlds: [%d] [%dK] [%s] [%s]",
                    orbit, k, CircumstellarZone.getZone(k), type));

            List<Planet> planets = planetFactory.createPlanet(system, primary, name, type, distance);
            system.addPlanets(planets);

            distance += Die.die(distance / 3, 2) + distance / 3;
        }

        // Potentially, add an asteroid belt in as well. Probably an icebelt by this point.
        int belt = 1;
        if (Die.d2() == 1) {
            distance *= 2;
            String      name = StarSystemFactory.getBeltName(primary, belt++);
            int k = Physics.getTemperatureOfOrbit(primary, distance);
            PlanetType type = getBelt(CircumstellarZone.getZone(k));
            List<Planet> planets = planetFactory.createPlanet(system, primary, name, type, distance);

            system.addPlanets(planets);
        }

        if (Die.d100() <= 50) {
            distance *= 100;
            String      name = StarSystemFactory.getBeltName(primary, belt++);
            PlanetType  type = PlanetType.OortCloud;
            List<Planet> planets = planetFactory.createPlanet(system, primary, name, type, distance);

            system.addPlanets(planets);
        }


        // Good chance of it suffering from solar flares.
        if (Die.d3() != 1) {
            system.addTradeCode(StarSystemCode.Sf);
        }
        setDescription(system, null);
    }


    /**
     * This type of star system only has a dust belt and a type M sub-dwarf. No major planets have formed.
     *
     * @param system        Star System to create belt in.
     */
    @SuppressWarnings("WeakerAccess")
    public void createDustOnly(StarSystem system) throws DuplicateObjectException {
        logger.info(String.format("Generating [BarrenMK] [DustOnly] system [%s]", system.getName()));

        StarGenerator starGenerator = new StarGenerator(worldgen, system, false);
        // On average a cool main-sequence star will be generated.
        Star primary = starGenerator.generatePrimary(Luminosity.VI,
                SpectralType.M3.getSpectralType(Die.dieV(6)));
        system.addStar(primary);
        system.setType(StarSystemType.SINGLE);

        // Place the belt around 1AU from the star.
        long distance = 75_000_000 + Die.dieV(25_000_000);
        String name = StarSystemFactory.getBeltName(primary, 1);

        List<Planet> allPlanets;

        allPlanets = worldgen.getPlanetFactory().createPlanet(system, system.getStars().get(0),
                name, PlanetType.DustDisc, distance);

        system.addTradeCode(StarSystemCode.Ba);
        if (Die.d4() == 1) {
            system.addTradeCode(StarSystemCode.Sf);
            system.setZone(Zone.AMBER);
        }

        system.setPlanets(allPlanets);
        setDescription(system, null);
    }

    public void createAsteroids(StarSystem system) throws DuplicateObjectException {
        logger.info(String.format("Generating [BarrenMK] [Asteroids] system [%s]", system.getName()));

        StarGenerator starGenerator = new StarGenerator(worldgen, system, false);
        PlanetFactory planetFactory = worldgen.getPlanetFactory();
        // On average a cool main-sequence star will be generated.
        Star primary = starGenerator.generatePrimary(new Roller<Luminosity>(Luminosity.VI, Luminosity.VI, Luminosity.V).roll(),
                SpectralType.M5.getSpectralType(Die.dieV(6)));
        system.addStar(primary);
        system.setType(StarSystemType.SINGLE);

        // Place the belt around 1AU from the star.
        long distance = Physics.getOrbitWithTemperature(primary, 200);
        String name = StarSystemFactory.getBeltName(primary, 1);
        List<Planet> planets;

        planets = planetFactory.createPlanet(system, primary, name, PlanetType.AsteroidBelt, distance,
                Belt.BeltFeature.WideRing);
        system.setPlanets(planets);
        // Belts may increase their distance based on width.
        distance = planets.get(0).getDistance();

        distance *= 2;
        name = StarSystemFactory.getPlanetName(primary, 1);
        planets = planetFactory.createPlanet(system, primary, name, PlanetType.Jovic, distance);
        system.addPlanets(planets);

        distance *= 3;
        name = StarSystemFactory.getBeltName(primary, 2);
        planets = planetFactory.createPlanet(system, primary, name, PlanetType.IceBelt, distance,
                IceBelt.IceBeltFeature.KuiperBelt);
        system.addPlanets(planets);


        addOortClouds(planetFactory, system, primary, 70, 45);


        system.addTradeCode(StarSystemCode.Ba);
        if (Die.d4() == 1) {
            system.addTradeCode(StarSystemCode.Sf);
            system.setZone(Zone.AMBER);
        }


        setDescription(system, null);
    }


    public void createHotGiant(StarSystem system) throws DuplicateObjectException {
        logger.info(String.format("Generating [BarrenMK] [EpiStellarJovian.properties] system [%s]", system.getName()));

        StarGenerator starGenerator = new StarGenerator(worldgen, system, false);
        // On average a cool main-sequence star will be generated.
        Star primary = starGenerator.generatePrimary(Luminosity.VI,
                SpectralType.M1.getSpectralType(Die.dieV(6)));
        system.addStar(primary);
        system.setType(StarSystemType.SINGLE);

        // Place the belt around 1AU from the star.
        long distance = 1_500_000 + Die.dieV(500_000);
        String name = StarSystemFactory.getPlanetName(primary, 1);

        PlanetFactory planetFactory = worldgen.getPlanetFactory();
        List<Planet> allPlanets;
        if (Die.d3() == 1) {
            allPlanets = planetFactory.createPlanet(system, system.getStars().get(0),
                    name, PlanetType.Sokarian, distance);
        } else {
            allPlanets = planetFactory.createPlanet(system, system.getStars().get(0),
                    name, PlanetType.Osirian, distance);
        }
        system.setPlanets(allPlanets);

        if (Die.d100() <= 60) {
            distance *= 100;
            name = StarSystemFactory.getBeltName(primary, 1);
            PlanetType  type = PlanetType.OortCloud;
            List<Planet> planets = planetFactory.createPlanet(system, primary, name, type, distance);

            system.addPlanets(planets);
        }

        system.addTradeCode(StarSystemCode.Ba);
        if (Die.d4() == 1) {
            system.addTradeCode(StarSystemCode.Sf);
            system.setZone(Zone.AMBER);
        }

        setDescription(system, null);
    }

    /**
     * Create a system with a single red dwarf star, with some worlds in the outer system. Most
     * worlds are small Jovians or icy worlds.
     *
     * @param system    Star system to create planets in.
     */
    public void createColdWorlds(StarSystem system) throws DuplicateStarException {
        system.setType(StarSystemType.SINGLE);

        // Create a cool red dwarf star.
        StarGenerator starGenerator = new StarGenerator(worldgen, system, false);
        Star          primary = starGenerator.generatePrimary(Luminosity.VI, SpectralType.M9.getSpectralType(Die.d6()));
        system.addStar(primary);

        PlanetFactory factory = worldgen.getPlanetFactory();

        long distance = (2 + Die.d4(2)) * Physics.AU + Die.die(Physics.AU);
        String name = StarSystemFactory.getPlanetName(primary, 1);
        List<Planet> planets = null;

        if (Die.d2() == 1) {
            // A small, cold, jovian world.
            planets = factory.createPlanet(system, primary, name, PlanetType.Neptunian, distance);
        } else {
            // A huge, icy, super-Earth.
            planets = factory.createPlanet(system, primary, name, PlanetType.Thean, distance);
        }
        system.addPlanets(planets);

        distance *= 2;
        name = StarSystemFactory.getBeltName(primary, 2);
        planets = factory.createPlanet(system, primary, name, PlanetType.IceBelt, distance,
                planets.get(planets.size()-1), IceBelt.IceBeltFeature.KuiperBelt);
        system.addPlanets(planets);

        setDescription(system, null);
    }

    public void createProto(StarSystem system) throws DuplicateStarException {
        system.setType(StarSystemType.SINGLE);

        // Create a cool red dwarf star.
        StarGenerator starGenerator = new StarGenerator(worldgen, system, false);
        Star          primary = starGenerator.generatePrimary(Luminosity.VI, SpectralType.M2.getSpectralType(Die.d6()));
        system.addStar(primary);

        PlanetFactory factory = worldgen.getPlanetFactory();

        long    distance = 5_000_000 + Die.die(5_000_000, 2);
        String  name;
        int     orbit = 1;

        name = StarSystemFactory.getPlanetName(primary, orbit++);
        system.addPlanets(factory.createPlanet(system, primary, name, PlanetType.ProtoFerrinian, distance));
        distance += 5_000_000;
        name = StarSystemFactory.getPlanetName(primary, orbit++);
        system.addPlanets(factory.createPlanet(system, primary, name, PlanetType.ProtoLithian, distance));
        distance += 5_000_000;
        name = StarSystemFactory.getPlanetName(primary, orbit++);
        system.addPlanets(factory.createPlanet(system, primary, name, PlanetType.ProtoCarbonian, distance));
        distance += 5_000_000;
        name = StarSystemFactory.getPlanetName(primary, orbit++);
        system.addPlanets(factory.createPlanet(system, primary, name, PlanetType.ProtoGelidian, distance));

        setDescription(system, null);
    }

    private void createHotGiants(StarSystem system) throws DuplicateObjectException {
        logger.info(String.format("Generating [DNBarrenM] [HotGiants] system [%s]", system.getName()));

        StarGenerator starGenerator = new StarGenerator(worldgen, system, false);
        PlanetFactory planetFactory = worldgen.getPlanetFactory();

        // On average a cool main-sequence star will be generated.
        Star primary = starGenerator.generatePrimary(Luminosity.V,
                SpectralType.M7.getSpectralType(Die.dieV(3)));
        system.addStar(primary);
        system.setType(StarSystemType.SINGLE);

        // Start at a reasonably hot orbit, but not too hot. Add a bit of variety.
        long distance = Physics.getOrbitWithTemperature(primary, 1500);
        distance += Die.dieV(distance / 5);

        // Create a random number of Dwarf Terrestrial worlds.
        int numPlanets = Die.d3();
        for (int orbit = 1; orbit <= numPlanets; orbit++) {
            String      name = StarSystemFactory.getPlanetName(primary, orbit);
            int         k = Physics.getTemperatureOfOrbit(primary, distance);
            PlanetType  type = getJovian(CircumstellarZone.getZone(k));

            List<Planet> planets = planetFactory.createPlanet(system, primary, name, type, distance);
            system.addPlanets(planets);

            distance += Die.die(distance / 3, 3);
        }

        // Good chance of it suffering from solar flares.
        if (Die.d4() != 1) {
            system.addTradeCode(StarSystemCode.Sf);
        }
        setDescription(system, null);
    }

    private void createColdGiants(StarSystem system) throws DuplicateObjectException {
        logger.info(String.format("Generating [DNBarrenM] [ColdGiants] system [%s]", system.getName()));

        StarGenerator starGenerator = new StarGenerator(worldgen, system, false);
        PlanetFactory planetFactory = worldgen.getPlanetFactory();

        // On average a cool main-sequence star will be generated.
        Star primary = starGenerator.generatePrimary(Luminosity.V,
                SpectralType.M7.getSpectralType(Die.dieV(3)));
        system.addStar(primary);
        system.setType(StarSystemType.SINGLE);

        // Start at a reasonably hot orbit, but not too hot. Add a bit of variety.
        long distance = Physics.getOrbitWithTemperature(primary, 200);
        distance += Die.dieV(distance / 5);

        // Create a random number of Dwarf Terrestrial worlds.
        int numPlanets = Die.d4() + 1;
        for (int orbit = 1; orbit <= numPlanets; orbit++) {
            String      name = StarSystemFactory.getPlanetName(primary, orbit);
            int         k = Physics.getTemperatureOfOrbit(primary, distance);
            PlanetType  type = getJovian(CircumstellarZone.getZone(k));

            List<Planet> planets = planetFactory.createPlanet(system, primary, name, type, distance);
            system.addPlanets(planets);

            distance += Die.die(distance / 5, 3);
        }

        // Good chance of it suffering from solar flares.
        if (Die.d3() != 1) {
            system.addTradeCode(StarSystemCode.Sf);
        }
        setDescription(system, null);
    }


    /**
     * A star system where the main world is a GeoHelian world.
     */
    public void createGiant(StarSystem system) throws DuplicateObjectException {
        logger.info(String.format("Generating [DNBarrenM] [RedGiant] system [%s]", system.getName()));

        StarGenerator starGenerator = new StarGenerator(worldgen, system, false);
        PlanetFactory planetFactory = worldgen.getPlanetFactory();

        // Create a red giant star.
        Star primary = starGenerator.generatePrimary(Luminosity.III, SpectralType.M3.getSpectralType(Die.dieV(4)));
        system.addStar(primary);
        system.setType(StarSystemType.SINGLE);

        long distance;
        int  orbit = 1;

        PlanetType type;

        if (Die.d2() == 1) {
            type = PlanetType.Hermian;
            distance = (50 + Die.dieV(6)) * Physics.MKM;
            String name = StarSystemFactory.getPlanetName(primary, orbit++);
            List<Planet> planets = planetFactory.createPlanet(system, system.getStars().get(0),
                    name, type, distance);
            system.addPlanets(planets);
        }

        if (Die.d2() == 1) {
            type = PlanetType.Lutian;
            distance = (100 + Die.dieV(8)) * Physics.MKM;
            String name = StarSystemFactory.getPlanetName(primary, orbit++);
            List<Planet> planets = planetFactory.createPlanet(system, system.getStars().get(0),
                    name, type, distance);
            system.addPlanets(planets);
        }

        if (Die.d2() == 1) {
            type = PlanetType.NecroGaian;
            distance = (150 + Die.dieV(12)) * Physics.MKM;
            String name = StarSystemFactory.getPlanetName(primary, orbit++);
            List<Planet> planets = planetFactory.createPlanet(system, system.getStars().get(0),
                    name, type, distance);
            system.addPlanets(planets);
        }

        distance = (750 + Die.dieV(100)) * Physics.MKM;
        type = PlanetType.Poseidonic;
        String name = StarSystemFactory.getPlanetName(primary, orbit++);
        List<Planet> planets = planetFactory.createPlanet(system, system.getStars().get(0),
                name, type, distance);
        system.addPlanets(planets);

        if (Die.d2() == 1) {
            distance = (1800 + Die.dieV(400)) * Physics.MKM;
            type = PlanetType.Saturnian;
            name = StarSystemFactory.getPlanetName(primary, orbit++);
            planets = planetFactory.createPlanet(system, system.getStars().get(0),
                    name, type, distance);
            system.addPlanets(planets);
        }

        if (Die.d2() == 1) {
            distance = (3800 + Die.dieV(800)) * Physics.MKM;
            type = PlanetType.Neptunian;
            name = StarSystemFactory.getPlanetName(primary, orbit++);
            planets = planetFactory.createPlanet(system, system.getStars().get(0),
                    name, type, distance);
            system.addPlanets(planets);
        }

        // Kuiper Belt
        distance = (70 + Die.dieV(10)) * Physics.AU;
        type = PlanetType.IceBelt;
        name = StarSystemFactory.getBeltName(primary, 1);
        planets = planetFactory.createPlanet(system, system.getStars().get(0),
                name, type, distance, IceBelt.IceBeltFeature.KuiperBelt);
        system.addPlanets(planets);

        // Oort Cloud
        if (Die.d100() <= 90) {
            name = StarSystemFactory.getOortCloudName(primary, 1);
            type = PlanetType.OortCloud;

            distance = (long)(Physics.AU * 10_000 * primary.getMass());
            planets = planetFactory.createPlanet(system, primary, name, type, distance,
                    OortCloud.OortCloudFeatures.InnerOort);
            system.addPlanets(planets);

            if (Die.d100() <= 90) {
                name = StarSystemFactory.getOortCloudName(primary, 2);
                distance = (long)(Physics.AU * 40_000 * primary.getMass());
                planets = planetFactory.createPlanet(system, primary, name, type, distance,
                        OortCloud.OortCloudFeatures.OuterOort);
                system.addPlanets(planets);
            }
        }
        system.addTradeCode(StarSystemCode.Ba);

        setDescription(system, null);
    }
}
