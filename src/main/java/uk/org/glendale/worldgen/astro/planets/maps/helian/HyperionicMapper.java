package uk.org.glendale.worldgen.astro.planets.maps.helian;

import uk.org.glendale.utils.graphics.Icosahedron;
import uk.org.glendale.utils.graphics.SimpleImage;
import uk.org.glendale.utils.graphics.Tile;
import uk.org.glendale.utils.rpg.Die;
import uk.org.glendale.worldgen.astro.planets.Planet;
import uk.org.glendale.worldgen.astro.planets.PlanetFeature;
import uk.org.glendale.worldgen.astro.planets.codes.Atmosphere;
import uk.org.glendale.worldgen.astro.planets.codes.Life;
import uk.org.glendale.worldgen.astro.planets.codes.PlanetType;
import uk.org.glendale.worldgen.astro.planets.generators.helian.Halcyonic;
import uk.org.glendale.worldgen.astro.planets.generators.helian.Hyperionic;
import uk.org.glendale.worldgen.astro.planets.maps.HelianMapper;
import uk.org.glendale.worldgen.astro.planets.tiles.Cratered;
import uk.org.glendale.worldgen.astro.planets.tiles.Rough;
import uk.org.glendale.worldgen.astro.planets.tiles.Speckled;
import uk.org.glendale.worldgen.web.Server;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static uk.org.glendale.worldgen.astro.planets.generators.Dwarf.DwarfFeature.*;
import static uk.org.glendale.worldgen.astro.planets.generators.Helian.HelianFeature.*;

public class HyperionicMapper extends HelianMapper {

    protected static final Tile DARK_GREY = new Tile("Dark Grey", "#404240", false, 2);
    protected static final Tile MID_GREY = new Tile("Mid Grey", "#848684", false, 2);
    protected static final Tile LIGHT_GREY = new Tile("Light Grey", "#8B8B88", false, 2);

    protected static final Tile RIFT = new Tile("Rift", "#404040", false, 1);
    protected static final Tile SILVER = new Tile("Silver", "#D0D0D0", true, 1);

    protected static final Tile CRATER_RIDGE = new Tile("Ridge", "#A0A0A0", false, 2);
    protected static final Tile CRATER_FLOOR = new Tile("Floor", "#606060", false, 5);

    public HyperionicMapper(final Planet planet, final int size) {
        super(planet, size);
    }

    public HyperionicMapper(final Planet planet) {
        super(planet, DEFAULT_FACE_SIZE);
    }

    private Tile getRandomColour(int height) {
        switch (Die.d4(2) + height / 24) {
            case 2:
                return HyperionicMapper.LIGHT_GREY;
            case 3: case 4: case 5: case 6: case 7: case 8: case 9:
                return HyperionicMapper.MID_GREY;
            case 10: case 11: case 12:
                return HyperionicMapper.DARK_GREY;
        }
        throw new IllegalStateException("getRandomColour: Invalid switch value.");
    }

    /**
     * Generate a Hermian surface landscape. This will be grey, barren and cratered.
     */
    public void generate() {
        super.generate();

        // Basic barren landscape.
        for (int y=0; y < getNumRows(); y++) {
            for (int x=0; x < getWidthAtY(y); x++) {
                Tile tile = getRandomColour(getHeight(x, y));
                setTile(x, y, tile);
            }
        }

        // Expand light and dark areas.
        flood(DARK_GREY, 2);
        flood(LIGHT_GREY, 2);

        int lava = 1;
        if (planet.hasFeature(LightVolcanic)) {
            flood(DARK_GREY, 3);
            lava = 3;
        } else if (planet.hasFeature(MediumVolcanic)) {
            flood(DARK_GREY, 6);
            lava = 5;
        } else if (planet.hasFeature(HeavyVolcanic)) {
            flood(DARK_GREY, 10);
            lava = 7;
        }

        // Apply craters, frequency depending on tile type.
        for (int y=0; y < getNumRows(); y++) {
            for (int x=0; x < getWidthAtY(y); x++) {
                if (getTile(x, y).equals(DARK_GREY)) {
                    if (Die.d10() <= lava) {
                        setTile(x, y, new Speckled(getTile(x, y)).colour("#FF2222").cover(lava * 5));
                    }
                } else {
                    if (Die.d3() == 1) {
                        setTile(x, y, new Cratered(getTile(x, y)));
                    } else {
                        setTile(x, y, new Rough(getTile(x, y)));
                    }
                }
            }
        }

        cleanBumpMap();
        if (planet.hasFeature(ReMelted)) {
            createCraters(-1, 25);
        } else {
            createCraters(0, 250);
        }

        if (planet != null) {
            List<PlanetFeature> features = planet.getFeatures();
            if (features != null && features.size() != 0) {
                generateFeatures(features);
            }
        }
        hasCloudMap = true;
    }

    private void generateMajorCrater(PlanetFeature crater) {
        int startY = 0;
        int direction = 1;

        if (crater == SouthCrater) {
            startY = getNumRows() - 1;
            direction = -1;
        }

        int tileY = startY;
        for (int tileX = 0; tileX < getWidthAtY(tileY); tileX++) {
            setTile(tileX, tileY, CRATER_RIDGE);
        }
        // Fill in the floor of the crater down to the edge.
        int craterEdge = startY + (getNumRows()/3 + Die.dieV(3)) * direction;
        while (tileY != craterEdge) {
            tileY += direction;
            for (int tileX = 0; tileX < getWidthAtY(tileY); tileX++) {
                setTile(tileX, tileY, CRATER_FLOOR);
                if (Die.d6() == 1) {
                    setTile(tileX, tileY, new Cratered(getTile(tileX, tileY)));
                }
            }
        }
        // Main ridge around edge of crater.
        for (int tileX = 0; tileX < getWidthAtY(tileY); tileX++) {
            if (Die.d6() == 1) {
                setTile(tileX, tileY, LIGHT_GREY);
            } else {
                setTile(tileX, tileY, CRATER_RIDGE);
            }
        }
        if (Die.d2() == 1) {
            // Place a mini-ridge around the mid-point.
            tileY = (startY + tileY) / 2 + Die.dieV(2);
            for (int tileX = 0; tileX < getWidthAtY(tileY); tileX++) {
                if (Die.d3() == 1) {
                    setTile(tileX, tileY, DARK_GREY);
                } else {
                    setTile(tileX, tileY, MID_GREY);
                }
            }
        }

    }

    private void generateFeatures(List<PlanetFeature> features) {
        for (PlanetFeature f : features) {
            if (f == GreatRift) {
                // A single rift split across the world.
                addRift(RIFT,8 + Die.d6(2));
            } else if (f == BrokenRifts) {
                // A number of small rifts in the surface of the world.
                int numRifts = 6 + Die.d4(2);
                for (int r = 0; r < numRifts; r++) {
                    addRift(RIFT,6 + Die.d4(2));
                }
            } else if (f == MetallicSea) {
                // A sea of molten metal, generally equatorial.
                int y = getNumRows() / 2 + Die.dieV(3);
                int x = getWidthAtY(y) / 2 + Die.dieV(2);
                setTile(x, y, SILVER);
                flood(SILVER, 7);
            } else if (f == NorthCrater || f == SouthCrater) {
                generateMajorCrater(f);
            }
        }

    }

    public List<SimpleImage> drawClouds(int width) {
        List<SimpleImage>  clouds = new ArrayList<>();
        cloudHeight = 5;

        int density = 60;
        String cloudColour = "#F0F0E0";
        for (int layer = 0; layer < 5; layer++) {
            Icosahedron cloud = getCloudLayer();

            for (int y = 0; y < cloud.getNumRows(); y++) {
                for (int x = 0; x < cloud.getWidthAtY(y); x++) {
                    int h = cloud.getHeight(x, y);
                    cloud.setHeight(x, y, density + h / 2);
                }
            }
            clouds.add(Icosahedron.stretchImage(cloud.drawTransparency(cloudColour, width), width));

            density /= 2;
            cloudColour = "#60C0FA";
        }
        return clouds;
    }

    public static void main(String[] args) throws IOException {
        Planet       planet = new Planet();
        planet.setName("Hyperionic");
        planet.setRadius(9000);
        planet.setDensity(6000);
        planet.setType(PlanetType.Halcyonic);
        planet.setHydrographics(0);
        planet.setTemperature(1000);
        planet.setPressure(1_000_000);
        planet.setAtmosphere(Atmosphere.SulphurCompounds);
        planet.setLife(Life.None);
        planet.addFeature(LightVolcanic);

        testOutput(planet, new Hyperionic(Server.getWorldGen(), null, null, null, 0),
                new HyperionicMapper(planet));
    }
}
