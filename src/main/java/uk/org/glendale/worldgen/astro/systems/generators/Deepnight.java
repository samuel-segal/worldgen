/*
 * Copyright (c) 2021, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */

package uk.org.glendale.worldgen.astro.systems.generators;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.org.glendale.utils.rpg.Die;
import uk.org.glendale.worldgen.WorldGen;
import uk.org.glendale.worldgen.astro.Physics;
import uk.org.glendale.worldgen.astro.planets.PlanetFactory;
import uk.org.glendale.worldgen.astro.planets.codes.PlanetType;
import uk.org.glendale.worldgen.astro.planets.generators.belt.OortCloud;
import uk.org.glendale.worldgen.astro.sectors.Sector;
import uk.org.glendale.worldgen.astro.stars.*;
import uk.org.glendale.worldgen.astro.systems.*;
import uk.org.glendale.worldgen.astro.systems.generators.deepnight.DNBarrenG;
import uk.org.glendale.worldgen.astro.systems.generators.deepnight.DNBarrenK;
import uk.org.glendale.worldgen.astro.systems.generators.deepnight.DNBarrenM;
import uk.org.glendale.worldgen.civ.CivilisationGenerator;
import uk.org.glendale.worldgen.civ.civilisation.FreeSettlers;
import uk.org.glendale.worldgen.civ.civilisation.Hermits;
import uk.org.glendale.worldgen.exceptions.DuplicateObjectException;

/**
 * Generate a barren, type M or K, star system. These systems have no native life, the worlds are often
 * cold and uninhabitable. Quite simple systems.
 */
public class Deepnight extends StarSystemGenerator {
    private static final Logger logger = LoggerFactory.getLogger(Deepnight.class);

    public Deepnight(WorldGen worldgen) {
        super(worldgen);
    }

    /**
     * Creates a barren star system. Such a system has one or more stars, with planets, but the
     * planets are barren with no civilisation or life of any kind. If the system has multiple stars,
     * the second star is much smaller and a long way away.
     *
     * @param sector    Sector to generate system in.
     * @param name      Name of the system.
     * @param x         X coordinate of the system in the sector (1-32).
     * @param y         Y coordinate of the system in the sector (1-40).
     *
     * @return          Created and persisted star system.
     * @throws DuplicateObjectException     If a duplicate system is created.
     */
    public StarSystem generate(Sector sector, String name, int x, int y) throws DuplicateObjectException {
        StarSystem system = createEmptySystem(sector, name, x, y);

        Deepnight generator = null;
        switch (Die.d6(3)) {
            case 2:
                generator = new DNBarrenM(worldgen);
                break;
            case 3:
                generator = new DNBarrenM(worldgen);
                break;
            case 4:
                generator = new DNBarrenM(worldgen);
                break;
            case 5:
                generator = new DNBarrenM(worldgen);
                break;
            case 6: case 7:
                generator = new DNBarrenG(worldgen);
                break;
            case 8: case 9:
                generator = new DNBarrenK(worldgen);
                break;
            case 10: case 11:
                generator = new DNBarrenM(worldgen);
                break;
            default:
                createEmptySystem(system);
        }
        if (generator != null) {
            system = generator.generate(sector, name, x, y);
            generator.colonise(system);
        }

        updateStarSystem(system);

        return system;
    }

    public void colonise(StarSystem system) {
        logger.info("Colonise " + system.getZone() + " star system " + system.getName());

        CivilisationGenerator generator = null;

        if (civName != null) {
            logger.info(String.format("Civilisation name set to be [%s]", civName));
            generator = getCivilisationByName(system);
        } else {
            if (system.getZone() == Zone.GREEN) {
                switch (Die.d6()) {
                    case 1: case 5: case 6:
                        generator = new Hermits(worldgen, system);
                        break;
                    case 2: case 3: case 4:
                        generator = new FreeSettlers(worldgen, system);
                        break;
                    default:
                        break;
                }
            }
        }
        if (generator != null) {
            generator.generate();
        }
    }

    /**
     * This system is empty, it only has a single red dwarf star and no planets.
     */
    public void createEmptySystem(StarSystem system) throws DuplicateObjectException {
        logger.info(String.format("Generating [BarrenMK] [EmptySystem] system [%s]", system.getName()));

        StarGenerator starGenerator = new StarGenerator(worldgen, system, false);
        // On average a cool main-sequence star will be generated.
        Star primary = starGenerator.generatePrimary(Luminosity.VI,
                SpectralType.M3.getSpectralType(Die.dieV(6)));
        system.addStar(primary);
        system.setType(StarSystemType.SINGLE);

        setDescription(system, null);
    }

    protected void addOortClouds(PlanetFactory factory, StarSystem system, Star primary, int chance1, int chance2) {
        String name;
        PlanetType type;
        long distance;

        if (Die.d100() <= chance1) {
            name = StarSystemFactory.getOortCloudName(primary, 1);
            type = PlanetType.OortCloud;

            distance = (long) (Physics.AU * 10_000 * primary.getMass());
            system.addPlanets(factory.createPlanet(system, primary, name, type, distance,
                    OortCloud.OortCloudFeatures.InnerOort));

            if (Die.d100() <= chance2) {
                name = StarSystemFactory.getOortCloudName(primary, 2);
                distance = (long) (Physics.AU * 40_000 * primary.getMass());
                system.addPlanets(factory.createPlanet(system, primary, name, type, distance,
                        OortCloud.OortCloudFeatures.OuterOort));
            }
        }
    }


}
