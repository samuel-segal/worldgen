/*
 * Copyright (c) 2020, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */
package uk.org.glendale.worldgen.astro.systems.generators.deepnight;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.org.glendale.utils.rpg.Roller;
import uk.org.glendale.worldgen.WorldGen;
import uk.org.glendale.worldgen.astro.planets.codes.PlanetType;
import uk.org.glendale.worldgen.astro.systems.CircumstellarZone;
import uk.org.glendale.worldgen.astro.systems.generators.Deepnight;

/**
 * Defines random planet tables for barren star systems.
 */
public abstract class DNBarren extends Deepnight {
    private static final Logger logger = LoggerFactory.getLogger(DNBarren.class);

    public DNBarren(WorldGen worldgen) {
        super(worldgen);
    }

    protected PlanetType getBelt(CircumstellarZone zone) {
        Roller<PlanetType> roller = new Roller<>();

        switch (zone) {
            case Exclusion:
                // Nothing here.
                break;
            case Epistellar:
                roller.add(PlanetType.VulcanianBelt);
                break;
            case Hot:
                roller.add(PlanetType.MetallicBelt, PlanetType.VulcanianBelt);
                break;
            case Inner: case Middle:
                roller.add(PlanetType.AsteroidBelt, 3).add(PlanetType.MetallicBelt);
                break;
            case Outer:
                roller.add(PlanetType.AsteroidBelt, 3).add(PlanetType.IceBelt);
                break;
            case Cold:
                roller.add(PlanetType.IceBelt, 3).add(PlanetType.AsteroidBelt);
                break;
            default:
                roller.add(PlanetType.IceBelt);
                break;
        }
        return roller.roll();
    }


    protected PlanetType getDwarfTerrestrial(CircumstellarZone zone) {
        Roller<PlanetType> roller = new Roller<>();

        switch (zone) {
            case Exclusion:
                break;
            case Epistellar:
                roller.add(PlanetType.Janian, PlanetType.Ferrinian);
                break;
            case Hot:
                roller.add(PlanetType.Hermian, 3).add(PlanetType.Ferrinian);
                break;
            case Inner: case Middle:
                roller.add(PlanetType.AreanLacustric, PlanetType.Selenian);
                break;
            case Outer:
                roller.add(PlanetType.EuArean, 3).add(PlanetType.EuArean, 2).
                        add(PlanetType.Bathic).add(PlanetType.Cerean);
                break;
            case Cold:
                roller.add(PlanetType.Cerean, PlanetType.Vestian, PlanetType.Gelidian);
                break;
            default:
                roller.add(PlanetType.Gelidian);
        }
        return roller.roll();
    }

    protected PlanetType getTerrestrial(CircumstellarZone zone) {
        Roller<PlanetType> roller = new Roller<>();

        switch (zone) {
            case Exclusion:
                break;
            case Epistellar:
                roller.add(PlanetType.JaniLithic);
                break;
            case Hot:
                roller.add(PlanetType.Lutian);
                break;
            case Inner: case Middle:
                roller.add(PlanetType.NecroGaian, 3).add(PlanetType.Cytherean, PlanetType.BathyPelagic);
                break;
            case Outer: case Cold:
                roller.add(PlanetType.Ymirian);
                break;
            default:
                roller.add(PlanetType.NecroGaian);
        }

        return roller.roll();
    }

    protected PlanetType getJovian(CircumstellarZone zone) {
        Roller<PlanetType> roller = new Roller<>();

        switch (zone) {
            case Exclusion:
                roller.add(PlanetType.Junic);
                break;
            case Epistellar:
                roller.add(PlanetType.Junic).add(PlanetType.Osirian, 3).add(PlanetType.Sokarian, 6);
                break;
            case Hot:
                roller.add(PlanetType.Junic).add(PlanetType.Osirian, 3).add(PlanetType.Sokarian, 6);
                break;
            case Inner: case Middle:
                roller.add(PlanetType.Jovic).add(PlanetType.Poseidonic, 3);
                break;
            case Outer:
                roller.add(PlanetType.Saturnian).add(PlanetType.Jovic, 3);
                break;
            case Cold:
                roller.add(PlanetType.Neptunian);
                break;
            default:
                roller.add(PlanetType.Neptunian);
        }
        return roller.roll();
    }

    protected PlanetType getHelian(CircumstellarZone zone) {
        Roller<PlanetType> roller = new Roller<>();

        switch (zone) {
            case Exclusion:
                break;
            case Epistellar:
                roller.add(PlanetType.Halcyonic, 3).add(PlanetType.Thetusean);
                break;
            case Hot: case Inner: case Middle:
                roller.add(PlanetType.Hyperionic);
                break;
            case Outer:
                roller.add(PlanetType.Hyperionic).add(PlanetType.Thean);
                break;
            default:
                roller.add(PlanetType.Thean);
                break;
        }
        return roller.roll();
    }
}
