/**
 * Temperature.java
 *
 * Copyright (c) 2007, Samuel Penn.
 * See the file LICENSE at the root of the project.
 */
package uk.org.glendale.worldgen.astro.planets.codes;

/**
 * Describes the average surface temperature of the planet. The temperature across a planet can vary widely.
 * Earth's average temperature is 'Standard', but can vary between 'ExtremelyCold' and 'VeryHot'.
 *
 * Worlds with temperatures between 'Cold' and 'Hot' are considered suitable for human habitation, though
 * 'Cold' and 'Hot' worlds aren't ideal. Both generally allow liquid water on the surface, and won't result
 * in immediate death without a survival suit.
 *
 * ExtremelyCold: Temperature rarely above 100K
 * VeryCold: Too cold for humans. Temperature rarely above 200K
 * Cold: Winter world, similar to Arctic conditions all over. Special life support
 *       is needed for a colony to survive. Rarely above 250K
 * Cool: Uncomfortable, long winters and short summers. Humans can survive
 *       without special life support.
 * Standard: Earth
 * Warm: Tropical climate, hot all over. Really nice to uncomfortable.
 * Hot: Too hot, difficult to survive. Rarely below 320K.
 * VeryHot: Rarely below 400K. Water boils. Humans cannot live.
 * ExtremelyHot: Rock melts.
 *
 */
public enum Temperature {
    AbsoluteZero(6, 0),
    DeepSpace(4, 3),          // Cosmic Background radiation temperature.
    UltraCold(6, 5),
    MethaneFreezes(6, 90),
    MethaneBoils(6, 112),
    AmmoniaFreezes(5, 195),
    VeryCold(4, 230),
    AmmoniaBoils(4, 240),
    Cold(3, 260),
    WaterFreezes(2, 273),    // Freezing point of water.
    Cool(2, 278),                // Overly cool.
    Standard(1, 288),            // Earth standard.
    Warm(1, 298),                // Overly warm.
    Hot(2, 320),
    VeryHot(3, 350),
    WaterBoils(6, 373),      // Boiling point of water.
    PaperBurns(6, 505),      // Temperature at which paper burns.
    LeadMelts(6, 750),       // Temperature at which lead melts.
    IronMelts(6, 1000),      // Average melting point of iron.
    SilicatesMelt(6, 1500),  // Many silicate rocks melt.
    IronBoils(6, 2500),      // Iron boils.
    SilicatesBoil(6, 5000),  // Rock boils.
    StellarSurface(6, 6500); // Temperature at the surface of a G2 star.

    final private double	habitability;
    final private int		kelvin;

    Temperature(int habitability, int kelvin) {
        this.habitability = habitability;
        this.kelvin = kelvin;
    }

    public double getHabitability() {
        return habitability;
    }

    /**
     * Gets the actual mid-point temperature for this Temperature value.
     *
     * @return  Temperature in Kelvin.
     */
    public int getKelvin() {
        return kelvin;
    }

    public boolean isColderThan(Temperature otherTemperature) {
        return ordinal() < otherTemperature.ordinal();
    }

    public boolean isHotterThan(Temperature otherTemperature) {
        return ordinal() > otherTemperature.ordinal();
    }

    /**
     * Get a temperature that is one level hotter than the current one.
     * If the temperature is already ExtremelyHot, then ExtremelyHot is
     * returned.
     */
    public Temperature getHotter() {
        if (this == StellarSurface) return StellarSurface;

        return Temperature.values()[ordinal()+1];
    }

    /**
     * Get a temperature that is one level colder than the current one.
     * If the temperature is already ExtremelyCold, then ExtremelyCold
     * is returned.
     */
    public Temperature getColder() {
        if (this == UltraCold) return UltraCold;

        return Temperature.values()[ordinal()-1];
    }

    /**
     * Given a temperature in Kelvin, return the suitable enum for that
     * temperature.
     *
     * @param kelvin    Temperature in Kelvin.
     * @return          Temperature enum.
     */
    public static Temperature getTemperature(int kelvin) {
        if (kelvin < AbsoluteZero.getKelvin()) {
            return AbsoluteZero;
        } else if (kelvin > StellarSurface.getKelvin()) {
            return StellarSurface;
        }

        Temperature t = AbsoluteZero;
        while (t != StellarSurface && kelvin > (t.getKelvin() + t.getHotter().getKelvin())/2) {
            t = t.getHotter();
        }

        return t;
    }
}