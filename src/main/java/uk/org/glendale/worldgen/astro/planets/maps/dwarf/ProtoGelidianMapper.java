/**
 * Copyright (C) 2020 Samuel Penn, sam@notasnark.net
 * See the file LICENSE at the root of the project.
 */
package uk.org.glendale.worldgen.astro.planets.maps.dwarf;

import uk.org.glendale.utils.graphics.Icosahedron;
import uk.org.glendale.utils.graphics.SimpleImage;
import uk.org.glendale.utils.graphics.Tile;
import uk.org.glendale.utils.rpg.Die;
import uk.org.glendale.worldgen.astro.planets.Planet;
import uk.org.glendale.worldgen.astro.planets.codes.Atmosphere;
import uk.org.glendale.worldgen.astro.planets.codes.PlanetType;
import uk.org.glendale.worldgen.astro.planets.generators.Dwarf;
import uk.org.glendale.worldgen.astro.planets.maps.DwarfMapper;
import uk.org.glendale.worldgen.astro.planets.tiles.Lava;
import uk.org.glendale.worldgen.astro.planets.tiles.Rough;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static uk.org.glendale.worldgen.astro.planets.generators.Dwarf.DwarfFeature.*;

/**
 * Defines a surface map for a ProtoLithian class world. These are extremely hot, and may be
 * molten or partially molten.
 */
public class ProtoGelidianMapper extends DwarfMapper {

    protected static final Tile WATER = new Tile("Water", "#9090F0", true, 3);

    public ProtoGelidianMapper(final Planet planet, final int size) {
        super(planet, size);
    }

    public ProtoGelidianMapper(final Planet planet) {
        super(planet, DEFAULT_FACE_SIZE);
    }

    private Tile getRandomColour() {
        return WATER;
    }

    public void generate() {
        generateHeightMap(DEFAULT_FACE_SIZE, DEFAULT_FACE_SIZE);

        // Basic barren landscape.
        for (int y=0; y < getNumRows(); y++) {
            for (int x=0; x < getWidthAtY(y); x++) {
                Tile tile = getRandomColour();
                setTile(x, y, tile);
            }
        }

        for (int y=0; y < getNumRows(); y++) {
            for (int x=0; x < getWidthAtY(y); x++) {
                int h = getHeight(x, y);
                setTile(x, y, WATER);
            }
        }

        cleanBumpMap();

        if (planet.getPressure() > 1_000) {
            hasCloudMap = true;
        }
    }

    public List<SimpleImage> drawClouds(int width) {
        List<SimpleImage>  clouds = new ArrayList<>();
        cloudHeight = 5;

        if (planet.getPressure() < 1_000) {
            // No significant atmosphere, so don't show anything.
            return clouds;
        }

        Icosahedron cloud = getCloudLayer();

        int modifier = (planet.getPressure() - 50_000) / 2_500;

        String cloudColour = "#FEFEFE";

        for (int y=0; y < cloud.getNumRows(); y++) {
            for (int x=0; x < cloud.getWidthAtY(y); x++) {
                int h = cloud.getHeight(x, y);
                cloud.setHeight(x, y, h + modifier);
            }
        }

        clouds.add(Icosahedron.stretchImage(cloud.drawTransparency(cloudColour, width), width));

        return clouds;
    }

    public static void main(String[] args) throws IOException {
        Planet p = new Planet();
        p.setType(PlanetType.ProtoLithian);
        p.setTemperature(800);
        p.setNightTemperature(700);
        p.addFeature(Dwarf.DwarfFeature.MetallicSea);
        ProtoGelidianMapper m = new ProtoGelidianMapper(p, DEFAULT_FACE_SIZE);

        System.out.println("ProtoLithian:");
        m.generate();
        SimpleImage img = m.draw(2048);
        img.save(new File("/home/sam/tmp/protolithian.jpg"));

    }
}
