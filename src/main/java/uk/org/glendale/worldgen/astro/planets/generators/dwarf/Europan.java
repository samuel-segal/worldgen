/*
 * Copyright (c) 2018, Samuel Penn (sam@glendale.org.uk).
 * See the file LICENSE at the root of the project.
 */

package uk.org.glendale.worldgen.astro.planets.generators.dwarf;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.org.glendale.utils.rpg.Die;
import uk.org.glendale.worldgen.WorldGen;
import uk.org.glendale.worldgen.astro.planets.MoonFeature;
import uk.org.glendale.worldgen.astro.planets.Planet;
import uk.org.glendale.worldgen.astro.planets.codes.Atmosphere;
import uk.org.glendale.worldgen.astro.planets.codes.MagneticField;
import uk.org.glendale.worldgen.astro.planets.codes.PlanetType;
import uk.org.glendale.worldgen.astro.planets.codes.Pressure;
import uk.org.glendale.worldgen.astro.planets.generators.Dwarf;
import uk.org.glendale.worldgen.astro.stars.Star;
import uk.org.glendale.worldgen.astro.systems.StarSystem;

import static uk.org.glendale.worldgen.astro.commodities.CommodityName.*;

/**
 * Europan sub-type of Plutonian GeoTidal worlds. Due to tidal stretching, these icy worlds have a
 * liquid ocean beneath their surface.
 */
public class Europan extends Dwarf {
    private static final Logger logger = LoggerFactory.getLogger(Europan.class);

    public Europan(WorldGen worldgen, StarSystem system, Star star, Planet previous, long distance) {
        super(worldgen, system, star, previous, distance);
    }

    private void addFeatures(Planet planet) {
        // No features to add at the moment.
    }

    public Planet getPlanet(String name) {
        Planet planet = definePlanet(name, PlanetType.Europan);
        int radius = 1500 + Die.dieV(300);

        planet.setRadius(radius);
        planet.setAtmosphere(Atmosphere.Vacuum);
        planet.setPressure(Pressure.None);
        planet.setMagneticField(MagneticField.None);

        addFeatures(planet);

        if (planet.hasFeature(MoonFeature.SmallMoon, DwarfFeature.Small)) {
            planet.setRadius((int) (planet.getRadius() * 0.67));
        } else if (planet.hasFeature(MoonFeature.LargeMoon, DwarfFeature.Large)) {
            planet.setRadius((int)(planet.getRadius() * 1.5));
        }

        if (planet.hasFeature(MoonFeature.TidallyLocked)) {
            planet.setDayLength(planet.getPeriod());
        } else if (planet.hasFeature(MoonFeature.AlmostLocked)) {
            planet.setDayLength(planet.getPeriod() * (94 + Die.dieV(6)) / 100);
        }

        // Define resources for this world.
        addSecondaryResource(planet, Water);

        return planet;
    }
}
