/**
 * Atmosphere.java
 *
 * Copyright (c) 2007, Samuel Penn.
 * See the file LICENSE at the root of the project.
 */
package uk.org.glendale.worldgen.astro.planets.codes;

/**
 * Defines the different types of atmosphere that are common on planets.
 *
 * @author Samuel Penn
 */
public enum Atmosphere {
    Vacuum(1.0, 4),
    Standard(1.0, 1),
    Chlorine(1.0, 5),
    Flourine(1.0, 5),
    Oxygen(0.25, 4),
    SulphurCompounds(0.9, 3),
    Methane(0.8, 3),
    Nitrogen(0.8, 3),
    NitrogenCompounds(0.8, 3),
    OrganicToxins(1.0, 2),
    LowOxygen(1.0, 2),
    Pollutants(1.0, 2),
    HighCarbonDioxide(0.75, 3),
    HighOxygen(1.0, 1),
    InertGases(1.0, 3),
    Hydrogen(1.0, 3),
    Primordial(0.9, 3),
    WaterVapour(0.8, 3),
    CarbonDioxide(0.6, 3),
    Tainted(1.0, 2),
    Exotic(1.0, 5);

    private final double  greenhouse;
    private final int	  habitability;

    Atmosphere(double greenhouse, int habitability) {
        this.greenhouse = greenhouse;
        this.habitability = habitability;
    }

    public boolean isNonWater() {
        switch (this) {
            case Chlorine:
            case Flourine:
            case Exotic:
            case SulphurCompounds:
            case NitrogenCompounds:
                return true;
        }
        return false;
    }

    /**
     * Get the greenhouse factor for this atmosphere. This modifies the
     * effective distance of the planet from the star, so values less
     * than 1.0 make the planet warmer.
     *
     * @return		Greenhouse modifier.
     */
    public double getGreenhouse() {
        return greenhouse;
    }

    /**
     * Get the Habitability Class. This ranges from 1 (no protection required)
     * to 6 (highest level of protection required).
     */
    public int getHabitability() {
        return habitability;
    }
}
