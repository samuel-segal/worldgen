/**
 * JovicMapper.java
 *
 * Copyright (C) 2017 Samuel Penn, sam@notasnark.net
 * See the file LICENSE at the root of the project.
 */
package uk.org.glendale.worldgen.astro.planets.maps.jovian;

import uk.org.glendale.utils.graphics.SimpleImage;
import uk.org.glendale.utils.graphics.Tile;
import uk.org.glendale.utils.rpg.Die;
import uk.org.glendale.worldgen.astro.planets.Planet;
import uk.org.glendale.worldgen.astro.planets.codes.PlanetType;
import uk.org.glendale.worldgen.astro.planets.generators.Jovian;
import uk.org.glendale.worldgen.astro.planets.generators.jovian.Neptunian;
import uk.org.glendale.worldgen.astro.planets.generators.jovian.Saturnian;
import uk.org.glendale.worldgen.astro.planets.maps.JovianMapper;
import uk.org.glendale.worldgen.astro.planets.maps.PlanetMapper;
import uk.org.glendale.worldgen.web.Server;

import java.io.File;
import java.io.IOException;

import static uk.org.glendale.worldgen.astro.planets.generators.jovian.Neptunian.NeptunianFeatures.WhiteBands;

/**
 * A cold Jovian world, found in the outer solar system.
 */
public class NeptunianMapper extends JovianMapper {

    protected static final Tile DARK = new Tile("Blue", "#4E82FC", false, 6);
    protected static final Tile LIGHT = new Tile("Green", "#C9EFF2", false, 4);
    protected static final Tile WHITE = new Tile("White", "#E9EFF2", false, 2);

    public NeptunianMapper(final Planet planet, final int size) {
        super(planet, size);
    }
    public NeptunianMapper(final Planet planet) {
        super(planet, DEFAULT_FACE_SIZE);
    }

    private Tile getRandomColour() {
        if (planet.hasFeature(Neptunian.NeptunianFeatures.DarkClouds)) {
            return NeptunianMapper.DARK;
        }
        return NeptunianMapper.LIGHT;
    }

    protected Tile getBandColour(Tile previousColour, Tile nextColour) {
        if (nextColour != null) {
            return nextColour;
        }
        if (previousColour == null) {
            return getRandomColour();
        }
        return previousColour.getVariant(Die.d8() - Die.d8());
    }

    private static final int H = 52;

    public void cloudFormations() {
        if (planet.hasFeature(WhiteBands)) {
            int s = getNumRows() / 2 + Die.dieV(20);
            int h = Die.d4(2);

            Tile mix = getTile(0, s);
            for (int y = s; y < s + h; y++) {
                mix = mix.getMix(WHITE);
                for (int x=0; x < getWidthAtY(y); x++) {
                    setHeight(x, y, H);
                    setTile(x, y, mix);
                }
            }
            for (int y = s + h; y < s + h * 2; y++) {
                mix = mix.getMix(getTile(0, y));
                for (int x=0; x < getWidthAtY(y); x++) {
                    setHeight(x, y, H);
                    setTile(x, y, mix);
                }
            }
        }
    }

    public static void main(String[] args) throws IOException {
        Planet planet = new Planet();
        planet.setName("Niflheim");
        planet.setType(PlanetType.Neptunian);
        planet.setRadius(50_000);

        planet.addFeature(Neptunian.NeptunianFeatures.DarkClouds);
        planet.addFeature(WhiteBands);

        testOutput(planet, new Neptunian(Server.getWorldGen(), null, null, null, 0),
                new NeptunianMapper(planet));
    }
}
