/*
 * Copyright (c) 2017, Samuel Penn (sam@glendale.org.uk).
 * See the file LICENSE at the root of the project.
 */

package uk.org.glendale.worldgen.astro.planets.generators.smallbody;

import uk.org.glendale.utils.graphics.Icosahedron;
import uk.org.glendale.utils.graphics.SimpleImage;
import uk.org.glendale.utils.rpg.Die;
import uk.org.glendale.worldgen.WorldGen;
import uk.org.glendale.worldgen.astro.planets.Planet;
import uk.org.glendale.worldgen.astro.planets.PlanetFeature;
import uk.org.glendale.worldgen.astro.planets.codes.*;
import uk.org.glendale.worldgen.astro.planets.generators.SmallBody;
import uk.org.glendale.worldgen.astro.stars.Star;
import uk.org.glendale.worldgen.astro.systems.StarSystem;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static uk.org.glendale.worldgen.astro.commodities.CommodityName.*;
import static uk.org.glendale.worldgen.astro.planets.generators.SmallBody.SmallBodyFeature.Hot;
import static uk.org.glendale.worldgen.astro.planets.generators.SmallBody.SmallBodyFeature.Molten;
import static uk.org.glendale.worldgen.astro.planets.generators.SmallBody.SmallBodyFeature.MoltenMetals;
import static uk.org.glendale.worldgen.astro.planets.generators.Terrestrial.TerrestrialFeature.*;
import static uk.org.glendale.worldgen.astro.planets.generators.smallbody.Vulcanian.VulcanianFeatures.MagneticFields;

/**
 * A Vulcanian is the only member of the Vulcanoidal Class. It is a hot asteroid close to a star.
 * They generally have very high metallic content.
 */
public class Vulcanian extends SmallBody {

    public enum VulcanianFeatures implements PlanetFeature {
        MagneticFields
    }

    public Vulcanian(WorldGen worldgen, StarSystem system, Star star, Planet previous, long distance) {
        super(worldgen, system, star, previous, distance);
    }

    public Planet getPlanet(String name) {
        return getPlanet(name, PlanetType.Vulcanian);
    }

    public Planet getPlanet(String name, PlanetType type) {
        Planet planet =  definePlanet(name, type);
        planet.setRadius(getRadius(planet) / 2);

        // Set default day length to be 2-3 hours.
        planet.setDayLength(3600 + Die.die(3600, 2));

        planet.setTemperature(planet.getTemperature() + Die.dieV(10));

        if (planet.getTemperature() > Temperature.SilicatesMelt.getKelvin()) {
            planet.addFeature(Molten);
            planet.setMagneticField(MagneticField.Weak);
            planet.setAtmosphere(Atmosphere.Exotic);
            planet.setPressure(Die.d20(2));
            planet.setHabitability(6);
        } else if (planet.getTemperature() > Temperature.IronMelts.getKelvin()) {
            planet.addFeature(MoltenMetals);
            planet.setMagneticField(MagneticField.VeryWeak);
            planet.setAtmosphere(Atmosphere.Exotic);
            planet.setPressure(Die.d12(2));
            planet.setHabitability(5);
        } else if (planet.getTemperature() > Temperature.LeadMelts.getKelvin()) {
            planet.addFeature(SmallBodyFeature.Hot);
            planet.setMagneticField(MagneticField.Minimal);
            planet.setHabitability(5);
        }

        // Chance of a quickly rotating rock with a strong magnetic field.
        if (Die.d2() == 1) {
            planet.setMagneticField(planet.getMagneticField().getStronger());
            if (Die.d3() == 1) {
                planet.setMagneticField(planet.getMagneticField().getStronger());
                planet.setDayLength(planet.getDayLength() / 2);
            }
        }

        // If there is a strong magnetic field, and lots of volatiles, then we can get
        // metallic particles arranging along field lines.
        if (planet.getMagneticField().isStrongerThan(MagneticField.Weak)) {
            if (planet.getTemperature() > Temperature.LeadMelts.getKelvin()) {
                planet.addFeature(MagneticFields);
            }
        }

        if (planet.hasFeature(Molten)) {
            addPrimaryResource(planet, SilicateOre);
            addSecondaryResource(planet, ExoticGases);
            addTertiaryResource(planet, SilicateCrystals);
            addTraceResource(planet, FerricOre);
            addTraceResource(planet, HeavyMetals);
            addTraceResource(planet, ExoticCrystals);
        } else if (planet.hasFeature(MoltenMetals)) {
            addPrimaryResource(planet, SilicateOre);
            addSecondaryResource(planet, FerricOre);
            addSecondaryResource(planet, SilicateCrystals);
            addSecondaryResource(planet, HeavyMetals);
            addSecondaryResource(planet, ExoticCrystals);
            addTertiaryResource(planet, ExoticGases);
            addTraceResource(planet, RareMetals);
        } else if (planet.hasFeature(Hot)) {
            addPrimaryResource(planet, SilicateOre);
            addPrimaryResource(planet, ExoticCrystals);
            addPrimaryResource(planet, Radioactives);
            addSecondaryResource(planet, FerricOre);
            addSecondaryResource(planet, SilicateCrystals);
            addSecondaryResource(planet, HeavyMetals);
            addTertiaryResource(planet, RareMetals);
            addTertiaryResource(planet, PreciousMetals);
        } else {
            addPrimaryResource(planet, SilicateOre);
            addPrimaryResource(planet, SilicateCrystals);
            addSecondaryResource(planet, FerricOre);
            addSecondaryResource(planet, ExoticCrystals);
            addSecondaryResource(planet, HeavyMetals);
            addSecondaryResource(planet, Radioactives);
            addTertiaryResource(planet, RareMetals);
            addTertiaryResource(planet, PreciousMetals);
        }

        return planet;
    }

}
