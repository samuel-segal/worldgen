/*
 * Copyright (c) 2018, Samuel Penn (sam@glendale.org.uk).
 * See the file LICENSE at the root of the project.
 */
package uk.org.glendale.worldgen.astro.systems.generators;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.org.glendale.utils.rpg.Die;
import uk.org.glendale.worldgen.WorldGen;
import uk.org.glendale.worldgen.astro.Physics;
import uk.org.glendale.worldgen.astro.planets.Planet;
import uk.org.glendale.worldgen.astro.planets.PlanetFactory;
import uk.org.glendale.worldgen.astro.planets.codes.PlanetGroup;
import uk.org.glendale.worldgen.astro.planets.codes.PlanetType;
import uk.org.glendale.worldgen.astro.sectors.Sector;
import uk.org.glendale.worldgen.astro.stars.Luminosity;
import uk.org.glendale.worldgen.astro.stars.SpectralType;
import uk.org.glendale.worldgen.astro.stars.Star;
import uk.org.glendale.worldgen.astro.stars.StarGenerator;
import uk.org.glendale.worldgen.astro.systems.StarSystem;
import uk.org.glendale.worldgen.astro.systems.StarSystemGenerator;
import uk.org.glendale.worldgen.astro.systems.StarSystemType;
import uk.org.glendale.worldgen.civ.CivilisationGenerator;
import uk.org.glendale.worldgen.civ.civilisation.Colony;
import uk.org.glendale.worldgen.civ.civilisation.FreeSettlers;
import uk.org.glendale.worldgen.exceptions.DuplicateObjectException;

import java.util.List;

import static uk.org.glendale.worldgen.astro.systems.StarSystemFactory.getBeltName;
import static uk.org.glendale.worldgen.astro.systems.StarSystemFactory.getPlanetName;

public class YellowStar extends StarSystemGenerator {
    private static final Logger logger = LoggerFactory.getLogger(Simple.class);

    public YellowStar(WorldGen worldgen) {
        super(worldgen);
    }

    public StarSystem generate(Sector sector, String name, int x, int y) throws DuplicateObjectException {
        StarSystem system = createEmptySystem(sector, name, x, y);

        switch (Die.d6()) {
            default:
                createRockyWorlds(system);
                break;
        }
        updateStarSystem(system);

        return system;
    }

    /**
     * Creates a small star system with small rocky worlds similar to the inner system of Sol.
     *
     * @param system    Star system to populate.
     * @throws DuplicateObjectException     Duplicate object was created.
     */
    @SuppressWarnings("WeakerAccess")
    public void createRockyWorlds(StarSystem system) throws DuplicateObjectException {
        system.setType(StarSystemType.SINGLE);
        StarGenerator starGenerator = new StarGenerator(worldgen, system, false);
        List<Planet> planets;

        // Generate a Sol-like star.
        Star primary = starGenerator.generatePrimary(Luminosity.V, SpectralType.G2.getSpectralType(Die.dieV(3)));

        PlanetFactory planetFactory = worldgen.getPlanetFactory();
        String  planetName;

        int orbit = 1, beltOrbit = 1;
        long distance = 50 * Physics.MKM + Die.dieV(5_000_000);

        while (distance < 500_000_000) {
            PlanetType type = PlanetFactory.determineRockyType(Physics.getTemperatureOfOrbit(primary, distance));

            if (type == null) {
                // Do nothing, just allow for an empty 'slot'.
            } else if (type.getGroup() == PlanetGroup.Belt) {
                // Belt type.
                planetName = getBeltName(primary, beltOrbit++);
                planets = planetFactory.createPlanet(system, primary, planetName, type, distance);
                system.addPlanets(planets);
                distance += planets.get(0).getRadius();
            } else {
                // Standard planet.
                planetName = getPlanetName(primary, orbit++);
                planets = planetFactory.createPlanet(system, primary, planetName, type, distance);
                system.addPlanets(planets);
            }

            distance = (distance * 3 + Die.die(15_000_000L)) / 2;
        }
        setDescription(system, null);
    }

    public void createTest(StarSystem system) throws DuplicateObjectException {
        system.setType(StarSystemType.SINGLE);
        StarGenerator starGenerator = new StarGenerator(worldgen, system, false);
        List<Planet> planets;

        // Generate a Sol-like star.
        Star primary = starGenerator.generatePrimary(Luminosity.V, SpectralType.G2.getSpectralType(Die.dieV(2)));

        PlanetFactory planetFactory = worldgen.getPlanetFactory();
        String  planetName;

        int orbit = 1, beltOrbit = 1;
        long distance = 50 * Physics.MKM + Die.dieV(5_000_000);

        distance = 50_000_000L;
        planets = planetFactory.createPlanet(system, primary, getPlanetName(primary, orbit++),
                PlanetType.Hermian, distance);
        system.addPlanets(planets);

        distance = 100_000_000L;
        planets = planetFactory.createPlanet(system, primary, getPlanetName(primary, orbit++),
                PlanetType.Cytherean, distance);
        system.addPlanets(planets);

        distance = 130_000_000L;
        planets = planetFactory.createPlanet(system, primary, getPlanetName(primary, orbit++),
                PlanetType.EoGaian, distance);
        system.addPlanets(planets);

        distance = 160_000_000L;
        planets = planetFactory.createPlanet(system, primary, getPlanetName(primary, orbit++),
                PlanetType.MesoGaian, distance);
        system.addPlanets(planets);

        distance = 230_000_000L;
        planets = planetFactory.createPlanet(system, primary, getPlanetName(primary, orbit++),
                PlanetType.EoArean, distance);
        system.addPlanets(planets);

        setDescription(system, null);

        distance = 260_000_000L;
        planets = planetFactory.createPlanet(system, primary, getPlanetName(primary, orbit++),
                PlanetType.MesoArean, distance);
        system.addPlanets(planets);

        setDescription(system, null);
    }

    public void createTest2(StarSystem system) throws DuplicateObjectException {
        system.setType(StarSystemType.SINGLE);
        StarGenerator starGenerator = new StarGenerator(worldgen, system, false);
        List<Planet> planets;

        // Generate a Sol-like star.
        Star primary = starGenerator.generatePrimary(Luminosity.V, SpectralType.G3.getSpectralType(Die.dieV(2)));

        PlanetFactory planetFactory = worldgen.getPlanetFactory();
        String  planetName;

        int orbit = 1, beltOrbit = 1;
        long distance = 50 * Physics.MKM + Die.dieV(5_000_000);

        distance = 100_000_000L;
        planets = planetFactory.createPlanet(system, primary, getPlanetName(primary, orbit++),
                PlanetType.Aggregate, distance);
        system.addPlanets(planets);

        distance = 150_000_000L;
        planets = planetFactory.createPlanet(system, primary, getPlanetName(primary, orbit++),
                PlanetType.Vestian, distance);
        system.addPlanets(planets);

        distance = 200_000_000L;
        planets = planetFactory.createPlanet(system, primary, getPlanetName(primary, orbit++),
                PlanetType.Cerean, distance);
        system.addPlanets(planets);

        setDescription(system, null);
    }

    public void colonise(StarSystem system) {
        logger.info("Colonise star system " + system.getName());

        CivilisationGenerator generator = null;

        generator = new Colony(worldgen, system);
        if (generator != null) {
            generator.generate();
        }
    }
}
