/*
 * Copyright (c) 2020, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */

package uk.org.glendale.worldgen.astro.planets.maps.dwarf;

import uk.org.glendale.utils.graphics.SimpleImage;
import uk.org.glendale.utils.graphics.Tile;
import uk.org.glendale.utils.rpg.Die;
import uk.org.glendale.worldgen.astro.planets.Planet;
import uk.org.glendale.worldgen.astro.planets.codes.PlanetType;
import uk.org.glendale.worldgen.astro.planets.generators.dwarf.Vestian;
import uk.org.glendale.worldgen.astro.planets.maps.DwarfMapper;
import uk.org.glendale.worldgen.astro.planets.tiles.Rough;
import uk.org.glendale.worldgen.text.TextGenerator;
import uk.org.glendale.worldgen.web.Server;

import java.io.File;
import java.io.IOException;

/**
 * Vestian worlds tend to be on the smaller side of Dwarf Terrestrial worlds. They are plain, high in silicates
 * and low in metals. Similar to SmallBody worlds, Vestian worlds have a deform map.
 */
public class VestianMapper extends DwarfMapper {
    protected static final Tile MARIA = new Tile("Maria", "#908070", false, 2);
    protected static final Tile SILICATES = new Tile("Silicates", "#909080", false, 2);

    public VestianMapper(final Planet planet, final int size) {
        super(planet, size);
    }

    public VestianMapper(final Planet planet) {
        super(planet, DEFAULT_FACE_SIZE);
    }

    private void flattenedWorld() {
        for (int y=0; y < getNumRows(); y++) {
            double modifier = 1.0 - (Math.abs(y - getNumRows() / 2.0) / (getNumRows() / 2.0));
            for (int x=0; x < getWidthAtY(y); x++) {
                setHeight(x, y, (int)(Math.sqrt(modifier) * 100) + Die.dieV(4));
            }
        }
    }

    private void dumbbellWorld() {
        for (int y=0; y < getNumRows(); y++) {
            int halfPoint = getNumRows() / 2 + Die.dieV(6);
            double modifier = (Math.abs(y - halfPoint) / (getNumRows() / 2.0));
            if (modifier < 0.1) {
                modifier = 0;
            }
            for (int x=0; x < getWidthAtY(y); x++) {
                setHeight(x, y, (int)(Math.sqrt(modifier) * 100) + Die.dieV(4));
            }
        }
    }

    private void eggWorld() {
        for (int y=0; y < getNumRows(); y++) {
            double modifier = Math.max((Math.abs(y - getNumRows() / 2.0) / (getNumRows() / 2.0)) / 2, 0.1);
            for (int x=0; x < getWidthAtY(y); x++) {
                setHeight(x, y, (int)(Math.sqrt(modifier) * 100) + Die.dieV(4));
            }
        }
    }

    private void irregularWorld() {
        for (int y=4; y < getNumRows() - 4; y++) {
            for (int x=0; x < getWidthAtY(y); x++) {
                double half = getWidthAtY(y) / 2.0;
                double modifier = Math.max(Math.abs(( (x - half) / half)), 0.1);
                setHeight(x, y, (int)((modifier) * (100 + Die.dieV(20))));
            }
        }

        int sum = 0;
        for (int x=0; x < getWidthAtY(0); x++) {
            sum += getHeight(x, 0);
        }
        for (int x=0; x < getWidthAtY(0); x++) {
            setHeight(x, 0, sum / getWidthAtY(0));
        }
        sum = 0;
        for (int x=0; x < getWidthAtY(getNumRows() - 1); x++) {
            sum += getHeight(x, getNumRows() - 1);
        }
        for (int x=0; x < getWidthAtY(getNumRows() - 1); x++) {
            setHeight(x, getNumRows() - 1, sum / getWidthAtY(getNumRows() - 1));
        }
    }

    /**
     * Transform the world into different shapes.
     */
    private void deformModifier() {
        if (planet.hasFeature(Vestian.VestianFeature.Flattened)) {
            flattenedWorld();
        } else if (planet.hasFeature(Vestian.VestianFeature.Dumbbell)) {
            dumbbellWorld();
        } else if (planet.hasFeature(Vestian.VestianFeature.Egg)) {
            eggWorld();
        } else if (planet.hasFeature(Vestian.VestianFeature.Irregular)) {
            irregularWorld();
        } else if (planet.hasFeature(Vestian.VestianFeature.Broken)) {
            irregularWorld();
            cleanBumpMap();
        }
    }

    public void generate() {
        generateHeightMap(24, DEFAULT_FACE_SIZE);
        hasHeightMap = false;
        hasDeformMap = true;

        int seaLevel = getSeaLevel(5);

        if (planet.hasFeature(Vestian.VestianFeature.Reddish)) {
            SILICATES.rgb("#A09080");
        } else if (planet.hasFeature(Vestian.VestianFeature.Greenish)) {
            SILICATES.rgb("#90A080");
        }

        // Basic barren landscape.
        for (int tileY=0; tileY < getNumRows(); tileY++) {
            for (int tileX=0; tileX < getWidthAtY(tileY); tileX++) {
                int h = getHeight(tileX, tileY);
                if (h < seaLevel) {
                    setTile(tileX, tileY, new Rough(MARIA.getShaded(50 + (h / 2))));
                } else {
                    setTile(tileX, tileY, new Rough(SILICATES.getShaded(50 + (h / 2))));
                }
            }
        }

        deformModifier();

        setCraterModifier(0.50);
        createCraters(2, 30);
        setCraterModifier(0.65);
        createCraters(1, 50);
        setCraterModifier(0.80);
        createCraters(-1, 250);
    }

    public static void main(String[] args) throws IOException {
        Planet       planet = new Planet();
        planet.setName("Vestian");
        planet.setType(PlanetType.Vestian);
        planet.setTemperature(200);
        //planet.addFeature(Vestian.VestianFeature.Reddish);

        testOutput(planet, new Vestian(Server.getWorldGen(), null, null, null, 0),
                new VestianMapper(planet));
    }
}
