/*
 * Copyright (c) 2018, Samuel Penn (sam@glendale.org.uk).
 * See the file LICENSE at the root of the project.
 */
package uk.org.glendale.worldgen.astro.planets.maps.smallbody;

import uk.org.glendale.utils.graphics.Tile;
import uk.org.glendale.worldgen.astro.planets.Planet;
import uk.org.glendale.worldgen.astro.planets.generators.SmallBody;
import uk.org.glendale.worldgen.astro.planets.maps.SmallBodyMapper;
import uk.org.glendale.worldgen.astro.planets.tiles.Rough;

/**
 * Metallic class asteroids.
 */
public class MetallicMapper extends SmallBodyMapper {
    public MetallicMapper(final Planet planet, final int size) {
        super(planet, size);
    }

    public MetallicMapper(final Planet planet) {
        super(planet, DEFAULT_FACE_SIZE);
    }

    private static final Tile SILICATES = new Tile("Silicates", "#A09070", false);
    private static final Tile METALS = new Tile("Metals", "#E0E0F0", false);
    private static final Tile HOT_METALS = new Tile("Hot Metals", "#F09090", false);
    private static final Tile MELT_METALS = new Tile("Melt Metals", "#FE7070", false);

    public void generate() {
        super.generate();

        int baseGrey = 40;
        for (int y = 0; y < getNumRows(); y++) {
            for (int x = 0; x < getWidthAtY(y); x++) {
                int h = getHeight(x, y);
                if (h < baseGrey) {
                    int  grey = 100 - (baseGrey - h);
                    Tile metal = METALS;
                    if (planet.hasFeature(SmallBody.SmallBodyFeature.Molten)) {
                        if (h < baseGrey * 0.5) {
                            metal = MELT_METALS;
                            setHeight(x, y, baseGrey / 2);
                        } else  {
                            metal = HOT_METALS;
                        }
                    } else if (planet.hasFeature(SmallBody.SmallBodyFeature.MoltenMetals)) {
                        if (h < baseGrey * 0.5) {
                            metal = HOT_METALS;
                        }
                    }
                    setTile(x, y, metal.getShaded(grey));
                } else {
                    int grey = baseGrey + getHeight(x, y) / 2;
                    setTile(x, y, new Rough(SILICATES.getShaded(grey)));
                }
            }
        }

        // Larger asteroids tend to be more spherical.
        int heightDividor = 1 + (int) (planet.getRadius() / 50);

        // After finishing with the height map, set it to more consistent values
        // so that the bump mapper can use it cleanly.
        smoothHeights(planet, heightDividor);
    }
}
