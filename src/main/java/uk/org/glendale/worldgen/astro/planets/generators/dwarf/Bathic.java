/*
 * Copyright (c) 2018, Samuel Penn (sam@glendale.org.uk).
 * See the file LICENSE at the root of the project.
 */

package uk.org.glendale.worldgen.astro.planets.generators.dwarf;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.org.glendale.utils.rpg.Die;
import uk.org.glendale.utils.rpg.Roller;
import uk.org.glendale.worldgen.WorldGen;
import uk.org.glendale.worldgen.astro.planets.GeneralFeature;
import uk.org.glendale.worldgen.astro.planets.MoonFeature;
import uk.org.glendale.worldgen.astro.planets.Planet;
import uk.org.glendale.worldgen.astro.planets.codes.Atmosphere;
import uk.org.glendale.worldgen.astro.planets.codes.MagneticField;
import uk.org.glendale.worldgen.astro.planets.codes.PlanetType;
import uk.org.glendale.worldgen.astro.planets.codes.Pressure;
import uk.org.glendale.worldgen.astro.planets.generators.Dwarf;
import uk.org.glendale.worldgen.astro.stars.Star;
import uk.org.glendale.worldgen.astro.systems.StarSystem;

import static uk.org.glendale.worldgen.astro.commodities.CommodityName.Water;

/**
 * Bathic type worlds are covered with a layer of liquid water, with an atmosphere mostly of water vapour.
 * They are only found in the circumstellar habitable zone, since any closer and they'll lose their water
 * too quickly to space, and further out they freeze.
 */
public class Bathic extends Dwarf {
    private static final Logger logger = LoggerFactory.getLogger(Bathic.class);

    public Bathic(WorldGen worldgen, StarSystem system, Star star, Planet previous, long distance) {
        super(worldgen, system, star, previous, distance);
    }

    private void addFeatures(Planet planet) {
        // No features to add at the moment.
    }

    public Planet getPlanet(String name) {
        Planet planet = definePlanet(name, PlanetType.Bathic);
        int radius = 1400 + Die.die(400, 2);

        planet.setRadius(radius);
        planet.setAtmosphere(Atmosphere.WaterVapour);
        planet.setPressure(new Roller<Pressure>().add(Pressure.Thin, Pressure.Thin, Pressure.VeryThin).roll());
        planet.setMagneticField(MagneticField.None);

        addFeatures(planet);

        if (planet.hasFeature(MoonFeature.SmallMoon, DwarfFeature.Small)) {
            planet.setRadius((int) (planet.getRadius() * 0.67));
        } else if (planet.hasFeature(MoonFeature.LargeMoon, DwarfFeature.Large)) {
            planet.setRadius((int)(planet.getRadius() * 1.5));
        }

        if (planet.hasFeature(MoonFeature.TidallyLocked, GeneralFeature.TideLocked)) {
            planet.setDayLength(planet.getPeriod());
        }

        // Define resources for this world.
        addPrimaryResource(planet, Water);

        return planet;
    }
}
