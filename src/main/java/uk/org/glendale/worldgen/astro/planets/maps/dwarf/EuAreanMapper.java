/*
 * Copyright (c) 2017, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */

package uk.org.glendale.worldgen.astro.planets.maps.dwarf;

import uk.org.glendale.utils.graphics.SimpleImage;
import uk.org.glendale.utils.graphics.Tile;
import uk.org.glendale.utils.rpg.Die;
import uk.org.glendale.worldgen.astro.planets.Planet;
import uk.org.glendale.worldgen.astro.planets.PlanetFeature;
import uk.org.glendale.worldgen.astro.planets.codes.Atmosphere;
import uk.org.glendale.worldgen.astro.planets.codes.Life;
import uk.org.glendale.worldgen.astro.planets.codes.PlanetType;
import uk.org.glendale.worldgen.astro.planets.generators.Dwarf;
import uk.org.glendale.worldgen.astro.planets.generators.dwarf.Arean;
import uk.org.glendale.worldgen.astro.planets.generators.dwarf.EuArean;
import uk.org.glendale.worldgen.astro.planets.generators.dwarf.MesoArean;
import uk.org.glendale.worldgen.astro.planets.maps.PlanetMapper;
import uk.org.glendale.worldgen.astro.planets.tiles.Cratered;
import uk.org.glendale.worldgen.web.Server;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.List;

import static uk.org.glendale.worldgen.astro.planets.generators.Dwarf.DwarfFeature.*;

/**
 * EuArean worlds are similar to Mars.
 */
public class EuAreanMapper extends AreanMapper {


    public EuAreanMapper(final Planet planet, final int size) {
        super(planet, size);
    }

    public EuAreanMapper(final Planet planet) {
        super(planet, DEFAULT_FACE_SIZE);
    }

    /**
     * Generate a Hermian surface landscape. This will be grey, barren and cratered.
     */
    public void generate() {
        super.generate();

        if (planet != null) {
            List<PlanetFeature> features = planet.getFeatures();
            if (features != null && features.size() != 0) {
                generateFeatures(features);
            }
        }
    }

    private void generateFeatures(List<PlanetFeature> features) {
        for (PlanetFeature f : features) {
            if (f == GreatRift) {
                // A single rift split across the world.
                addRift(RIFT,24 + Die.d12(3));
                flood(RIFT, 1);
            } else if (f == BrokenRifts) {
                // A number of small rifts in the surface of the world.
                int numRifts = 6 + Die.d4(2);
                for (int r = 0; r < numRifts; r++) {
                    addRift(RIFT,6 + Die.d4(2));
                }
            }
        }

    }

    public static void main(String[] args) throws IOException {
        Planet       planet = new Planet();
        planet.setName("Elysee");
        planet.setType(PlanetType.EuArean);
        planet.setRadius(4000);
        planet.setHydrographics(40);
        planet.setTemperature(420);
        planet.setPressure(100);
        planet.setAtmosphere(Atmosphere.Vacuum);
        planet.setLife(Life.None);
        planet.addFeature(Arean.AreanFeature.GreyDesert);

        testOutput(planet, new EuArean(Server.getWorldGen(), null, null, null, 0),
                new MesoAreanMapper(planet));
    }
}
