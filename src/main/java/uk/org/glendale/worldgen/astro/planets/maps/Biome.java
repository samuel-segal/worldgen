/*
 * Copyright (c) 2020, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */
package uk.org.glendale.worldgen.astro.planets.maps;

import uk.org.glendale.utils.rpg.Die;
import uk.org.glendale.worldgen.astro.commodities.CommodityName;
import uk.org.glendale.worldgen.astro.commodities.Resource;
import uk.org.glendale.worldgen.astro.planets.Planet;

import java.util.ArrayList;
import java.util.List;

/**
 * A Biome represents the type of ecology available on a tile.
 */
public class Biome {
    final CommodityName name;
    String  colour;
    int     density;

    Biome(final CommodityName name, final String colour) {
        this.name = name;
        this.colour = colour;
    }

    public Biome colour(final String colour) {
        this.colour = colour;
        return this;
    }

    public Biome density(final int density) {
        //this.density = density;
        return this;
    }

    public List<String> colours(List<String> colours, Planet planet) {
        int r = 1000;

        if (name != null) {
            r = planet.getResource(name);
            //System.out.println("> " + name + " = " + r);
        }

        for (int i=0; i < density; i++) {
            if (Die.die(1000) <= r) {
                colours.add(colour);
            }
        }

        return colours;
    }
}
