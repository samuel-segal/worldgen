/*
 * Copyright (c) 2018, Samuel Penn (sam@glendale.org.uk).
 * See the file LICENSE at the root of the project.
 */
package uk.org.glendale.worldgen.astro.planets.maps.terrestrial;

import uk.org.glendale.utils.graphics.Icosahedron;
import uk.org.glendale.utils.graphics.SimpleImage;
import uk.org.glendale.utils.graphics.Tile;
import uk.org.glendale.utils.rpg.Die;
import uk.org.glendale.worldgen.astro.Physics;
import uk.org.glendale.worldgen.astro.planets.Planet;
import uk.org.glendale.worldgen.astro.planets.PlanetFeature;
import uk.org.glendale.worldgen.astro.planets.codes.PlanetType;
import uk.org.glendale.worldgen.astro.planets.generators.terrestrial.Cytherean;
import uk.org.glendale.worldgen.astro.planets.maps.PlanetMapper;
import uk.org.glendale.worldgen.astro.planets.maps.TerrestrialMapper;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static uk.org.glendale.worldgen.astro.planets.generators.terrestrial.Cytherean.CythereanFeature.*;

public class CythereanMapper extends TerrestrialMapper {
    public CythereanMapper(final Planet planet) {
        super(planet, DEFAULT_FACE_SIZE);
    }

    protected static final Tile LAND = new Tile("Land", "#A07050", false, 2);
    protected static final Tile SLUDGE = new Tile("Sludge", "#202040", true, 2);
    protected static final Tile LAVA = new Tile("Lava", "#D04040", false, 2);

    private void generateFeatures(List<PlanetFeature> features) {
    }

    private void shadeLandAndWater() {
        for (int y=0; y < getNumRows(); y++) {
            for (int x=0; x < getWidthAtY(y); x++) {
                if (getTile(x, y).isWater()) {
                    setTile(x, y, getTile(x, y).getShaded((getHeight(x, y) + 200) / 3));
                    setHeight(x, y, getHeight(x, y) / 2);
                } else {
                    setTile(x, y, getTile(x, y).getShaded((getHeight(x, y) + 100) / 2));
                }
            }
        }
    }

    public void generate() {
        super.generate();
        if (planet.hasFeature(CythereanXeric)) {
            setWater(SLUDGE);
        } else {
            setWater();
        }

        int continents = 4 + Die.d6();

        // Calculate things as they were, before the runaway greenhouse effect.
        if (planet.hasFeature(CythereanHydro)) {
            createContinents(continents, planet.getHydrographics() * 2);
            shadeLandAndWater();

            floodToPercentage(LAND, 100 - planet.getHydrographics(), true);
            shadeLandAndWater();
        } else if (planet.hasFeature(CythereanXeric)) {
            createContinents(continents, 30 + Die.d20(2));
            shadeLandAndWater();

            floodToPercentage(LAND, 100 - (15 + Die.d10(2)), true);
            shadeLandAndWater();
            floodToPercentage(LAND, 100 - planet.getHydrographics(), true);
            shadeLandAndWater();
        } else {
            createContinents(continents, 30 + Die.d20(2));
            for (int y=0; y < getNumRows(); y++) {
                for (int x=0; x < getWidthAtY(y); x++) {
                    setTile(x, y, LAND.getShaded((100 + getHeight(x, y)) / 2));
                }
            }
        }

        // These types of worlds still have ongoing volcanic activity.
        if (planet.hasFeature(CythereanHydro) || planet.hasFeature(CythereanXeric)) {
            int level = 70 + (planet.hasFeature(CythereanXeric)?10:0);

            for (int y=0; y < getNumRows(); y++) {
                for (int x=0; x < getWidthAtY(y); x++) {
                    int h = getHeight(x, y);

                    if (h > level) {
                        setTile(x, y, LAVA.getShaded((h - 65) * 3));
                    }
                }
            }
        }

        // After finishing with the height map, set it to more consistent values
        // so that the bump mapper can use it cleanly.
        for (int y=0; y < getNumRows(); y++) {
            for (int x=0; x < getWidthAtY(y); x++) {
                int h = getHeight(x, y);
                if (getTile(x, y).isWater()) {
                    h = 0;
                } else if (h < 25) {
                    h = 10;
                } else if (h < 50) {
                    h = 30;
                } else if (h < 75) {
                    h = 60;
                } else {
                    h = 100;
                }
                setHeight(x, y, h);
            }
        }

        generateFeatures(planet.getFeatures());
        hasCloudMap = true;
    }

    /**
     * Lower cloud layer is almost completely opaque.
     */
    private SimpleImage drawLowerCloudLayer(int width) throws IOException {
        Icosahedron cloud = getCloudLayer();

        int modifier = planet.getPressure() / Physics.STANDARD_PRESSURE;

        String cloudColour = "#AFA184";
        if (planet.hasFeature(DarkClouds)) {
            cloudColour = "#6F6144";
        } else if (planet.hasFeature(RedClouds)) {
            cloudColour = "#9F4030";
        } else if (planet.hasFeature(PinkClouds)) {
            modifier += 20;
        }

        if (planet.hasFeature(CythereanHydro)) {
            modifier *= 0.25;
            cloudColour = "#444444";
        } else if (planet.hasFeature(CythereanXeric)) {
            modifier *= 0.50;
            cloudColour = "#807864";
        }

        for (int y=0; y < cloud.getNumRows(); y++) {
            for (int x=0; x < cloud.getWidthAtY(y); x++) {
                int h = cloud.getHeight(x, y);
                cloud.setHeight(x, y, modifier + h / 2);
            }
        }

        return Icosahedron.stretchImage(cloud.drawTransparency(cloudColour, width), width);
    }

    /**
     * Upper cloud layer is more transparent, to allow the darker lower clouds to be seen.
     */
    private SimpleImage drawUpperCloudLayer(int width) throws IOException {
        Icosahedron cloud = getCloudLayer();

        int modifier = planet.getPressure() / Physics.STANDARD_PRESSURE;
        int lowerLimit = 25;

        String cloudColour = "#A0A89A";
        if (planet.hasFeature(DarkClouds)) {
            cloudColour = "#80887A";
        } else if (planet.hasFeature(PinkClouds)) {
            cloudColour = "#AF7164";
            lowerLimit = 50;
        }
        if (planet.hasFeature(CythereanHydro)) {
            lowerLimit = 45;
        } else if (planet.hasFeature(CythereanXeric)) {
            lowerLimit = 35;
        }

        for (int y=0; y < cloud.getNumRows(); y++) {
            for (int x=0; x < cloud.getWidthAtY(y); x++) {
                int h = cloud.getHeight(x, y);
                if (h < lowerLimit) {
                    cloud.setHeight(x, y, 0);
                } else {
                    if (planet.hasFeature(CythereanHydro)) {
                        cloud.setHeight(x, y, (modifier + h));
                    } else if (planet.hasFeature(CythereanXeric)) {
                        cloud.setHeight(x, y, (modifier + h) / 2);
                    } else {
                        cloud.setHeight(x, y, (modifier + h) / 3);
                    }
                }
            }
        }

        return Icosahedron.stretchImage(cloud.drawTransparency(cloudColour, width), width);
    }

    /**
     * Generate and draw the cloud layers for this world. Cytherean worlds have thick clouds,
     * so two layers of clouds are drawn. The lower layer provides complete coverage, and the
     * upper layer provides less coverage. They have subtle colour differences to make them
     * less boring.
     *
     * Cloud layers are automatically stretched before being stored.
     *
     * @param width     Width of the texture to draw.
     * @return          List of two cloud layers.
     */
    public List<SimpleImage> drawClouds(int width) {
        List<SimpleImage>  clouds = new ArrayList<>();
        cloudHeight = 5;

        try {
            clouds.add(drawLowerCloudLayer(width));
            clouds.add(drawUpperCloudLayer(width));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return clouds;
    }

    public static void main(String[] args) throws IOException {
        Planet planet = new Planet();
        planet.setName("Foo I");
        planet.setType(PlanetType.Cytherean);
        planet.setTemperature(300);
        planet.setHydrographics(25);
        planet.addFeature(Cytherean.CythereanFeature.CythereanHydro);
        PlanetMapper p = new CythereanMapper(planet);

        System.out.println("Cytherean:");
        p.generate();
        SimpleImage img = p.draw(2048);
        img.save(new File("/home/sam/tmp/cytherean.png"));
        Icosahedron.stretchImage(p.drawHeightMap(2048), 2048).save(new File("/home/sam/tmp/cytherean_h.png"));
        p.drawClouds(2048).get(0).save(new File("/home/sam/tmp/cytherean_c.png"));

    }
}
