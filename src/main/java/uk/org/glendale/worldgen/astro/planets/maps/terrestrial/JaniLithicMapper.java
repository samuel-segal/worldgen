/*
 * Copyright (c) 2021, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */

package uk.org.glendale.worldgen.astro.planets.maps.terrestrial;

import uk.org.glendale.utils.graphics.Icosahedron;
import uk.org.glendale.utils.graphics.SimpleImage;
import uk.org.glendale.utils.graphics.Tile;
import uk.org.glendale.worldgen.astro.Physics;
import uk.org.glendale.worldgen.astro.commodities.CommodityName;
import uk.org.glendale.worldgen.astro.planets.Planet;
import uk.org.glendale.worldgen.astro.planets.PlanetFeature;
import uk.org.glendale.worldgen.astro.planets.codes.Atmosphere;
import uk.org.glendale.worldgen.astro.planets.codes.Life;
import uk.org.glendale.worldgen.astro.planets.codes.PlanetType;
import uk.org.glendale.worldgen.astro.planets.generators.terrestrial.JaniLithic;
import uk.org.glendale.worldgen.astro.planets.generators.terrestrial.Lutian;
import uk.org.glendale.worldgen.astro.planets.maps.TerrestrialMapper;
import uk.org.glendale.worldgen.astro.planets.tiles.Rough;
import uk.org.glendale.worldgen.web.Server;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static uk.org.glendale.worldgen.astro.planets.generators.Terrestrial.TerrestrialFeature.*;

public class JaniLithicMapper extends TerrestrialMapper {
    public JaniLithicMapper(final Planet planet) {
        super(planet, DEFAULT_FACE_SIZE);
    }

    protected final Tile MOUNTAINS = new Tile("Mountains", "#706045", false, 2);
    protected final Tile PLAINS = new Tile("Plains", "#B08055", false, 3);
    protected final Tile DESERT = new Tile("Desert", "#D09050", false, 2);

    private void generateFeatures(List<PlanetFeature> features) {
    }

    public void generate() {
        super.generate();

        for (int y=0; y < getNumRows(); y++) {
            for (int x=0; x < getWidthAtY(y); x++) {
                if (getHeight(x, y) > 85) {
                    setTile(x, y, MOUNTAINS.getShaded(getHeight(x, y)));
                } else if (getHeight(x, y) < 25) {
                    setTile(x, y, PLAINS.getShaded((getHeight(x, y) + 100) / 2));
                } else {
                    setTile(x, y, DESERT.getShaded((getHeight(x, y) + 100) / 2));
                }
                int l = Math.abs(getLongitude(x, y));
                if (l < 90) {
                    setTile(x, y, getTile(x, y).getShaded(l/2 + 30));
                }
            }
        }

        // After finishing with the height map, set it to more consistent values
        // so that the bump mapper can use it cleanly.
        for (int y=0; y < getNumRows(); y++) {
            for (int x=0; x < getWidthAtY(y); x++) {
                if (getTile(x, y).isWater()) {
                    setHeight(x, y, 0);
                } else {
                    int h = getHeight(x, y);
                    if (h < 90) {
                        h = 50;
                    } else {
                        h = 100;
                    }
                    setHeight(x, y, h);
                }
                setTile(x, y, new Rough(getTile(x, y)));
            }
        }
        createCraters(1, 10);

        generateFeatures(planet.getFeatures());
//        cleanBumpMap();

        hasHeightMap = true;
    }


    public static void main(String[] args) throws IOException {
        Planet       planet = new Planet();
        planet.setName("JaniLithic");
        planet.setType(PlanetType.JaniLithic);
        planet.setHydrographics(0);
        planet.setTemperature(550);
        planet.setNightTemperature(250);
        planet.setPressure(10_000);
        planet.setAtmosphere(Atmosphere.CarbonDioxide);
        planet.setLife(Life.None);

        testOutput(planet, new JaniLithic(Server.getWorldGen(), null, null, null, 0),
                new JaniLithicMapper(planet));
    }
}
