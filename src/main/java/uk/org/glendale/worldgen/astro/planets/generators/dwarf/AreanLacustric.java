/*
 * Copyright (c) 2017, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */

package uk.org.glendale.worldgen.astro.planets.generators.dwarf;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.org.glendale.utils.rpg.Die;
import uk.org.glendale.worldgen.WorldGen;
import uk.org.glendale.worldgen.astro.planets.Planet;
import uk.org.glendale.worldgen.astro.planets.codes.*;
import uk.org.glendale.worldgen.astro.stars.Star;
import uk.org.glendale.worldgen.astro.systems.StarSystem;

import static uk.org.glendale.worldgen.astro.commodities.CommodityName.Oxygen;
import static uk.org.glendale.worldgen.astro.commodities.CommodityName.Water;

/**
 * A GeoCyclic Arean-Lacustric world of the Dwarf Terrestrial Group. This world is in a state of
 * high atmosphere and water, with a well evolved ecosystem.
 */
public class AreanLacustric extends Arean {
    private static final Logger logger = LoggerFactory.getLogger(AreanLacustric.class);

    public AreanLacustric(WorldGen worldgen, StarSystem system, Star star, Planet previous, long distance) {
        super(worldgen, system, star, previous, distance);
    }

    public Planet getPlanet(String name) {
        Planet planet = definePlanet(name, PlanetType.MesoArean);
        int radius = 3000 + Die.die(500, 2);

        planet.setRadius(radius);
        planet.setAtmosphere(Atmosphere.CarbonDioxide);
        planet.setPressure(50_000 + Die.die(40_000, 2));
        planet.setTemperature((planet.getTemperature() + 320) / 2);
        planet.setMagneticField(MagneticField.Minimal);

        int pascals = planet.getPressure();
        // Ironically, the chance of life increases as the atmosphere thins and the seas evaporate. It's
        // a race between life evolving and the planet dying.
        int lifeBonus = 0;
        if (pascals < 70_000) {
            planet.setMagneticField(MagneticField.Weak);
            planet.setHydrographics(Die.d8(3));
            planet.setTemperature(Math.max(planet.getTemperature(), 280 + Die.d6(3)));
            lifeBonus = 0;
        } else if (pascals < 90_000) {
            planet.setMagneticField(MagneticField.Standard);
            planet.setHydrographics(Die.d12(5));
            planet.setTemperature(Math.max(planet.getTemperature(), 285 + Die.d6(5)));
            lifeBonus = 3;
        } else {
            planet.setMagneticField(MagneticField.Standard);
            planet.setHydrographics(40 + Die.d10(4));
            planet.setTemperature(Math.max(planet.getTemperature(), 285 + Die.d6(5)));
            lifeBonus = 5;
        }
        planet.setNightTemperature(planet.getTemperature() - Die.d6(2));

        planet.setAtmosphere(Atmosphere.Standard);
        switch (Die.d6(2) + lifeBonus) {
            case 2: case 3: case 4:
                planet.setLife(Life.ComplexOcean);
                planet.setHabitability(2);
                break;
            case 5: case 6: case 7:
                planet.setLife(Life.SimpleLand);
                planet.setHabitability(2);
                break;
            case 8: case 9: case 10:
                planet.setLife(Life.ComplexLand);
                planet.setHabitability(1);
                break;
            default:
                planet.setLife(Life.Extensive);
                planet.setHabitability(1);
                addSecondaryResource(planet, Oxygen);
                break;
        }
        addPrimaryResource(planet, Oxygen);
        addPrimaryResource(planet, Water);

        // Define basic resources for this world. Biological resources will be defined in the text template.
        addMineralResources(planet);

        return planet;
    }
}
