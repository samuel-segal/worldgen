/*
 * Copyright (c) 2017, Samuel Penn.
 * See the file LICENSE at the root of the project.
 */
package uk.org.glendale.worldgen.astro.systems;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.org.glendale.utils.graphics.SimpleImage;
import uk.org.glendale.worldgen.WorldGen;
import uk.org.glendale.worldgen.astro.Physics;
import uk.org.glendale.worldgen.astro.planets.Planet;
import uk.org.glendale.worldgen.astro.planets.PlanetFactory;
import uk.org.glendale.worldgen.astro.planets.codes.PlanetGroup;
import uk.org.glendale.worldgen.astro.planets.codes.PlanetType;
import uk.org.glendale.worldgen.astro.planets.maps.PlanetMapper;
import uk.org.glendale.worldgen.astro.planets.maps.belt.*;
import uk.org.glendale.worldgen.astro.stars.Star;

import java.awt.*;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static uk.org.glendale.worldgen.astro.systems.StarSystemImage.ScaleType.MKM;

/**
 * Draws a map of the given star system.
 */
public class StarSystemImage {
    private static final Logger logger = LoggerFactory.getLogger(StarSystemImage.class);

    private final StarSystem system;
    private List<Planet> planets;

    private SimpleImage image		= null;
    private long		scale		= -1;
    private int         width       = 1000;
    private int         ox          = width / 2;
    private int         oy          = width / 2;
    private long        currentTime = 0;

    private boolean     drawZones   = false;
    private boolean     drawTitle   = false;
    private boolean     drawLabels  = true;
    private boolean     drawShadows = false;
    private static final long SHADOW_DISTANCE = 100_000_000_000L;
    private long        shadowX     =  0;
    private long        shadowY     = 0;
    private ScaleType   scaleType   = MKM;
    private long        maxDistance = 150;
    private int         zoom        = 0;
    private int         parentPlanet = 0;
    private int         highlightedPlanet = 0;

    public enum ScaleType {
        NONE,
        AU,
        MKM,
        KM
    }

    public enum Zoom {
        COG(0),
        PRIMARY(1),
        SECONDARY(2),
        TERTIARY(4),
        INNER(1024),
        MIDDLE(2048),
        OUTER(4096),
        ALL(8192);

        private final int code;

        Zoom(int code) {
            this.code = code;
        }

        public boolean equals(Zoom type) {
            return ((this.code & type.code) > 0);
        }

        public boolean equals(int zoom) {
            return ((this.code & zoom) > 0);
        }
    }

    /**
     * Create a new StarSystemImage for the given system.
     *
     * @param worldgen
     *            WorldGen object.
     * @param system
     *            StarSystem to draw map for.
     */
    public StarSystemImage(WorldGen worldgen, StarSystem system) {
        PlanetFactory planetFactory = worldgen.getPlanetFactory();
        this.system = system;
        this.planets = planetFactory.getPlanets(system);

        this.currentTime = worldgen.getUniverse().getSimTime();
    }

    /**
     * Set the scale of the map to be generated. Scale gives size of each pixel,
     * in kilometres. So scale = 1000, 1 pixel = 1000km.
     *
     * @param scale
     *            Size of the map.
     */
    public void setScale(long scale) {
        this.scale = scale;
        this.maxDistance = (long) (width * scale * 0.7);
    }

    /**
     * Gets the scale of the map. This is the number of km per pixel.
     *
     * @return      Scale in kilometres per pixel.
     */
    public long getScale() {
        return scale;
    }

    public void setZoom(final int zoom) {
        this.zoom = zoom;
    }

    public int getZoom() {
        return zoom;
    }

    public void setWidth(int width) {
        this.width = Math.max(width, 500);
        this.ox = width / 2;
        this.oy = width / 2;
    }

    /**
     * Set whether to display jump mask and shadows. If mask is true, then jump masks are displayed around
     * planets and stars. If shadowX or shadowY are non-zero, then jump shadows are also displayed. Shadows
     * show which parts of the system are 'in shadow' from the given point.
     *
     * shadowX and shadowY are in parsecs relative to this system. So 0, +1 would mean calculate jump shadows
     * for a destination (or source) 1 parsec 'beneath' this map.
     *
     * @param mask      If true, display jump masks.
     * @param shadowX   X offset in parsecs for jump destination to calculate shadows.
     * @param shadowY   Y offset in parsecs for jump destination to calculate shadows.
     */
    public void setShadows(boolean mask, int shadowX, int shadowY) {
        this.drawShadows = mask;

        if (shadowX%2 != 0) {
            // Odd number of columns difference, so Y shifts down.
            this.shadowY = SHADOW_DISTANCE / 2;
        }
        this.shadowY += shadowY * SHADOW_DISTANCE;
        this.shadowX += shadowX * SHADOW_DISTANCE / Math.sqrt(3);
    }

    /**
     * Sets the planet to focus on. If set, displays the planetary system of this planet
     * rather than the solar system as a whole.
     *
     * @param planetId      Parent planet to focus on.
     */
    public void setPlanet(final int planetId) {
        this.parentPlanet = planetId;

        Planet planet = planets.stream().filter(p -> p.getId() == planetId).findFirst().orElse(null);
        if (planet != null && planet.isMoon()) {
            parentPlanet = planet.getMoonOf();
            highlightedPlanet = planetId;
        }
    }

    public int getWidth() {
        return width;
    }

    /**
     * Sets whether to draw the habitable zones on the map or not. Defaults to false.
     *
     * @param drawZones     True if the habitable zones should be drawn.
     */
    public void setZones(boolean drawZones) {
        this.drawZones = drawZones;
    }

    /**
     * Gets the current time in seconds since the start of the epoc. This is normally set
     * to the current time of the simulation when the class is instantiated, unless it has
     * been manually overridden.
     *
     * @return  Time in seconds since the start of the simulation epoch.
     */
    public long getTime() {
        return currentTime;
    }

    /**
     * Sets the current time in seconds since the start of the epoc for this map. This
     * is used to determine where objects are in their orbits.
     *
     * @param time  Time in seconds since the start of the simulation epoch.
     */
    public void setTime(long time) {
        if (time < 0L) {
            throw new IllegalArgumentException("Time cannot be set to be negative.");
        }
        this.currentTime = time;
    }

    /**
     * Get the angular position of an object in orbit around its primary. This is based on the
     * current simulation time (or map time if that has been explicitly set). Assumes that planets
     * orbit counter-clockwise.
     *
     * @param planet        Planet
     * @return              Angle, between 0 and 360 degrees.
     */
    private double getAngleOffset(Planet planet) {
        double angle = (planet.getDistance() + planet.getId()) % 360;

        long period = Math.max(1, planet.getPeriod());

        angle += (360.0 * (getTime() % period)) / period;
        angle %= 360;

        // Switch to counter clockwise.
        angle = (360 - angle) % 360;

        return angle;
    }

    private double getAngleOffset(Star star, int index) {
        double angle = 180 * index;
        long   period = Math.max(1, star.getPeriod());

        angle += (360.0 * (getTime() % period) / (1.0 * period));
        angle %= 360;

        // Switch to counter clockwise.
        angle = (360 - angle) % 360;

        return angle;
    }

    /**
     * Set a default scale based on the size of the solar system.
     */
    private void setScale() {
        maxDistance = Physics.MKM;
        int onlyParent = 0;

        if (parentPlanet > 0) {
            maxDistance = 1_000;
            for (Planet p : planets) {
                if (p.getMoonOf() == parentPlanet) {
                    if (p.getDistance() + p.getRadius() > maxDistance) {
                        maxDistance = p.getDistance() + p.getRadius();
                    }
                }
            }
            maxDistance *= 1.2;
            scale = (maxDistance * 2) / width;

            return;
        }

        if (Zoom.PRIMARY.equals(zoom)) {
            onlyParent = system.getStars().get(0).getId();
        } else if (Zoom.SECONDARY.equals(zoom)) {
            if (system.getStars().size() > 1) {
                onlyParent = system.getStars().get(1).getId();
            }
        } else if (Zoom.TERTIARY.equals(zoom)) {
            if (system.getStars().size() > 2) {
                onlyParent = system.getStars().get(2).getId();
            }
        }
        // Set Scale
        switch (system.getType()) {
            case EMPTY:
                maxDistance = Physics.AU;
                break;
            case SINGLE:
            case CONJOINED_BINARY:
            case CLOSE_BINARY:
                // In these cases, all planets orbit the centre of gravity of the entire system.
                if (onlyParent == 0) {
                    for (Star s : system.getStars()) {
                        maxDistance += s.getDistance();
                    }
                }
                for (Planet p: planets) {
                    if (onlyParent > 0 && p.getParentId() != onlyParent) {
                        continue;
                    }
                    if (!p.isMoon()) {
                        long d = p.getDistance();
                        if (Zoom.INNER.equals(zoom) && d > Physics.getOrbitWithTemperature(system.getStars().get(0), CircumstellarZone.Outer.getLowerKelvin())) {
                            continue;
                        } else if (Zoom.MIDDLE.equals(zoom) && d > Physics.getOrbitWithTemperature(system.getStars().get(0), CircumstellarZone.Cold.getLowerKelvin())) {
                            continue;
                        } else if (!Zoom.ALL.equals(zoom) && p.getType() == PlanetType.OortCloud) {
                            continue;
                        }
                        if (p.getType().getGroup().equals(PlanetGroup.Belt)) {
                            d += p.getRadius();
                        }
                        if (d > maxDistance) {
                            maxDistance = d;
                        }
                    }
                }
                break;
            case MEDIUM_BINARY:
            case FAR_BINARY:
                // Smaller star orbits a larger star. Secondary star will be further away than
                // all the planets of the primary, so we just need to know how big the system
                // of the secondary star is, and add that to the distance between the two
                // stars.
                List<Star> stars = system.getStars();
                for (Planet p: planets) {
                    if (onlyParent > 0) {
                        if (p.getParentId() != onlyParent) {
                            continue;
                        }
                    } else {
                        if (p.getParentId() != stars.get(1).getId()) {
                            continue;
                        }
                    }
                    if (!p.isMoon()) {
                        long d = p.getDistance();
                        if (p.getType().getGroup().equals(PlanetGroup.Belt)) {
                            d += p.getRadius();
                        }
                        if (d > maxDistance) {
                            maxDistance = d;
                        }
                    }
                }
                // Add distance between stars.
                if (onlyParent == 0) {
                    maxDistance += stars.get(1).getDistance();
                }
                break;
            default:
                // More complex systems not yet fully supported, so just guess.
                maxDistance = 1500 * Physics.MKM;
                break;
        }
        maxDistance *= 1.2;
        scale = (maxDistance * 2) / width;
    }

    /**
     * Gets the pixel coordinates of each of the stars in the system. 0,0 is considered to be
     * the centre of the system. Stars are keyed by index, with a Point(x,y) used to define
     * their scaled pixel coordinates.
     *
     * @return  Map of pixel coordinates, keyed on star index.
     */
    private Map<Integer, Point> getStarPositions() {
        Map<Integer, Point> coords = new HashMap<>();

        switch(system.getType()) {
            case EMPTY:
                break;
            case SINGLE:
                coords.put(0, new Point(0, 0));
                break;
            case CONJOINED_BINARY:
            case CLOSE_BINARY:
            case MEDIUM_BINARY:
            case FAR_BINARY:
                if (system.getStars().get(0).getParentId() < 0) {
                    // Stars are orbiting a common centre of gravity.
                    for (int s = 0; s < system.getStars().size(); s++) {
                        Star star = system.getStars().get(s);
                        double angle = getAngleOffset(star, s);
                        double x = (Math.cos(Math.toRadians(angle)) * star.getDistance());
                        double y = (Math.sin(Math.toRadians(angle)) * star.getDistance());
                        coords.put(s, new Point((int) getScaledPixels(x), (int) getScaledPixels(y)));
                    }
                } else {
                    // First star is at the centre, second star is orbiting it.
                    coords.put(0, new Point(0, 0));
                    Star star = system.getStars().get(1);
                    double angle = getAngleOffset(star, 1);
                    double x = (Math.cos(Math.toRadians(angle)) * star.getDistance());
                    double y = (Math.sin(Math.toRadians(angle)) * star.getDistance());
                    coords.put(1, new Point((int) getScaledPixels(x), (int) getScaledPixels(y)));
                }
                break;
            default:
                coords.put(0, new Point(0, 0));
                Star star = system.getStars().get(1);
                double angle = getAngleOffset(star, 1);
                double x = (Math.cos(Math.toRadians(angle)) * star.getDistance());
                double y = (Math.sin(Math.toRadians(angle)) * star.getDistance());
                coords.put(1, new Point((int) getScaledPixels(x), (int) getScaledPixels(y)));
                break;
        }

        return coords;
    }

    public SimpleImage draw() {
        //noinspection SuspiciousNameCombination
        image = new SimpleImage(width, width, "#ffffff");
        if (scale == -1) {
            setScale();
        }

        if (parentPlanet > 0) {
            return drawPlanetarySystem();
        } else {
            return drawStarSystem();
        }
    }

    private SimpleImage drawPlanetarySystem() {
        scaleType = ScaleType.KM;
        ox = oy = width / 2;

        Planet parent = planets.stream().filter(p -> p.getId() == parentPlanet).findFirst().orElse(null);

        if (parent != null) {
            if (highlightedPlanet > 0) {
                image.circle(ox, oy, (int) (parent.getRadius() / scale),
                        SimpleImage.getLighter(parent.getType().getColour()));
                image.circleOutline(ox, oy, (int) (parent.getRadius() / scale), parent.getType().getColour());
            } else {
                image.circle(ox, oy, (int) (parent.getRadius() / scale), parent.getType().getColour());
            }
            for (Planet p : planets) {
                if (p.getMoonOf() == parentPlanet) {
                    if (p.getType().getGroup().equals(PlanetGroup.Belt)) {
                        drawBelt(null, ox, oy, p, null);
                    } else {
                        if (highlightedPlanet > 0 && highlightedPlanet != p.getId()) {
                            image.circleOutline(ox, oy, (int) (p.getDistance() / scale), "#DDDDDD");
                        } else {
                            image.circleOutline(ox, oy, (int) (p.getDistance() / scale), "#888888");
                        }

                        double angle = getAngleOffset(p);
                        int     x = (int) (Math.cos(Math.toRadians(angle)) * p.getDistance());
                        int     y = (int) (Math.sin(Math.toRadians(angle)) * p.getDistance());

                        x /= scale;
                        y /= scale;
                        int r = (int) (p.getRadius() / scale);
                        if (r < 3) {
                            r = 3;
                        }
                        if (highlightedPlanet > 0 && highlightedPlanet != p.getId()) {
                            image.circle(ox + x, oy + y, r,
                                    SimpleImage.getLighter(p.getType().getColour(), 2));
                        } else {
                            image.circle(ox + x, oy + y, r, p.getType().getColour());
                            drawPlanetLabel(ox + x, oy + y, angle, p);
                        }
                    }
                }
            }

            drawScale();
        } else {
            logger.error(String.format("Unable to find planet [%d] to draw", parentPlanet));
        }
        return image;
    }

    private SimpleImage drawStarSystem() {
        logger.info(String.format("Drawing system map for [%s] at scale of [%d]", system.getName(), getScale()));

        Map<Integer,Point> coords = getStarPositions();

        Star primary, secondary;
        switch(system.getType()) {
            case EMPTY:
                // Nothing to do.
                break;
            case SINGLE:
                primary = system.getStars().get(0);
                drawGrid(primary, ox, oy);
                drawStar(primary, ox, oy);
                break;
            case CONJOINED_BINARY:
            case CLOSE_BINARY:
            case MEDIUM_BINARY:
            case FAR_BINARY:
                primary = system.getStars().get(0);
                secondary = system.getStars().get(1);

                if (Zoom.PRIMARY.equals(zoom)) {
                    ox = width/2 - (int) coords.get(0).getX();
                    oy = width/2 - (int) coords.get(0).getY();
                    drawGrid(primary, width / 2, width / 2);
                } else if (Zoom.SECONDARY.equals(zoom)) {
                    ox = width/2 - (int) coords.get(1).getX();
                    oy = width/2 - (int) coords.get(1).getY();
                    drawGrid(secondary, width / 2, width / 2);
                } else {
                    drawGrid(primary, ox, oy);
                }

                drawStar(primary, ox + (int) coords.get(0).getX(), oy + (int) coords.get(0).getY());
                drawStar(secondary, ox + (int) coords.get(1).getX(), oy + (int) coords.get(1).getY());
                if (primary.getParentId() < 0 || secondary.getParentId() < 0) {
                    Star cog = new Star();
                    cog.setCentreOfGravity(StarSystem.PRIMARY_COG);
                    drawStar(cog, ox, oy);
                }
                break;
        }
        drawScale();

        return image;
    }

    /**
     * Gets a distance scaled to fit on the map. Distance is specified in KM,
     * and value is returned in pixels.
     *
     * @param km     Actual star system distance in kilometres.
     * @return       Scaled distance in pixels.
     */
    private double getScaledPixels(double km) {
        return (km / scale);
    }

    /**
     * Draws a scale line along the bottom of the map.
     */
    private void drawScale() {
        double scaleWidth;
        String label;

        if (scaleType == ScaleType.NONE) {
            return;
        }

        if (maxDistance < 10_000) {
            scaleWidth = 0.010;
        } else if (maxDistance < 30_000) {
            scaleWidth = 0.025;
        } else if (maxDistance < 100_000) {
            scaleWidth = 0.05;
        } else if (maxDistance < 300_000) {
            scaleWidth = 0.10;
        } else if (maxDistance < 1_000_000) {
            scaleWidth = 0.25;
        } else if (maxDistance < 3 * Physics.MKM) {
            scaleWidth = 0.50;
        } else if (maxDistance < 10 * Physics.MKM) {
            scaleWidth = 1;
        } else if (maxDistance < 30 * Physics.MKM) {
            scaleWidth = 10;
        } else if (maxDistance < 100 * Physics.MKM) {
            scaleWidth = 25;
        } else if (maxDistance < 300 * Physics.MKM) {
            scaleWidth = 50;
        } else if (maxDistance < 1000 * Physics.MKM) {
            scaleWidth = 100;
        } else if (maxDistance < 3_000 * Physics.MKM) {
            scaleWidth = 500;
        } else if (maxDistance < 10_000 * Physics.MKM) {
            scaleWidth = 150 * 10;
        } else if (maxDistance < 30_000 * Physics.MKM) {
            scaleWidth = 150 * 25;
        } else if (maxDistance < 1_000 * Physics.AU) {
            scaleWidth = 150 * 100;
        } else if (maxDistance < 3_000 * Physics.AU) {
            scaleWidth = 150 * 500;
        } else if (maxDistance < 10_000 * Physics.AU) {
            scaleWidth = 150 * 1_000;
        } else if (maxDistance < 30_000 * Physics.AU) {
            scaleWidth = 150 * 5_000;
        } else {
            scaleWidth = 150 * 10_000;
        }

        if (scaleWidth < 1) {
            DecimalFormat format = new DecimalFormat();
            format.setMaximumFractionDigits(0);
            label = format.format(scaleWidth * 1_000_000) + " km";
        } else if (scaleWidth < 1000) {
            label = ((int) scaleWidth) + " Mkm";
        } else {
            label = ((int) (scaleWidth / 150)) + " AU";
        }

        int left = width / 2;
        int right = width / 2 + (int) getScaledPixels(scaleWidth * Physics.MKM);
        image.line(left, width - 10, right, width - 10, "#000000", 2);
        image.line(left, width - 10, left, width - 20, "#000000", 2);
        image.line(right, width - 10, right, width - 20, "#000000", 2);

        int pos = (left + right) / 2 - image.getTextWidth(label, 0, 12) / 2;
        image.text(pos, width - 20, label, 0, 12, "#000000");

    }

    /**
     * Draw a grid that displays the habitable zones around the star.
     */
    private void drawGrid(Star star, int cx, int cy) {
        // Bare grid lines.
        //noinspection SuspiciousNameCombination
        image.line(width / 2, 0, width / 2, width, "#E0E0E0", 1);
        image.line(0, width / 2, width, width / 2, "#E0E0E0", 1);

        int ambient = 0;
        for (Star s : system.getStars()) {
            if (s.getId() != star.getId()) {
                ambient = Physics.getTemperatureOfOrbit(ambient, s, s.getDistance() + star.getDistance());
            }
        }

        int start = 0;
        final int w = 5;
        for (CircumstellarZone zone : CircumstellarZone.values()) {
            int k = (int) Math.pow(Math.max(3, Math.pow(zone.getLowerKelvin(), 4) - Math.pow(ambient, 4)), 0.25);
            int end = (int) getScaledPixels(Physics.getOrbitWithTemperature(star, k));
            if (end < 1) {
                continue;
            }
            String colour = zone.getColour() + "80"; // Add alpha mask to the colour.
            image.line(cx + start + 1, cy, cx + (end), cy, colour, w);
            image.line(cx - start - 1, cy, cx - (end), cy, colour, w);
            image.line(cx, cy + start + 1, cx, cy + (end), colour, w);
            image.line(cx, cy - start - 1, cx, cy - (end), colour, w);
            start = end + w;
            if (start > width) {
                break;
            }
        }

    }

    private void drawBelt(Star star, int cx, int cy, Planet planet, List<Planet> moons) {
        if (planet.getDistance() - planet.getRadius() > image.getWidth() * getScale() * 2) {
            // Don't bother to try and draw anything we can't see. Belts can take a long
            // time to draw when zoomed in, so skip them when possible.
            return;
        }
        switch (planet.getType()) {
            case AsteroidBelt:
                drawAsteroidBelt(star, cx, cy, planet, moons);
                break;
            case MetallicBelt:
                drawMetallicBelt(star, cx, cy, planet, moons);
                break;
            case VulcanianBelt:
                drawVulcanianBelt(star, cx, cy, planet, moons);
                break;
            case OortCloud:
                drawOortCloud(star, cx, cy, planet, moons);
                break;
            case IceBelt:
                drawIceBelt(star, cx, cy, planet, moons);
                break;
            case DustDisc:
                drawDustDisc(star, cx, cy, planet, moons);
                break;
            case PlanetesimalDisc:
                drawPlanetesimalDisc(star, cx, cy, planet, moons);
                break;
            case IceRing:
                drawIceRing(cx, cy, planet);
                break;
            default:
                // Invalid or unsupported type.
                break;
        }
    }

    /**
     * Draw a planetoid within a belt. A planetoid is treated as a moon, but belongs to a belt,
     * so rather than orbiting around, it orbits with the asteroid belt. It is basically a large
     * asteroid, but treated as an individual planet.
     *
     * @param star          Star to centre it around.
     * @param cx            X coordinate of centre of orbit (pixels).
     * @param cy            Y coordinate of centre of orbit (pixels).
     * @param belt          Belt this planetoid is part of.
     * @param planetoid     Planetoid to draw.
     */
    private void drawPlanetoid(Star star, int cx, int cy, Planet belt, Planet planetoid) {
        logger.info("drawPlanetoid");

        long km = belt.getDistance() + planetoid.getDistance();
        double angle = getAngleOffset(planetoid);

        int pxDistance = (int) getScaledPixels(km);
        int     x = cx + (int) (Math.cos(Math.toRadians(angle)) * pxDistance);
        int     y = cy + (int) (Math.sin(Math.toRadians(angle)) * pxDistance);

        int radius = 4;
        image.circle(x, y, radius, belt.getType().getColour());

        if (drawLabels && pxDistance > 40) {
            drawPlanetLabel(x, y, angle, planetoid);
        }
    }

    /**
     * Draws a belt of planetesimals, displaying them as multiple dots on the map in a
     * ring around their origin. Location of each dot is random, but seeded so the belt
     * should be drawn identically each time.
     *
     * @param star      Parent star.
     * @param cx        X Origin of star, in pixels.
     * @param cy        Y origin of star, in pixels.
     * @param planet    Planet to draw.
     */
    private void drawAsteroidBelt(Star star, int cx, int cy, Planet planet, List<Planet> moons) {
        PlanetMapper mapper = new AsteroidBeltMapper(planet);
        mapper.setTime(getTime());
        mapper.drawOrbit(image, star, cx, cy, getScale(), moons);
    }

    private void drawOortCloud(Star star, int cx, int cy, Planet planet, List<Planet> moons) {
        PlanetMapper mapper = new OortCloudMapper(planet);
        mapper.setTime(getTime());
        mapper.drawOrbit(image, star, cx, cy, getScale(), moons);
    }

    private void drawIceBelt(Star star, int cx, int cy, Planet planet, List<Planet> moons) {
        PlanetMapper mapper = new IceBeltMapper(planet);
        mapper.setTime(getTime());
        mapper.drawOrbit(image, star, cx, cy, getScale(), moons);
    }

    private void drawVulcanianBelt(Star star, int cx, int cy, Planet planet, List<Planet> moons) {
        PlanetMapper mapper = new VulcanianBeltMapper(planet);
        mapper.setTime(getTime());
        mapper.drawOrbit(image, star, cx, cy, getScale(), moons);
    }

    private void drawMetallicBelt(Star star, int cx, int cy, Planet planet, List<Planet> moons) {
        PlanetMapper mapper = new MetallicBeltMapper(planet);
        mapper.setTime(getTime());
        mapper.drawOrbit(image, star, cx, cy, getScale(), moons);
    }

    private void drawPlanetesimalDisc(Star star, int cx, int cy, Planet planet, List<Planet> moons) {
        PlanetMapper mapper = new PlanetesimalDiscMapper(planet);
        mapper.setTime(getTime());
        mapper.drawOrbit(image, star, cx, cy, getScale(), moons);
    }

    private void drawDustDisc(Star star, int cx, int cy, Planet planet, List<Planet> moons) {
        PlanetMapper mapper = new DustDiscMapper(planet);
        mapper.setTime(getTime());
        mapper.drawOrbit(image, star, cx, cy, getScale(), moons);
    }

    private void drawIceRing(int cx, int cy, Planet planet) {
        PlanetMapper mapper = new IceRingMapper(planet);
        mapper.setTime(getTime());
        if (highlightedPlanet > 0 && highlightedPlanet != planet.getId()) {
            mapper.setFaded(true);
        }
        mapper.drawOrbit(image, null, cx, cy, getScale(), null);
    }

    /**
     * Draw the planet, with its orbit and any labels.
     *
     * @param star          Star to centre it around.
     * @param cx            X coordinate of centre of orbit (pixels).
     * @param cy            Y coordinate of centre of orbit (pixels).
     * @param planet        Planet to draw.
     * @param distance      Distance from star (pixels).
     */
    private void drawPlanet(Star star, int cx, int cy, Planet planet, int distance) {
        logger.info("drawPlanet");

        double angle = getAngleOffset(planet);
        int     x = cx + (int) (Math.cos(Math.toRadians(angle)) * distance);
        int     y = cy + (int) (Math.sin(Math.toRadians(angle)) * distance);

        image.circleOutline(cx, cy, distance, "#777777");
        if (drawShadows) {
            long radius = Physics.getJumpMask(planet.getRadius(), planet.getMass());
            if (shadowX == 0 && shadowY == 0) {
                image.circle(x, y, (int) (radius / scale), "#00000040");
            } else {
                image.roundedLine(x, y, x - (int) (shadowX / scale), y - (int) (shadowY / scale),
                        "#00000040", (radius * 2) / scale);
            }
        } else {
            image.circle(x, y, planet.getType().getGroup().getSize(), planet.getType().getColour());
        }

        if (drawLabels && distance > 40) {
            drawPlanetLabel(x, y, angle, planet);
        }
    }

    /**
     * Draws labels for a planet. For a planet, this is the name, type and distance from the
     * primary star. For planetoids within an asteroid belt, we just show the name. Since the
     * only time we display a Moon is when it's a planetoid, we can use this to determine
     * the planet's status.
     *
     * @param x         X pixels planet was drawn at.
     * @param y         Y pixels planet was drawn at.
     * @param angle     Angle around the star planet was drawn (in degrees).
     * @param planet    Planet to show information on.
     */
    private void drawPlanetLabel(int x, int y, double angle, Planet planet) {
        String text, dtext;

        if (planet.isMoon()) {
            // This is a planetoid inside an asteroid belt. We only display the name of
            // planetoid in this case.
            text = planet.getName().replaceAll(".* ", "");
            dtext = "";
        } else {
            text = planet.getName().replaceAll(".* ", "");
            text += " / " + planet.getType();

            long distance = planet.getDistance();

            if (distance > 1_000_000_000) {
                DecimalFormat format = new DecimalFormat("#,### AU");
                dtext = format.format(distance / 150_000_000);
            } else if (distance >= 10_000_000) {
                DecimalFormat format = new DecimalFormat("#,### Mkm");
                dtext = format.format(distance / 1_000_000);
            } else {
                DecimalFormat format = new DecimalFormat("#,### km");
                dtext = format.format(distance);
            }
        }

        int fontSize = 12;
        int textWidth = image.getTextWidth(text, 0, fontSize);
        int padding = 10;

        if (angle < 90) {
            // Bottom right.
            image.text(x + padding, y + padding, text, 0, fontSize, "#000000");
            image.text(x + padding, y + (int)(padding * 2.5), dtext, 0, fontSize, "#000000");
        } else if (angle < 180) {
            // Bottom left.
            image.text(x - padding - textWidth, y + padding, text, 0, fontSize, "#000000");
            image.text(x - padding - textWidth, y + (int)(padding * 2.5), dtext, 0, fontSize, "#000000");
        } else if (angle < 270) {
            // Top left.
            image.text(x - padding - textWidth, y - padding, text, 0, fontSize, "#000000");
            image.text(x - padding - textWidth, y + (int)(padding * 0.5), dtext, 0, fontSize, "#000000");
        } else {
            // Top right.
            image.text(x + padding, y - padding, text, 0, fontSize, "#000000");
            image.text(x + padding, y + (int)(padding * 0.5), dtext, 0, fontSize, "#000000");
        }

    }

    private void drawStar(Star star, int cx, int cy) {
        // Draw regions.

        // If this is a real star, then draw it. If the Id is negative, then this is a 'centre of gravity',
        // and not an actual star. Don't draw the star, but do draw the planets around the COG.
        if (star.getId() > 0) {
            if (drawShadows) {
                int starRadius = (int) (Physics.getJumpMask(star.getRadius(), star.getMass() * Physics.SOL_MASS) / scale);

                if (shadowX == 0 && shadowY == 0) {
                    image.circle(cx, cy, starRadius, "#00000040");
                } else {
                    image.roundedLine(cx, cy, cx - (int) (shadowX / scale), cy - (int) (shadowY / scale),
                            "#00000040", starRadius * 2);
                }
            }
            int starRadius = (int) (star.getRadius() / scale);
            starRadius = Math.max(5, starRadius);

            image.circle(cx, cy, starRadius, star.getSpectralType().getRGBColour());
            image.circleOutline(cx, cy, starRadius, "#000000");
        }

        for (Planet planet : planets) {
            if ((planet.getParentId() != star.getId() || planet.isMoon())) {
                continue;
            }

            int distance = (int) getScaledPixels(planet.getDistance());
            logger.info(String.format("Planet [%s] at [%d] KM", planet.getName(), distance));

            switch (planet.getType().getGroup()) {
                case Belt:
                    drawBelt(star, cx, cy, planet, planets.stream().filter(p -> p.getMoonOf() == planet.getId()).collect(Collectors.toList()));
                    break;
                default:
                    drawPlanet(star, cx, cy, planet, distance);
                    break;
            }
        }
    }
}
