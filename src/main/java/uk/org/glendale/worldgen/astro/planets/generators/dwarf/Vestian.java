/*
 * Copyright (c) 2020, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */

package uk.org.glendale.worldgen.astro.planets.generators.dwarf;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.org.glendale.utils.rpg.Die;
import uk.org.glendale.worldgen.WorldGen;
import uk.org.glendale.worldgen.astro.planets.MoonFeature;
import uk.org.glendale.worldgen.astro.planets.Planet;
import uk.org.glendale.worldgen.astro.planets.PlanetFeature;
import uk.org.glendale.worldgen.astro.planets.codes.*;
import uk.org.glendale.worldgen.astro.planets.generators.Dwarf;
import uk.org.glendale.worldgen.astro.planets.maps.dwarf.MesoAreanMapper;
import uk.org.glendale.worldgen.astro.stars.Star;
import uk.org.glendale.worldgen.astro.systems.StarSystem;
import uk.org.glendale.worldgen.web.Server;

import static uk.org.glendale.worldgen.astro.commodities.CommodityName.*;

/**
 * Vestian worlds are a subtype of Dwarf Terrestrial Lithic worlds. They are rocky silicate worlds
 * with moderate ices and other volatiles. They show signs of ancient geological activity, but nothing
 * recent. They are generally airless and dead.
 */
public class Vestian extends Dwarf {
    private static final Logger logger = LoggerFactory.getLogger(Vestian.class);

    public enum VestianFeature implements PlanetFeature {
        MagneticField,
        Reddish,
        Greenish,
        Flattened,
        Egg,
        Dumbbell,
        Irregular,
        Broken;
    }

    public Vestian(WorldGen worldgen, StarSystem system, Star star, Planet previous, long distance) {
        super(worldgen, system, star, previous, distance);
    }

    public Planet getPlanet(String name) {
        Planet planet = definePlanet(name, PlanetType.Vestian);
        int radius = 200 + Die.die(80, 2);

        planet.setRadius(radius);
        planet.setAtmosphere(Atmosphere.Vacuum);
        planet.setPressure(Pressure.None);
        planet.setMagneticField(MagneticField.None);

        setAutomaticFeatures(planet);

        switch (Die.d20()) {
            case 1:
                planet.addFeature(VestianFeature.Broken);
                break;
            case 2: case 3:
                planet.addFeature(VestianFeature.Irregular);
                break;
            case 4: case 5: case 6:
                planet.addFeature(VestianFeature.Dumbbell);
                break;
            case 7: case 8: case 9: case 10:
                planet.addFeature(VestianFeature.Egg);
                break;
            case 11: case 12: case 13: case 14: case 15:
                planet.addFeature(VestianFeature.Flattened);
                break;
        }

        switch (Die.d6(3)) {
            case 7:
                planet.addFeature(VestianFeature.MagneticField);
                planet.setMagneticField(MagneticField.Weak);
                break;
        }

        // Define resources for this world.
        addPrimaryResource(planet, SilicateOre);
        addSecondaryResource(planet, SilicateCrystals);
        addSecondaryResource(planet, FerricOre);
        addTraceResource(planet, HeavyMetals);
        addTertiaryResource(planet, Water);

        return planet;
    }
}
