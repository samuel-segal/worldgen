package uk.org.glendale.worldgen.astro.planets.generators.helian;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.org.glendale.utils.rpg.Die;
import uk.org.glendale.worldgen.WorldGen;
import uk.org.glendale.worldgen.astro.Physics;
import uk.org.glendale.worldgen.astro.planets.Planet;
import uk.org.glendale.worldgen.astro.planets.codes.*;
import uk.org.glendale.worldgen.astro.planets.generators.Helian;
import uk.org.glendale.worldgen.astro.stars.Star;
import uk.org.glendale.worldgen.astro.systems.StarSystem;

import static uk.org.glendale.worldgen.astro.Physics.HOUR;
import static uk.org.glendale.worldgen.astro.commodities.CommodityName.*;

/**
 * These are massive rocky worlds far from their star. They have an icy crust, and thick Helium atmospheres.
 */
public class Thean extends Helian {
    private static final Logger logger = LoggerFactory.getLogger(Thean.class);

    public Thean(WorldGen worldgen, StarSystem system, Star star, Planet previous, long distance) {
        super(worldgen, system, star, previous, distance);
    }

    public Planet getPlanet(String name, PlanetType type) {
        Planet planet =  definePlanet(name, type);
        planet.setRadius(9_000 + Die.die(2_200, 2));

        planet.setAtmosphere(Atmosphere.InertGases);
        int sizeMod = (int) planet.getRadius() / 1000 - 8;
        planet.setPressure(Physics.STANDARD_PRESSURE / 2 * Die.d6(sizeMod) + Die.die(Physics.STANDARD_PRESSURE));

        switch (Die.d6(2) + sizeMod) {
            case 2: case 3: case 4: case 5: case 6:
                planet.setMagneticField(MagneticField.VeryWeak);
                planet.addFeature(HelianFeature.LightVolcanic);
                break;
            case 7: case 8: case 9: case 10:
                planet.setMagneticField(MagneticField.Weak);
                planet.addFeature(HelianFeature.MediumVolcanic);
                break;
            default:
                planet.setMagneticField(MagneticField.Standard);
                planet.addFeature(HelianFeature.HeavyVolcanic);
                break;
        }

        planet.setDayLength(6 * HOUR + Die.die(6, 1 + sizeMod) * 3 * HOUR + Die.die(12 * HOUR));

        addPrimaryResource(planet, Water);
        addSecondaryResource(planet, SilicateOre);
        addTertiaryResource(planet, ExoticCrystals);
        addTertiaryResource(planet, Hydrogen);

        return planet;
    }
}
