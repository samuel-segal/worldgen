/*
 * Copyright (c) 2018, Samuel Penn (sam@glendale.org.uk).
 * See the file LICENSE at the root of the project.
 */

package uk.org.glendale.worldgen.astro.planets.maps.dwarf;

import uk.org.glendale.utils.graphics.SimpleImage;
import uk.org.glendale.utils.graphics.Tile;
import uk.org.glendale.worldgen.astro.planets.Planet;
import uk.org.glendale.worldgen.astro.planets.codes.PlanetType;
import uk.org.glendale.worldgen.astro.planets.generators.dwarf.Gelidian;
import uk.org.glendale.worldgen.astro.planets.generators.jovian.Saturnian;
import uk.org.glendale.worldgen.astro.planets.maps.DwarfMapper;
import uk.org.glendale.worldgen.astro.planets.maps.jovian.SaturnianMapper;
import uk.org.glendale.worldgen.astro.planets.tiles.Icy;
import uk.org.glendale.worldgen.astro.planets.tiles.Rough;
import uk.org.glendale.worldgen.astro.planets.tiles.Snow;
import uk.org.glendale.worldgen.text.TextGenerator;
import uk.org.glendale.worldgen.web.Server;

import java.io.File;
import java.io.IOException;

/**
 * Gelidian worlds are icy and dormant.
 */
public class GelidianMapper extends DwarfMapper {
    protected static final Tile ICE_PLAINS = new Tile("Ice Plains", "#D2D0CB", false, 2);
    protected static final Tile HIGHLANDS = new Tile("Highlands", "#E0E0E0", false, 2);

    public GelidianMapper(final Planet planet, final int size) {
        super(planet, size);
    }

    public GelidianMapper(final Planet planet) {
        super(planet, DEFAULT_FACE_SIZE);
    }

    public void generate() {

        generateHeightMap(24, DEFAULT_FACE_SIZE);
        int seaLevel = getSeaLevel(5);

        // Basic barren landscape.
        for (int tileY=0; tileY < getNumRows(); tileY++) {
            for (int tileX=0; tileX < getWidthAtY(tileY); tileX++) {
                int h = getHeight(tileX, tileY);
                if (h > 70) {
                    setTile(tileX, tileY, HIGHLANDS.getShaded(80 + (h / 5)));
                } else {
                    setTile(tileX, tileY, new Snow(ICE_PLAINS.getShaded(70 + (h / 3))));
                }
            }
        }

        cleanBumpMap();
        createCraters(2, 5);
        createCraters(1, 20);
        createCraters(-1, 50);

        // Basic barren landscape.
        for (int tileY=0; tileY < getNumRows(); tileY++) {
            for (int tileX=0; tileX < getWidthAtY(tileY); tileX++) {
                int h = getHeight(tileX, tileY);
                if (h < 15) {
                    setTile(tileX, tileY, new Rough(ICE_PLAINS.getShaded(70 + (h / 3))));
                    setHeight(tileX, tileY, 10);
                } else if (h < 70) {
                    setHeight(tileX, tileY, 50);
                } else {
                    setHeight(tileX, tileY, 90);
                }
            }
        }
    }

    public static void main(String[] args) throws IOException {
        Planet planet = new Planet();
        planet.setName("Wildeman");
        planet.setType(PlanetType.Gelidian);
        planet.setRadius(1600);

        testOutput(planet, new Gelidian(Server.getWorldGen(), null, null, null, 0),
                new GelidianMapper(planet));

    }
}
