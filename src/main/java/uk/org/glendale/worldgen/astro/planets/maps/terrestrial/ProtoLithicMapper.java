/**
 * ProtoLithicMapper.java
 *
 * Copyright (C) 2019 Samuel Penn, sam@glendale.org.uk
 * See the file LICENSE at the root of the project.
 */
package uk.org.glendale.worldgen.astro.planets.maps.terrestrial;

import uk.org.glendale.utils.graphics.Icosahedron;
import uk.org.glendale.utils.graphics.SimpleImage;
import uk.org.glendale.utils.graphics.Tile;
import uk.org.glendale.utils.rpg.Die;
import uk.org.glendale.worldgen.astro.planets.Planet;
import uk.org.glendale.worldgen.astro.planets.codes.Atmosphere;
import uk.org.glendale.worldgen.astro.planets.codes.PlanetType;
import uk.org.glendale.worldgen.astro.planets.maps.TerrestrialMapper;
import uk.org.glendale.worldgen.astro.planets.tiles.Lava;
import uk.org.glendale.worldgen.astro.planets.tiles.Rough;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static uk.org.glendale.worldgen.astro.planets.generators.Terrestrial.TerrestrialFeature.*;

/**
 * Defines a surface map for a ProtoLithic class world. These are extremely hot, and may be
 * molten or partially molten.
 */
public class ProtoLithicMapper extends TerrestrialMapper {

    protected static final Tile DARK_SURFACE = new Tile("Dark", "#371C07", false, 3);
    protected static final Tile MID_SURFACE = new Tile("Mid", "#502B11", false, 3);
    protected static final Tile LIGHT_SURFACE = new Tile("Light", "#763D10", false, 3);

    protected static final Tile MOLTEN_IRON = new Tile("Iron", "#FE1807", true, 2);
    protected static final Tile MOLTEN_LEAD = new Tile("Lead", "#707070", true, 6);

    protected static final Tile SCORCHED = new Tile("Scorched", "#201010", false, 3);
    protected static final Tile HOT = new Tile("Hot", "#FE1807", false, 3);

    protected static final Tile RIFT = new Tile("Rift", "#2B1807", false, 1);


    public ProtoLithicMapper(final Planet planet, final int size) {
        super(planet, size);
    }

    public ProtoLithicMapper(final Planet planet) {
        super(planet, DEFAULT_FACE_SIZE);
    }

    private Tile getRandomColour() {
        if (planet.hasFeature(MoltenSurface)) {
            return HOT;
        } else if (planet.hasFeature(MoltenMetals)) {
            switch (Die.d6()) {
                case 1:
                    return ProtoLithicMapper.MID_SURFACE;
                default:
                    return ProtoLithicMapper.DARK_SURFACE;
            }
        } else {
            switch (Die.d6()) {
                case 1:
                    return ProtoLithicMapper.LIGHT_SURFACE;
                case 6:
                    return ProtoLithicMapper.DARK_SURFACE;
                default:
                    return ProtoLithicMapper.MID_SURFACE;
            }
        }
    }

    public void generate() {
        generateHeightMap(DEFAULT_FACE_SIZE, DEFAULT_FACE_SIZE);

        // Basic barren landscape.
        for (int y=0; y < getNumRows(); y++) {
            for (int x=0; x < getWidthAtY(y); x++) {
                Tile tile = getRandomColour();
                setTile(x, y, tile);
            }
        }

        // Expand light and dark areas.
        flood(DARK_SURFACE, 4);
        flood(LIGHT_SURFACE, 2);

        for (int y=0; y < getNumRows(); y++) {
            for (int x=0; x < getWidthAtY(y); x++) {
                int h = getHeight(x, y);

                if (planet.hasFeature(MoltenSurface)) {
                    setTile(x, y, new Rough(HOT.getShaded(100 - h)));
                } else if (planet.hasFeature(MoltenMetals)) {
                    if (h < 35) {
                        setTile(x, y, MOLTEN_IRON.getShaded(h * 4));
                        setHeight(x, y, 10);
                    } else if (h < 45) {
                        setTile(x, y, getTile(x, y).getShaded(h));
                    } else {
                        setTile(x, y, getTile(x, y).getShaded(h));
                    }
                } else if (planet.hasFeature(MetallicSea)) {
                    if (h < 15) {
                        setTile(x, y, MOLTEN_LEAD.getShaded(h * 4));
                        setHeight(x, y, 10);
                    } else if (h < 25) {
                        setTile(x, y, getTile(x, y).getShaded(h));
                    } else {
                        setTile(x, y, getTile(x, y).getShaded(h));
                    }
                } else {
                    int baseHeat = planet.getTemperature() / 20;
                    if (h < baseHeat) {
                        setTile(x, y, getTile(x, y).getMix(HOT, 1 + (baseHeat - h) / 15));
                    } else if (h < baseHeat * 1.5) {
                        setTile(x, y, SCORCHED.getShaded(h));
                    } else if (h < baseHeat * 2) {
                        setTile(x, y, SCORCHED.getShaded(h));
                    }
                }
            }
        }
        flood(MOLTEN_IRON, 2);
        flood(MOLTEN_LEAD, 4);

        int hotHeight = planet.getTemperature() / 20;
        for (int y=0; y < getNumRows(); y++) {
            for (int x=0; x < getWidthAtY(y); x++) {
                if (getTile(x, y).isWater()) {
                    setHeight(x, y, 10);
                    if (getTile(x, y) == MOLTEN_LEAD) {
                        setTile(x, y, new Rough(getTile(x, y)));
                    }
                } else {
                    if (getHeight(x, y) < hotHeight) {
                        setTile(x, y, new Lava(getTile(x, y)));
                    } else {
                        setTile(x, y, new Rough(getTile(x, y)));
                    }
                }
            }
        }

        cleanBumpMap();

        if (planet.getPressure() > 1_000) {
            hasCloudMap = true;
        }
    }

    public List<SimpleImage> drawClouds(int width) {
        List<SimpleImage>  clouds = new ArrayList<>();
        cloudHeight = 5;

        if (planet.getPressure() < 1_000) {
            // No significant atmosphere, so don't show anything.
            return clouds;
        }

        Icosahedron cloud = getCloudLayer();
        int modifier = (planet.getPressure() - 50_000) / 2_500;

        String cloudColour = "#4F4124";
        if (planet.getAtmosphere() == Atmosphere.Hydrogen) {
            cloudColour = "#704124";
        }

        for (int y=0; y < cloud.getNumRows(); y++) {
            for (int x=0; x < cloud.getWidthAtY(y); x++) {
                int h = cloud.getHeight(x, y);
                cloud.setHeight(x, y, h + modifier);
            }
        }

        clouds.add(Icosahedron.stretchImage(cloud.drawTransparency(cloudColour, width), width));

        return clouds;
    }

    public static void main(String[] args) throws IOException {
        Planet p = new Planet();
        p.setType(PlanetType.ProtoLithic);
        p.setTemperature(800);
        p.setNightTemperature(700);
        ProtoLithicMapper m = new ProtoLithicMapper(p, DEFAULT_FACE_SIZE);

        System.out.println("ProtoLithic:");
        m.generate();
        SimpleImage img = m.draw(2048);
        img.save(new File("/home/sam/tmp/ProtoLithic.jpg"));

    }
}
