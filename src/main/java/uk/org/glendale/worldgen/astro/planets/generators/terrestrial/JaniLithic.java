/*
 * Copyright (c) 2021, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */

package uk.org.glendale.worldgen.astro.planets.generators.terrestrial;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.org.glendale.utils.rpg.Die;
import uk.org.glendale.worldgen.WorldGen;
import uk.org.glendale.worldgen.astro.planets.GeneralFeature;
import uk.org.glendale.worldgen.astro.planets.Planet;
import uk.org.glendale.worldgen.astro.planets.codes.Atmosphere;
import uk.org.glendale.worldgen.astro.planets.codes.Life;
import uk.org.glendale.worldgen.astro.planets.codes.PlanetType;
import uk.org.glendale.worldgen.astro.planets.codes.TradeCode;
import uk.org.glendale.worldgen.astro.planets.generators.Terrestrial;
import uk.org.glendale.worldgen.astro.stars.Star;
import uk.org.glendale.worldgen.astro.systems.StarSystem;
import uk.org.glendale.worldgen.text.TextGenerator;

import static uk.org.glendale.worldgen.astro.commodities.CommodityName.FerricOre;
import static uk.org.glendale.worldgen.astro.commodities.CommodityName.SilicateOre;

/**
 * JaniLithic worlds are tidally locked to their parent star, existing at the inner edge of where
 * planets can form.
 */
public class JaniLithic extends Terrestrial {
    private static final Logger logger = LoggerFactory.getLogger(JaniLithic.class);

    public JaniLithic(WorldGen worldgen, StarSystem system, Star star, Planet previous, long distance) {
        super(worldgen, system, star, previous, distance);
    }

    public Planet getPlanet(String name) {
        Planet planet = definePlanet(name, PlanetType.JaniLithic);

        planet.setHabitability(4);
        planet.setHydrographics(0);
        planet.setAtmosphere(Atmosphere.CarbonDioxide);
        planet.setPressure(Die.die(30_000) + 1_000);
        planet.setLife(Life.None);
        planet.setDayLength(planet.getPeriod());
        planet.setTemperature((int) (planet.getTemperature() * 1.4));
        planet.setNightTemperature(planet.getTemperature() / 2);

        addPrimaryResource(planet, SilicateOre);
        addSecondaryResource(planet, FerricOre);

        planet.addTradeCode(TradeCode.De, TradeCode.Ba);
        planet.addFeature(GeneralFeature.TideLocked);

        TextGenerator text = new TextGenerator(planet);
        planet.setDescription(text.getFullDescription());

        return planet;
    }

    private void addFeatures(Planet planet) {
    }
}
