/*
 * Copyright (c) 2017, Samuel Penn (sam@glendale.org.uk).
 * See the file LICENSE at the root of the project.
 */

package uk.org.glendale.worldgen.astro.systems.generators;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.org.glendale.utils.rpg.Die;
import uk.org.glendale.worldgen.WorldGen;
import uk.org.glendale.worldgen.astro.Physics;
import uk.org.glendale.worldgen.astro.planets.Planet;
import uk.org.glendale.worldgen.astro.planets.PlanetFactory;
import uk.org.glendale.worldgen.astro.planets.codes.PlanetType;
import uk.org.glendale.worldgen.astro.planets.generators.Dwarf;
import uk.org.glendale.worldgen.astro.planets.generators.belt.IceBelt;
import uk.org.glendale.worldgen.astro.sectors.Sector;
import uk.org.glendale.worldgen.astro.stars.*;
import uk.org.glendale.worldgen.astro.systems.StarSystem;
import uk.org.glendale.worldgen.astro.systems.StarSystemGenerator;
import uk.org.glendale.worldgen.astro.systems.StarSystemType;
import uk.org.glendale.worldgen.exceptions.DuplicateObjectException;

import static uk.org.glendale.worldgen.astro.systems.StarSystemFactory.getPlanetName;
import static uk.org.glendale.worldgen.astro.systems.StarSystemFactory.getBeltName;

import java.util.List;

/**
 * Creates a simple star system, primarily for testing boring and predictable systems.
 */
public class Simple extends StarSystemGenerator {
    private static final Logger logger = LoggerFactory.getLogger(Simple.class);

    public Simple(WorldGen worldgen) {
        super(worldgen);
    }

    public StarSystem generate(Sector sector, String name, int x, int y) throws DuplicateObjectException {
        StarSystem system = createEmptySystem(sector, name, x, y);

        switch (Die.d6()) {
            case 1:
                createSol(system);
                break;
            case 2:
                createRockyWorlds(system);
                break;
            case 3:
                createHotWorlds(system);
                break;
            default:
                createSingleStar(system);
                break;
        }
        updateStarSystem(system);

        return system;
    }

    /**
     * This is an empty system, only used as a marker. It has no stars and no planets.
     * @param system    System to be set.
     */
    public void createEmpty(StarSystem system) {
        system.setType(StarSystemType.EMPTY);
        setDescription(system, null);
    }


    /**
     * This constructs a star system very similar to that of Sol. It is designed for testing purposes
     * to see how traditional worlds look under the generation system. Worlds of specific types will
     * be created, with variations only within the type.
     *
     * @param system    Star system to populate.
     * @throws DuplicateStarException       Duplicate object was created.
     */
    @SuppressWarnings("WeakerAccess")
    public void createSol(StarSystem system) throws DuplicateStarException {
        system.setType(StarSystemType.SINGLE);
        StarGenerator starGenerator = new StarGenerator(worldgen, system, false);
        List<Planet> planets;

        // Generate a Sol-like star.
        Star primary;
        switch (Die.d6(1)) {
            case 1:
                // Hotter.
                primary = starGenerator.generatePrimary(Luminosity.V, SpectralType.G1);
                break;
            case 6:
                // Cooler.
                primary = starGenerator.generatePrimary(Luminosity.V, SpectralType.G3);
                break;
            default:
                primary = starGenerator.generatePrimary(Luminosity.V, SpectralType.G2);
                break;
        }

        PlanetFactory planetFactory = worldgen.getPlanetFactory();
        String  planetName;
        int     orbit = 1;

        // Mercury.
        long distance = 50 * Physics.MKM + Die.dieV(5_000_000);
        planetName = getPlanetName(primary, orbit++);
        planets = planetFactory.createPlanet(system, primary, planetName, PlanetType.Hermian, distance);
        system.addPlanets(planets);

        // Venus.
        distance = 110 * Physics.MKM + Die.dieV(5_000_000);
        planetName = getPlanetName(primary, orbit++);
        planets = planetFactory.createPlanet(system, primary, planetName, PlanetType.Cytherean, distance);
        system.addPlanets(planets);

        // Earth.
        distance = 150 * Physics.MKM + Die.dieV(5_000_000);
        planetName = getPlanetName(primary, orbit++);
        planets = planetFactory.createPlanet(system, primary, planetName, PlanetType.EoGaian, distance);
        system.addPlanets(planets);

        // Mars.
        distance = 230 * Physics.MKM + Die.dieV(5_000_000);
        planetName = getPlanetName(primary, orbit++);
        planets = planetFactory.createPlanet(system, primary, planetName, PlanetType.EuArean, distance);
        system.addPlanets(planets);

        // Asteroids.
        distance = (390 + Die.d10(2)) * Physics.MKM;
        planetName = getBeltName(primary, 1);
        planets = planetFactory.createPlanet(system, primary, planetName, PlanetType.AsteroidBelt, distance);
        system.addPlanets(planets);

        // Jupiter.
        distance = 750 * Physics.MKM + Die.dieV(30_000_000);
        planetName = getPlanetName(primary, orbit++);
        planets = planetFactory.createPlanet(system, primary, planetName, PlanetType.Jovic, distance);
        system.addPlanets(planets);

        // Saturn.
        distance = 1500 * Physics.MKM + Die.dieV(100_000_000);
        planetName = getPlanetName(primary, orbit++);
        planets = planetFactory.createPlanet(system, primary, planetName, PlanetType.Saturnian, distance);
        system.addPlanets(planets);

        // Uranus.
        distance = (19L + Die.d4()) * Physics.AU + Die.die(Physics.AU);
        planetName = getPlanetName(primary, orbit++);
        planets = planetFactory.createPlanet(system, primary, planetName, PlanetType.Neptunian, distance);
        system.addPlanets(planets);

        // Neptune.
        distance = (long)((28L + (long)Die.d6()) * Physics.AU) + Die.die(Physics.AU);
        planetName = getPlanetName(primary, orbit++);
        planets = planetFactory.createPlanet(system, primary, planetName, PlanetType.Neptunian, distance);
        system.addPlanets(planets);

        // Kuiper Belt
        distance = (38L + Die.d6()) * Physics.AU + Die.die(Physics.AU);
        planetName = getBeltName(primary, 2);
        planets = planetFactory.createPlanet(system, primary, planetName, PlanetType.IceBelt, distance,
                planets.get(planets.size() - 1), IceBelt.IceBeltFeature.KuiperBelt);
        system.addPlanets(planets);

        setDescription(system, null);
    }

    /**
     * Creates a small star system with small rocky worlds similar to the inner system of Sol.
     *
     * @param system    Star system to populate.
     * @throws DuplicateObjectException     Duplicate object was created.
     */
    @SuppressWarnings("WeakerAccess")
    public void createRockyWorlds(StarSystem system) throws DuplicateObjectException {
        system.setType(StarSystemType.SINGLE);
        StarGenerator starGenerator = new StarGenerator(worldgen, system, false);
        List<Planet> planets;

        // Generate a Sol-like star.
        Star primary = starGenerator.generatePrimary(Luminosity.V, SpectralType.G2.getSpectralType(Die.dieV(2)));

        PlanetFactory planetFactory = worldgen.getPlanetFactory();
        String  planetName;

        // Vulcan.
        long distance = 20 * Physics.MKM + Die.dieV(5_000_000);
        planetName = getPlanetName(primary, 1);
        planets = planetFactory.createPlanet(system, primary, planetName, PlanetType.Ferrinian, distance);
        system.addPlanets(planets);

        // Mercury.
        distance = 50 * Physics.MKM + Die.dieV(5_000_000);
        planetName = getPlanetName(primary, 2);
        planets = planetFactory.createPlanet(system, primary, planetName, PlanetType.Hermian, distance);
        system.addPlanets(planets);

        // Venus.
        distance = 110 * Physics.MKM + Die.dieV(5_000_000);
        planetName = getPlanetName(primary, 3);
        planets = planetFactory.createPlanet(system, primary, planetName, PlanetType.Cytherean, distance);
        system.addPlanets(planets);

        // Earth.
        distance = 150 * Physics.MKM + Die.dieV(5_000_000);
        planetName = getPlanetName(primary, 4);
        planets = planetFactory.createPlanet(system, primary, planetName, PlanetType.EoGaian, distance);
        system.addPlanets(planets);

        // Mars.
        distance = 230 * Physics.MKM + Die.dieV(5_000_000);
        planetName = getPlanetName(primary, 5);
        planets = planetFactory.createPlanet(system, primary, planetName, PlanetType.EoArean, distance);
        system.addPlanets(planets);

        setDescription(system, null);
    }

    /**
     * Creates a small star system a single EoGaian world.
     *
     * @param system    Star system to populate.
     * @throws DuplicateObjectException     Duplicate object was created.
     */
    @SuppressWarnings("WeakerAccess")
    public void createEoGaian(StarSystem system) throws DuplicateObjectException {
        system.setType(StarSystemType.SINGLE);
        StarGenerator starGenerator = new StarGenerator(worldgen, system, false);
        List<Planet> planets;

        // Generate a Sol-like star.
        Star primary = starGenerator.generatePrimary(Luminosity.V, SpectralType.G2);

        PlanetFactory planetFactory = worldgen.getPlanetFactory();
        String  planetName = getPlanetName(primary, 1);

        long distance = 150 * Physics.MKM + Die.dieV(5_000_000);
        planets = planetFactory.createPlanet(system, primary, planetName, PlanetType.EoGaian, distance);
        system.addPlanets(planets);

        setDescription(system, null);
    }

    /**
     * Creates a small star system a single EoGaian world.
     *
     * @param system    Star system to populate.
     * @throws DuplicateObjectException     Duplicate object was created.
     */
    @SuppressWarnings("WeakerAccess")
    public void createMesoGaian(StarSystem system) throws DuplicateObjectException {
        system.setType(StarSystemType.SINGLE);
        StarGenerator starGenerator = new StarGenerator(worldgen, system, false);
        List<Planet> planets;

        // Generate a Sol-like star.
        Star primary = starGenerator.generatePrimary(Luminosity.V, SpectralType.G2);

        PlanetFactory planetFactory = worldgen.getPlanetFactory();
        String  planetName = getPlanetName(primary, 1);

        long distance = 150 * Physics.MKM + Die.dieV(5_000_000);
        planets = planetFactory.createPlanet(system, primary, planetName, PlanetType.MesoGaian, distance);
        system.addPlanets(planets);

        setDescription(system, null);
    }

    /**
     * Creates a small star system a single EoGaian world.
     *
     * @param system    Star system to populate.
     * @throws DuplicateObjectException     Duplicate object was created.
     */
    @SuppressWarnings("WeakerAccess")
    public void createNecroGaian(StarSystem system) throws DuplicateObjectException {
        system.setType(StarSystemType.SINGLE);
        StarGenerator starGenerator = new StarGenerator(worldgen, system, false);
        List<Planet> planets;

        // Generate a Sol-like star.
        Star primary = starGenerator.generatePrimary(Luminosity.V, SpectralType.G2);

        PlanetFactory planetFactory = worldgen.getPlanetFactory();
        String  planetName = getPlanetName(primary, 1);

        long distance = 150 * Physics.MKM + Die.dieV(5_000_000);
        planets = planetFactory.createPlanet(system, primary, planetName, PlanetType.NecroGaian, distance);
        system.addPlanets(planets);

        setDescription(system, null);
    }

    /**
     * Creates a small star system with small rocky worlds similar to the inner system of Sol.
     *
     * @param system    Star system to populate.
     * @throws DuplicateObjectException     Duplicate object was created.
     */
    @SuppressWarnings("WeakerAccess")
    public void createHotWorlds(StarSystem system) throws DuplicateObjectException {
        system.setType(StarSystemType.SINGLE);
        StarGenerator starGenerator = new StarGenerator(worldgen, system, false);
        List<Planet> planets;

        // Generate a hot star.
        Star primary = starGenerator.generatePrimary(Luminosity.V, SpectralType.F5.getSpectralType(Die.dieV(3)));

        PlanetFactory planetFactory = worldgen.getPlanetFactory();
        String  planetName;

        // Vulcan.
        long distance = 20 * Physics.MKM + Die.dieV(5_000_000);
        planetName = getPlanetName(primary, 1);
        planets = planetFactory.createPlanet(system, primary, planetName, PlanetType.ProtoLithian, distance);
        system.addPlanets(planets);

        // Mercury.
        distance = 50 * Physics.MKM + Die.dieV(5_000_000);
        planetName = getPlanetName(primary, 2);
        planets = planetFactory.createPlanet(system, primary, planetName, PlanetType.Hermian, distance);
        system.addPlanets(planets);

        // Venus.
        distance = 110 * Physics.MKM + Die.dieV(5_000_000);
        planetName = getPlanetName(primary, 3);
        planets = planetFactory.createPlanet(system, primary, planetName, PlanetType.Cytherean, distance);
        system.addPlanets(planets);

        setDescription(system, null);
    }

    /**
     * Generates a simple predictable star system that is similar to Sol, with rocky planets making
     * up the inner worlds, an asteroid belt and some large Jovian worlds in the outer system. There
     * is more variety here than for the 'Sol' system.
     */
    @SuppressWarnings("WeakerAccess")
    public void createSingleStar(StarSystem system) throws DuplicateObjectException {
        system.setType(StarSystemType.SINGLE);

        PlanetFactory planetFactory = worldgen.getPlanetFactory();
        StarGenerator starGenerator = new StarGenerator(worldgen, system, false);
        List<Planet> planets;

        // Generate a Sol-like star.
        Luminosity l = Luminosity.V;
        SpectralType hr = SpectralType.G2.getSpectralType(Die.dieV(2));
        Star primary = starGenerator.generatePrimary(l, hr);

        int orbit = 1;
        int belts = 1;
        String planetName;

        switch (Die.d6(2)) {
            case 4:
                planetName = getBeltName(primary, belts++);
                planets = planetFactory.createPlanet(system, primary, planetName, PlanetType.VulcanianBelt,
                        (30 + Die.d12()) * Physics.MKM);
                system.addPlanets(planets);
                break;
            case 5:
                planetName = getPlanetName(primary, orbit++);
                planets = planetFactory.createPlanet(system, primary, planetName, PlanetType.Janian,
                        (40 + Die.d6(2)) * Physics.MKM);
                system.addPlanets(planets);
                break;
            case 6: case 7: case 8:
                planetName = getPlanetName(primary, orbit++);
                planets = planetFactory.createPlanet(system, primary, planetName, PlanetType.Hermian,
                        (40 + Die.d12(2)) * Physics.MKM);
                system.addPlanets(planets);
                break;
            case 9: case 10:
                planetName = getPlanetName(primary, orbit++);
                planets = planetFactory.createPlanet(system, primary, planetName, PlanetType.Ferrinian,
                        (40 + Die.d8(2)) * Physics.MKM);
                system.addPlanets(planets);
                break;
            default:
                // No planet.
        }

        if (Die.d3() > 1) {
            planetName = getPlanetName(primary, orbit++);
            planets = planetFactory.createPlanet(system, primary, planetName, PlanetType.Cytherean,
                    (70 + Die.d10(2) + orbit * 15) * Physics.MKM);
            system.addPlanets(planets);
        }

        planetName = getPlanetName(primary, orbit++);
        planets = planetFactory.createPlanet(system, primary, planetName, PlanetType.EoGaian,
                (140 + Die.d10(2)) * Physics.MKM);
        system.addPlanets(planets);

        switch (Die.d6()) {
            case 1: case 2:
                planetName = getPlanetName(primary, orbit++);
                planets = planetFactory.createPlanet(system, primary, planetName, PlanetType.MesoArean,
                        (230 + Die.d20(2)) * Physics.MKM);
                system.addPlanets(planets);
                break;
            case 3: case 4: case 5:
                planetName = getPlanetName(primary, orbit++);
                planets = planetFactory.createPlanet(system, primary, planetName, PlanetType.EuArean,
                        (230 + Die.d20(2)) * Physics.MKM);
                system.addPlanets(planets);
                break;
            default:
                // No planet.
        }

        switch (Die.d3()) {
            case 1:
                planetName = getBeltName(primary, belts++);
                planets = planetFactory.createPlanet(system, primary, planetName, PlanetType.AsteroidBelt,
                        (400 + Die.d20(5)) * Physics.MKM);
                system.addPlanets(planets);
                break;
            case 2:
                planetName = getPlanetName(primary, orbit++);
                planets = planetFactory.createPlanet(system, primary, planetName, PlanetType.Cerean,
                        (400 + Die.d20(5)) * Physics.MKM, Dwarf.DwarfFeature.Large);
                system.addPlanets(planets);
                break;
            default:
                // No planet.
                break;
        }

        planetName = getPlanetName(primary, orbit++);
        planets = planetFactory.createPlanet(system, primary, planetName, PlanetType.Jovic,
                (700 + Die.d100(1)) * Physics.MKM);
        system.addPlanets(planets);

        if (Die.d2() == 1) {
            planetName = getPlanetName(primary, orbit++);
            planets = planetFactory.createPlanet(system, primary, planetName, PlanetType.Saturnian,
                    (1400 + Die.d100(2)) * Physics.MKM);
            system.addPlanets(planets);
        }

        if (Die.d2() == 1) {
            planetName = getPlanetName(primary, orbit++);
            planets = planetFactory.createPlanet(system, primary, planetName, PlanetType.Neptunian,
                    (20 + Die.d6( 2)) * Physics.AU + Die.die(Physics.AU));
            system.addPlanets(planets);
        }

        // Kuiper Belt
        long distance = (40L + Die.d6()) * Physics.AU + Die.die(Physics.AU);
        planetName = getBeltName(primary, 2);
        planets = planetFactory.createPlanet(system, primary, planetName, PlanetType.IceBelt, distance,
                planets.get(planets.size() - 1), IceBelt.IceBeltFeature.KuiperBelt);
        system.addPlanets(planets);

        setDescription(system, null);
    }

    public void colonise(StarSystem system) {

    }
}
