/**
 * Copyright (c) 2007, Samuel Penn.
 * See the file LICENSE at the root of the project.
 */
package uk.org.glendale.worldgen.astro.planets.codes;

/**
 * Defines the evolutionary stage of life on a planet.
 */
public enum Life {
    None(3),
    Organic(3),
    Archaean(3),
    Aerobic(3),
    ComplexOcean(2),
    SimpleLand(1),
    ComplexLand(1),
    Extensive(1);

    private final int habitability;
    Life(int habitability) {
        this.habitability = habitability;
    }

    public int getHabitability() {
        return habitability;
    }

    /**
     * True iff this type of life is simpler than the one passed.
     */
    public boolean isSimplerThan(Life type) {
        return type.ordinal() > ordinal();
    }

    /**
     * True iff this type of life is more complex than the one passed.
     */
    public boolean isMoreComplexThan(Life type) {
        return type.ordinal() < ordinal();
    }
}
