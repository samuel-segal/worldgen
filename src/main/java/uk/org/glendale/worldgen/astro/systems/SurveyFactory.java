package uk.org.glendale.worldgen.astro.systems;

import org.hibernate.exception.ConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.org.glendale.worldgen.astro.Physics;
import uk.org.glendale.worldgen.astro.sectors.NoSuchSectorException;
import uk.org.glendale.worldgen.astro.sectors.Sector;
import uk.org.glendale.worldgen.astro.sectors.SectorFactory;
import uk.org.glendale.worldgen.astro.stars.Star;
import uk.org.glendale.worldgen.web.Server;
import uk.org.glendale.worldgen.web.api.UniverseAPI;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SurveyFactory {
    private static final Logger logger = LoggerFactory.getLogger(SurveyFactory.class);
    private final EntityManager session;

    private static final String SURVEY_QUERY = "FROM Survey WHERE systemId = :system";

    public SurveyFactory(EntityManager session) {
        this.session = session;
    }

    /**
     * Gets the survey data for the given system. Returns a new survey object with a value of zero
     * if none is found. For now, we always return the global survey data, without specifiying a
     * the ship identifier.
     *
     * @param systemId      Star system to obtain survey data on.
     * @return              Survey object
     */
    public Survey getSurvey(int systemId) {
        Query query = session.createQuery(SURVEY_QUERY);
        query.setParameter("system", systemId);

        try {
            Survey survey = (Survey) query.getSingleResult();

            return survey;
        } catch (NoResultException e) {
            return new Survey(systemId, 0);
        }
    }

    public void persist(Survey survey) {
        try {
            session.persist(survey);
            session.flush();
        } catch (ConstraintViolationException e) {
            //throw new DuplicateSurveyException(survey);
        }
    }

    public static int getDistance(Sector sector1, int x1, int y1, Sector sector2, int x2, int y2) {
        int dx = (x2 - x1);
        int dy = (y2 - y1);

        dx = Math.abs(dx);
        dy = dy + (dx / 2);

        if (x1 % 2 != 0 && x2 % 2 == 0) {
            dy++;
        }

        return Math.max(dx - dy, Math.max(dy, dx));
    }

    public int performSurvey(Sector oSector, int ox, int oy, int radius, int scan) {
        logger.info("performSurvey: Running a survey with radius [" + radius + "] and effect [" + scan + "]");

        int count = 0;
        StarSystemFactory systemFactory = Server.getWorldGen().getStarSystemFactory();
        SectorFactory sectorFactory = Server.getWorldGen().getSectorFactory();
        for (int y = oy - radius; y <= oy + radius; y++) {
            for (int x = ox - radius; x <= ox + radius; x++) {
                // Local x and y coordinates within the sector.
                int lx = x, ly = y;

                Sector sector = oSector;
                // Calculate coordinates outside the source sector.
                if (lx < 1 || lx > Sector.WIDTH || ly < 1 || ly > Sector.HEIGHT) {
                    int sectorX = oSector.getX(), sectorY = oSector.getY();
                    while (lx < 1) {
                        sectorX--;
                        lx += Sector.WIDTH;
                    }
                    while (lx > Sector.WIDTH) {
                        sectorX++;
                        lx -= Sector.WIDTH;
                    }
                    while (ly < 1) {
                        sectorY--;
                        ly += Sector.HEIGHT;
                    }
                    while (ly > Sector.HEIGHT) {
                        sectorY++;
                        ly -= Sector.HEIGHT;
                    }
                    try {
                        sector = sectorFactory.getSector(sectorX, sectorY);
                    } catch (NoSuchSectorException e) {
                        // If there isn't a sector, there's no stars here.
                        continue;
                    }
                }

                try {
                    StarSystem system = systemFactory.getStarSystem(sector, lx, ly);

                    int d = getDistance(oSector, ox, oy, oSector, x, y);
                    int lm = 0;
                    if (system.getStars().size() > 0) {
                        // Get a luminosity modifier based on the log of the star's power output.
                        double l = Physics.getSolarLuminosity(system.getStars().get(0));
                        lm = (int)(l * Math.sqrt(10));
                    }
                    if (d <= radius) {
                        count++;
                        double r = 3.0 + lm;
                        if (r < 1) {
                            r = -3 / (lm + 1);
                        }
                        int quality = (int)Math.round(scan - d / r);
                        logger.debug("Surveying world [" + system.getName() + "] SI [" + quality + "]");
                        if (quality > 0) {
                            int level = (int) Math.max(0, (Math.log(quality) / Math.log(2)) + 1);
                            Survey survey = getSurvey(system.getId());
                            if (level > survey.getScan()) {
                                survey.setScan(level);
                                persist(survey);
                            }
                        }
                    }
                } catch (NoSuchStarSystemException e) {
                    // Nothing to do.
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return count;
    }

    public Map<String,Object> getSystemSurvey(StarSystem system) {
        Survey survey = getSurvey(system.getId());

        Map<String,Object> data = new HashMap<>();

        if (survey.getScan() < 1) {
            // Nothing known about this system
            return data;
        }

        data.put("name", system.getName());
        data.put("x", system.getX());
        data.put("y", system.getY());

        if (survey.getScan() > 0) {
            data.put("type", system.getType().toString());
        }
        if (survey.getScan() > 1) {
            int s = 1;
            for (Star star : system.getStars()) {
                String text = star.getName();
                long au = (star.getDistance() + Physics.AU / 2) / Physics.AU;

                double sl = Physics.getSolarLuminosity(star);
                if (sl > 1 / Math.pow(10, survey.getScan())) {
                    Map<String, Object> starData = new HashMap<>();

                    switch (survey.getScan()) {
                        case 1:
                            au = ((au + 50) / 100) * 100;
                            break;
                        case 2:
                            au = ((au + 5) / 10) * 10;
                            break;
                        case 3:
                            au = ((au + 2) / 5) * 5;
                            break;
                    }
                    starData.put("au", au);

                    if (survey.getScan() >= 2) {
                        starData.put("luminosity", star.getLuminosity().toString());
                    }
                    if (survey.getScan() >= 3) {
                        starData.put("type", star.getSpectralType().toString());
                    }
                    if (survey.getScan() >= 4) {
                        // Provide estimate of star radius.
                        long km = star.getRadius() * 2;
                        long acc = 500_000 / (int) Math.pow(5, survey.getScan() - 4);
                        km = ((km + acc / 2) / acc) * acc;


                    }

                    data.put("star" + s++, starData);
                }


            }
        }

        return data;
    }




    public static void test(int x1, int y1, int x2, int y2, int exp) {
        int d= getDistance(null, x1, y1, null, x2, y2);

        String wrong = "";
        if (d != exp) {
            wrong = " != " + exp;
        }
        System.out.println("["+x1+","+y1+"] -> ["+x2+","+y2+"] = " + d + wrong);
    }

    public static void main(String[] args) {
        /*
        test(4,4, 2,2, 3);
        test(4,4, 1, 1, 5);
        test(5,5, 2,2, 4);
        test(5,5, 1,1, 6);
        test(5, 5, 7, 2, 4);
        test(5, 5, 8, 1, 5);
        test(5, 5, 8, 2, 4);
        test(4, 1, 6, 5, 5);
        test(8, 4, 1, 1, 7);
        test(8, 4, 15, 1, 7);
         */

        for (double i=0.0003; i < 1000; i *= 10) {
            System.out.println(i+": " + (int)Math.round(Math.log10(i)));
        }
    }

}
