package uk.org.glendale.worldgen.astro.planets.generators.helian;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.org.glendale.utils.rpg.Die;
import uk.org.glendale.worldgen.WorldGen;
import uk.org.glendale.worldgen.astro.Physics;
import uk.org.glendale.worldgen.astro.planets.*;
import uk.org.glendale.worldgen.astro.planets.codes.Atmosphere;
import uk.org.glendale.worldgen.astro.planets.codes.MagneticField;
import uk.org.glendale.worldgen.astro.planets.codes.PlanetType;
import uk.org.glendale.worldgen.astro.planets.generators.Helian;
import uk.org.glendale.worldgen.astro.stars.Star;
import uk.org.glendale.worldgen.astro.systems.StarSystem;
import uk.org.glendale.worldgen.astro.systems.StarSystemFactory;

import java.util.ArrayList;
import java.util.List;

import static uk.org.glendale.worldgen.astro.Physics.HOUR;
import static uk.org.glendale.worldgen.astro.commodities.CommodityName.*;

/**
 * These are massive carbon rich terrestrial worlds in tight solar orbits.
 */
public class Thetusean extends Helian {
    private static final Logger logger = LoggerFactory.getLogger(Thetusean.class);

    public Thetusean(WorldGen worldgen, StarSystem system, Star star, Planet previous, long distance) {
        super(worldgen, system, star, previous, distance);
    }

    public Planet getPlanet(String name, PlanetType type) {
        Planet planet =  definePlanet(name, type);
        planet.setRadius(9_000 + Die.die(2_500, 2));

        planet.setHabitability(6);
        planet.setAtmosphere(Atmosphere.CarbonDioxide);
        int sizeMod = (int) planet.getRadius() / 1000 - 8;
        planet.setPressure(Physics.STANDARD_PRESSURE * Die.d6(sizeMod) + Die.die(Physics.STANDARD_PRESSURE));
        int k = planet.getTemperature() + 100;
        planet.setTemperature(k);

        switch (Die.d6(2) + sizeMod) {
            case 2: case 3: case 4:
                planet.setMagneticField(MagneticField.Standard);
                planet.addFeature(HelianFeature.LightVolcanic);
                break;
            case 5: case 6: case 7: case 8:
                planet.setMagneticField(MagneticField.Strong);
                planet.addFeature(HelianFeature.MediumVolcanic);
                break;
            default:
                planet.setMagneticField(MagneticField.VeryStrong);
                planet.addFeature(HelianFeature.HeavyVolcanic);
                break;
        }

        planet.addFeature(GeneralFeature.TideLocked);
        planet.setDayLength(planet.getPeriod());

        addPrimaryResource(planet, CarbonicOre);
        addPrimaryResource(planet, CarbonicCrystals);
        addTertiaryResource(planet, FerricOre);
        addTertiaryResource(planet, ExoticCrystals);
        addTertiaryResource(planet, Helium);

        return planet;
    }

    public List<Planet> getMoons(Planet primary, PlanetFactory factory) {
        List<Planet> moons = new ArrayList<Planet>();

        int numberOfMoons = getNumberOfMoons() - 2;
        if (numberOfMoons < 1) {
            // Nothing to do.
            return moons;
        }

        logger.info(String.format("Creating %d moons for [%s]", numberOfMoons, primary.getName()));

        long distance = primary.getRadius() * (Die.d2(2));

        for (int m=0; m < numberOfMoons; m++) {
            String name = StarSystemFactory.getMoonName(primary.getName(), m + 1);

            logger.info("Adding moon " + name);

            List<PlanetFeature> features = new ArrayList<PlanetFeature>();
            features.add(MoonFeature.Moon);

            PlanetType type = PlanetType.Carbonaceous;

            Planet moon = factory.createMoon(system, star, name, type,
                    distance, primary,
                    features.toArray(new PlanetFeature[0]));

            moons.add(moon);
            distance += Die.die((int) primary.getRadius(), 3);
        }

        return moons;
    }

}
