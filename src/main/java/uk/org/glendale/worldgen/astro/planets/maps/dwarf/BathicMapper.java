/*
 * Copyright (c) 2018, Samuel Penn (sam@glendale.org.uk).
 * See the file LICENSE at the root of the project.
 */

package uk.org.glendale.worldgen.astro.planets.maps.dwarf;

import uk.org.glendale.utils.graphics.Tile;
import uk.org.glendale.worldgen.astro.planets.Planet;
import uk.org.glendale.worldgen.astro.planets.codes.PlanetType;
import uk.org.glendale.worldgen.astro.planets.codes.Temperature;
import uk.org.glendale.worldgen.astro.planets.generators.dwarf.Bathic;
import uk.org.glendale.worldgen.astro.planets.generators.dwarf.Gelidian;
import uk.org.glendale.worldgen.astro.planets.maps.DwarfMapper;
import uk.org.glendale.worldgen.astro.planets.tiles.Rough;
import uk.org.glendale.worldgen.astro.planets.tiles.Snow;
import uk.org.glendale.worldgen.web.Server;

import java.io.IOException;

/**
 * Bathic worlds are water covered.
 */
public class BathicMapper extends DwarfMapper {
    protected static final Tile WATER = new Tile("Water", "#9090F0", true, 2);
    protected static final Tile ICE = new Tile("Ice", "#FEFEFE", true, 2);

    public BathicMapper(final Planet planet, final int size) {
        super(planet, size);
    }

    public BathicMapper(final Planet planet) {
        super(planet, DEFAULT_FACE_SIZE);
    }

    public void generate() {

        generateHeightMap(24, DEFAULT_FACE_SIZE);

        // Sea scape, with possible ice.
        for (int tileY=0; tileY < getNumRows(); tileY++) {
            for (int tileX=0; tileX < getWidthAtY(tileY); tileX++) {
                int h = getHeight(tileX, tileY);
                int k = getTemperature(tileX, tileY);
                if (k < Temperature.WaterFreezes.getKelvin()) {
                    setTile(tileX, tileY, ICE.getShaded(80 + h / 5));
                    setHeight(tileX, tileY, 50 + h/2);
                } else {
                    h = 80 + h / 5;
                    setTile(tileX, tileY, WATER.getShaded(h));
                    setHeight(tileX, tileY, 0);
                }
            }
        }

        cleanBumpMap();

        hasHeightMap = true;
        hasCloudMap = true;

    }

    public static void main(String[] args) throws IOException {
        Planet planet = new Planet();
        planet.setName("Bathic");
        planet.setType(PlanetType.Bathic);
        planet.setRadius(2600);
        planet.setTemperature(275);

        testOutput(planet, new Bathic(Server.getWorldGen(), null, null, null, 0),
                new BathicMapper(planet));

    }
}
