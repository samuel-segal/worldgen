/*
 * Copyright (c) 2020, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */
package uk.org.glendale.worldgen.astro.planets.tiles;

import uk.org.glendale.utils.graphics.SimpleImage;
import uk.org.glendale.utils.graphics.Tile;
import uk.org.glendale.utils.rpg.Die;

/**
 * Display the amount of wetness of the tile.
 */
public class Wetness extends Tile {

    public Wetness(Tile tile) {
        super(tile);
    }

    public Wetness(Tile tile, int height) {
        super(tile);
        setRGB("#F0F0F0");
        setRGB(getShade(getRGB(), height));
    }

    public void addDetail(SimpleImage image, int x, int y, int w, int h) {
        int height = Math.abs(h);
        for (int yy = 0; yy < height; yy++) {
            int width = (int) ((1.0 * w * (height - yy)) / (1.0 * height));
            for (int xx = -width; xx < width; xx++) {
                if (Die.d100() <= getWetness()) {
                    image.dot(x + xx + w, y + (int)(Math.signum(h) * yy), "#0000FF");
                }
            }
        }
    }
}
