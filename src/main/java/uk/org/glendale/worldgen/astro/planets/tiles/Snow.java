/*
 * Copyright (c) 2018, Samuel Penn (sam@glendale.org.uk).
 * See the file LICENSE at the root of the project.
 */
package uk.org.glendale.worldgen.astro.planets.tiles;

import uk.org.glendale.utils.graphics.SimpleImage;
import uk.org.glendale.utils.graphics.Tile;
import uk.org.glendale.utils.rpg.Die;

/**
 * Based on the Rough texture, there is a good possibility of there being ice/snow present on the
 * surface, as well as normal shading variation. Initially added for Gelidaceous asteroids.
 */
public class Snow extends Tile {

    public Snow(Tile tile) {
        super(tile.getName(), tile.getRGB(), tile.isWater(), tile.getRandom());
    }

    public void addDetail(SimpleImage image, int x, int y, int w, int h) {
        String dark = getShiftedColour(0.9);
        String light = getShiftedColour(1.1);
        String lightSnow = "#F0F0F0";
        String darkSnow = "#A0A0A0";

        int height = Math.abs(h);
        for (int yy = 0; yy < height; yy++) {
            int width = (int) ((1.0 * w * (height - yy)) / (1.0 * height));
            for (int xx = -width; xx < width; xx++) {
                String colour = getRGB();
                switch (Die.d10()) {
                    case 1: case 2:
                        // No change.
                        break;
                    case 3: case 4:
                        colour = light;
                        break;
                    case 5: case 6:
                        colour = dark;
                        break;
                    case 7: case 8:
                        colour = lightSnow;
                        break;
                    case 9: case 10:
                        colour = darkSnow;
                        break;
                }
                image.dot(x + xx + w, y + (int)(Math.signum(h) * yy), colour);
            }
        }
    }
}
