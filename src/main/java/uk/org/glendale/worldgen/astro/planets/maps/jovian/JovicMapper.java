/**
 * JovicMapper.java
 *
 * Copyright (C) 2017 Samuel Penn, sam@glendale.org.uk
 * See the file LICENSE at the root of the project.
 */
package uk.org.glendale.worldgen.astro.planets.maps.jovian;

import uk.org.glendale.utils.graphics.SimpleImage;
import uk.org.glendale.utils.graphics.Tile;
import uk.org.glendale.utils.rpg.Die;
import uk.org.glendale.worldgen.astro.planets.Planet;
import uk.org.glendale.worldgen.astro.planets.codes.Life;
import uk.org.glendale.worldgen.astro.planets.codes.PlanetType;
import uk.org.glendale.worldgen.astro.planets.generators.Jovian;
import uk.org.glendale.worldgen.astro.planets.generators.dwarf.Selenian;
import uk.org.glendale.worldgen.astro.planets.generators.jovian.Jovic;
import uk.org.glendale.worldgen.astro.planets.maps.JovianMapper;
import uk.org.glendale.worldgen.astro.planets.maps.PlanetMapper;
import uk.org.glendale.worldgen.astro.planets.maps.dwarf.SelenianMapper;
import uk.org.glendale.worldgen.text.TextGenerator;
import uk.org.glendale.worldgen.web.Server;

import java.awt.*;
import java.io.File;
import java.io.IOException;

/**
 * A type of large Jovian world, similar to Jupiter. Tends to have brownish/reddish bands,
 * consisting of many colours. Storms tend to be quite active.
 */
public class JovicMapper extends JovianMapper {

    protected static final Tile CREAM = new Tile("Cream", "#DDDDAA", false, 6);
    protected static final Tile DARK_BROWN = new Tile("Dark Brown", "#B68E50", false, 4);
    protected static final Tile LIGHT_BROWN = new Tile("Light Brown", "#D3AD6E", false, 4);
    protected static final Tile YELLOW = new Tile("Yellow", "#CCCC99", false, 6);
    protected static final Tile RED = new Tile("Red", "#D08E50", false, 4);
    protected static final Tile SPOT = new Tile("Spot", "#D08E50", false, 4);

    protected static final Tile BLACK_BANDS = new Tile("Black Bands", "#101010", false, 4);
    protected static final Tile RED_BANDS = new Tile("Red Bands", "#F04040", false, 4);
    protected static final Tile GREEN_BANDS = new Tile("Green Bands", "#70D070", false, 4);
    protected static final Tile BLUE_BANDS = new Tile("Blue Bands", "#7070D0", false, 4);

    public JovicMapper(final Planet planet, final int size) {
        super(planet, size);
    }
    public JovicMapper(final Planet planet) {
        super(planet, DEFAULT_FACE_SIZE);
    }

    private Tile getRandomColour() {
        switch (Die.d6()) {
            case 1:
                return JovicMapper.CREAM;
            case 2:
                return JovicMapper.YELLOW;
            case 3: case 4:
                return JovicMapper.LIGHT_BROWN;
            case 5:
                return JovicMapper.DARK_BROWN;
            case 6:
                if (planet.hasFeature(Jovic.JovicFeature.BlackBands)) {
                    return JovicMapper.BLACK_BANDS;
                } else if (planet.hasFeature(Jovic.JovicFeature.RedBands)) {
                    return JovicMapper.RED_BANDS;
                } else if (planet.hasFeature(Jovic.JovicFeature.GreenBands)) {
                    return JovicMapper.GREEN_BANDS;
                } else if (planet.hasFeature(Jovic.JovicFeature.BlueBands)) {
                    return JovicMapper.BLUE_BANDS;
                } else {
                    return JovicMapper.RED;
                }
        }
        throw new IllegalStateException("getRandomColour: Invalid switch value.");
    }

    protected Tile getBandColour(Tile previousColour, Tile nextColour) {
        if (nextColour != null) {
            // We've already chosen the next colour, so return that.
            return nextColour;
        }
        if (previousColour == null) {
            return getRandomColour();
        }
        if (Die.d2() == 1) {
            return getRandomColour();
        } else {
            return previousColour.getVariant(Die.d8() - Die.d8());
        }
    }

    private void spot(int x, int y) {
        Tile tile = SPOT.getMix(getTile(x, y));

        // We need a tile that is facing in the right direction (point up).
        Point p = getUpDown(x, y);
        if (p.y < y) {
            x = getEast(x, y);
        }

        int shade = 50;
        int graduation = 1;
        int startX = x;
        int startY = y;
        int length = 5;
        int height = 1;
        int loops = 3;

        setTile(x, y, tile.getShaded(shade));
        for (int loop = 0; loop < loops; loop++) {
            // Start by going East.
            for (int l = 0; l <= length; l++) {
                x = getEast(x, y);
                setTile(x, y, tile.getShaded(shade));
                shade += graduation;
                System.out.println("E " + x + " " + y);
            }
            // Now drop down to below start level.
            for (int h = 0; h < height; h++) {
                System.out.println("D " + x + " " + y);
                p = getUpDown(x, y);
                x = p.x;
                y = p.y;
                setTile(x, y, tile.getShaded(shade));
                shade += graduation;

                if (h < height - 1) {
                    x = getEast(x, y);
                    setTile(x, y, tile.getShaded(shade));
                    shade += graduation;
                }
            }
            // Now drop to bottom line
            for (int h = 0; h < height - 1; h++) {
                x = getWest(x, y);
                setTile(x, y, tile.getShaded(shade));
                shade += graduation;
                p = getUpDown(x, y);
                x = p.x;
                y = p.y;
                setTile(x, y, tile.getShaded(shade));
                shade += graduation;
            }
            // Now go West.
            length += 2;
            for (int l = 0; l <= length; l++) {
                x = getWest(x, y);
                setTile(x, y, tile.getShaded(shade));
                shade += graduation;
            }
            // Now up to start line

            if (loop > 0) {
                break;
            }
            // Now go up.
            height += 1;
            for (int h = 0; h < height; h++) {
                p = getUpDown(x, y);
                x = p.x;
                y = p.y;
                setTile(x, y, tile.getShaded(shade));
                shade += graduation;

                x = getEast(x, y);
                setTile(x, y, tile.getShaded(shade));
                shade += graduation;
            }
            length -= 1;
            //shade += 25;
//            if (1 < 2) break;
        }


    }

    private static final int H = 52;
    private void swirl(int x, int y) {
        Tile tile = getTile(x, y);


        Point p = getUpDown(x, y);
        int px = (int)p.x, py = (int)p.y;
        setTile(px, py, tile.getMix(getTile(px, py), 1));
        setHeight(px, py, H);

        px = (py < y)?getEast(px, py):getWest(px, py);
        setTile(px, py, tile.getMix(getTile(px, py), 1));
        setHeight(px, py, H);

        p = getUpDown(px, py);
        px = (int)p.x;
        py = (int)p.y;
        setTile(px, py, tile.getMix(getTile(px, py), 1));
        setHeight(px, py, H);

        px = (py < y)?getEast(px, py):getWest(px, py);
        setTile(px, py, tile.getMix(getTile(px, py), 1));
        setHeight(px, py, H);

        px = (py < y)?getEast(px, py):getWest(px, py);
        setTile(px, py, tile.getMix(getTile(px, py), 1));
        setHeight(px, py, H);

        px = (py < y)?getEast(px, py):getWest(px, py);
        setTile(px, py, tile.getMix(getTile(px, py), 1));
        setHeight(px, py, H);

        px = (py < y)?getEast(px, py):getWest(px, py);
        setTile(px, py, tile.getMix(getTile(px, py), 1));
        setHeight(px, py, H);

        p = getUpDown(px, py);
        px = (int)p.x;
        py = (int)p.y;
        setTile(px, py, tile.getMix(getTile(px, py), 1));
        setHeight(px, py, H);

        px = (py > y)?getEast(px, py):getWest(px, py);
        setTile(px, py, tile.getMix(getTile(px, py), 2));
        setHeight(px, py, H);

        px = (py > y)?getEast(px, py):getWest(px, py);
        setTile(px, py, tile.getMix(getTile(px, py), 2));
        setHeight(px, py, H);
    }

    protected void cloudFormations() {

        // Add swirls
        for (int y=3; y < getNumRows() - 3; y++) {
            if (Die.d6() != 1) {
                continue;
            }
            for (int x=0; x < getWidthAtY(y); x++) {
                setHeight(x, y, H);
            }
            for (int x=0; x < getWidthAtY(y); x++) {
                if (Die.d12() == 1) {
                    swirl(x, y);
                    x += 8;
                    /*
                } else if (Die.d100() == 1 && x < getWidthAtY(y) - 12) {
                    spot(x, y);
                    x += 12;
                    break;
                    */
                }
            }
            y += 2;
        }

    }

    public static void main(String[] args) throws IOException {
        Planet       planet = new Planet();
        planet.setName("BigWorld");
        planet.setType(PlanetType.Jovic);
        planet.setTemperature(200);
        planet.setRadius(150000);
        planet.addFeature(Jovic.JovicFeature.RedBands);
        planet.addFeature(Jovic.JovicFeature.InternalHeat);
        testOutput(planet, new Jovic(Server.getWorldGen(), null, null, null, 0),
                new JovicMapper(planet));

    }
}
