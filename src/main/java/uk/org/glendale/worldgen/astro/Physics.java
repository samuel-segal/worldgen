/*
 * Copyright (c) 2018, Samuel Penn (sam@glendale.org.uk).
 * See the file LICENSE at the root of the project.
 */

package uk.org.glendale.worldgen.astro;

import uk.org.glendale.worldgen.Config;
import uk.org.glendale.worldgen.astro.stars.Luminosity;
import uk.org.glendale.worldgen.astro.stars.SpectralType;
import uk.org.glendale.worldgen.astro.stars.Star;

import java.math.BigDecimal;
import java.math.MathContext;
import java.text.DecimalFormat;

import static java.lang.Math.PI;
import static java.lang.Math.pow;

/**
 * Static helper class which provides common physics methods and constant values.
 */
public class Physics {
    // Radius of Sol, in kilometres.
    public static final long SOL_RADIUS = 695700;

    // Surface temperature of Sol, in Kelvin.
    public static final int  SOL_TEMPERATURE = 5780;

    // Mass of Sol, in KG.
    public static final double SOL_MASS = 2e30;

    // Luminosity of Sol. Energy output in Watts.
    public static final double SOL_LUMINOSITY = 3.828e26;

    public static final long STANDARD_DAY = 86400L;

    public static final long STANDARD_WEEK = 7 * STANDARD_DAY;

    // Length of a standard year, in seconds.
    public static final long STANDARD_YEAR = 365 * STANDARD_DAY;

    // Length of hour in seconds.
    public static final long HOUR = 3600L;

    // Mass of the Earth, in kg.
    public static final double EARTH_MASS = 5.972e24;

    // Surface gravity of Earth.
    public static final double EARTH_GRAVITY = 9.807;

    // Standard one atmosphere pressure, in Pascals.
    public static final int STANDARD_PRESSURE = 100_000;

    // Temperature of space at 1 AU from Sol, in Kelvin.
    public static final int STANDARD_TEMPERATURE = 278;

    // Distance of one Astronomical Unit, in kilometres.
    public static final long  AU = 150_000_000L;

    // Distance unit of millions of kilometres.
    public static final long MKM = 1_000_000L;

    public static final double G = 6.67408e-11;

    public static final double g = 9.8046;

    public static final long SNOW_DISTANCE = 400 * MKM;
    public static final long OUTER_DISTANCE = 250 * MKM;
    public static final long INNER_DISTANCE = 100 * MKM;
    public static final long MINIMUM_DISTANCE = 25 * MKM;

    // Sigma, used in Stefan's Law for calculating stellar luminosity.
    public static final double σ = 5.67e-8;

    // Can't be instantiated.
    private Physics() {

    }

    /**
     * Rounds a number to the given number of significant digits.
     *
     * @param number      Number to be rounded.
     * @param digits      Number of digits to round it to. Must be at least one.
     *
     * @return            The number after it has been rounded.
     */
    public static long round(long number, int digits) {
        int sign = (number < 0)?-1:1;
        number = Math.abs(number);

        if (digits < 1) {
            throw new IllegalArgumentException("Number of significant digits must be at least one.");
        }
        if (number > pow(10, digits)) {
            int  log = (int) Math.log10(number);
            digits = 1 + log - digits;

            number += pow(10, digits) / 2;
            number -= number % (long)pow(10, digits);
        }

        return sign * number;
    }

    public static long round(long number) {
        return round(number, 4);
    }

    public static long round(double number) {
        return round((long) number, 4);
    }

    /**
     * Round a double value to the given precision of significant digits.
     *
     * @param number    Number to be rounded.
     * @param digits    Number of significant digits to round to.
     * @return          The number after it has been rounded.
     */
    public static double round(double number, int digits) {
        BigDecimal bd = new BigDecimal(number);
        bd = bd.round(new MathContext(digits));
        return bd.doubleValue();
    }


    /**
     * Gets the energy output of a star in Watts. This scales to the surface area of the star, and the
     * fourth power of temperature. This is based on Stefan's Law:
     *
     *  L = σAT⁴
     *  where σ (Sigma) = 5.67 x 10⁻⁸Wm⁻²K⁻⁴
     *        A is surface area of the star (m²)
     *        T is surface temperature of the star (Kelvin)
     *
     * See https://en.wikipedia.org/wiki/Solar_luminosity
     *
     * @param star  Star to calculate the luminosity of.
     * @return      Luminosity in watts.
     */
    public static double getStellarLuminosity(Star star) {
        double area = 4.0 * PI * pow(star.getRadius() * 1_000L, 2);
        double temperature = star.getSpectralType().getSurfaceTemperature();

        return σ * area * pow(temperature, 4);
    }

    /**
     * Gets the energy output of a star, relative to Sol. One solar luminosity is equal
     * to 3.828 x 10^26 W.
     *
     * See https://en.wikipedia.org/wiki/Solar_luminosity
     *
     * @param star  Star to calculate luminosity for.
     * @return      Luminosity relative to Sol.
     */
    public static double getSolarLuminosity(Star star) {
        return getStellarLuminosity(star) / SOL_LUMINOSITY;
    }

    /**
     * Gets the black body temperature at a given orbital distance from a star. Regardless
     * of distance, the minimum temperature returned will always be 3K.
     *
     * @param star  Star the planet is orbiting.
     * @param km    Distance from the star in kilometres.
     * @return      Black body temperature in Kelvin.
     */
    public static int getTemperatureOfOrbit(final Star star, final long km) {
        double l = getStellarLuminosity(star);
        double b = 16.0 * σ * PI * pow(km * 1_000L, 2);

        return (int) Math.round(Math.max(3, pow(l / b, 0.25)));
    }

    /**
     * Gets the black body temperature at a given orbital distance from a star. Regardless
     * of distance, the minimum temperature returned will always be 3K.
     *
     * @param luminosity  Luminosity (relative to Sol) of star the planet is orbiting.
     * @param km          Distance from the centre of star in kilometres.
     * @return            Black body temperature in Kelvin.
     */
    public static int getTemperatureOfOrbit(final double luminosity, final long km) {
        double l = luminosity * Physics.SOL_LUMINOSITY;
        double b = 16.0 * σ * PI * pow(km * 1_000L, 2);

        return (int) Math.round(Math.max(3, pow(l / b, 0.25)));
    }

    /**
     * Gets the black body temperature at a given orbital distance from a star, including an
     * ambient temperature from one or more nearby stars.
     *
     * The ambient temperature is considered to be the temperature at this star's distance
     * from the second star. This should be the average for any planet in orbit.
     *
     * @param ambient   Ambient temperature, in Kelvin.
     * @param star      Parent star for this orbit.
     * @param km        Distance from parent in kilometres.
     * @return          Black body temperature in Kelvin.
     */
    public static int getTemperatureOfOrbit(final int ambient, final Star star, final long km) {
        double l = getStellarLuminosity(star);
        double b = 16.0 * σ * PI * pow(km * 1_000L, 2);

        return (int) Math.round(Math.max(3, pow(pow(ambient, 4) + l / b, 0.25)));
    }

    /**
     * Gets the temperature of a planet at a given orbit, with a given albedo. The planet's
     * albedo will range from 0.0 to 1.0. An albdeo of zero means that no light is reflected
     * from the planet, and it is a perfect black body. An albedo of 1.0 means that the planet
     * is reflecting all incoming light.
     *
     * Earth has an albedo of about 0.3. Mercury and the Moon have 0.12. Mars 0.16. Venus 0.75.
     * The gas giants are similar to Earth.
     *
     * @param ambient   Average ambient temperature in Kelvin.
     * @param star      Star the planet is orbiting.
     * @param km        Distance from the star in kilometres.
     * @param albedo    Albedo of the planet, from 0.0 to 1.0.
     * @return          Temperature in kelvin.
     */
    public static int getTemperatureOfOrbit(final int ambient, final Star star, final long km, final double albedo) {
        int kelvin = getTemperatureOfOrbit(ambient, star, km);

        return (int) Math.round(kelvin * pow(1.0 - albedo, 0.25));
    }

    /**
     * Combine one or more temperatures together into a single total. This does not perform a
     * simple addition, but uses a power scaling law.
     *
     * @param kelvin    One or more temperatures to combine together.
     * @return          Combined temperature total.
     */
    public static int combineAmbient(int... kelvin) {
        double total = 0;

        for (int k : kelvin) {
            total = pow(pow(total, 4) + pow(k, 4), 0.25);
        }

        return (int) total;
    }

    /**
     * Gets the distance (kilometres) from the star at which the black body temperature is equal
     * to the given value. Used for working out where the Circumstellar Habitable Zone is.
     *
     * @param star      Star that the planet is orbiting.
     * @param kelvin    Temperature (kelvin) we want.
     * @return          Distance in kilometres.
     */
    public static long getOrbitWithTemperature(Star star, int kelvin) {
        double l = getStellarLuminosity(star);

        double rhs = l / (16.0 * σ * PI * pow(kelvin, 4));

        return (long) (pow(rhs, 0.5) / 1_000L);
    }

    /**
     * Gets the distance (kilometres) from the star at which the black body temperature is equal
     * to the given (kelvin) value. Used for working out where the Circumstellar Habitable Zone is.
     *
     * @param luminosity    Luminosity of star, relative to Sol.
     * @param kelvin        Temperature (kelvin) we want.
     * @return              Distance in kilometres.
     */
    public static long getOrbitWithTemperature(double luminosity, int kelvin) {
        double l = luminosity * SOL_LUMINOSITY;

        double rhs = l / (16.0 * σ * PI * pow(kelvin, 4));

        return (long) (pow(rhs, 0.5) / 1_000L);
    }

    /**
     * Gets the multiplier for different temperature bands around this star
     * based on that of Sol. A star four times as luminous as Sol would have
     * range bands which are twice as far.
     *
     * Luminosity is considered to increase by the square of the radius and
     * linearly with surface temperature. See:
     *
     * https://en.wikipedia.org/wiki/Circumstellar_habitable_zone
     *
     * Maybe we can also use this:
     * https://en.wikipedia.org/wiki/Planetary_equilibrium_temperature
     *
     * @return  Constant to multiply temperature range bands by.
     */
    public static double getSolarConstant(Star star) {
        if (star == null) {
            return 0.0;
        }
        // Start with the basic surface temperature.
        double t = (1.0 * star.getSpectralType().getSurfaceTemperature()) / SpectralType.G2.getSurfaceTemperature();
        // Radius of the star.
        double r = (1.0 * star.getRadius()) / SOL_RADIUS;

        // Get power output relative to Sol.
        return Math.pow(r, 2.0) * Math.pow(t, 4);
    }

    /**
     * Gets the length of a solar day, given the period of orbit and sidereal period.
     * The sidereal period is the 'length of day' stored for a planet, and is the time
     * taken to rotate 360 degrees. The solar day is the time between noon on one day
     * and noon on the next.
     *
     * This assumes a planet rather than a moon.
     *
     * @param period        Orbital period, in seconds.
     * @param sidereal      Length of sidereal day, in seconds.
     * @return              Length of solar day, in seconds. Or zero for tidally locked.
     */
    public static long getSolarDay(long period, long sidereal) {
        if (period == sidereal) {
            // Planet is tidally locked, so days and nights are infinite in length.
            return 0;
        }

        return period * sidereal / (period - sidereal);
    }

    /**
     * Gets the orbital period of a pair of bodies in seconds, given their total mass and distance
     * from each other. For planets orbiting a star, the total mass can generally be assumed equal
     * to the solar mass, and the distance can be assumed to be the distance of the planet from
     * the centre of the star.
     *
     * @param kg    Total mass of the two bodies in kg.
     * @param m     Distance between the two bodies in metres.
     * @return      Orbital period in seconds.
     */
    public static long getOrbitalPeriod(double kg, long m) {
        double rhs = Math.pow(m, 3);
        rhs = rhs / ( G * kg);

        return (long) (2 * Math.PI * Math.pow(rhs, 0.5));
    }

    /**
     * Gets the boiling point of water at a given pressure. The equation for this is
     * taken from the following site, with values for water hardcoded in.
     *
     * http://physicstasks.eu/1702/boiling-point-of-water-at-high-pressure
     *
     * @param pressure  Pressure, in Pascals.
     * @return          Boiling point, in kelvin.
     */
    public static int getBoilingPoint(int pressure) {
        double p = (pressure - 101_325) * 1.249;
        p = p / 2.256E6;

        double k = 373.15 * Math.pow(Math.E, p);

        return (int) (k + 0.5);
    }

    // Acceleration due to gravity for 1m radius and 1°/second rotational velocity.
    private static final double A = 0.00030461741978670857;

    /**
     * Gets the artificial acceleration due to gravity from a spinning object. Takes the radius of the
     * object (in metres) and its period (in seconds).
     *
     * @param m     Radius of spinning object in metres.
     * @param s     Period of rotation in seconds.
     * @return      Artificial gravity in m/s² at the inside surface.
     */
    public static double getGravityOfSpin(final long m, final long s) {
        final double Ω = 360.0 / (1.0 * s); // Angular velocity in degrees/second.

        return A * m * Ω * Ω;
    }

    public static long getPeriodOfSpin(final long m, final double a) {

        return Math.round(360.0 / Math.sqrt( (1.0 * a) / (1.0 * A * m)));

    }

    /**
     * Gets the radius of the jump mask in kilometres. Will be based either on the diameter of the planet, or
     * its mass, depending on the configuration settings.
     *
     * @param radius    Radius of object, in kilometres.
     * @param mass      Mass of the object in kilogrammes.
     * @return          Radius of jump mask in kilometres.
     */
    public static long getJumpMask(final long radius, final double mass) {
        if (Config.getConfiguration().isGravityMask()) {
            return (long) Math.pow(Config.getConfiguration().getJumpGravityLimit() * mass, 1.0 / 3.0) / 1000;
        } else {
            return Config.getConfiguration().getJumpDiameterLimit() * radius * 2;
        }
    }

    public static void main(String[] args) {

        long s = getPeriodOfSpin(1000, 9.8);
        System.out.println(s);
        System.out.println(getGravityOfSpin(1000, s));
        System.exit(0);

        DecimalFormat format = new DecimalFormat();

        Star star1 = new Star("Alpha", null, 0, 0, Luminosity.V, SpectralType.K2);
        Star star2 = new Star("Beta", null, 0, 0, Luminosity.V, SpectralType.K9);
        star1.setStandardRadius().setStandardMass();
        star2.setStandardRadius().setStandardMass();

        System.out.println(star1.getRadius());
        System.out.println(star2.getRadius());

        double solarLuminosity = getSolarLuminosity(star1) + getSolarLuminosity(star2);

        for (int k = 100; k < 1_000; k += 25) {
            System.out.println(String.format("% 10d K : %s km", k , format.format(Physics.getOrbitWithTemperature(solarLuminosity, k))));
        }

        System.out.println(Physics.getOrbitWithTemperature(solarLuminosity, 273) / 150_000_000.0);
    }
}
