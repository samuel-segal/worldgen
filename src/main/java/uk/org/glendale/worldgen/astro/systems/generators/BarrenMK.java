/*
 * Copyright (c) 2021, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */

package uk.org.glendale.worldgen.astro.systems.generators;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.org.glendale.utils.rpg.Die;
import uk.org.glendale.utils.rpg.Roller;
import uk.org.glendale.worldgen.WorldGen;
import uk.org.glendale.worldgen.astro.Physics;
import uk.org.glendale.worldgen.astro.planets.Planet;
import uk.org.glendale.worldgen.astro.planets.PlanetFactory;
import uk.org.glendale.worldgen.astro.planets.codes.PlanetType;
import uk.org.glendale.worldgen.astro.planets.generators.belt.IceBelt;
import uk.org.glendale.worldgen.astro.sectors.Sector;
import uk.org.glendale.worldgen.astro.stars.*;
import uk.org.glendale.worldgen.astro.systems.*;
import uk.org.glendale.worldgen.civ.CivilisationGenerator;
import uk.org.glendale.worldgen.civ.civilisation.FreeSettlers;
import uk.org.glendale.worldgen.civ.civilisation.Hermits;
import uk.org.glendale.worldgen.exceptions.DuplicateObjectException;

import java.util.List;

/**
 * Generate a barren, type M or K, star system. These systems have no native life, the worlds are often
 * cold and uninhabitable. Quite simple systems.
 */
public class BarrenMK extends StarSystemGenerator {
    private static final Logger logger = LoggerFactory.getLogger(BarrenMK.class);

    public BarrenMK(WorldGen worldgen) {
        super(worldgen);
    }

    /**
     * Creates a barren star system. Such a system has one or more stars, with planets, but the
     * planets are barren with no civilisation or life of any kind. If the system has multiple stars,
     * the second star is much smaller and a long way away.
     *
     * @param sector    Sector to generate system in.
     * @param name      Name of the system.
     * @param x         X coordinate of the system in the sector (1-32).
     * @param y         Y coordinate of the system in the sector (1-40).
     *
     * @return          Created and persisted star system.
     * @throws DuplicateObjectException     If a duplicate system is created.
     */
    public StarSystem generate(Sector sector, String name, int x, int y) throws DuplicateObjectException {
        StarSystem system = createEmptySystem(sector, name, x, y);

        switch (Die.d6(2)) {
            case 2:
                createEmptySystem(system);
                break;
            case 3:
                createDustOnly(system);
                break;
            case 4:
                createAsteroidsOnly(system);
                break;
            case 5:
                createEpiStellarJovian(system);
                break;
            case 6:
                //createSmallDwarfPair(system);
                break;
            case 7:
                //createAsteroidBelt(system);
                break;
            case 8:
                createColdWorlds(system);
                break;
        }

        colonise(system);

        updateStarSystem(system);

        return system;
    }

    public void colonise(StarSystem system) {
        logger.info("Colonise " + system.getZone() + " star system " + system.getName());

        CivilisationGenerator generator = null;

        if (civName != null) {
            logger.info(String.format("Civilisation name set to be [%s]", civName));
            generator = getCivilisationByName(system);
        } else {
            if (system.getZone() == Zone.GREEN) {
                switch (Die.d6()) {
                    case 1: case 5: case 6:
                        generator = new Hermits(worldgen, system);
                        break;
                    case 2: case 3: case 4:
                        generator = new FreeSettlers(worldgen, system);
                        break;
                    default:
                        break;
                }
            }
        }
        if (generator != null) {
            generator.generate();
        }
    }

    /**
     * This system is empty, it only has a single red dwarf star and no planets.
     */
    public void createEmptySystem(StarSystem system) throws DuplicateObjectException {
        logger.info(String.format("Generating [BarrenMK] [EmptySystem] system [%s]", system.getName()));

        StarGenerator starGenerator = new StarGenerator(worldgen, system, false);
        // On average a cool main-sequence star will be generated.
        Star primary = starGenerator.generatePrimary(Luminosity.VI,
                SpectralType.M3.getSpectralType(Die.dieV(6)));
        system.addStar(primary);
        system.setType(StarSystemType.SINGLE);

        setDescription(system, null);
    }

    /**
     * This type of star system only has a dust belt and a type M sub-dwarf. No major planets have formed.
     *
     * @param system        Star System to create belt in.
     */
    @SuppressWarnings("WeakerAccess")
    public void createDustOnly(StarSystem system) throws DuplicateObjectException {
        logger.info(String.format("Generating [BarrenMK] [DustOnly] system [%s]", system.getName()));

        StarGenerator starGenerator = new StarGenerator(worldgen, system, false);
        // On average a cool main-sequence star will be generated.
        Star primary = starGenerator.generatePrimary(Luminosity.VI,
                SpectralType.M3.getSpectralType(Die.dieV(6)));
        system.addStar(primary);
        system.setType(StarSystemType.SINGLE);

        // Place the belt around 1AU from the star.
        long distance = 75_000_000 + Die.dieV(25_000_000);
        String name = StarSystemFactory.getBeltName(primary, 1);

        List<Planet> allPlanets;

        allPlanets = worldgen.getPlanetFactory().createPlanet(system, system.getStars().get(0),
                name, PlanetType.DustDisc, distance);

        system.addTradeCode(StarSystemCode.Ba);
        if (Die.d4() == 1) {
            system.addTradeCode(StarSystemCode.Sf);
            system.setZone(Zone.AMBER);
        }

        system.setPlanets(allPlanets);
        setDescription(system, null);
    }

    public void createAsteroidsOnly(StarSystem system) throws DuplicateObjectException {
        logger.info(String.format("Generating [BarrenMK] [AsteroidsOnly] system [%s]", system.getName()));

        StarGenerator starGenerator = new StarGenerator(worldgen, system, false);
        // On average a cool main-sequence star will be generated.
        Star primary = starGenerator.generatePrimary(Luminosity.VI,
                SpectralType.M1.getSpectralType(Die.dieV(6)));
        system.addStar(primary);
        system.setType(StarSystemType.SINGLE);

        // Place the belt around 1AU from the star.
        long distance = 60_000_000 + Die.dieV(25_000_000);
        String name = StarSystemFactory.getBeltName(primary, 1);

        List<Planet> allPlanets;

        allPlanets = worldgen.getPlanetFactory().createPlanet(system, system.getStars().get(0),
                name, PlanetType.AsteroidBelt, distance);

        system.addTradeCode(StarSystemCode.Ba);
        if (Die.d4() == 1) {
            system.addTradeCode(StarSystemCode.Sf);
            system.setZone(Zone.AMBER);
        }

        system.setPlanets(allPlanets);
        setDescription(system, null);
    }

    private PlanetType getDwarfTerrestrial(CircumstellarZone zone) {
        Roller<PlanetType> roller = new Roller<>();

        switch (zone) {
            case Exclusion:
                roller.add(PlanetType.Ferrinian);
                break;
            case Epistellar:
                roller.add(PlanetType.Janian, PlanetType.Ferrinian);
                break;
            case Hot:
                roller.add(PlanetType.Hermian);
                break;
            case Inner: case Middle:
                roller.add(PlanetType.AreanLacustric, PlanetType.Selenian);
                break;
            case Outer:
                roller.add(PlanetType.AreanLacustric, 2).add(PlanetType.EuArean, 3).
                        add(PlanetType.MesoArean, PlanetType.EoArean, PlanetType.Bathic);
                break;
            case Cold:
                roller.add(PlanetType.Cerean, PlanetType.Vestian, PlanetType.Gelidian);
                break;
            default:
                roller.add(PlanetType.Gelidian);
        }
        return roller.roll();
    }

    private PlanetType getTerrestrial(CircumstellarZone zone) {
        Roller<PlanetType> roller = new Roller<>();

        switch (zone) {
            case Exclusion:
                break;
            case Epistellar:
                roller.add(PlanetType.JaniLithic);
                break;
            default:
                roller.add(PlanetType.NecroGaian);
        }

        return roller.roll();
    }

    public void createEpiStellarJovian(StarSystem system) throws DuplicateObjectException {
        logger.info(String.format("Generating [BarrenMK] [EpiStellarJovian.properties] system [%s]", system.getName()));

        StarGenerator starGenerator = new StarGenerator(worldgen, system, false);
        // On average a cool main-sequence star will be generated.
        Star primary = starGenerator.generatePrimary(Luminosity.VI,
                SpectralType.M1.getSpectralType(Die.dieV(6)));
        system.addStar(primary);
        system.setType(StarSystemType.SINGLE);

        // Place the belt around 1AU from the star.
        long distance = 1_500_000 + Die.dieV(500_000);
        String name = StarSystemFactory.getPlanetName(primary, 1);

        List<Planet> allPlanets;
        if (Die.d3() == 1) {
            allPlanets = worldgen.getPlanetFactory().createPlanet(system, system.getStars().get(0),
                    name, PlanetType.Sokarian, distance);
        } else {
            allPlanets = worldgen.getPlanetFactory().createPlanet(system, system.getStars().get(0),
                    name, PlanetType.Osirian, distance);
        }

        system.addTradeCode(StarSystemCode.Ba);
        if (Die.d4() == 1) {
            system.addTradeCode(StarSystemCode.Sf);
            system.setZone(Zone.AMBER);
        }

        system.setPlanets(allPlanets);
        setDescription(system, null);
    }

    /**
     * A star system where the main world is a GeoHelian world.
     */
    public void createHelian(StarSystem system) throws DuplicateObjectException {
        logger.info(String.format("Generating [BarrenMK] [Helian] system [%s]", system.getName()));

        StarGenerator starGenerator = new StarGenerator(worldgen, system, false);
        // On average a cool main-sequence star will be generated.
        Star primary = starGenerator.generatePrimary(Luminosity.VI,
                SpectralType.M1.getSpectralType(Die.dieV(6)));
        system.addStar(primary);
        system.setType(StarSystemType.SINGLE);

        // Place the belt around 1AU from the star.
        long distance = 2_500_000 + Die.die(500_000);
        int  numPlanets = Die.d4() + 1;
        String name = StarSystemFactory.getPlanetName(primary, 1);

        List<Planet> planets;
        if (Die.d3() == 1) {
            planets = worldgen.getPlanetFactory().createPlanet(system, system.getStars().get(0),
                    name, PlanetType.Halcyonic, distance);
        } else {
            distance *= 3;
            planets = worldgen.getPlanetFactory().createPlanet(system, system.getStars().get(0),
                    name, PlanetType.Hyperionic, distance);
        }
        system.addPlanets(planets);

        for (int i = 2; i <= numPlanets; i++) {
            distance *= 1.2;
            distance += Die.die(2_000_000, 5);

            CircumstellarZone zone = CircumstellarZone.getZone(Physics.getTemperatureOfOrbit(primary, distance));
            PlanetType type = getDwarfTerrestrial(zone);

            name = StarSystemFactory.getPlanetName(primary, i);
            planets = worldgen.getPlanetFactory().createPlanet(system, system.getStars().get(0),
                name, type, distance);
            system.addPlanets(planets);
        }

        system.addTradeCode(StarSystemCode.Ba);
        if (Die.d2() == 1) {
            system.addTradeCode(StarSystemCode.Sf);
            system.setZone(Zone.AMBER);
        }

        setDescription(system, null);
    }
    /**
     * Create a system with a single red dwarf star, with some worlds in the outer system. Most
     * worlds are small Jovians or icy worlds.
     *
     * @param system    Star system to create planets in.
     */
    public void createColdWorlds(StarSystem system) throws DuplicateStarException {
        system.setType(StarSystemType.SINGLE);

        // Create a cool red dwarf star.
        StarGenerator starGenerator = new StarGenerator(worldgen, system, false);
        Star          primary = starGenerator.generatePrimary(Luminosity.VI, SpectralType.M9.getSpectralType(Die.d6()));
        system.addStar(primary);

        PlanetFactory factory = worldgen.getPlanetFactory();

        long distance = (2 + Die.d4(2)) * Physics.AU + Die.die(Physics.AU);
        String name = StarSystemFactory.getPlanetName(primary, 1);
        List<Planet> planets = null;

        if (Die.d2() == 1) {
            // A small, cold, jovian world.
            planets = factory.createPlanet(system, primary, name, PlanetType.Neptunian, distance);
        } else {
            // A huge, icy, super-Earth.
            planets = factory.createPlanet(system, primary, name, PlanetType.Thean, distance);
        }
        system.addPlanets(planets);

        distance *= 2;
        name = StarSystemFactory.getBeltName(primary, 2);
        planets = factory.createPlanet(system, primary, name, PlanetType.IceBelt, distance,
                planets.get(planets.size()-1), IceBelt.IceBeltFeature.KuiperBelt);
        system.addPlanets(planets);

        setDescription(system, null);
    }

    public void createProtoWorlds(StarSystem system) throws DuplicateStarException {
        system.setType(StarSystemType.SINGLE);

        // Create a cool red dwarf star.
        StarGenerator starGenerator = new StarGenerator(worldgen, system, false);
        Star          primary = starGenerator.generatePrimary(Luminosity.VI, SpectralType.M2.getSpectralType(Die.d6()));
        system.addStar(primary);

        PlanetFactory factory = worldgen.getPlanetFactory();

        long    distance = 5_000_000 + Die.die(5_000_000, 2);
        String  name;
        int     orbit = 1;

        name = StarSystemFactory.getPlanetName(primary, orbit++);
        system.addPlanets(factory.createPlanet(system, primary, name, PlanetType.ProtoFerrinian, distance));
        distance += 5_000_000;
        name = StarSystemFactory.getPlanetName(primary, orbit++);
        system.addPlanets(factory.createPlanet(system, primary, name, PlanetType.ProtoLithian, distance));
        distance += 5_000_000;
        name = StarSystemFactory.getPlanetName(primary, orbit++);
        system.addPlanets(factory.createPlanet(system, primary, name, PlanetType.ProtoCarbonian, distance));
        distance += 5_000_000;
        name = StarSystemFactory.getPlanetName(primary, orbit++);
        system.addPlanets(factory.createPlanet(system, primary, name, PlanetType.ProtoGelidian, distance));

        setDescription(system, null);
    }

    public static void main(String[] args) {
        Roller<PlanetType> roller = new Roller<>();

        System.out.println(roller.add(PlanetType.AsteroidBelt, 3).add(PlanetType.Asimovian, PlanetType.Brammian).roll());
    }
}
