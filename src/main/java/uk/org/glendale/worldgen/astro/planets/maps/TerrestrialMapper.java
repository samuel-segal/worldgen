/*
 * Copyright (c) 2017, Samuel Penn (sam@glendale.org.uk).
 * See the file LICENSE at the root of the project.
 */

package uk.org.glendale.worldgen.astro.planets.maps;

import uk.org.glendale.utils.graphics.Icosahedron;
import uk.org.glendale.utils.graphics.Tile;
import uk.org.glendale.utils.rpg.Die;
import uk.org.glendale.worldgen.astro.planets.Planet;

public class TerrestrialMapper extends PlanetMapper {
    protected static final int    DEFAULT_FACE_SIZE = 24;

    protected static final Tile WATER = new Tile("Water", "#7070C0", true, 2);
    protected static final Tile LAND = new Tile("Land", "#807050", false, 3);
    protected static final Tile LAND_ICE = new Tile("Glacier", "#C0C0C0", false, 2);
    protected static final Tile SEA_ICE = new Tile("Sea Ice", "#D0D0D0", true, 2);

    public TerrestrialMapper(final Planet planet, int size) {
        super(planet, size);
    }

    public void generate() {
        generateHeightMap(24, DEFAULT_FACE_SIZE);
    }

    /**
     * Covers the surface with water.
     */
    protected void setWater(Tile water) {
        for (int y=0; y < getNumRows(); y++) {
            for (int x=0; x < getWidthAtY(y); x++) {
                setTile(x, y, water);
            }
        }
    }

    protected void setWater() {
        setWater(WATER);
    }

    protected void createContinents(int num) {
        createContinents(num, planet.getHydrographics());
    }

    /**
     * Create a number of continents on a terrestrial world.
     *
     * @param num               Number of continents (1+).
     * @param hydrographics     Percentage of surface covered by water (0-100).
     */
    protected void createContinents(int num, int hydrographics) {

        hydrographics = Math.max(0, hydrographics);
        hydrographics = Math.min(100, hydrographics);

        for (int c=0; c < num; c++) {
            int y = Die.rollZero(getNumRows() - 4) + 2;
            int x = Die.rollZero(getWidthAtY(y));
            setTile(x, y, LAND);
        }

        floodToPercentage(LAND, 100 - hydrographics, true);
    }

    protected void setIceCaps() {
        setIceCaps(LAND_ICE, SEA_ICE);
    }
}
