/*
 * Copyright (c) 2018, Samuel Penn (sam@glendale.org.uk).
 * See the file LICENSE at the root of the project.
 */
package uk.org.glendale.worldgen.astro.planets.maps.belt;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.org.glendale.utils.graphics.SimpleImage;
import uk.org.glendale.worldgen.astro.Physics;
import uk.org.glendale.worldgen.astro.planets.Planet;
import uk.org.glendale.worldgen.astro.planets.maps.PlanetMapper;
import uk.org.glendale.worldgen.astro.stars.Star;

import java.util.List;
import java.util.Random;

public class PlanetesimalDiscMapper extends AsteroidBeltMapper {
    protected static final Logger logger = LoggerFactory.getLogger(DustDiscMapper.class);

    public PlanetesimalDiscMapper(final Planet planet, final int size) {
        super(planet, size);

        hasMainMap = false;
        hasOrbitMap = true;
    }

    public PlanetesimalDiscMapper(final Planet planet) {
        super(planet, DEFAULT_FACE_SIZE);

        hasMainMap = false;
        hasOrbitMap = true;
    }

    public void generate() {
        // Nothing do do here.
    }

    public void drawOrbit(SimpleImage image, Star star, int cx, int cy, long kmPerPixel, List<Planet> moons) {
        Random  random = new Random(planet.getId());
        long    distance = planet.getDistance();

        logger.info(String.format("Drawing orbit for [%s] at [%d]km scale [%d]", planet.getName(), distance, kmPerPixel));

        final int number = getNumberOfPlanetesimals(planet, kmPerPixel) * 2;
        logger.info("drawPlanetesimalDisc " + number);

        for (int a = 0; a < number; a++) {
            double  d = planet.getDistance() + random.nextFloat() * planet.getRadius() - random.nextFloat() * planet.getRadius();
            double  angle = random.nextDouble() * 360.0 + getAngleOffset(star, d);
            int     x = cx + (int) (Math.cos(Math.toRadians(angle)) * d / kmPerPixel);
            int     y = cy + (int) (Math.sin(Math.toRadians(angle)) * d / kmPerPixel);

            image.dot(x, y, SimpleImage.getLighter(planet.getType().getColour()));
        }

        if (moons != null) {
            for (Planet moon : moons) {
                random = new Random(moon.getId());
                double  d = planet.getDistance() + moon.getDistance();
                double  angle = random.nextDouble() * 360.0 + getAngleOffset(star, d);
                int     x = cx + (int) (Math.cos(Math.toRadians(angle)) * d / kmPerPixel);
                int     y = cy + (int) (Math.sin(Math.toRadians(angle)) * d / kmPerPixel);

                int radius = 4;
                image.circle(x, y, radius, planet.getType().getColour());

                drawPlanetLabel(image, x, y, angle, moon);
            }
        }
    }


}
