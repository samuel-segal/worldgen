/*
 * Copyright (c) 2017, Samuel Penn (sam@glendale.org.uk).
 * See the file LICENSE at the root of the project.
 */

package uk.org.glendale.worldgen.astro.planets.generators.smallbody;

import uk.org.glendale.utils.rpg.Die;
import uk.org.glendale.worldgen.WorldGen;
import uk.org.glendale.worldgen.astro.planets.Planet;
import uk.org.glendale.worldgen.astro.planets.PlanetFeature;
import uk.org.glendale.worldgen.astro.planets.codes.Atmosphere;
import uk.org.glendale.worldgen.astro.planets.codes.PlanetType;
import uk.org.glendale.worldgen.astro.planets.codes.Temperature;
import uk.org.glendale.worldgen.astro.planets.generators.SmallBody;
import uk.org.glendale.worldgen.astro.stars.Star;
import uk.org.glendale.worldgen.astro.systems.StarSystem;

import static uk.org.glendale.worldgen.astro.commodities.CommodityName.*;

/**
 * A Gelidaceous asteroid is an ice-rich body with volatile content greater than 50%. They tend to be
 * stable, and volatile loss is minimal.
 *
 * There may be some unusual examples of such asteroids that have recently migrated to the inner system.
 * Most don't survive long, but a few have picked up characteristics that allow them to survive. They
 * tend to be larger than normal. The smaller ones evaporate too quickly.
 */
public class Oort extends SmallBody {

    public enum GelidaceousFeature implements PlanetFeature {
        RockSurface,
        SootSurface,
        SnowCovered
    }

    public Oort(WorldGen worldgen, StarSystem system, Star star, Planet previous, long distance) {
        super(worldgen, system, star, previous, distance);
    }

    public Planet getPlanet(String name) {
        return getPlanet(name, PlanetType.Oort);
    }

    public Planet getPlanet(String name, PlanetType type) {
        Planet planet =  definePlanet(name, type);
        planet.setRadius(getRadius(planet));
        planet.setTemperature(planet.getTemperature() + Die.dieV(4));

        // Set default day length to be 2-3 hours.
        planet.setDayLength(3600 + Die.die(3600, 2));

        if (Die.d3() == 1) {
            planet.addFeature(GelidaceousFeature.SnowCovered);
        }

        addPrimaryResource(planet, Water);
        addTraceResource(planet, CarbonicOre);

        return planet;
    }
}
