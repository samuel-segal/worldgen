/*
 * Copyright (c) 2017, Samuel Penn (sam@glendale.org.uk).
 * See the file LICENSE at the root of the project.
 */

package uk.org.glendale.worldgen.astro.planets.maps.dwarf;

import uk.org.glendale.utils.graphics.Icosahedron;
import uk.org.glendale.utils.graphics.SimpleImage;
import uk.org.glendale.utils.graphics.Tile;
import uk.org.glendale.utils.rpg.Die;
import uk.org.glendale.worldgen.astro.planets.Planet;
import uk.org.glendale.worldgen.astro.planets.PlanetFeature;
import uk.org.glendale.worldgen.astro.planets.codes.PlanetType;
import uk.org.glendale.worldgen.astro.planets.generators.dwarf.Arean;
import uk.org.glendale.worldgen.astro.planets.maps.PlanetMapper;
import uk.org.glendale.worldgen.astro.planets.tiles.Cratered;
import uk.org.glendale.worldgen.astro.planets.tiles.Rough;
import uk.org.glendale.worldgen.text.TextGenerator;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static uk.org.glendale.worldgen.astro.planets.generators.Dwarf.DwarfFeature.GreatRift;
import static uk.org.glendale.worldgen.astro.planets.generators.Terrestrial.TerrestrialFeature.*;

/**
 * An EoArean world is an early stage of an Arean world. These are dwarf terrestrial worlds which begin
 * with a thick atmosphere and possibly water cover, but which lose their atmosphere to space as they
 * cool. As the atmosphere things, the water evaporates or freezes leaving a dry, airless world.
 *
 * The EoArean stage is when the early atmosphere still exists, and there is still the possibility of
 * water cover.
 */
public class EoAreanMapper extends AreanMapper {
    private static final Tile DARK_RED = new Tile("Dark Red", "#706050", false, 2);
    private static final Tile MID_RED = new Tile("Mid Red", "#908070", false, 3);
    private static final Tile LIGHT_RED = new Tile("Light Red", "#B0A090", false, 2);
    private static final Tile WATER = new Tile("Water", "#7777FF", true, 2);
    private static final Tile DARK_SALT = new Tile("Dark Salt", "#C0C0C0", false, 2);
    private static final Tile LIGHT_SALT = new Tile("Light Salt", "#E0E0E0", false, 2);
    private static final Tile MURKY_WATER = new Tile("Murky Water", "#553333", true, 5);

    public EoAreanMapper(final Planet planet, final int size) {
        super(planet, size);
    }

    public EoAreanMapper(final Planet planet) {
        super(planet, DEFAULT_FACE_SIZE);
    }

    protected Tile getLowPlains(int height) {
        return DARK_RED;
    }
    protected Tile getHighPlains(int height) {
        return MID_RED;
    }
    protected Tile getMountains(int height) {
        return LIGHT_RED;
    }


    protected Tile getWater() {
        if (planet.hasFeature(Arean.AreanFeature.MurkySea)) {
            return MURKY_WATER;
        }
        return WATER;
    }

    protected Tile getRandomColour(int tileX, int tileY) {
        int h = getHeight(tileX, tileY);

        if (planet.hasFeature(Arean.AreanFeature.SaltPlains)) {
            if (h < 16) {
                return DARK_SALT.getShaded(75 + h);
            } else if (h < 50) {
                return LIGHT_SALT.getShaded(100 - (h / 5));
            } else if (h < 75) {
                return DARK_RED.getShaded(75 + h);
            } else if (h < 90) {
                return LIGHT_RED.getShaded(100 - (h / 5));
            } else {
                return MID_RED.getShaded(85 + (h - 75) / 2);
            }
        } else {
            if (h < 16) {
                return DARK_RED.getShaded(75 + h);
            } else if (h < 76) {
                return LIGHT_RED.getShaded(100 - (h / 5));
            } else {
                return MID_RED.getShaded(85 + (h - 75) / 2);
            }
        }
    }

    /**
     * Generate a Arean surface landscape. This will be grey, barren and cratered.
     */
    public void generate() {
        super.generate();

        hasCloudMap = true;
        cleanBumpMap();
    }

    private void generateFeatures(List<PlanetFeature> features) {
        for (PlanetFeature f : features) {
        }
    }

    public List<SimpleImage> drawClouds(int width) {
        List<SimpleImage>  clouds = new ArrayList<>();
        Icosahedron cloud = getCloudLayer();

        int cloudLimit = 30;
        if (planet.hasFeature(Dry)) {
            cloudLimit = 60;
        } else if (planet.hasFeature(Wet)) {
            cloudLimit = 15;
        }
        for (int y=0; y < cloud.getNumRows(); y++) {
            for (int x=0; x < cloud.getWidthAtY(y); x++) {
                int h = cloud.getHeight(x, y);
                if (h < cloudLimit / 4) {
                    cloud.setHeight(x, y, 0);
                } else if (h < cloudLimit) {
                    cloud.setHeight(x, y, cloud.getHeight(x, y) / 2);
                }
            }
        }

        String cloudColour = "#E0C0B0";
        clouds.add(Icosahedron.stretchImage(cloud.drawTransparency(cloudColour, width), width));

        return clouds;
    }

    public static void main(String[] args) throws IOException {
        Planet       planet = new Planet();
        planet.setName("New Colchis");
        planet.setType(PlanetType.EoArean);
        planet.setHydrographics(50);
        planet.setTemperature(320);
        planet.addFeature(Arean.AreanFeature.EquatorialSea);
        TextGenerator generator = new TextGenerator(planet);
        String text = generator.getFullDescription();

        PlanetMapper p = new EoAreanMapper(planet);

        System.out.println("EoArean:");
        p.generate();
        SimpleImage img = p.draw(2048);
        img.save(new File("/home/sam/tmp/EoArean.png"));
        System.out.println(text);
    }
}
