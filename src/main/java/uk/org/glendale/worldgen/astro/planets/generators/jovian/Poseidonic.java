/*
 * Copyright (c) 2017, Samuel Penn (sam@glendale.org.uk).
 * See the file LICENSE at the root of the project.
 */

package uk.org.glendale.worldgen.astro.planets.generators.jovian;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.org.glendale.utils.rpg.Die;
import uk.org.glendale.worldgen.WorldGen;
import uk.org.glendale.worldgen.astro.planets.Planet;
import uk.org.glendale.worldgen.astro.planets.codes.*;
import uk.org.glendale.worldgen.astro.planets.generators.Jovian;
import uk.org.glendale.worldgen.astro.stars.Star;
import uk.org.glendale.worldgen.astro.systems.StarSystem;

import static uk.org.glendale.worldgen.astro.commodities.CommodityName.*;
import static uk.org.glendale.worldgen.astro.planets.generators.Jovian.JovianFeature.*;

/**
 * Poseidonic, sub-Jovian world, within the snow line with lots of water vapour. Tends to have whitish clouds.
 */
public class Poseidonic extends Jovian {
    private static final Logger logger = LoggerFactory.getLogger(Poseidonic.class);

    public Poseidonic(WorldGen worldgen, StarSystem system, Star star, Planet previous, long distance) {
        super(worldgen, system, star, previous, distance);
    }

    private void generateFeatures(Planet planet) {
        planet.addFeature(WaterClouds);
    }

    public Planet getPlanet(String name, PlanetType type) {
        Planet planet =  definePlanet(name, type);
        planet.setRadius(20_000 + Die.die(5000, 4));

        planet.setAtmosphere(Atmosphere.Hydrogen);
        planet.setPressure(Pressure.SuperDense);

        switch (Die.d6(2)) {
            case 2: case 3:
                planet.setMagneticField(MagneticField.VeryStrong);
                break;
            case 10: case 11: case 12:
                planet.setMagneticField(MagneticField.Standard);
            default:
                planet.setMagneticField(MagneticField.Strong);
                break;
        }

        // Set default day length to be around 10 hours.
        planet.setDayLength(8 * 3600 + Die.die(3600, 2));

        generateFeatures(planet);

        addPrimaryResource(planet, Hydrogen);
        addSecondaryResource(planet, Water);
        addTertiaryResource(planet, Helium);

        return planet;
    }
}
