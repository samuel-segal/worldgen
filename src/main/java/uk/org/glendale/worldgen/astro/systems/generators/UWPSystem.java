package uk.org.glendale.worldgen.astro.systems.generators;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.org.glendale.utils.rpg.Die;
import uk.org.glendale.utils.rpg.Roller;
import uk.org.glendale.worldgen.WorldGen;
import uk.org.glendale.worldgen.astro.Physics;
import uk.org.glendale.worldgen.astro.planets.GeneralFeature;
import uk.org.glendale.worldgen.astro.planets.Planet;
import uk.org.glendale.worldgen.astro.planets.PlanetFactory;
import uk.org.glendale.worldgen.astro.planets.codes.PlanetType;
import uk.org.glendale.worldgen.astro.sectors.Sector;
import uk.org.glendale.worldgen.astro.stars.Luminosity;
import uk.org.glendale.worldgen.astro.stars.SpectralType;
import uk.org.glendale.worldgen.astro.stars.Star;
import uk.org.glendale.worldgen.astro.stars.StarGenerator;
import uk.org.glendale.worldgen.astro.systems.*;
import uk.org.glendale.worldgen.exceptions.DuplicateObjectException;

import java.util.List;

import static uk.org.glendale.worldgen.astro.systems.StarSystemFactory.getBeltName;
import static uk.org.glendale.worldgen.astro.systems.StarSystemFactory.getPlanetName;

public class UWPSystem extends StarSystemGenerator {
    private static final Logger logger = LoggerFactory.getLogger(UWPSystem.class);

    public UWPSystem(WorldGen worldgen) {
        super(worldgen);
    }

    /**
     * Creates a barren star system. Such a system has one or more stars, with planets, but the
     * planets are barren with no civilisation or life of any kind. If the system has multiple stars,
     * the second star is much smaller and a long way away.
     *
     * @param sector    Sector to generate system in.
     * @param name      Name of the system.
     * @param x         X coordinate of the system in the sector (1-32).
     * @param y         Y coordinate of the system in the sector (1-40).
     *
     * @return          Created and persisted star system.
     * @throws DuplicateObjectException     If a duplicate system is created.
     */
    public StarSystem generate(Sector sector, String name, int x, int y) throws DuplicateObjectException {
        return factory.createStarSystem(sector, name.trim(), x, y, StarSystemType.EMPTY);
    }

    @Override
    public void colonise(StarSystem system) {

    }

    private PlanetType getDwarfTerrestrial(CircumstellarZone zone) {
        Roller<PlanetType> roller = new Roller<>();

        switch (zone) {
            case Exclusion:
                roller.add(PlanetType.Ferrinian);
                break;
            case Epistellar:
                roller.add(PlanetType.Janian, PlanetType.Ferrinian);
                break;
            case Hot:
                roller.add(PlanetType.Hermian);
                break;
            case Inner: case Middle:
                roller.add(PlanetType.AreanLacustric, PlanetType.Selenian);
                break;
            case Outer:
                roller.add(PlanetType.AreanLacustric, 2).add(PlanetType.EuArean, 3).
                        add(PlanetType.MesoArean, PlanetType.EoArean, PlanetType.Bathic);
                break;
            case Cold:
                roller.add(PlanetType.Cerean, PlanetType.Vestian, PlanetType.Gelidian);
                break;
            default:
                roller.add(PlanetType.Gelidian);
        }
        return roller.roll();
    }

    private PlanetType getTerrestrial(CircumstellarZone zone) {
        Roller<PlanetType> roller = new Roller<>();

        switch (zone) {
            case Exclusion:
                break;
            case Epistellar:
                roller.add(PlanetType.JaniLithic);
                break;
            case Hot:
                roller.add(PlanetType.Lutian);
                break;
            case Inner:
                roller.add(PlanetType.Cytherean);
                break;
            case Outer: case Cold:
                roller.add(PlanetType.Ymirian);
                break;
            default:
                roller.add(PlanetType.NecroGaian);
        }

        return roller.roll();
    }

    private PlanetType getJovian(CircumstellarZone zone) {
        Roller<PlanetType> roller = new Roller<>();
        switch (zone) {
            case Exclusion:
                break;
            case Epistellar:
                roller.add(PlanetType.Osirian, PlanetType.Sokarian);
                break;
            case Hot:
                roller.add(PlanetType.Poseidonic);
                break;
            case Inner:
                roller.add(PlanetType.Poseidonic);
                break;
            case Middle:
                roller.add(PlanetType.Poseidonic);
                break;
            case Outer:
                roller.add(PlanetType.Jovic, PlanetType.Poseidonic);
                break;
            case Cold:
                roller.add(PlanetType.Saturnian);
                break;
            default:
                roller.add(PlanetType.Neptunian);
        }

        return roller.roll();
    }


    private PlanetType getSmallBody() {
        return PlanetType.Carbonaceous;
    }

    private PlanetType getDwarfTerrestrial() {
        return PlanetType.EuArean;
    }

    private PlanetType getTerrestrial() {
        return PlanetType.EuGaian;
    }

    private PlanetType getJovian() {
        return PlanetType.Saturnian;
    }

    public StarSystem generate(Sector sector, UWP uwp) throws DuplicateObjectException {
        if (sector == null || sector.getId() == 0) {
            throw new IllegalArgumentException("StarSystem must be part of an existing Sector");
        }

        logger.info(String.format("createUWPSystem: [%s] [%s]", sector.getName(), uwp.getUWP()));

        String name = uwp.getName();
        StarSystem system = factory.createStarSystem(sector, name, uwp.getX(), uwp.getY(), StarSystemType.EMPTY);
        system.setUWP(uwp.getLine());

        String[] starData = uwp.getStars();
        system.setType(StarSystemType.SINGLE);
        StarGenerator starGenerator = new StarGenerator(worldgen, system, false);

        int s = 0;
        SpectralType hrType = SpectralType.valueOf(starData[s]);
        Luminosity luminosity = Luminosity.valueOf(starData[s+1]);

        Star primary = starGenerator.generatePrimary(luminosity, hrType);


        List<Planet> planets;
        PlanetFactory planetFactory = worldgen.getPlanetFactory();
        PlanetType  type = null;
        String  planetName;
        int     orbit = 1;
        int     belt = 1;
        long    mainWorldDistance = Physics.getOrbitWithTemperature(primary, 255);
        int     otherWorlds = Die.d6() - 1;

        if (otherWorlds > 3) {
            type = getDwarfTerrestrial(CircumstellarZone.getZone(Physics.getTemperatureOfOrbit(primary, mainWorldDistance / 3)));
            planetName = getPlanetName(primary, orbit++);
            planets = planetFactory.createPlanet(system, primary, planetName, type, mainWorldDistance / 3, GeneralFeature.NoMoons);
            system.addPlanets(planets);

            type = getTerrestrial(CircumstellarZone.getZone(Physics.getTemperatureOfOrbit(primary, mainWorldDistance / 2)));
            planetName = getPlanetName(primary, orbit++);
            planets = planetFactory.createPlanet(system, primary, planetName, type, (2 * mainWorldDistance) / 3, GeneralFeature.NoMoons);
            system.addPlanets(planets);
            otherWorlds -= 2;
        } else if (otherWorlds > 1) {
            type = getDwarfTerrestrial(CircumstellarZone.getZone(Physics.getTemperatureOfOrbit(primary, mainWorldDistance / 2)));
            planetName = getPlanetName(primary, orbit++);
            planets = planetFactory.createPlanet(system, primary, planetName, type, mainWorldDistance / 2, GeneralFeature.NoMoons);
            system.addPlanets(planets);
            otherWorlds -= 1;
        }

        // Primary world.
        int         size = uwp.getSize();
        if (size > 8000) {
            type = getTerrestrial();
        } else if (size > 2000) {
            type = getDwarfTerrestrial();
        } else {
            type = getSmallBody();
        }

        if (size == 0) {
            planetName = getBeltName(primary, belt++);
            planets = planetFactory.createPlanet(system, primary, planetName, PlanetType.AsteroidBelt, mainWorldDistance, GeneralFeature.NoMoons);
        } else {
            planetName = getPlanetName(primary, orbit++);
            planets = planetFactory.createPlanet(system, primary, planetName, type, mainWorldDistance, GeneralFeature.UWP);
            planets.get(0).setRadius(size / 2);
        }
        planets.get(0).setStarPort(uwp.getStarPort());
        planets.get(0).setPopulation(uwp.getPopulation());
        planets.get(0).setTechLevel(uwp.getTechLevel());
        planets.get(0).setLawLevel(uwp.getLawLevel());
        planets.get(0).setGovernment(uwp.getGovernment());
        planets.get(0).setAtmosphere(uwp.getAtmosphere());
        planets.get(0).setPressure(uwp.getPressure());
        system.addPlanets(planets);

        if (otherWorlds > 0) {
            type = getDwarfTerrestrial(CircumstellarZone.getZone(Physics.getTemperatureOfOrbit(primary, mainWorldDistance * 2)));
            planetName = getPlanetName(primary, orbit++);
            planets = planetFactory.createPlanet(system, primary, planetName, type, mainWorldDistance * 2, GeneralFeature.NoMoons);
            system.addPlanets(planets);
            otherWorlds -= 1;
        }

        if (uwp.getBelts() > 0) {
            type = PlanetType.AsteroidBelt;
            planetName = getBeltName(primary, belt++);
            planets = planetFactory.createPlanet(system, primary, planetName, type, mainWorldDistance * 3, GeneralFeature.NoMoons);
            system.addPlanets(planets);
        }

        long distance = mainWorldDistance * 5;
        int  gasGiants = uwp.getGasGiants();
        while (gasGiants-- > 0) {
            type = getJovian();
            planetName = getPlanetName(primary, orbit++);
            planets = planetFactory.createPlanet(system, primary, planetName, type, distance);
            system.addPlanets(planets);
            distance *= 2;
        }

        if (uwp.getBelts() > 1) {
            type = PlanetType.IceBelt;
            planetName = getBeltName(primary, belt++);
            planets = planetFactory.createPlanet(system, primary, planetName, type, distance * 2, GeneralFeature.NoMoons);
            system.addPlanets(planets);
        }


        setDescription(system, null);


        return system;
    }
}
