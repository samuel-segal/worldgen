/**
 * JovicMapper.java
 *
 * Copyright (C) 2017 Samuel Penn, sam@notasnark.net
 * See the file LICENSE at the root of the project.
 */
package uk.org.glendale.worldgen.astro.planets.maps.jovian;

import uk.org.glendale.utils.graphics.Tile;
import uk.org.glendale.utils.rpg.Die;
import uk.org.glendale.worldgen.astro.planets.Planet;
import uk.org.glendale.worldgen.astro.planets.codes.PlanetType;
import uk.org.glendale.worldgen.astro.planets.generators.jovian.Neptunian;
import uk.org.glendale.worldgen.astro.planets.generators.jovian.Poseidonic;
import uk.org.glendale.worldgen.astro.planets.maps.JovianMapper;
import uk.org.glendale.worldgen.web.Server;

import java.io.IOException;

import static uk.org.glendale.worldgen.astro.planets.generators.jovian.Neptunian.NeptunianFeatures.WhiteBands;

/**
 * A cold Jovian world, found in the outer solar system.
 */
public class PoseidonicMapper extends JovianMapper {

    protected static final Tile DARK = new Tile("Brown", "#D0C0A0", false, 6);
    protected static final Tile LIGHT = new Tile("Light", "#E0E0D0", false, 4);
    protected static final Tile WHITE = new Tile("White", "#F0F0F0", false, 2);

    public PoseidonicMapper(final Planet planet, final int size) {
        super(planet, size);
    }
    public PoseidonicMapper(final Planet planet) {
        super(planet, DEFAULT_FACE_SIZE);
    }

    private Tile getRandomColour() {
        switch (Die.d6()) {
            case 1:
                return PoseidonicMapper.DARK;
            case 2: case 3:
                return PoseidonicMapper.LIGHT;
            default:
                return PoseidonicMapper.WHITE;
        }
    }

    protected Tile getBandColour(Tile previousColour, Tile nextColour) {
        if (nextColour != null) {
            return nextColour;
        }
        if (previousColour == null || Die.d6() == 1) {
            return getRandomColour();
        }
        return previousColour.getVariant(Die.d8() - Die.d8());
    }

    private static final int H = 52;

    public void cloudFormations() {
        if (planet.hasFeature(WhiteBands)) {
            int s = getNumRows() / 2 + Die.dieV(20);
            int h = Die.d4(2);

            Tile mix = getTile(0, s);
            for (int y = s; y < s + h; y++) {
                mix = mix.getMix(WHITE);
                for (int x=0; x < getWidthAtY(y); x++) {
                    setHeight(x, y, H);
                    setTile(x, y, mix);
                }
            }
            for (int y = s + h; y < s + h * 2; y++) {
                mix = mix.getMix(getTile(0, y));
                for (int x=0; x < getWidthAtY(y); x++) {
                    setHeight(x, y, H);
                    setTile(x, y, mix);
                }
            }
        }
    }

    public static void main(String[] args) throws IOException {
        Planet planet = new Planet();
        planet.setName("Poseidonic");
        planet.setType(PlanetType.Poseidonic);
        planet.setRadius(30_000);


        testOutput(planet, new Poseidonic(Server.getWorldGen(), null, null, null, 0),
                new PoseidonicMapper(planet));
    }
}
