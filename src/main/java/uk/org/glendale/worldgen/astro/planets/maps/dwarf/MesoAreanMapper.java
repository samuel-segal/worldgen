/*
 * Copyright (c) 2020, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */

package uk.org.glendale.worldgen.astro.planets.maps.dwarf;

import uk.org.glendale.utils.graphics.Icosahedron;
import uk.org.glendale.utils.graphics.SimpleImage;
import uk.org.glendale.utils.graphics.Tile;
import uk.org.glendale.utils.rpg.Die;
import uk.org.glendale.worldgen.astro.commodities.Commodity;
import uk.org.glendale.worldgen.astro.commodities.CommodityName;
import uk.org.glendale.worldgen.astro.commodities.Frequency;
import uk.org.glendale.worldgen.astro.commodities.Resource;
import uk.org.glendale.worldgen.astro.planets.Planet;
import uk.org.glendale.worldgen.astro.planets.PlanetFeature;
import uk.org.glendale.worldgen.astro.planets.PlanetGenerator;
import uk.org.glendale.worldgen.astro.planets.codes.Atmosphere;
import uk.org.glendale.worldgen.astro.planets.codes.Life;
import uk.org.glendale.worldgen.astro.planets.codes.PlanetType;
import uk.org.glendale.worldgen.astro.planets.generators.dwarf.Arean;
import uk.org.glendale.worldgen.astro.planets.generators.dwarf.MesoArean;
import uk.org.glendale.worldgen.astro.planets.maps.PlanetMapper;
import uk.org.glendale.worldgen.astro.planets.tiles.Speckled;
import uk.org.glendale.worldgen.text.TextGenerator;
import uk.org.glendale.worldgen.web.Server;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static uk.org.glendale.worldgen.astro.planets.generators.Dwarf.DwarfFeature.*;

/**
 * MesoArean worlds are wet versions of Mars.
 */
public class MesoAreanMapper extends AreanMapper {
    private final Tile WATER        = new Tile("Water", "#7777FF").water(true).random(2);
    private final Tile ICE          = new Tile("Ice", "#FEFEFE").random(2);
    private final Tile MOUNTAIN     = new Tile("Mountain", "#A07020").mountain(true).random(2);
    private final Tile PLAINS       = new Tile("Plains", "#F0A0000").random(2);
    private final Tile DESERT       = new Tile("Desert", "#E0C000").random(5);
    private final Tile FOREST       = new Tile("Forest", "#449944").random(4);
    private final Tile SCRUB        = new Tile("Scrub", "#779944").random(4);

    protected static final Tile RIFT = new Tile("Rift", "#604040", false, 1);


    public MesoAreanMapper(final Planet planet, final int size) {
        super(planet, size);
    }

    public MesoAreanMapper(final Planet planet) {
        super(planet, DEFAULT_FACE_SIZE);
    }

    protected Tile getDesert() {
        return DESERT;
    }
    protected Tile getPlains() {
        return PLAINS;
    }
    protected Tile getMountains() {
        return MOUNTAIN;
    }
    protected Tile getWater() {
        return WATER;
    }
    protected Tile getIce() {
        return ICE;
    }

    /**
     * Generate an Arean surface landscape.
     */
    public void generate() {
        super.generate();

        if (planet != null) {
            List<PlanetFeature> features = planet.getFeatures();
            if (features != null && features.size() != 0) {
                generateFeatures(features);
            }
        }
        cleanBumpMap();

        if (planet.getPressure() >= 1_000) {
            hasCloudMap = true;
        }
    }

    private void generateFeatures(List<PlanetFeature> features) {
        for (PlanetFeature f : features) {
            if (f == GreatRift) {
                // A single rift split across the world.
                addRift(RIFT,8 + Die.d6(2));
            }
        }
    }

    public List<SimpleImage> drawClouds(int width) {
        List<SimpleImage>  clouds = new ArrayList<>();
        cloudHeight = 5;

        if (planet.getPressure() < 1_000) {
            // No significant atmosphere, so don't show anything.
            return clouds;
        }

        Icosahedron cloud = getCloudLayer();
        int modifier = planet.getPressure() / 2_500;

        String cloudColour = "#F0E0E0";

        for (int y=0; y < cloud.getNumRows(); y++) {
            for (int x=0; x < cloud.getWidthAtY(y); x++) {
                int h = cloud.getHeight(x, y) / 2;
                if (h > 35) {
                    cloud.setHeight(x, y, (h + modifier) / 2);
                } else {
                    cloud.setHeight(x, y, 0);
                }
            }
        }
        clouds.add(Icosahedron.stretchImage(cloud.drawTransparency(cloudColour, width), width));

        return clouds;
    }

    public static void main(String[] args) throws IOException {
        Planet       planet = new Planet();
        planet.setName("Egurgadi");
        planet.setType(PlanetType.MesoArean);
        planet.setHydrographics(90);
        planet.setTemperature(298);
        planet.setPressure(30_000);
        planet.setAtmosphere(Atmosphere.Standard);
        planet.setLife(Life.SimpleLand);
        planet.addResource(new Commodity() {{
            setName(CommodityName.Woods.getName());
            setFrequency(Frequency.COMMON);
        }}, 0);
        planet.addResource(new Commodity() {{
            setName(CommodityName.Grasses.getName());
            setFrequency(Frequency.COMMON);
        }}, 300);
        planet.addResource(new Commodity() {{
            setName(CommodityName.Shrubs.getName());
            setFrequency(Frequency.COMMON);
        }}, 100);

        testOutput(planet, new MesoArean(Server.getWorldGen(), null, null, null, 0),
                new MesoAreanMapper(planet));
    }
}
