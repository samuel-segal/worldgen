/*
 * Copyright (c) 2018, Samuel Penn (sam@glendale.org.uk).
 * See the file LICENSE at the root of the project.
 */
package uk.org.glendale.worldgen.astro.planets.maps.belt;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.org.glendale.utils.graphics.SimpleImage;
import uk.org.glendale.worldgen.astro.planets.Planet;
import uk.org.glendale.worldgen.astro.stars.Star;

import java.util.List;
import java.util.Random;

/**
 * Mapper for Ice Belts. This is functionally identical to the mapper for Asteroid Belts.
 * It just needs its own class so that it can be found by the reflection methods.
 */
public class OortCloudMapper extends AsteroidBeltMapper {
    protected static final Logger logger = LoggerFactory.getLogger(OortCloudMapper.class);

    public OortCloudMapper(final Planet planet, final int size) {
        super(planet, size);
    }

    public OortCloudMapper(final Planet planet) {
        super(planet, DEFAULT_FACE_SIZE);
    }

    public void drawOrbit(SimpleImage image, Star star, int cx, int cy, long kmPerPixel, List<Planet> moons) {
        Random random = new Random(planet.getId());

        final int number = getNumberOfPlanetesimals(planet, kmPerPixel);
        int     s = (int) ((planet.getDistance() / kmPerPixel) / 1000);
        logger.info("draw" + planet.getType() + " " + number);

        for (int a = 0; a < number; a++) {
            double  d = planet.getDistance() + random.nextFloat() * planet.getRadius() - random.nextFloat() * planet.getRadius();
            double  angle = random.nextDouble() * 360.0 + getAngleOffset(star, d);
            int     x = cx + (int) (Math.cos(Math.toRadians(angle)) * d / kmPerPixel);
            int     y = cy + (int) (Math.sin(Math.toRadians(angle)) * d / kmPerPixel);

            // Determine size class of this asteroid, based on random distribution.
            int     size = random.nextInt(100) + a;
            if (size < 10) {
                // Giant asteroid, > 100km radius.
                image.rectangleFill(x - 1 - s, y - 1 - s, 3 + s * 2, 3 + s * 2, "#000000");
            } else if (size < 30) {
                // Large asteroid, > 30km radius.
                image.rectangleFill(x - s, y - s, 2 + s * 2, 2 + s * 2, "#000000");
            } else if (size < 100) {
                // Medium asteroid, > 10km radius.
                image.rectangleFill(x - s / 2, y - s / 2, 2 + s, 2 + s, SimpleImage.getDarker(planet.getType().getColour()));
            } else if (size < 1000) {
                // Small asteroid, > 3km radius.
                image.rectangleFill(x - s / 4, y - s / 4, 2 + s / 2, 2 + s / 2, planet.getType().getColour());
            } else {
                // Don't draw anything.
                image.rectangleFill(x, y, 1, 1, SimpleImage.getLighter(planet.getType().getColour()));
            }
        }

        if (moons != null) {
            for (Planet moon : moons) {
                random = new Random(moon.getId());
                double  d = planet.getDistance() + moon.getDistance();
                double  angle = random.nextDouble() * 360.0 + getAngleOffset(star, d);
                int     x = cx + (int) (Math.cos(Math.toRadians(angle)) * d / kmPerPixel);
                int     y = cy + (int) (Math.sin(Math.toRadians(angle)) * d / kmPerPixel);

                int radius = 4;
                image.circle(x, y, radius, planet.getType().getColour());

                drawPlanetLabel(image, x, y, angle, moon);
            }
        }
    }
}
