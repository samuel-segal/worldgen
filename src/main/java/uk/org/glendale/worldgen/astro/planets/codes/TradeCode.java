/*
 * Copyright (C) 2018 Samuel Penn, sam@glendale.org.uk
 * See the file LICENSE at the root of the project.
 */
package uk.org.glendale.worldgen.astro.planets.codes;

public enum TradeCode {
    Ba, // Barren: Has no permanent population
    Va, // Vacuum: Has no atmosphere
    Hi,
    Lo,
    Ri,
    Po,
    De,
    Ic,
    Wa,
    As,
    Ag,
    Na,
    In,
    Ni,
    Fl,
    Brd, Bgr, Bbl, Bbk, // Red, Green, Blue or Dark shade to things.
    Bsp, // Belt, spokes.
    Bva, // Belt, variable density.
    Sg, // Spin gravity
    Lk  // Tide Locked
}
