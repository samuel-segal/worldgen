/*
 * Copyright (c) 2018, Samuel Penn (sam@glendale.org.uk).
 * See the file LICENSE at the root of the project.
 */

package uk.org.glendale.worldgen.astro.planets.generators.terrestrial;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.org.glendale.utils.rpg.Die;
import uk.org.glendale.worldgen.WorldGen;
import uk.org.glendale.worldgen.astro.planets.Planet;
import uk.org.glendale.worldgen.astro.planets.codes.Atmosphere;
import uk.org.glendale.worldgen.astro.planets.codes.Life;
import uk.org.glendale.worldgen.astro.planets.codes.PlanetType;
import uk.org.glendale.worldgen.astro.planets.generators.Terrestrial;
import uk.org.glendale.worldgen.astro.stars.Star;
import uk.org.glendale.worldgen.astro.systems.StarSystem;
import uk.org.glendale.worldgen.text.TextGenerator;

import static uk.org.glendale.worldgen.astro.commodities.CommodityName.*;

/**
 * EuGaian worlds are Tectonic Terrestrial worlds similar to an early Earth.
 */
public class EuGaian extends Terrestrial {
    private static final Logger logger = LoggerFactory.getLogger(EuGaian.class);

    public EuGaian(WorldGen worldgen, StarSystem system, Star star, Planet previous, long distance) {
        super(worldgen, system, star, previous, distance);
    }

    /**
     * Late Stage Meso Gaian worlds have complex ocean life, with a wide range of aquatic vertebrates.
     * There might be simple life, such as lichens, starting to gain a foothold on land.
     */
    private void typical(Planet planet) {
        planet.setAtmosphere(Atmosphere.Standard);
        planet.setPressure(80_000 + Die.die(20_000, 2));
        planet.setLife(Life.Extensive);

        addSecondaryResource(planet, Oxygen);
        addSecondaryResource(planet, OrganicChemicals);
        addTertiaryResource(planet, OrganicGases);
        planet.setTemperature( (planet.getTemperature() + 320) / 2);
    }

    public Planet getPlanet(String name) {
        Planet planet = definePlanet(name, PlanetType.MesoGaian);

        setTerrestrialProperties(planet);
        planet.setHabitability(2);
        planet.setHydrographics(55 + Die.d12(2));

        typical(planet);
        planet.setNightTemperature(planet.getTemperature() - Die.d6(2));

        if (planet.getHydrographics() < 30) {
            planet.addFeature(TerrestrialFeature.Dry);
        }

        //generateLife(planet);
        addFeatures(planet);

        addPrimaryResource(planet, Water);

        TextGenerator text = new TextGenerator(planet);
        planet.setDescription(text.getFullDescription());

        return planet;
    }

    private void addFeatures(Planet planet) {
    }
}
