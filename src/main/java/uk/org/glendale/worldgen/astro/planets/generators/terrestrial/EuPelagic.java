/*
 * Copyright (c) 2021, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */

package uk.org.glendale.worldgen.astro.planets.generators.terrestrial;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.org.glendale.utils.rpg.Die;
import uk.org.glendale.worldgen.WorldGen;
import uk.org.glendale.worldgen.astro.planets.Planet;
import uk.org.glendale.worldgen.astro.planets.codes.Atmosphere;
import uk.org.glendale.worldgen.astro.planets.codes.Life;
import uk.org.glendale.worldgen.astro.planets.codes.PlanetType;
import uk.org.glendale.worldgen.astro.planets.generators.Terrestrial;
import uk.org.glendale.worldgen.astro.stars.Star;
import uk.org.glendale.worldgen.astro.systems.StarSystem;
import uk.org.glendale.worldgen.text.TextGenerator;

import static uk.org.glendale.worldgen.astro.commodities.CommodityName.*;

/**
 * BathyGaian worlds are Tectonic Terrestrial worlds covered in deep oceans and with a thick, hot, atmosphere.
 * There is normally life in the oceans, though the ocean floors are anoxic and lifeless.
 */
public class EuPelagic extends Terrestrial {
    private static final Logger logger = LoggerFactory.getLogger(EuPelagic.class);

    public EuPelagic(WorldGen worldgen, StarSystem system, Star star, Planet previous, long distance) {
        super(worldgen, system, star, previous, distance);
    }

    public Planet getPlanet(String name) {
        Planet planet = definePlanet(name, PlanetType.EuPelagic);

        planet.setHabitability(6);
        planet.setHydrographics(100);
        if (Die.d3() == 1) {
            planet.setAtmosphere(Atmosphere.Oxygen);
        } else {
            planet.setAtmosphere(Atmosphere.HighOxygen);
        }
        planet.setPressure(50_000 + Die.d6(2) * Die.die(25_000)  + Die.die(25_000));
        planet.setLife(Life.ComplexOcean);

        planet.setTemperature( 300 + Die.d100());
        planet.setNightTemperature(planet.getTemperature());

        addPrimaryResource(planet, Fish);

        addPrimaryResource(planet, Water);
        addSecondaryResource(planet, OrganicChemicals);
        addTertiaryResource(planet, OrganicGases);

        TextGenerator text = new TextGenerator(planet);
        planet.setDescription(text.getFullDescription());

        return planet;
    }

    private void addFeatures(Planet planet) {
    }
}
