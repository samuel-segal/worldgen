/*
 * Copyright (c) 2020, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */
package uk.org.glendale.worldgen.astro.planets.tiles;

import uk.org.glendale.utils.graphics.SimpleImage;
import uk.org.glendale.utils.graphics.Tile;
import uk.org.glendale.utils.rpg.Die;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Add a vegetation layer to a tile. This normally displays as greens, but can vary based on type of
 * vegetation and solar type.
 */
public class Climate extends Tile {
    List<String> colours = new ArrayList<>();

    public Climate(Tile tile) {
        super(tile);
    }

    public Climate list(List<String> colours, int height) {
        this.colours = new ArrayList<>();
        for (String s : colours) {
            this.colours.add(getShade(s, 50 + height / 2));
        }


        Map<String, Integer> map = new HashMap<>();
        String baseColour = getShade(getRGB(), 50 + height / 2);
        int max = 0;
        for (String s : this.colours) {
            if (map.get(s) != null) {
                map.put(s, map.get(s) + 1);
            } else {
                map.put(s, 1);
            }
            if (map.get(s) > max) {
                rgb(s);
            }
        }
        if (max < 3) {
            this.colours.add(baseColour);
            this.colours.add(baseColour);
            this.colours.add(baseColour);
            rgb(baseColour);
        }
        if (max < 5) {
            this.colours.add(baseColour);
            this.colours.add(baseColour);
            this.colours.add(baseColour);
            rgb(baseColour);
        }
        if (max < 7) {
            this.colours.add(baseColour);
            this.colours.add(baseColour);
            this.colours.add(baseColour);
        }

        return this;
    }

    public void addDetail(SimpleImage image, int x, int y, int w, int h) {
        if (colours.size() == 0) {
            return;
        }
        int height = Math.abs(h);
        for (int yy = 0; yy <= height; yy++) {
            int width = (int) ((1.0 * w * (height - yy)) / (1.0 * height));
            for (int xx = -width; xx <= width; xx++) {
                image.dot(x + xx + w, y + (int)(Math.signum(h) * yy), colours.get(Die.rollZero(colours.size())));
            }
        }
    }
}
