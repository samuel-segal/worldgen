package uk.org.glendale.worldgen.astro.systems;

import javax.persistence.*;

/**
 * Defines survey data for a star system. It returns the scan value
 */
@Entity
@Table(name = "survey")
public class Survey {
    @Id
    @GeneratedValue
    @Column(name = "id")
    private int       id;

    @Column(name = "system_id")
    private int       systemId;

    @Column(name = "ship_id")
    private int       shipId;

    @Column(name = "scan")
    private int       scan;

    public Survey() {
    }

    public Survey(int systemId, int scan) {
        this.systemId = systemId;
        this.scan = scan;
    }

    public int getId() {
        return id;
    }

    public int getSystemId() {
        return systemId;
    }

    public int getShipId() {
        return shipId;
    }

    /**
     * Gets the level of scan data for this star system.
     *
     * @return  Scan data, from zero updates.
     */
    public int getScan() {
        return scan;
    }

    public void setScan(final int scan) {
        this.scan = Math.max(0, scan);
    }

    public void addScan(final int scan) {
        this.scan = Math.max(0, this.scan + scan);
    }
}
