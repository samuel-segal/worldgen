/*
 * Copyright (c) 2021, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */

package uk.org.glendale.worldgen.astro.planets.maps.terrestrial;

import uk.org.glendale.utils.graphics.Icosahedron;
import uk.org.glendale.utils.graphics.SimpleImage;
import uk.org.glendale.utils.graphics.Tile;
import uk.org.glendale.utils.rpg.Die;
import uk.org.glendale.worldgen.astro.Physics;
import uk.org.glendale.worldgen.astro.commodities.CommodityName;
import uk.org.glendale.worldgen.astro.planets.Planet;
import uk.org.glendale.worldgen.astro.planets.PlanetFeature;
import uk.org.glendale.worldgen.astro.planets.codes.Atmosphere;
import uk.org.glendale.worldgen.astro.planets.codes.Life;
import uk.org.glendale.worldgen.astro.planets.codes.PlanetType;
import uk.org.glendale.worldgen.astro.planets.generators.Terrestrial;
import uk.org.glendale.worldgen.astro.planets.generators.terrestrial.EoGaian;
import uk.org.glendale.worldgen.astro.planets.generators.terrestrial.Lutian;
import uk.org.glendale.worldgen.astro.planets.maps.TerrestrialMapper;
import uk.org.glendale.worldgen.astro.planets.maps.dwarf.MesoAreanMapper;
import uk.org.glendale.worldgen.astro.planets.tiles.Lava;
import uk.org.glendale.worldgen.astro.planets.tiles.Rough;
import uk.org.glendale.worldgen.web.Server;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static uk.org.glendale.worldgen.astro.planets.generators.Terrestrial.TerrestrialFeature.*;

public class LutianMapper extends TerrestrialMapper {
    public LutianMapper(final Planet planet) {
        super(planet, DEFAULT_FACE_SIZE);
    }

    protected final Tile MOUNTAINS = new Tile("Mountains", "#906045", false, 2);
    protected final Tile PLAINS = new Tile("Plains", "#D08055", false, 3);
    protected final Tile DESERT = new Tile("Desert", "#F09050", false, 2);

    private void generateFeatures(List<PlanetFeature> features) {
    }

    public void generate() {
        super.generate();

        for (int y=0; y < getNumRows(); y++) {
            for (int x=0; x < getWidthAtY(y); x++) {
                if (getHeight(x, y) > 85) {
                    setTile(x, y, MOUNTAINS.getShaded(getHeight(x, y)));
                } else if (getHeight(x, y) < 25) {
                    setTile(x, y, PLAINS.getShaded((getHeight(x, y) + 100) / 2));
                } else {
                    setTile(x, y, DESERT.getShaded((getHeight(x, y) + 100) / 2));
                }
            }
        }

        // After finishing with the height map, set it to more consistent values
        // so that the bump mapper can use it cleanly.
        for (int y=0; y < getNumRows(); y++) {
            for (int x=0; x < getWidthAtY(y); x++) {
                if (getTile(x, y).isWater()) {
                    setHeight(x, y, 0);
                } else {
                    int h = getHeight(x, y);
                    if (h < 90) {
                        h = 50;
                    } else {
                        h = 100;
                    }
                    setHeight(x, y, h);
                }
                setTile(x, y, new Rough(getTile(x, y)));
            }
        }
        createCraters(1, 10);

        generateFeatures(planet.getFeatures());
//        cleanBumpMap();

        // Mark world as having clouds.
        hasCloudMap = false;
        hasHeightMap = true;
    }

    /**
     * Lower cloud layer is almost completely opaque.
     */
    private SimpleImage drawLowerCloudLayer(int width) throws IOException {
        Icosahedron cloud = getCloudLayer();

        int modifier = planet.getPressure() / Physics.STANDARD_PRESSURE;

        String cloudColour = "#F0C0C0";
        if (planet.hasFeature(EarlyStage)) {
            cloudColour = "#F0C090";
            modifier += 15;
        }
        if (planet.hasFeature(VolcanicFlats)) {
            cloudColour = "#806040";
            modifier = 50;
        }
        if (planet.getAtmosphere() == Atmosphere.Hydrogen) {
            cloudColour = "F5F0E5";
            modifier += 20;
        }

        // Algae uses photo-synthesis, so the presence of algae requires sufficient sunlight
        // to be reaching the surface.
        int algae = planet.getResource(CommodityName.Algae);
        int lowerLimit = algae / 50;

        if (algae > 600) {
            modifier = 0;
        } else if (algae > 300) {
            modifier /= 10;
        } else if (algae > 100) {
            modifier /= 5;
        } else if (algae > 0) {
            modifier /= 2;
        }

        for (int y=0; y < cloud.getNumRows(); y++) {
            for (int x=0; x < cloud.getWidthAtY(y); x++) {
                int h = cloud.getHeight(x, y);
                if (h < lowerLimit) {
                    cloud.setHeight(x, y, 0);
                } else {
                    cloud.setHeight(x, y, modifier + h / 2);
                }
            }
        }

        return Icosahedron.stretchImage(cloud.drawTransparency(cloudColour, width), width);
    }

    /**
     * Upper cloud layer is more transparent, to allow the darker lower clouds to be seen.
     */
    private SimpleImage drawUpperHaze(int width) throws IOException {
        Icosahedron cloud = getCloudLayer();

        int lowerLimit = 0;
        String cloudColour = "#F0A84A";
        if (planet.getAtmosphere() == Atmosphere.Hydrogen) {
            lowerLimit = 25;
        }
        int algae = planet.getResource(CommodityName.Algae);
        lowerLimit += algae / 20;

        for (int y=0; y < cloud.getNumRows(); y++) {
            for (int x=0; x < cloud.getWidthAtY(y); x++) {
                int h = cloud.getHeight(x, y);
                if (h < lowerLimit) {
                    cloud.setHeight(x, y, 0);
                } else {
                    if (planet.hasFeature(EarlyStage)) {
                        cloud.setHeight(x, y, h - lowerLimit);
                    } else if (planet.hasFeature(LateStage)) {
                        cloud.setHeight(x, y, h / 4);
                    } else {
                        cloud.setHeight(x, y, h / 2);
                    }
                }
            }
        }

        return Icosahedron.stretchImage(cloud.drawTransparency(cloudColour, width), width);
    }

    public List<SimpleImage> drawClouds(int width) {
        List<SimpleImage>  clouds = new ArrayList<>();

        cloudHeight = 5;

        try {
            clouds.add(drawLowerCloudLayer(width));
            clouds.add(drawUpperHaze(width));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return clouds;
    }


    public static void main(String[] args) throws IOException {
        Planet       planet = new Planet();
        planet.setName("Lutian");
        planet.setType(PlanetType.Lutian);
        planet.setHydrographics(00);
        planet.setTemperature(350);
        planet.setPressure(40_000);
        planet.setAtmosphere(Atmosphere.CarbonDioxide);
        planet.setLife(Life.None);

        testOutput(planet, new Lutian(Server.getWorldGen(), null, null, null, 0),
                new LutianMapper(planet));
    }
}
