/**
 * Pressure.java
 *
 * Copyright (c) 2007, Samuel Penn.
 * See the file LICENSE at the root of the project.
 */
package uk.org.glendale.worldgen.astro.planets.codes;

/**
 * Defines the various levels of atmospheric pressure. 'Standard' is considered normal for humans.
 * Thin and Dense are survivable (though uncomfortable) without a pressure suit or breathing mask.
 * Thinner or Denser atmospheres require special survival gear for a human to survive.
 *
 * @author Samuel Penn
 */
public enum Pressure {
    /**
     * This is a vacuum world with no appreciable atmosphere. It requires a full pressure suit
     * for survival by humans, and produces no weather of any note. It is less than 0.001 atmospheres.
     */
    None(0, 1.0, 4),
    /**
     * A Trace atmosphere is less than 0.1 atmospheres. It requires a full pressure suit for
     * survival by humans.
     */
    Trace(100, 1.0, 4),
    /**
     * Very Thin atmospheres are between 0.2 and 0.45 atmospheric pressure. They can be survived
     * with breathing gear, but don't require a full pressure suit.
     */
    VeryThin(20_000, 1.0, 3),
    /**
     * Thin atmospheres range from 0.45 to 0.6 atmospheric pressure. Humans can live and work in
     * such atmospheres without breathing equipment, but many tasks may be fatiguing.
     */
    Thin(45_000, 0.95, 2),
    /**
     * Standard atmospheric pressure ranges between 0.6 and 1.5. This is considered normal for
     * humans.
     */
    Standard(60_000, 0.90, 1),
    Dense(150_000, 0.80, 1),
    VeryDense(500_000, 0.70, 3),
    SuperDense(2_500_000, 0.50, 5);

    private final int     pascals;
    private final double  greenhouse;
    private final int     habitability;

    Pressure(int pascals, double greenhouse, int habitability) {
        this.pascals = pascals;
        this.greenhouse = greenhouse;
        this.habitability = habitability;
    }

    /**
     * Get the effective stellar distance modifier. A thick atmosphere
     * retains heat, meaning the world is warmer as if it were closer to
     * the star.
     */
    public double getGreenhouse() {
        return greenhouse;
    }

    public int getPascals() {
        return pascals;
    }

    public int getHabitability() {
        return habitability;
    }

    public static Pressure getPressure(int pascals) {
        Pressure pressure = None;

        for (Pressure p : Pressure.values()) {
            if (pascals < p.getPascals()) {
                break;
            }
            pressure = p;
        }
        return pressure;
    }

}
