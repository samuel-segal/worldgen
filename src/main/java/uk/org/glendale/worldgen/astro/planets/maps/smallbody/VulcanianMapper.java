/*
 * Copyright (c) 2018, Samuel Penn (sam@glendale.org.uk).
 * See the file LICENSE at the root of the project.
 */
package uk.org.glendale.worldgen.astro.planets.maps.smallbody;

import uk.org.glendale.utils.graphics.SimpleImage;
import uk.org.glendale.utils.graphics.Tile;
import uk.org.glendale.utils.rpg.Die;
import uk.org.glendale.worldgen.astro.planets.Planet;
import uk.org.glendale.worldgen.astro.planets.PlanetFeature;
import uk.org.glendale.worldgen.astro.planets.generators.SmallBody;
import uk.org.glendale.worldgen.astro.planets.generators.smallbody.Vulcanian;
import uk.org.glendale.worldgen.astro.planets.maps.SmallBodyMapper;
import uk.org.glendale.worldgen.astro.planets.tiles.Rough;
import uk.org.glendale.worldgen.web.Server;

import java.util.ArrayList;
import java.util.List;

import static uk.org.glendale.worldgen.astro.planets.generators.SmallBody.SmallBodyFeature.*;

/**
 * Vulcanian class asteroids.
 */
public class VulcanianMapper extends SmallBodyMapper {
    public VulcanianMapper(final Planet planet, final int size) {
        super(planet, size);
    }

    public VulcanianMapper(final Planet planet) {
        super(planet, DEFAULT_FACE_SIZE);
    }

    private static final Tile SILICATES = new Tile("Silicates", "#909070", false);
    private static final Tile METALS = new Tile("Metals", "#A0A0B0", false);
    private static final Tile BLACKENED = new Tile("Blackened", "#202020", false);
    private static final Tile HOT = new Tile("Hot", "#F06060", false);
    private static final Tile MOLTEN = new Tile("MOLTEN", "#FE9090", false);

    public void generate() {
        super.generate();


        int baseGrey = 50;
        for (int y = 0; y < getNumRows(); y++) {
            for (int x = 0; x < getWidthAtY(y); x++) {
                int h = getHeight(x, y);
                int grey;
                int molten = 0;
                Tile tile = SILICATES;

                if (planet.hasFeature(Hot)) {
                    grey = Math.max(5, 80 - h);
                    tile = HOT;
                } else if (planet.hasFeature(MoltenMetals)) {
                    grey = Math.max(5, 100 - h);
                    tile = HOT;
                } else if (planet.hasFeature(Molten)) {
                    grey = Math.max(5, 100 - h);
                    tile = MOLTEN;
                    molten = 25;
                } else {
                    grey = baseGrey + h / 2;
                }

                if (h > molten) {
                    setTile(x, y, new Rough(tile.getShaded(grey)));
                } else {
                    setTile(x, y, MOLTEN);
                    setHeight(x, y, molten);
                }
            }
        }

        // Larger asteroids tend to be more spherical.
        int heightDividor = 3;
        if (planet.getRadius() > 240) {
            heightDividor = 1 + (int) (planet.getRadius() / 80);
        }
        if (planet.hasFeature(MoltenMetals)) {
            heightDividor += 3;
        } else if (planet.hasFeature(Molten)) {
            heightDividor += 10;
        }

        if (planet.hasFeature(Vulcanian.VulcanianFeatures.MagneticFields)) {
            hasCloudMap = true;
        }

        // After finishing with the height map, set it to more consistent values
        // so that the bump mapper can use it cleanly.
        smoothHeights(planet, heightDividor);
    }

    public List<SimpleImage> drawClouds(int width) {
        List<SimpleImage>  clouds = new ArrayList<>();

        SimpleImage cloud = new SimpleImage(width, width / 2);

        cloudHeight = 25;
        for (int y = 0; y < cloud.getHeight(); y++) {
            //double yd = Math.pow(1 - Math.abs(width / 4.0 - y) / (width / 4.0), 2);
            for (int x = 0; x < cloud.getWidth(); x++) {
                int    cycle = width / 8;
                double density = Math.sqrt(Math.sin((2.0 * Math.PI * (x % cycle)) / cycle));

                if (Die.d100() < (100 * density) && Die.d4() == 1) {
                    cloud.dot(x, y, "#FE9090");
                }
            }
        }
        clouds.add(cloud);

        return clouds;
    }
}
