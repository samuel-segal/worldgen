/*
 * Copyright (c) 2018, Samuel Penn (sam@glendale.org.uk).
 * See the file LICENSE at the root of the project.
 */

package uk.org.glendale.worldgen.astro.planets.generators.smallbody;

import uk.org.glendale.utils.rpg.Die;
import uk.org.glendale.worldgen.WorldGen;
import uk.org.glendale.worldgen.astro.commodities.CommodityName;
import uk.org.glendale.worldgen.astro.planets.Planet;
import uk.org.glendale.worldgen.astro.planets.PlanetFeature;
import uk.org.glendale.worldgen.astro.planets.codes.Atmosphere;
import uk.org.glendale.worldgen.astro.planets.codes.PlanetType;
import uk.org.glendale.worldgen.astro.planets.codes.Temperature;
import uk.org.glendale.worldgen.astro.planets.generators.SmallBody;
import uk.org.glendale.worldgen.astro.stars.Star;
import uk.org.glendale.worldgen.astro.systems.StarSystem;

import static uk.org.glendale.worldgen.astro.commodities.CommodityName.*;

/**
 * A member of the Cometary Class, a Kuiper object is generally a dirty snowball in a stable orbit
 * in a system's Kuiper Belt. They are similar to, but dirtier than, Oort objects.
 */
public class Kuiper extends SmallBody {

    public Kuiper(WorldGen worldgen, StarSystem system, Star star, Planet previous, long distance) {
        super(worldgen, system, star, previous, distance);
    }

    public Planet getPlanet(String name) {
        return getPlanet(name, PlanetType.Kuiper);
    }

    public Planet getPlanet(String name, PlanetType type) {
        Planet planet =  definePlanet(name, type);
        planet.setRadius(getRadius(planet));
        planet.setTemperature(planet.getTemperature() + Die.dieV(4));

        // Set default day length to be 2-3 hours.
        planet.setDayLength(3600 + Die.die(3600, 2));

        planet.setPressure(0);
        planet.setAtmosphere(Atmosphere.Vacuum);

        addPrimaryResource(planet, Water);
        addTertiaryResource(planet, CommodityName.OrganicChemicals);

        switch (Die.d8()) {
            case 1: case 2: case 3:
                addTraceResource(planet, ExoticCrystals);
                break;
            default:
                // Nothing special.
        }

        return planet;
    }
}
