/*
 * Copyright (c) 2018, Samuel Penn (sam@glendale.org.uk).
 * See the file LICENSE at the root of the project.
 */
package uk.org.glendale.worldgen.astro.planets.tiles;

import uk.org.glendale.utils.graphics.SimpleImage;
import uk.org.glendale.utils.graphics.Tile;
import uk.org.glendale.utils.rpg.Die;

/**
 * Generates a dappled pattern on the tiles when they are displayed, giving a rougher
 * look to the surface.
 */
public class Rough extends Tile {

    public Rough(Tile tile) {
        super(tile.getName(), tile.getRGB(), tile.isWater(), tile.getRandom());
    }

    public void addDetail(SimpleImage image, int x, int y, int w, int h) {
        textured(image, x, y, w, h);
    }
}
