/*
 * Copyright (c) 2017, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */

package uk.org.glendale.worldgen.astro.planets.maps.terrestrial;

import uk.org.glendale.utils.graphics.Icosahedron;
import uk.org.glendale.utils.graphics.SimpleImage;
import uk.org.glendale.utils.graphics.Tile;
import uk.org.glendale.utils.rpg.Die;
import uk.org.glendale.worldgen.astro.commodities.Commodity;
import uk.org.glendale.worldgen.astro.commodities.CommodityName;
import uk.org.glendale.worldgen.astro.commodities.Frequency;
import uk.org.glendale.worldgen.astro.planets.Planet;
import uk.org.glendale.worldgen.astro.planets.PlanetFeature;
import uk.org.glendale.worldgen.astro.planets.codes.Atmosphere;
import uk.org.glendale.worldgen.astro.planets.codes.Life;
import uk.org.glendale.worldgen.astro.planets.codes.PlanetType;
import uk.org.glendale.worldgen.astro.planets.generators.Terrestrial;
import uk.org.glendale.worldgen.astro.planets.generators.dwarf.Arean;
import uk.org.glendale.worldgen.astro.planets.generators.dwarf.EuArean;
import uk.org.glendale.worldgen.astro.planets.generators.terrestrial.EuGaian;
import uk.org.glendale.worldgen.astro.planets.maps.PlanetMapper;
import uk.org.glendale.worldgen.astro.planets.maps.TerrestrialMapper;
import uk.org.glendale.worldgen.astro.planets.maps.dwarf.MesoAreanMapper;
import uk.org.glendale.worldgen.astro.planets.tiles.Rough;
import uk.org.glendale.worldgen.text.TextGenerator;
import uk.org.glendale.worldgen.web.Server;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static uk.org.glendale.worldgen.astro.planets.generators.Terrestrial.TerrestrialFeature.*;

public class EuGaianMapper extends TerrestrialMapper {
    public EuGaianMapper(final Planet planet) {
        super(planet, DEFAULT_FACE_SIZE);
    }

    protected static final Tile MESOGAIAN = new Tile("Land", "#604030", false, 3);
    protected static final Tile SANDY = new Tile("Land", "#806050", false, 3);
    protected static final Tile VOLCANO = new Tile("Volcano", "#F05050", false, 3);

    private void generateFeatures(List<PlanetFeature> features) {
    }

    public void generate() {
        super.generate();

        if (planet.hasFeature(CyanSeas)) {
            WATER.rgb("#00F0E0");
        }
        if (planet.hasFeature(VolcanicFlats, Volcanoes)) {
            LAND_ICE.rgb("#E0E0E0");
            SEA_ICE.rgb("#E0E0F0");
            SANDY.rgb("#101010");
        }

        if (planet.hasFeature(FractalIslands)) {
            setWater(WATER);
            int height = getSeaLevel(planet.getHydrographics());
            for (int y=0; y < getNumRows(); y++) {
                for (int x = 0; x < getWidthAtY(y); x++) {
                    if (getHeight(x, y) >= height) {
                        setTile(x, y, LAND);
                    }
                }
            }
        } else {
            setWater();

            if (planet.hasFeature(ManyIslands)) {
                createContinents(12 + Die.d6(3));
            } else {
                createContinents(6 + Die.d6());
            }
        }


        for (int y=0; y < getNumRows(); y++) {
            for (int x=0; x < getWidthAtY(y); x++) {
                if (getTile(x, y).isWater()) {
                    setTile(x, y, getTile(x, y).getShaded((getHeight(x, y) + 200) / 3));
                    setHeight(x, y, 0);
                } else {
                    if (getHeight(x, y) > 75 && planet.hasFeature(Volcanoes) && Die.d10() == 1) {
                        setTile(x, y, new Rough(VOLCANO));
                    } else if (getHeight(x, y) > 50) {
                        setTile(x, y, new Rough(MESOGAIAN.getShaded((getHeight(x, y) + 100) / 2)));
                    } else {
                        setTile(x, y, new Rough(SANDY.getShaded((getHeight(x, y) + 100) / 2)));
                    }
                }
            }
        }

        createCraters(0, 20);
        generateFeatures(planet.getFeatures());
        setIceCaps(getIce(), getIce());
        generateWetness();
        generateBiomes();
        cleanBumpMap();

        // Mark world as having clouds.
        hasCloudMap = true;
        hasHeightMap = true;
    }

    /**
     * Lower cloud layer is almost completely opaque.
     */
    private SimpleImage drawLowerCloudLayer(int width) throws IOException {
        Icosahedron cloud = getCloudLayer();

        String cloudColour = "#FEFEFE";
        int lowerLimit = 45;

        for (int y=0; y < cloud.getNumRows(); y++) {
            for (int x=0; x < cloud.getWidthAtY(y); x++) {
                int h = cloud.getHeight(x, y);
                if (h < lowerLimit) {
                    cloud.setHeight(x, y, 0);
                } else {
                    cloud.setHeight(x, y, h);
                }
            }
        }

        return Icosahedron.stretchImage(cloud.drawTransparency(cloudColour, width), width);
    }

    public List<SimpleImage> drawClouds(int width) {
        List<SimpleImage>  clouds = new ArrayList<>();

        cloudHeight = 5;

        try {
            clouds.add(drawLowerCloudLayer(width));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return clouds;
    }

    public static void main(String[] args) throws IOException {
        Planet planet = new Planet();
        planet.setName("Cyclone");
        planet.setType(PlanetType.EuGaian);
        planet.setTemperature(295);
        planet.setAtmosphere(Atmosphere.Standard);
        planet.setPressure(150_000);
        planet.setHydrographics(90);
//        planet.addFeature(Volcanoes);
//        planet.addFeature(FractalIslands);

        planet.setLife(Life.Extensive);
        planet.addResource(new Commodity() {{
            setName(CommodityName.Woods.getName());
            setFrequency(Frequency.COMMON);
        }}, 800);
        planet.addResource(new Commodity() {{
            setName(CommodityName.Grasses.getName());
            setFrequency(Frequency.COMMON);
        }}, 800);
        planet.addResource(new Commodity() {{
            setName(CommodityName.Shrubs.getName());
            setFrequency(Frequency.COMMON);
        }}, 500);

        testOutput(planet, new EuGaian(Server.getWorldGen(), null, null, null, 0),
                new EuGaianMapper(planet));
    }
}
