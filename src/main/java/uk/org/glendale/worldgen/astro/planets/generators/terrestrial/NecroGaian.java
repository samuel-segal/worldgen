/*
 * Copyright (c) 2018, Samuel Penn (sam@glendale.org.uk).
 * See the file LICENSE at the root of the project.
 */

package uk.org.glendale.worldgen.astro.planets.generators.terrestrial;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.org.glendale.utils.rpg.Die;
import uk.org.glendale.worldgen.WorldGen;
import uk.org.glendale.worldgen.astro.planets.Planet;
import uk.org.glendale.worldgen.astro.planets.codes.Atmosphere;
import uk.org.glendale.worldgen.astro.planets.codes.Life;
import uk.org.glendale.worldgen.astro.planets.codes.PlanetType;
import uk.org.glendale.worldgen.astro.planets.generators.Terrestrial;
import uk.org.glendale.worldgen.astro.stars.Star;
import uk.org.glendale.worldgen.astro.systems.StarSystem;
import uk.org.glendale.worldgen.text.TextGenerator;

import static uk.org.glendale.worldgen.astro.commodities.CommodityName.*;

/**
 * NecroGaian worlds are Tectonic Terrestrial worlds which have failed. Often this is because some
 * event in their history wiped out all life - the most common events are extreme glaciation or
 * life not surviving the oxygenation of the atmosphere. The planet's temperature and pressure is
 * fine, there just isn't any life.
 */
public class NecroGaian extends Terrestrial {
    private static final Logger logger = LoggerFactory.getLogger(NecroGaian.class);

    public NecroGaian(WorldGen worldgen, StarSystem system, Star star, Planet previous, long distance) {
        super(worldgen, system, star, previous, distance);
    }

    public Planet getPlanet(String name) {
        Planet planet = definePlanet(name, PlanetType.NecroGaian);

        setTerrestrialProperties(planet);
        planet.setHabitability(3);

        // Less land than other Gaian types. Life increases land formation.
        planet.setNightTemperature(planet.getTemperature() - Die.d6(2));
        planet.setAtmosphere(Atmosphere.Nitrogen);
        planet.setHydrographics(65 + Die.d12(2));
        planet.setLife(Life.None);

        if (planet.getTemperature() < 250) {
            planet.addFeature(TerrestrialFeature.IceWorld);
        } else if (planet.getTemperature() < 275) {
            planet.addFeature(TerrestrialFeature.IceWorld);
        } else if (planet.getTemperature() > 320) {
            int k = planet.getTemperature();
            planet.setHydrographics(65 + Die.d12(2) - (k - 290));
            if (planet.getHydrographics() < 1) {
                planet.addFeature(TerrestrialFeature.Dry);
            }
            if  (k > 700) {
                planet.setPressure(planet.getPressure() / 25);
            } else if (k > 500) {
                planet.setPressure(planet.getPressure() / 10);
            } else if (k > 400) {
                planet.setPressure(planet.getPressure() / 5);
            } else if (k > 350) {
                planet.setPressure(planet.getPressure() / 2);
            }
        }

        addTertiaryResource(planet, OrganicChemicals);

        if (planet.getHydrographics() < 30) {
            planet.addFeature(TerrestrialFeature.Dry);
        }

        addFeatures(planet);

        // Define resources for this world.
        addSecondaryResource(planet, SilicateOre);
        addSecondaryResource(planet, SilicateCrystals);
        addSecondaryResource(planet, CarbonicOre);
        addTertiaryResource(planet, FerricOre);

        addPrimaryResource(planet, Water);

        TextGenerator text = new TextGenerator(planet);
        planet.setDescription(text.getFullDescription());

        return planet;
    }

    private void addFeatures(Planet planet) {
    }
}
