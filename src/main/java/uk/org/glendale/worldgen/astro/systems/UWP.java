package uk.org.glendale.worldgen.astro.systems;

import uk.org.glendale.worldgen.astro.planets.codes.Atmosphere;
import uk.org.glendale.worldgen.astro.planets.codes.Government;
import uk.org.glendale.worldgen.astro.planets.codes.Pressure;
import uk.org.glendale.worldgen.astro.planets.codes.StarPort;
import uk.org.glendale.worldgen.astro.sectors.SubSector;
import uk.org.glendale.worldgen.astro.stars.Luminosity;
import uk.org.glendale.worldgen.astro.stars.SpectralType;
import uk.org.glendale.worldgen.astro.stars.Star;

import java.util.ArrayList;
import java.util.List;

public class UWP {
    private final String line;
    private final String uwp;

    public UWP(String line) {
        this.line = line;
        this.uwp = line.substring(19, 28);
    }

    public String getLine() {
        return line;
    }

    public String getUWP() {
        return uwp;
    }

    public String getName() {
        String name = line.substring(0, 14).trim();

        if (name.length() == 0) {
            int x = getX();
            int y = getY();
            name = SubSector.getSubSector(x, y) + "-" + line.substring(14, 18);
        }
        return name;
    }

    public int getX() {
        String coord = line.substring(14, 18);
        return StarSystemFactory.getXCoord(coord);
    }

    public int getY() {
        String coord = line.substring(14, 18);
        return StarSystemFactory.getYCoord(coord);
    }

    public StarPort getStarPort() {
        return StarPort.valueOf(uwp.substring(0, 1));
    }

    public int getSize() {
        return Integer.parseInt(uwp.substring(1, 2), 16) * 1600;
    }

    public Atmosphere getAtmosphere() {
        Atmosphere atmosphere = Atmosphere.Vacuum;
        int code = Integer.parseInt(uwp.substring(2, 3), 16);

        switch (code) {
            case 0:
                atmosphere = Atmosphere.Vacuum;
                break;
            case 10: case 15:
                atmosphere = Atmosphere.Exotic;
                break;
            case 11:
                atmosphere = Atmosphere.Chlorine;
                break;
            case 12:
                atmosphere = Atmosphere.Hydrogen;
                break;
            default:
                atmosphere = Atmosphere.Standard;
                break;
        }

        return atmosphere;
    }

    public Pressure getPressure() {
        Pressure pressure = Pressure.None;
        int code = Integer.parseInt(uwp.substring(2, 3), 16);

        switch (code) {
            case 0:
                pressure = Pressure.None;
                break;
            case 1:
                pressure = Pressure.Trace;
                break;
            case 2: case 3:
                pressure = Pressure.VeryThin;
                break;
            case 4: case 5:
                pressure = Pressure.Thin;
                break;
            case 8: case 9:
                pressure = Pressure.Dense;
                break;
            case 12: case 13:
                pressure = Pressure.VeryDense;
                break;
            case 14:
                pressure = Pressure.Thin;
                break;
            default:
                pressure = Pressure.Standard;
                break;
        }

        return pressure;
    }

    public int getHydrosphere() {
        return Integer.parseInt(uwp.substring(3, 4), 16) * 10;
    }

    public long getPopulation() {
        int code = Integer.parseInt(uwp.substring(4, 5), 16);
        int p = Integer.parseInt(line.substring(51, 52));

        if (code == 0) {
            return 0;
        } else if (code == 1) {
            return p;
        }
        return (long) Math.pow(10, code) * p;
    }

    public Government getGovernment() {
        Government government = Government.Anarchy;
        int code = Integer.parseInt(uwp.substring(5, 6), 16);

        switch (code) {
            case 0:
                government = Government.Anarchy;
                break;
            case 1:
                government = Government.Corporation;
                break;
            case 2:
                government = Government.ParticipatingDemocracy;
                break;
            case 3:
                government = Government.SelfPerpetuatingOligarchy;
                break;
            case 4:
                government = Government.RepresentativeDemocracy;
                break;
            case 5:
                government = Government.FeudalTechnocracy;
                break;
            case 6:
                government = Government.Captive;
                break;
            case 7:
                government = Government.Balkanization;
                break;
            case 8:
                government = Government.CivilService;
                break;
            case 9:
                government = Government.ImpersonalBureaucracy;
                break;
            case 10:
                government = Government.CharismaticLeader;
                break;
            case 11:
                government = Government.NonCharismaticLeader;
                break;
            case 12:
                government = Government.CharismaticOligarchy;
                break;
            case 13:
                government = Government.TheocraticDictatorship;
                break;
            case 14:
                government = Government.TheocraticOligarchy;
                break;
            case 15:
                government = Government.TotalitarianOligarchy;
                break;
        }
        return government;
    }

    public int getLawLevel() {
        return Integer.parseInt(uwp.substring(6, 7), 16);
    }

    public int getTechLevel() {
        return Integer.parseInt(uwp.substring(8, 9), 16);
    }

    public String[] getStars() {
        List<Star> stars = new ArrayList<>();

        return line.substring(58).split(" ");
    }

    public int getOtherWorlds() {
        return Integer.parseInt(line.substring(51, 52));
    }

    public int getBelts() {
        return Integer.parseInt(line.substring(52, 53));
    }

    public int getGasGiants() {
        return Integer.parseInt(line.substring(53, 54));
    }

    public static void main(String[] args) {
        UWP uwp = new UWP("Narval        0805 D525688-7  M Ni Da           A  603 Cz G4 V M6 V");

        System.out.println(uwp.getPopulation());
        System.out.println(uwp.getTechLevel());
    }
}
