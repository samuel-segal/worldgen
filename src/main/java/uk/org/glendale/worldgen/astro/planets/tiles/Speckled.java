/*
 * Copyright (c) 2018, Samuel Penn (sam@glendale.org.uk).
 * See the file LICENSE at the root of the project.
 */
package uk.org.glendale.worldgen.astro.planets.tiles;

import uk.org.glendale.utils.graphics.SimpleImage;
import uk.org.glendale.utils.graphics.Tile;
import uk.org.glendale.utils.rpg.Die;

/**
 * Adds a coloured speckling to a tile. The proportion of speckles is controlled by the cover value,
 * which should be between 0 (no speckles) to 100 (full colour).
 */
public class Speckled extends Tile {
    private String  colour = "#FEFEFE";
    private int     cover = 50;

    public Speckled(Tile tile) {
        super(tile);
    }

    public Speckled colour(final String colour) {
        this.colour = colour;

        return this;
    }

    public Speckled cover(final int percentage) {
        this.cover = percentage;

        return this;
    }

    /**
     * Called to modify the tile image after it has been drawn on the map.
     *
     * @param image     Entire map image to be drawn on.
     * @param x         X position (in pixels) of tile centre.
     * @param y         Y position (in pixels) of tile centre.
     * @param w         Width of this tile (pixels).
     * @param h         Height of this tile (pixels).
     */
    public void addDetail(SimpleImage image, int x, int y, int w, int h) {
        int height = Math.abs(h);
        for (int yy = 0; yy <= height; yy++) {
            int width = (int) ((1.0 * w * (height - yy)) / (1.0 * height));
            for (int xx = -width; xx <= width; xx++) {
                if (Die.d100() <= cover) {
                    image.dot(x + xx + w, y + (int)(Math.signum(h) * yy), colour);
                }
            }
        }
    }
}
