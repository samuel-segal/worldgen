/*
 * Copyright (c) 2018, Samuel Penn (sam@glendale.org.uk).
 * See the file LICENSE at the root of the project.
 */
package uk.org.glendale.worldgen.astro.planets.generators.belt;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.org.glendale.utils.rpg.Die;
import uk.org.glendale.worldgen.WorldGen;
import uk.org.glendale.worldgen.astro.Physics;
import uk.org.glendale.worldgen.astro.planets.Planet;
import uk.org.glendale.worldgen.astro.planets.codes.PlanetType;
import uk.org.glendale.worldgen.astro.planets.generators.Belt;
import uk.org.glendale.worldgen.astro.stars.Star;
import uk.org.glendale.worldgen.astro.systems.StarSystem;

import static uk.org.glendale.worldgen.astro.commodities.CommodityName.Water;
import static uk.org.glendale.worldgen.astro.planets.generators.Belt.BeltFeature.ThinRing;
import static uk.org.glendale.worldgen.astro.planets.generators.Belt.BeltFeature.WideRing;

/**
 * A Rock Ring is a type of planetary ring which is found around planets, especially Jovian worlds.
 * It consists of large chunks of rock, often metres in diameter, and are probably due to the breakup
 * of larger bodies such as moons or large asteroids.
 */
public class RockRing extends Belt {
    private static final Logger logger = LoggerFactory.getLogger(RockRing.class);

    public RockRing(WorldGen worldgen, StarSystem system, Star star, Planet previous, long distance) {
        super(worldgen, system, star, previous, distance);
    }

    private void addFeatures(Planet planet) {
        switch (Die.d6(2)) {
            case 2: case 3: case 4:
                planet.addFeature(ThinRing);
                break;
            case 10: case 11: case 12:
                planet.addFeature(WideRing);
                break;
        }
    }

    public Planet getPlanet(String name) {
        Planet planet = definePlanet(name, PlanetType.RockRing);
        long radius = 5_000 + Die.die(2_000, 3);
        int density = 2_000 + Die.dieV(500);

        if (planet.getFeatures().size() != 0) {
            addFeatures(planet);
        }

        if (planet.hasFeature(ThinRing)) {
            radius /= 3;
            density /= 2;
        } else if (planet.hasFeature(WideRing)) {
            radius *= 5;
        }

        if (parentPlanet != null) {
            planet.setTemperature(Physics.getTemperatureOfOrbit(star, parentPlanet.getDistance()));
        } else {
            planet.setTemperature(Physics.getTemperatureOfOrbit(star, distance));
        }
        planet.setNightTemperature(planet.getTemperature());

        addFeatures(planet);

        radius = checkDistance(planet, radius);

        planet.setRadius(radius);
        planet.setDensity(density);

        addPrimaryResource(planet, Water);

        return planet;
    }
}
