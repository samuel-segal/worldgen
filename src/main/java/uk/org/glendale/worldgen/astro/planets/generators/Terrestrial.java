/*
 * Copyright (c) 2018, Samuel Penn (sam@glendale.org.uk).
 * See the file LICENSE at the root of the project.
 */

package uk.org.glendale.worldgen.astro.planets.generators;

import uk.org.glendale.utils.rpg.Die;
import uk.org.glendale.worldgen.WorldGen;
import uk.org.glendale.worldgen.astro.Physics;
import uk.org.glendale.worldgen.astro.commodities.CommodityName;
import uk.org.glendale.worldgen.astro.planets.*;
import uk.org.glendale.worldgen.astro.planets.codes.*;
import uk.org.glendale.worldgen.astro.stars.Star;
import uk.org.glendale.worldgen.astro.systems.StarSystem;
import uk.org.glendale.worldgen.astro.systems.StarSystemFactory;
import uk.org.glendale.worldgen.exceptions.UnsupportedException;

import java.util.ArrayList;
import java.util.List;

import static uk.org.glendale.worldgen.astro.commodities.CommodityName.*;

/**
 * The Terrestrial group contains Earth-sized terrestrial worlds, and include the various Gaian
 * world types which are most likely to harbour life.
 */
public class Terrestrial extends PlanetGenerator {

    public enum TerrestrialFeature implements PlanetFeature {
        EarlyStage,
        LateStage,
        Small,
        Large,
        Dense,
        Light,
        VolcanicFlats,
        Volcanoes,
        Pangaea,
        EquatorialOcean,
        FractalIslands,
        ManyIslands,
        RedIce,
        BacterialMats,
        BorderedInBlack,
        BorderedInGreen,
        MoltenSurface,
        MoltenMetals,
        MetallicSea,
        CyanSeas,
        Cold,
        Warm,
        IceWorld,
        Dry,
        Wet;
    }

    public Terrestrial(WorldGen worldgen, StarSystem system, Star star, Planet previous, long distance) {
        super(worldgen, system, star, previous, distance);
    }

    /**
     * Get a generated planet. Can't be called directly on the Terrestrial class, because
     * we don't know exactly what type of planet to create.
     *
     * Call getPlanet(String, PlanetType) instead.
     *
     * @param name  Name of planet to be generated.
     * @return      Always throws UnsupportedException().
     */
    public Planet getPlanet(String name) {
        throw new UnsupportedException("Must define planet type");
    }

    @Override
    public Planet getPlanet(String name, PlanetType type) {
        Planet planet = definePlanet(name, type);
        planet.setRadius(Die.die(1000, 2) + 5500);

        return planet;
    }

    protected void setTerrestrialProperties(Planet planet) {
        planet.setRadius(5500 + Die.die(1000, 2));
        planet.setAtmosphere(Atmosphere.Primordial);
        planet.setDayLength(20 * 3600 + Die.die(7200, 4));

        // Check for random features.
        if (planet.getFeatures().size() == 0) {
            switch (Die.d20()) {
                case 1:
                    planet.addFeature(TerrestrialFeature.Small);
                    break;
                case 20:
                    planet.addFeature(TerrestrialFeature.Large);
            }
            switch (Die.d20()) {
                case 1:
                    planet.addFeature(TerrestrialFeature.Light);
                    break;
                case 20:
                    planet.addFeature(TerrestrialFeature.Dense);
            }
        }

        if (planet.hasFeature(TerrestrialFeature.Light)) {
            planet.setDensity((int) (planet.getDensity() * 0.9));
        } else if (planet.hasFeature(TerrestrialFeature.Dense)) {
            planet.setDensity((int) (planet.getDensity() * 1.1));
        }
        if (planet.hasFeature(TerrestrialFeature.Large)) {
            planet.setRadius((int) (planet.getRadius() * 1.3));
        } else if (planet.hasFeature(TerrestrialFeature.Small)) {
            planet.setRadius((int) (planet.getRadius() * 0.67));
        }

        // Atmospheric pressure is most likely to be Standard. Modified slightly by planet size.
        int pressure = (int) (Math.sqrt(planet.getRadius()) * (1_150 + Die.d100(3)));
        int temperature = planet.getTemperature();
        if (pressure > Physics.STANDARD_PRESSURE * 2.0) {
            temperature *= 1.10;
        } else if (pressure > Physics.STANDARD_PRESSURE * 1.5) {
            temperature *= 1.07;
        } else if (pressure > Physics.STANDARD_PRESSURE * 1.2) {
            temperature *= 1.05;
        } else if (pressure > Physics.STANDARD_PRESSURE * 0.8) {
            temperature *= 1.03;
        } else if (pressure > Physics.STANDARD_PRESSURE * 0.4) {
            temperature *= 1.02;
        } else {
            temperature *= 1.01;
        }
        planet.setTemperature(temperature);
        planet.setPressure(pressure);

        // Magnetic field is likely to be weak or standard.
        switch (Die.d6(2)) {
            case 2:
                planet.setMagneticField(MagneticField.Minimal);
                break;
            case 3: case 4:
                planet.setMagneticField(MagneticField.VeryWeak);
                break;
            case 5: case 6: case 7:
                planet.setMagneticField(MagneticField.Weak);
                break;
            case 8: case 9: case 10: case 11: case 12:
                planet.setMagneticField(MagneticField.Standard);
                break;
        }

        addPrimaryResource(planet, SilicateOre);
        addTertiaryResource(planet, SilicateCrystals);
        addSecondaryResource(planet, CarbonicOre);
        addTraceResource(planet, CarbonicCrystals);
        addTertiaryResource(planet, FerricOre);

        if (planet.hasFeature(TerrestrialFeature.Dense)) {
            addTertiaryResource(planet, FerricOre);
            addTraceOrTertiary(planet, HeavyMetals);
            addTraceOrTertiary(planet, Radioactives);
            addTraceOrTertiary(planet, RareMetals);
            addTraceOrTertiary(planet, PreciousMetals);
        } else if (planet.hasFeature(TerrestrialFeature.Light)) {
            addTraceOrTertiary(planet, FerricOre);
            addTraceMaybe(planet, HeavyMetals);
            addTraceMaybe(planet, Radioactives);
            addTraceMaybe(planet, RareMetals);
            addTraceMaybe(planet, PreciousMetals);
        } else {
            addTertiaryResource(planet, FerricOre);
            addTraceResource(planet, HeavyMetals);
            addTraceResource(planet, Radioactives);
            addTraceResource(planet, RareMetals);
            addTraceResource(planet, PreciousMetals);
        }
    }

    private void addTraceOrTertiary(Planet planet, CommodityName resource) {
        if (Die.d2() == 1) {
            addTertiaryResource(planet, resource);
        } else {
            addTraceResource(planet, resource);
        }
    }

    private void addTraceMaybe(Planet planet, CommodityName resource) {
        if (Die.d2() == 1) {
            addTraceResource(planet, resource);
        }
    }

    public List<Planet> getMoons(Planet primary) {
        List<Planet> moons = new ArrayList<>();

        if (primary.hasFeature(GeneralFeature.NoMoons, GeneralFeature.FewMoons)) {
            return moons;
        }

        Planet  moon = new Planet();
        moon.setMoonOf(primary.getId());
        moon.setName(primary.getName() + "a");
        moon.setType(PlanetType.Selenian);

        switch (Die.d6()) {
            case 1: case 2: case 3:
                // No moons.
                break;
            case 4: case 5:
                moon.setDistance((150 + Die.die(50, 2)) * 1000 + Die.die(1000));
                moon.addFeature(MoonFeature.SmallMoon);
                moon.addFeature(MoonFeature.TidallyLocked);
                moons.add(moon);
                break;
            case 6:
                moon.setDistance((300 + Die.die(100, 2)) * 1000 + Die.die(1000));
                if (Die.d3() == 1) {
                    moon.addFeature(MoonFeature.AlmostLocked);
                } else {
                    moon.addFeature(MoonFeature.TidallyLocked);
                }
                moons.add(moon);
                break;
        }

        return moons;
    }

    /**
     * Set organic resources for a world with a life level of None. These will have just basic organic chemicals,
     * with possibly
     * @param planet    Planet to set resources on.
     */
    private void lifeNone(Planet planet) {
        switch (Die.d6()) {
            case 1: case 2: case 3:
                // No organic chemicals.
                break;
            case 4: case 5:
                // Trace amounts of organics.
                addTraceResource(planet, OrganicChemicals);
                break;
            case 6:
                // Some basic organics. Not enough to count as 'Organic'.
                addTertiaryResource(planet, OrganicChemicals);
                break;
        }
    }

    /**
     * Set organic resources for a world with a life level of Organic. They will be rich in the precursors to
     * life, and in their later stages may have formed basic uni-cellular organisms, though they are not yet
     * widespread enough for the world to be considered 'Archaean'.
     *
     * @param planet    Planet to set resources on.
     */
    private void lifeOrganic(Planet planet) {
        switch (Die.d6(2)) {
            case 2: case 3: case 4: case 5:
                // None: Just simple organic chemicals.
                addSecondaryResource(planet, OrganicChemicals);
                break;
            case 6: case 7: case 8: case 9:
                // None: Just simple organic chemicals.
                addPrimaryResource(planet, OrganicChemicals);
                break;
            case 10: case 11:
                addPrimaryResource(planet, OrganicChemicals);
                addTraceResource(planet, Prokaryotes);
                break;
            case 12:
                addPrimaryResource(planet, OrganicChemicals);
                addTertiaryResource(planet, Prokaryotes);
                break;
        }
    }

    /**
     * Archaean worlds have basic life, but haven't evolved into oxygen breathing life forms yet. The formation
     * of Cyanobacteria, which begins the process of oxygen generation, is relatively rapid when it does occur.
     *
     * @param planet    Planet to set resources on.
     */
    private void lifeArchaean(Planet planet) {
        addSecondaryResource(planet, OrganicChemicals);
        addPrimaryResource(planet, Prokaryotes);

        switch (Die.d6(2)) {
            case 2: case 3: case 4: case 5: case 6:
                addSecondaryResource(planet, Prokaryotes);
                break;
            case 7: case 8: case 9:
                addPrimaryResource(planet, Prokaryotes);
                if (Die.d4() == 1) {
                    planet.addFeature(TerrestrialFeature.BacterialMats);
                }
                break;
            case 10: case 11:
                addPrimaryResource(planet, Prokaryotes);
                addTraceResource(planet, Algae);
                break;
            case 12:
                addPrimaryResource(planet, Prokaryotes);
                if (Die.d4() == 1) {
                    if (Die.d3() == 1) {
                        planet.addFeature(TerrestrialFeature.BorderedInBlack);
                    } else {
                        planet.addFeature(TerrestrialFeature.BorderedInGreen);
                    }
                    addPrimaryResource(planet, Algae);
                } else {
                    addSecondaryResource(planet, Algae);
                }
                break;
        }
    }

    /**
     * Aerobic worlds have a significant amount of Oxygen in their atmospheres.
     *
     * @param planet    Planet to set resources on.
     */
    private void lifeAerobic(Planet planet) {
        addPrimaryResource(planet, Prokaryotes);

        switch (Die.d6(2)) {
            case 2:
                addTertiaryResource(planet, Protozoa);
                break;
            case 3:
                addSecondaryResource(planet, Protozoa);
                break;
            case 4:
                addPrimaryResource(planet, Protozoa);
                addTertiaryResource(planet, Metazoa);
                break;
            case 5:
                addPrimaryResource(planet, Protozoa);
                addSecondaryResource(planet, Metazoa);
                break;
            case 6: case 7:
                addPrimaryResource(planet, Metazoa);
                addSecondaryResource(planet, Protozoa);
                addTertiaryResource(planet, Algae);

                if (Die.d3() == 1) addTraceResource(planet, Sponges);
                if (Die.d6() == 1) addTraceResource(planet, Molluscs);
                if (Die.d6() == 1) addTraceResource(planet, Crawlers);
                if (Die.d4() == 1) addTraceResource(planet, Jellies);
                if (Die.d3() == 1) addTraceResource(planet, Kelp);
                break;
            case 12:
                addTraceResource(planet, Fish);
                // Fall through.
            default:
                addPrimaryResource(planet, Metazoa);
                addTertiaryResource(planet, Protozoa);
                addSecondaryResource(planet, Algae);
                if (Die.d2() == 1) addTertiaryResource(planet, Sponges);
                if (Die.d6() == 1) addTertiaryResource(planet, Molluscs);
                if (Die.d4() == 1) addTertiaryResource(planet, Crawlers);
                if (Die.d3() == 1) addTertiaryResource(planet, Jellies);

                // Determine primary life type.
                switch (Die.d4(2)) {
                    case 2:
                        addSecondaryResource(planet, Molluscs);
                        break;
                    case 3: case 4:
                        addSecondaryResource(planet, Sponges);
                        break;
                    case 5: case 6:
                        addSecondaryResource(planet, Jellies);
                        break;
                    case 7: case 8:
                        addSecondaryResource(planet, Crawlers);
                        break;
                }
                if (Die.d3() == 1) {
                    addSecondaryResource(planet, Kelp);
                } else {
                    addTertiaryResource(planet, Kelp);
                }
                break;
        }
    }

    /**
     * Aerobic worlds have a significant amount of Oxygen in their atmospheres.
     *
     * @param planet    Planet to set resources on.
     */
    private void lifeComplexOcean(Planet planet) {
        addTertiaryResource(planet, Algae);
        addTertiaryResource(planet, Metazoa);
        addTraceResource(planet, Protozoa);
        addTraceResource(planet, Sponges);

        switch (Die.d6(2)) {
            case 2: case 3: case 4: case 5:
                addSecondaryResource(planet, Fish);
                addSecondaryResource(planet, Molluscs);
                addSecondaryResource(planet, Crawlers);
                addSecondaryResource(planet, Jellies);
                addSecondaryResource(planet, Metazoa);
                addTertiaryResource(planet, Protozoa);
                break;
            case 6: case 7: case 8:
                addPrimaryResource(planet, Fish);
                addTertiaryResource(planet, Kelp);
                break;
            case 9: case 10:
                addPrimaryResource(planet, Fish);
                addTertiaryResource(planet, Crawlers);
                addTertiaryResource(planet, Kelp);
                break;
            case 11: case 12:
                addPrimaryResource(planet, Fish);
                addSecondaryResource(planet, Crawlers);
                addTertiaryResource(planet, Kelp);
                if (Die.d2() == 1) {
                    addTraceResource(planet, Fungus);
                }
                break;
        }
    }


    /**
     * Set the organic resources found on Terrestrial worlds, given their life level.
     *
     * @param planet    Generate organic resources for this planet
     */
    public void generateLife(Planet planet) {
        switch (planet.getLife()) {
            case None:
                lifeNone(planet);
                break;
            case Organic:
                lifeOrganic(planet);
                break;
            case Archaean:
                lifeArchaean(planet);
                break;
            case Aerobic:
                lifeAerobic(planet);
                break;
            case ComplexOcean:
                lifeComplexOcean(planet);
                break;
        }
    }

    /**
     * Generate a list of moons for this terrestrial world.
     *
     * @param primary   Planet which the moons orbit.
     * @param factory   Factory for creating new moons.
     * @return          List of Moons.
     */
    public List<Planet> getMoons(Planet primary, PlanetFactory factory) {
        List<Planet> moons;

        switch (primary.getType().getClassification()) {
            case ProtoThermic:
                moons = getProtoThermicMoons(primary, factory);
                break;
            default:
                moons = getTerrestrialMoons(primary, factory);
                break;
        }

        return moons;
    }

    /**
     * Get moons around a proto-planet. These are most likely to be proto-planets themselves.
     *
     * @param primary       Primary planet to add moons to.
     * @param factory       PlanetFactory object.
     * @return              List of Moons.
     */
    private List<Planet> getProtoThermicMoons(Planet primary, PlanetFactory factory) {
        List<Planet> moons = new ArrayList<>();

        switch (Die.d6()) {
            case 1:
                // Single large moon, probably captured.
                String name = StarSystemFactory.getMoonName(primary.getName(), 1);
                long distance = primary.getRadius() * (40 + Die.d10(2));
                PlanetType type = PlanetType.ProtoLithian;
                List<PlanetFeature> features = new ArrayList<>();
                features.add(MoonFeature.LargeMoon);
                Planet moon = factory.createMoon(system, star, name, type,
                        distance, primary,
                        features.toArray(new PlanetFeature[0]));

                moons.add(moon);
                break;
            case 2:
                // A planetary ring.
                break;
            case 3: case 4:
                // A small number of small moons.
                break;
            default:
                // No moons.
                break;
        }

        return moons;
    }

    private List<Planet> getTerrestrialMoons(Planet primary, PlanetFactory factory) {
        List<Planet> moons = new ArrayList<>();
        int numberOfMoons = Die.d2() - 1;

        logger.info(String.format("Creating %d moons for [%s]", numberOfMoons, primary.getName()));
        if (numberOfMoons > 0) {
            long distance = primary.getRadius() * (40 + Die.d10(2));

            if (primary.getType() == PlanetType.ProtoLithian) {
                distance *= 0.50;
            } else if (primary.getType() == PlanetType.EoGaian) {
                distance *= 0.65;
            } else if (primary.getType() == PlanetType.MesoGaian) {
                distance *= 0.80;
            }

            for (int m=0; m < numberOfMoons; m++) {
                String name = StarSystemFactory.getMoonName(primary.getName(), m + 1);

                logger.info("Adding moon " + name);

                // We only want to call out an asteroid as a 'moon' if it is particularly large,
                // so ensure that the planetoid generated is of sufficient size to be interesting.
                List<PlanetFeature> features = new ArrayList<>();

                PlanetType type = PlanetType.Selenian;

                switch (Die.d6()) {
                    case 1:
                        features.add(MoonFeature.LargeMoon);
                        break;
                    case 2: case 3:
                        features.add(MoonFeature.SmallMoon);
                        break;
                    default:
                        // Nothing.
                }

                switch (Die.d6()) {
                    case 1: case 2: case 3:
                        features.add(MoonFeature.TidallyLocked);
                        break;
                    case 4: case 5:
                        features.add(MoonFeature.AlmostLocked);
                        break;
                    default:
                        // Nothing.
                }

                Planet moon = factory.createMoon(system, star, name, type,
                        distance, primary,
                        features.toArray(new PlanetFeature[0]));

                moons.add(moon);
                distance *= 1.5;
            }
        }

        return moons;
    }

}
