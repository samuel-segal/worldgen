/*
 * Copyright (c) 2017, Samuel Penn (sam@glendale.org.uk).
 * See the file LICENSE at the root of the project.
 */

package uk.org.glendale.worldgen.astro.systems.generators;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.org.glendale.utils.rpg.Die;
import uk.org.glendale.worldgen.WorldGen;
import uk.org.glendale.worldgen.astro.Physics;
import uk.org.glendale.worldgen.astro.planets.Planet;
import uk.org.glendale.worldgen.astro.planets.PlanetFactory;
import uk.org.glendale.worldgen.astro.planets.codes.PlanetType;
import uk.org.glendale.worldgen.astro.planets.generators.belt.IceBelt;
import uk.org.glendale.worldgen.astro.sectors.Sector;
import uk.org.glendale.worldgen.astro.stars.*;
import uk.org.glendale.worldgen.astro.systems.*;
import uk.org.glendale.worldgen.civ.CivilisationGenerator;
import uk.org.glendale.worldgen.civ.civilisation.FreeSettlers;
import uk.org.glendale.worldgen.civ.civilisation.Hermits;
import uk.org.glendale.worldgen.exceptions.DuplicateObjectException;

import java.util.ArrayList;
import java.util.List;

import static uk.org.glendale.worldgen.astro.Physics.MKM;

/**
 * Generate a barren star system. These are devoid of native life and habitable planets. Most are
 * G/K/M stars with a few barren worlds orbiting them. Most worlds will lack an atmosphere, and
 * have no surface water.
 */
public class Barren extends StarSystemGenerator {
    private static final Logger logger = LoggerFactory.getLogger(Barren.class);

    public Barren(WorldGen worldgen) {
        super(worldgen);
    }

    /**
     * Creates a barren star system. Such a system has one or more stars, with planets, but the
     * planets are barren with no civilisation or life of any kind. If the system has multiple stars,
     * the second star is much smaller and a long way away.
     *
     * @param sector    Sector to generate system in.
     * @param name      Name of the system.
     * @param x         X coordinate of the system in the sector (1-32).
     * @param y         Y coordinate of the system in the sector (1-40).
     *
     * @return          Created and persisted star system.
     * @throws DuplicateObjectException     If a duplicate system is created.
     */
    public StarSystem generate(Sector sector, String name, int x, int y) throws DuplicateObjectException {
        StarSystem system = createEmptySystem(sector, name, x, y);

        switch (Die.d6()) {
            case 1:
                createRedGiant(system);
                break;
            case 2: case 3:
                createSmallDwarf(system);
                break;
            case 4:
                createProtoStar(system);
                break;
            case 5:
                createSmallDwarfPair(system);
                break;
            case 6:
                createAsteroidBelt(system);
                break;
            case 7:
                createColdWorlds(system);
                break;
        }

        colonise(system);

        updateStarSystem(system);

        return system;
    }

    public void colonise(StarSystem system) {
        logger.info("Colonise " + system.getZone() + " star system " + system.getName());

        CivilisationGenerator generator = null;

        if (civName != null) {
            logger.info(String.format("Civilisation name set to be [%s]", civName));
            generator = getCivilisationByName(system);
        } else {
            if (system.getZone() == Zone.GREEN) {
                switch (Die.d6()) {
                    case 1: case 5: case 6:
                        generator = new Hermits(worldgen, system);
                        break;
                    case 2: case 3: case 4:
                        generator = new FreeSettlers(worldgen, system);
                        break;
                    default:
                        break;
                }
            }
        }
        if (generator != null) {
            generator.generate();
        }
    }

    /**
     * Creates a single main-sequence star with a single asteroid belt around it. The star will mostly
     * likely be a K-type star, with a chance of cool G or warm M. Depending on the temperature of the
     * belt, it may be a Vulcanian or Ice belt rather than a typical asteroid belt.
     *
     * @param system        Star System to create belt in.
     */
    @SuppressWarnings("WeakerAccess")
    public void createAsteroidBelt(StarSystem system) throws DuplicateObjectException {
        logger.info(String.format("Generating [Barren] [AsteroidBelt] system [%s]", system.getName()));
        StarGenerator starGenerator = new StarGenerator(worldgen, system, false);
        // On average a cool main-sequence star will be generated.
        Star primary = starGenerator.generatePrimary(Luminosity.V,
                SpectralType.K7.getSpectralType(Die.dieV(8)));
        system.addStar(primary);
        system.setType(StarSystemType.SINGLE);

        // Place the belt around 1AU from the star.
        long distance = Physics.AU + Die.dieV(50_000_000);
        String name = StarSystemFactory.getBeltName(primary, 1);
        String type = "AsteroidBelt";

        logger.info(String.format("Creating barren system [%s] with belt at [%d]km (%d)",
                primary.toString(), distance, primary.getHotDistance()));

        List<Planet> allPlanets;
        CircumstellarZone zone = CircumstellarZone.getZone(Physics.getTemperatureOfOrbit(primary, distance));
        switch (zone) {
            case Exclusion:
            case Epistellar: case Hot:
                allPlanets = addVulcanianBelt(system, name, distance);
                type = "VulcanianBelt";
                break;
            case Cold:
                switch (Die.d3()) {
                    case 1: case 2:
                        allPlanets = addAsteroidBelt(system, name, distance);
                        break;
                    default:
                        allPlanets = addIceBelt(system, name, distance);
                        type = "IceBelt";
                }
                break;
            case Stygia: case Tartarus:
                allPlanets = addIceBelt(system, name, distance);
                type = "IceBelt";
                break;
            default:
                allPlanets = addAsteroidBelt(system, name, distance);
                break;
        }
        system.addTradeCode(StarSystemCode.Ba);
        if (Die.d6() == 1) {
            system.addTradeCode(StarSystemCode.Sf);
            system.setZone(Zone.AMBER);
        }

        system.setPlanets(allPlanets);
        setDescription(system, type);
    }

    private List<Planet> addVulcanianBelt(StarSystem system, String name, long distance) {
        return worldgen.getPlanetFactory().createPlanet(system, system.getStars().get(0),
                name, PlanetType.VulcanianBelt, distance);
    }

    private List<Planet> addAsteroidBelt(StarSystem system, String name, long distance) {
        return worldgen.getPlanetFactory().createPlanet(system, system.getStars().get(0),
                name, PlanetType.AsteroidBelt, distance);
    }

    private List<Planet> addIceBelt(StarSystem system, String name, long distance) {
        return worldgen.getPlanetFactory().createPlanet(system, system.getStars().get(0),
                name, PlanetType.IceBelt, distance);
    }

    /**
     * Create a system with a single red dwarf star, with some worlds in the outer system. Most
     * worlds are small Jovians or icy worlds.
     *
     * @param system    Star system to create planets in.
     */
    public void createColdWorlds(StarSystem system) throws DuplicateStarException {
        system.setType(StarSystemType.SINGLE);

        // Create a cool red dwarf star.
        StarGenerator starGenerator = new StarGenerator(worldgen, system, false);
        Star          primary = starGenerator.generatePrimary(Luminosity.VI, SpectralType.M9.getSpectralType(Die.d6()));
        system.addStar(primary);

        PlanetFactory factory = worldgen.getPlanetFactory();

        long distance = (2 + Die.d4(2)) * Physics.AU + Die.die(Physics.AU);
        String name = StarSystemFactory.getPlanetName(primary, 1);
        List<Planet> planets = null;

        if (Die.d2() == 1) {
            // A small, cold, jovian world.
            planets = factory.createPlanet(system, primary, name, PlanetType.Neptunian, distance);
        } else {
            // A huge, icy, super-Earth.
            planets = factory.createPlanet(system, primary, name, PlanetType.Thean, distance);
        }
        system.addPlanets(planets);

        distance *= 2;
        name = StarSystemFactory.getBeltName(primary, 2);
        planets = factory.createPlanet(system, primary, name, PlanetType.IceBelt, distance,
                planets.get(planets.size()-1), IceBelt.IceBeltFeature.KuiperBelt);
        system.addPlanets(planets);

        setDescription(system, null);
    }

    private PlanetType getHotType(int kelvin) {
        switch (Die.d6()) {
            case 1: case 2: case 3:
                return PlanetType.Ferrinian;
            case 4: case 5:
                return PlanetType.Hermian;
            case 6:
                return PlanetType.ProtoLithian;
        }
        return PlanetType.Hermian;
    }

    private PlanetType getAreanType(int kelvin) {
        switch (Die.d2()) {
            case 1: return PlanetType.EoArean;
            case 2: return PlanetType.MesoArean;
            case 3: return PlanetType.MesoArean;
            case 4: return PlanetType.EuArean;
        }
        return PlanetType.EoArean;
    }

    /**
     * Create a single cool (K or M) dwarf star with a few barren worlds around it.
     *
     * @param system    Star system to generate.
     *
     * @throws DuplicateStarException   If duplicate star is created.
     */
    @SuppressWarnings("WeakerAccess")
    public void createSmallDwarf(StarSystem system) throws DuplicateStarException {
        system.setType(StarSystemType.SINGLE);
        StarGenerator starGenerator = new StarGenerator(worldgen, system, false);
        PlanetFactory factory = worldgen.getPlanetFactory();

        Star primary = starGenerator.generatePrimary(Luminosity.V,
                SpectralType.K6.getSpectralType(Die.dieV(6)));
        system.addStar(primary);
        system.setType(StarSystemType.SINGLE);

        logger.info(String.format("Generating [Barren] [SmallDwarf] system [%s]", primary));

        int  numPlanets = Die.d3() + 2;
        int  orbit = 1;
        long distance = primary.getMinimumDistance() * 2;
        logger.info(String.format("Constant [%f] Distance [%,d]", Physics.getSolarConstant(primary), distance));
        for (int p = 0; p < numPlanets; p++) {
            int orbitTemperature = Physics.getTemperatureOfOrbit(primary, distance);
            logger.info(String.format("Orbit Temperature is %dK at distance %,dkm", orbitTemperature, distance));

            String name = null;
            List<Planet> planets = null;
            CircumstellarZone zone = CircumstellarZone.getZone(orbitTemperature);

            switch (zone) {
                case Exclusion: case Epistellar:
                    // Don't add any planets here. Instead, shift the
                    distance = Physics.getOrbitWithTemperature(primary, CircumstellarZone.Hot.getKelvin());
                    break;
                case Hot:
                    name = StarSystemFactory.getPlanetName(primary, orbit++);
                    planets = factory.createPlanet(system, primary, name, getHotType(orbitTemperature), distance);
                    break;
                case Inner:
                    name = StarSystemFactory.getPlanetName(primary, orbit++);
                    planets = factory.createPlanet(system, primary, name, PlanetType.Cytherean, distance);
                    break;
                case Middle:
                    name = StarSystemFactory.getPlanetName(primary, orbit++);
                    planets = factory.createPlanet(system, primary, name, getAreanType(orbitTemperature), distance);
                    break;
                case Outer:
                    name = StarSystemFactory.getPlanetName(primary, orbit++);
                    planets = factory.createPlanet(system, primary, name, getAreanType(orbitTemperature), distance);
                    break;
                case Cold:
                    name = StarSystemFactory.getPlanetName(primary, orbit++);
                    planets = factory.createPlanet(system, primary, name, getAreanType(orbitTemperature), distance);
                    break;
                case Stygia: case Tartarus:
                    // Nothing here.
                    break;
            }

            if (planets != null) {
                system.addPlanets(planets);
            }

            distance *= (9.0 + Die.d3(3)) / 10;
            distance += 5_000_000;
        }
        // Finish off with a distant belt of ice.
        distance *= 2;
        while (Physics.getTemperatureOfOrbit(primary, distance) > 100) {
            distance *= 1.5;
        }
        String name = StarSystemFactory.getBeltName(primary, 1);
        List<Planet> planets = factory.createPlanet(system, primary, name, PlanetType.IceBelt, distance, IceBelt.IceBeltFeature.KuiperBelt);
        system.addPlanets(planets);

        system.addTradeCode(StarSystemCode.Ba);
        if (Die.d10() == 1) {
            system.addTradeCode(StarSystemCode.Sf);
            system.setZone(Zone.AMBER);
        }

        setDescription(system, null);
    }

    public void createRockyDwarf(StarSystem system) throws DuplicateStarException {
        system.setType(StarSystemType.SINGLE);
        StarGenerator starGenerator = new StarGenerator(worldgen, system, false);
        PlanetFactory factory = worldgen.getPlanetFactory();

        Star primary = starGenerator.generatePrimary(Luminosity.V,
                SpectralType.K4.getSpectralType(Die.dieV(6)));
        system.addStar(primary);
        system.setType(StarSystemType.SINGLE);

        logger.info(String.format("Generating [Barren] [RockyDwarf] system [%s]", primary));

        int  numPlanets = Die.d3() + 4;
        int  orbit = 1;
        long distance = primary.getMinimumDistance() * 5;
        logger.info(String.format("Constant [%f] Distance [%,d]", Physics.getSolarConstant(primary), distance));
        for (int p = 0; p < numPlanets; p++) {
            int orbitTemperature = Physics.getTemperatureOfOrbit(primary, distance);
            logger.info(String.format("Orbit Temperature is %dK at distance %,dkm", orbitTemperature, distance));

            List<Planet> planets = null;
            if (Die.d6() == 1) {
                // No planet in this orbit.
            } else if (orbitTemperature > 350) {
                // Mercury
                String name = StarSystemFactory.getPlanetName(primary, orbit++);
                planets = factory.createPlanet(system, primary, name, PlanetType.Hermian, distance);
                distance += 5_000_000;
            } else if (orbitTemperature > 275 && Die.d2() == 1) {
                // Venus
                String name = StarSystemFactory.getPlanetName(primary, orbit++);
                planets = factory.createPlanet(system, primary, name, PlanetType.Cytherean, distance);
            } else {
                String name;
                switch (Die.d6()) {
                    case 1:
                        name = StarSystemFactory.getPlanetName(primary, orbit++);
                        planets = factory.createPlanet(system, primary, name, PlanetType.Selenian, distance);
                        break;
                    case 2: case 3: case 4: case 5:
                        name = StarSystemFactory.getPlanetName(primary, orbit++);
                        planets = factory.createPlanet(system, primary, name, getAreanType(orbitTemperature), distance);
                        break;
                    case 6:
                        name = StarSystemFactory.getBeltName(primary, 1);
                        planets = factory.createPlanet(system, primary, name, PlanetType.AsteroidBelt, distance);
                        system.addPlanets(planets);
                        break;
                }
            }

            if (planets != null) {
                system.addPlanets(planets);
            }

            distance *= (9.0 + Die.d3(3)) / 10;
            distance += 5_000_000;
        }
        // Finish off with a distant belt of ice.
        distance *= 2;
        while (Physics.getTemperatureOfOrbit(primary, distance) > 100) {
            distance *= 1.5;
        }
        String name = StarSystemFactory.getBeltName(primary, 1);
        List<Planet> planets = factory.createPlanet(system, primary, name, PlanetType.IceBelt,
                distance, IceBelt.IceBeltFeature.KuiperBelt);
        system.addPlanets(planets);

        system.addTradeCode(StarSystemCode.Ba);
        if (Die.d10() == 1) {
            system.addTradeCode(StarSystemCode.Sf);
            system.setZone(Zone.AMBER);
        }

        setDescription(system, null);
    }



    /**
     * Creates a small dwarf star with a single proto-planetary disc.
     */
    @SuppressWarnings("WeakerAccess")
    public void createProtoStar(StarSystem system) throws DuplicateStarException {
        system.setType(StarSystemType.SINGLE);
        StarGenerator starGenerator = new StarGenerator(worldgen, system, false);
        Star primary;
        String type;

        switch (Die.d6()) {
            case 1: case 2: case 3:
                type = "CoolDwarf";
                primary = starGenerator.generatePrimary(Luminosity.VI,
                        SpectralType.M0.getSpectralType(Die.dieV(6)));
                if (Die.d2() == 1) system.addTradeCode(StarSystemCode.Sf);
                break;
            case 4: case 5:
                type = "WarmDwarf";
                primary = starGenerator.generatePrimary(Luminosity.V,
                        SpectralType.K0.getSpectralType(Die.dieV(4)));
                if (Die.d3() == 1) system.addTradeCode(StarSystemCode.Sf);
                break;
            default:
                type = "SubGiant";
                primary = starGenerator.generatePrimary(Luminosity.IV,
                        SpectralType.A9.getSpectralType(Die.dieV(6)));
                if (Die.d4() == 1) {
                    system.addTradeCode(StarSystemCode.Sf);
                } else if (Die.d2() == 1) {
                    system.addTradeCode(StarSystemCode.Ji);
                }
                break;
        }
        system.addStar(primary);
        if (system.hasTradeCode(StarSystemCode.Sf, StarSystemCode.Ji)) {
            system.setZone(Zone.AMBER);
        }
        system.addTradeCode(StarSystemCode.Ba);

        PlanetFactory factory = worldgen.getPlanetFactory();
        String name = StarSystemFactory.getBeltName(primary, 1);
        long   distance = primary.getMinimumDistance() * 10;
        if (Die.d3() == 1) {
            system.addPlanets(factory.createPlanet(system, primary, name, PlanetType.PlanetesimalDisc, distance));
        } else {
            system.addPlanets(factory.createPlanet(system, primary, name, PlanetType.DustDisc, distance));
            type += ".Dust";
        }

        setDescription(system, type);
    }

    /**
     * Creates a brown dwarf star. Most will be without any planets, but a few will have
     * some cold minor worlds.
     */
    public void createBrownDwarf(StarSystem system) throws DuplicateStarException {
        system.setType(StarSystemType.SINGLE);
        StarGenerator starGenerator = new StarGenerator(worldgen, system, false);
        List<Planet> planets = new ArrayList<Planet>();
        Star primary = starGenerator.generateBrownDwarfPrimary();

        if (Die.d3() == 1) {
            PlanetFactory planetFactory = worldgen.getPlanetFactory();
            String planetName;
            int distance = 5_000_000 + Die.d6(2) * 1_000_000;

            planetName = StarSystemFactory.getPlanetName(primary, 1);
            planets = planetFactory.createPlanet(system, primary, planetName, PlanetType.AsteroidBelt, distance);
        }

        system.setPlanets(planets);
        setDescription(system, null);
    }


    public void createBerlichingen(StarSystem system) throws DuplicateStarException {
        system.setType(StarSystemType.CLOSE_BINARY);
        StarGenerator starGenerator = new StarGenerator(worldgen, system, true);

        int distance = 2_500_000;
        Star primary = starGenerator.generatePrimary(Luminosity.V, SpectralType.M3);
        primary.setDistance(distance);

        // Get a possibly cooler variant of the primary star. There is a 50% chance that
        // it is at least one step cooler, and 50% chance for each step down beyond that.
        Star secondary = starGenerator.generateSecondary(Luminosity.V, SpectralType.M3);

        // Work out orbital distance and period of the pair.
        double totalMass = primary.getMass() + secondary.getMass();

        long period = Physics.getOrbitalPeriod(totalMass * Physics.SOL_MASS, distance * 1_000);
        primary.setPeriod(period);
        secondary.setPeriod(period);

        primary.setDistance(Physics.round(distance * secondary.getMass() / totalMass));
        secondary.setDistance(Physics.round(distance * primary.getMass() / totalMass));

        primary.setParentId(StarSystem.PRIMARY_COG);
        secondary.setParentId(StarSystem.PRIMARY_COG);

        Star centre = new Star(system.getName(), system, 0, 0,
                primary.getLuminosity(), primary.getSpectralType());
        centre.setMass(primary.getMass() + secondary.getMass());
        centre.setCentreOfGravity(primary.getParentId());

        centre = primary;
        centre.setMass(totalMass);
        centre.setRadius((int) (primary.getRadius() * 1.2));
        PlanetFactory planetFactory = worldgen.getPlanetFactory();

        List<Planet> allPlanets = new ArrayList<>();
        List<Planet> planets;
        String       planetName;
        int          orbit = 1;

        distance = 15_000_000;

        for (int p=0; p < 8; p++) {
            planetName = StarSystemFactory.getPlanetName(centre, orbit++);
            planets = planetFactory.createPlanet(system, centre, planetName, PlanetType.Hermian, distance);
            logger.info(String.format("Created world [%s]", planetName));
            distance *= 2;
            allPlanets.addAll(planets);
        }
//        addBarrenWorlds(system, primary, secondary);


        setDescription(system, null);
    }


    /**
     * Creates a system with a couple of close binary dwarf stars. The two stars are very similar
     * in mass, if not identical, and orbit a common centre of gravity. The distance between the
     * two stars for a close binary is less than 1 AU.
     *
     * @param system        Star system to add stars to.
     * @throws DuplicateStarException  If stars already exist.
     */
    @SuppressWarnings("WeakerAccess")
    public void createSmallDwarfPair(StarSystem system) throws DuplicateStarException {
        system.setType(StarSystemType.CLOSE_BINARY);
        StarGenerator starGenerator = new StarGenerator(worldgen, system, true);

        int distance = 5_000_000 + Die.d6(2) * 100_000;
        Star primary = starGenerator.generatePrimary(Luminosity.VI, SpectralType.G8.getSpectralType(-Die.d12()));
        primary.setDistance(distance);

        // Get a possibly cooler variant of the primary star. There is a 50% chance that
        // it is at least one step cooler, and 50% chance for each step down beyond that.
        SpectralType hr = primary.getSpectralType().getSpectralType(-Die.d6());
        Star secondary = starGenerator.generateSecondary(Luminosity.VI, hr);

        // Work out orbital distance and period of the pair.
        double totalMass = primary.getMass() + secondary.getMass();

        long period = Physics.getOrbitalPeriod(totalMass * Physics.SOL_MASS, distance * 1_000);
        primary.setPeriod(period);
        secondary.setPeriod(period);

        primary.setDistance(Physics.round(distance * secondary.getMass() / totalMass));
        secondary.setDistance(Physics.round(distance * primary.getMass() / totalMass));


        primary.setParentId(StarSystem.PRIMARY_COG);
        secondary.setParentId(StarSystem.PRIMARY_COG);

        addBarrenWorlds(system, primary, secondary);

        setDescription(system, null);
    }

    /**
     * Create a red giant system. Such systems will have had their inner planets swallowed or
     * burnt to a cinder long ago, leaving a warmed outer system of planets.
     *
     * @param system    System to create stars and planets in.
     * @throws DuplicateStarException If stars already exist.
     */
    @SuppressWarnings("WeakerAccess")
    public void createRedGiant(StarSystem system) throws DuplicateStarException {
        system.setType(StarSystemType.SINGLE);
        StarGenerator starGenerator = new StarGenerator(worldgen, system, false);

        Star primary = starGenerator.generateRedGiantPrimary();
        primary.setStandardMass();
        primary.setStandardRadius();
        system.addStar(primary);

        addBarrenWorlds(system, primary);
        setDescription(system, null);
    }

    /**
     * Creates a proto-planetary disc around the star.
     *
     * @param system    Star System worlds are being added to.
     * @param star      Star worlds are orbiting around.
     */
    private void addProtoWorlds(StarSystem system, Star star) {
        PlanetFactory planetFactory = worldgen.getPlanetFactory();
        String      planetName;
        long        distance = star.getMinimumDistance();
        List<Planet> allPlanets = new ArrayList<>();
        List<Planet> planets;

        switch (Die.d6()) {
            case 1: case 2: case 3:
                // Large proto-planetary disc.
                distance *= 10;

                planetName = StarSystemFactory.getBeltName(star, 1);
                planets = planetFactory.createPlanet(system, star, planetName, PlanetType.DustDisc, distance);
                distance += planets.get(0).getRadius() * 2;
                allPlanets.addAll(planets);
                break;
            case 4: case 5:
                distance *= 3;
                planetName = StarSystemFactory.getPlanetName(star, 1);
                planets = planetFactory.createPlanet(system, star, planetName, PlanetType.DustDisc, distance);
                distance += planets.get(0).getRadius() * 2;
                allPlanets.addAll(planets);

                planetName = StarSystemFactory.getPlanetName(star, 1);
                PlanetType type = null;
                switch (Die.d6()) {
                    case 1: case 2: case 3:
                        type = PlanetType.Carbonaceous;
                        break;
                    case 4: case 5:
                        type = PlanetType.Silicaceous;
                        break;
                    case 6:
                        if (Physics.getTemperatureOfOrbit(star, distance) < 270) {
                            type = PlanetType.Gelidaceous;
                        } else {
                            type = PlanetType.Aggregate;
                        }
                        break;
                }
                planets = planetFactory.createPlanet(system, star, planetName, type, distance, planets.get(0));
                distance *= 2;
                allPlanets.addAll(planets);
                planetName = StarSystemFactory.getPlanetName(star, 1);
                planets = planetFactory.createPlanet(system, star, planetName, PlanetType.DustDisc, distance, planets.get(0));
                allPlanets.addAll(planets);
                break;
            case 6:
                // Large planetesimal disc.
                distance *= 10;
                planetName = StarSystemFactory.getPlanetName(star, 1);
                planets = planetFactory.createPlanet(system, star, planetName, PlanetType.PlanetesimalDisc, distance);
                allPlanets.addAll(planets);
                break;
        }

        system.setPlanets(allPlanets);
    }

    /**
     * Creates planets in a barren system.
     *
     * @param system    System to create planets in.
     * @param star      Parent star of these planets.
     */
    private void addBarrenWorlds(StarSystem system, Star star) {
        PlanetFactory planetFactory = worldgen.getPlanetFactory();
        String          planetName;
        int             orbit = 1;
        long            distance = star.getMinimumDistance() + Die.d10(2) * MKM;
        List<Planet> allPlanets = new ArrayList<Planet>();
        List<Planet> planets;

        switch (Die.d6()) {
            case 1:
                planetName = StarSystemFactory.getPlanetName(star, orbit++);
                planets = planetFactory.createPlanet(system, star, planetName, PlanetType.VulcanianBelt, distance);
                logger.info(String.format("Created world [%s]", planetName));
                distance += planets.get(0).getRadius() * 3;
                allPlanets.addAll(planets);

                planetName = StarSystemFactory.getPlanetName(star, orbit++);
                planets = planetFactory.createPlanet(system, star, planetName, PlanetType.Saturnian, distance);
                logger.info(String.format("Created world [%s]", planetName));
                distance += (planets.get(0).getRadius() / 500) + Die.dieV(20);
                allPlanets.addAll(planets);
                break;
            case 2: case 3:
                planetName = StarSystemFactory.getPlanetName(star, orbit++);
                planets = planetFactory.createPlanet(system, star, planetName, PlanetType.Hermian, distance);
                logger.info(String.format("Created world [%s]", planetName));
                distance += Die.d20(3);
                allPlanets.addAll(planets);
                break;
            case 4: case 5:
                planetName = StarSystemFactory.getPlanetName(star, orbit++);
                planets = planetFactory.createPlanet(system, star, planetName, PlanetType.Ferrinian, distance);
                logger.info(String.format("Created world [%s]", planetName));
                distance += Die.d20(3);
                allPlanets.addAll(planets);
                break;
            case 6:
                distance += 50 + Die.d20(3);
                break;
        }

        if (Die.d2() == 1) {
            planetName = StarSystemFactory.getPlanetName(star, orbit);
            planets = planetFactory.createPlanet(system, star, planetName, PlanetType.AsteroidBelt, distance);
            logger.info(String.format("Created world [%s]", planetName));
            allPlanets.addAll(planets);
        }

        system.setPlanets(allPlanets);
    }

    /**
     * Adds worlds to a close binary star system. The parent of the planets will be the COG identifier
     * of the two stars (so -1 or -2).
     *
     * @param system        System to add planets to.
     * @param primary       Primary star in system.
     * @param secondary     Secondary star in system.
     */
    private void addBarrenWorlds(StarSystem system, Star primary, Star secondary) {
        PlanetFactory planetFactory = worldgen.getPlanetFactory();
        String          planetName;
        int             belt = 1;
        long            distance = (primary.getMinimumDistance() + secondary.getMinimumDistance()) * (8 + Die.d4());
        List<Planet> allPlanets = new ArrayList<>();
        List<Planet> planets;

        String name = String.format("%s %s/%s", system.getName(),
                primary.getName().replaceAll(".* ", ""),
                secondary.getName().replaceAll(".* ", ""));

        Star center = new Star(name, system, 0, 0,
                primary.getLuminosity(), primary.getSpectralType());
        center.setMass(primary.getMass() + secondary.getMass());
        center.setCentreOfGravity(primary.getParentId());

        switch (Die.d6()) {
            case 1:
                planetName = StarSystemFactory.getBeltName(center, belt++);
                planets = planetFactory.createPlanet(system, center, planetName, PlanetType.VulcanianBelt, distance);
                logger.info(String.format("Created world [%s]", planetName));
                distance += planets.get(0).getRadius() * 3;
                allPlanets.addAll(planets);

                planetName = StarSystemFactory.getPlanetName(center, 1);
                planets = planetFactory.createPlanet(system, center, planetName, PlanetType.Saturnian, distance);
                logger.info(String.format("Created world [%s]", planetName));
                distance += (planets.get(0).getRadius() * 2) + Die.die(20_000_000);
                allPlanets.addAll(planets);
                break;
            case 2: case 3:
                planetName = StarSystemFactory.getPlanetName(center, 1);
                planets = planetFactory.createPlanet(system, center, planetName, PlanetType.Hermian, distance);
                logger.info(String.format("Created world [%s]", planetName));
                distance += Die.d20(3) * 1_000_000;
                allPlanets.addAll(planets);
                break;
            case 4: case 5:
                planetName = StarSystemFactory.getPlanetName(center, 1);
                planets = planetFactory.createPlanet(system, center, planetName, PlanetType.Ferrinian, distance);
                logger.info(String.format("Created world [%s]", planetName));
                distance += Die.d20(3) * 1_000_000;
                allPlanets.addAll(planets);
                break;
            case 6:
                distance += 50_000_000 + Die.d20(3) * 1_000_000;
                break;
        }

        if (Die.d2() == 1) {
            planetName = StarSystemFactory.getBeltName(center, belt);
            planets = planetFactory.createPlanet(system, center, planetName, PlanetType.AsteroidBelt, distance);
            logger.info(String.format("Created world [%s]", planetName));
            allPlanets.addAll(planets);
        }

        system.setPlanets(allPlanets);
    }
}
