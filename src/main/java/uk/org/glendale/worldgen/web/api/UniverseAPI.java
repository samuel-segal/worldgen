/*
 * Copyright (c) 2019, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */
package uk.org.glendale.worldgen.web.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.Request;
import spark.Response;
import uk.org.glendale.worldgen.WorldGen;
import uk.org.glendale.worldgen.astro.Universe;
import uk.org.glendale.worldgen.astro.sectors.Sector;
import uk.org.glendale.worldgen.astro.sectors.SectorFactory;
import uk.org.glendale.worldgen.astro.systems.StarSystemFactory;
import uk.org.glendale.worldgen.astro.systems.SurveyFactory;
import uk.org.glendale.worldgen.web.Controller;
import uk.org.glendale.worldgen.web.Server;

import java.util.Map;

import static spark.Spark.get;

/**
 * API endpoint for providing high level information about the universe.
 */
public class UniverseAPI extends Controller {
    private static final Logger logger = LoggerFactory.getLogger(UniverseAPI.class);


    public void setupEndpoints() {
        logger.info("Setting up endpoints for UniverseAPI");
        get("/api/universe", (request, response) -> getUniverse(request, response), json());
        get("/api/statistics", (request, response) -> getStatistics(request, response), json());
        get("/api/scan/:sector/:xy", (request, response) -> scanSystems(request, response), json());
    }

    /**
     * Gets basic meta data on this universe.
     *
     * @param request       Request object.
     * @param response      Response object.
     * @return              JSON data.
     */
    public Object getUniverse(Request request, Response response) {
        logger.info("getUniverse:");

        try (WorldGen worldGen = Server.getWorldGen()) {
            Universe u = worldGen.getUniverse();

            return u;

        } catch (Exception e) {

        }
        return null;
    }

    /**
     * Gets statistics on the data in this universe.
     *
     * @param request       Request object.
     * @param response      Response object.
     * @return              JSON data.
     */
    public Object getStatistics(Request request, Response response) {
        logger.info("getStatistics:");

        try (WorldGen worldGen = Server.getWorldGen()) {
            Map<String,Long> statistics = worldGen.getStatistics();

            return statistics;
        } catch (Exception e) {

        }

        return null;
    }

    public Object scanSystems(Request request, Response response) {

        try (WorldGen wg = Server.getWorldGen()) {
            String sectorId = getStringParam(request, "sector");
            String xy = getStringParam(request, "xy");
            int radius = getIntParamWithDefault(request, "radius", 1);
            int effect = getIntParamWithDefault(request, "effect", 3);

            System.out.println(sectorId);
            System.out.println(xy);

            SectorFactory sectorFactory = wg.getSectorFactory();
            Sector        sector = sectorFactory.getSectorByIdentifier(sectorId);
            int           x = StarSystemFactory.getXCoord(xy);
            int           y = StarSystemFactory.getYCoord(xy);

            SurveyFactory surveyFactory = wg.getSurveyFactory();
            int num = surveyFactory.performSurvey(sector, x, y, radius, effect);

            System.out.println("Performed");
            return "Done on " + num;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }
}
