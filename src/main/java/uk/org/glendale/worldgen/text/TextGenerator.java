/**
 * Copyright (c) 2007, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */
package uk.org.glendale.worldgen.text;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.org.glendale.utils.misc.NumberToWords;
import uk.org.glendale.utils.rpg.Die;
import uk.org.glendale.worldgen.astro.Physics;
import uk.org.glendale.worldgen.astro.commodities.Commodity;
import uk.org.glendale.worldgen.astro.commodities.CommodityFactory;
import uk.org.glendale.worldgen.astro.commodities.CommodityName;
import uk.org.glendale.worldgen.astro.planets.Planet;
import uk.org.glendale.worldgen.astro.planets.PlanetFeature;
import uk.org.glendale.worldgen.astro.planets.PlanetGenerator;
import uk.org.glendale.worldgen.astro.planets.codes.*;
import uk.org.glendale.worldgen.astro.planets.generators.dwarf.EoArean;
import uk.org.glendale.worldgen.astro.systems.StarSystem;
import uk.org.glendale.worldgen.astro.systems.StarSystemCode;
import uk.org.glendale.worldgen.astro.systems.Zone;
import uk.org.glendale.worldgen.civ.CivilisationFeature;
import uk.org.glendale.worldgen.civ.CivilisationGenerator;
import uk.org.glendale.worldgen.civ.Facility;
import uk.org.glendale.worldgen.civ.FacilityType;

import java.lang.reflect.Method;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.*;

/**
 * Create a random description for a planet. Descriptions are made up of phrases
 * which are read from a resource file, allowing easy changing. The phrases
 * support a very simple form of flow control, allowing a phrase to consist of
 * random elements, to reference other elements, and to choose based on planet
 * values.
 *
 * [a|b|c] Select random one of a, b or c {a} Insert phrase referenced by key a
 * {a|b|c} Select random one of a, b or c and reference that key.
 * (50?phrase) 50% chance of displaying phrase (25?a|b) 25% chance of a, otherwise b
 * (Prop>20?a:b) Get property of planet, compare with value, then a otherwise b.
 * $Prop Value of property for planet.
 *
 *
 * @author Samuel Penn
 */
public class TextGenerator {
    private static final Logger logger = LoggerFactory.getLogger(TextGenerator.class);
    private StringBuffer buffer = new StringBuffer();
    private Planet planet = null;
    private Facility facility = null;
    private StarSystem system = null;
    private PlanetGenerator generator = null;
    private CivilisationGenerator civGenerator = null;

    private Properties phrases = null;

    /**
     * Get the phrase for the given key from the resource bundle. Some keys will
     * have a number of possible options (in the form key, key.1, key.2 etc). If
     * a key has several options, one will be selected randomly.
     *
     * @param key
     *            Key to use to find a phrase.
     * @return The selected phrase, or null if none found.
     */
    private String getPhrase(String key) {
        String text = null;

        text = phrases.getProperty(key);
        if (text != null) {
            int i = 0;
            while (phrases.getProperty(key + "." + (i + 1)) != null) {
                i++;
            }
            if (i > 0) {
                int choice = Die.rollZero(i + 1);
                if (choice != 0)
                    text = phrases.getProperty(key + "." + choice);
            }
        }

        return text;
    }

    /**
     * Gets whether the named phrase has been defined. Only looks for the exact key,
     * doesn't try to find other associated keys.
     *
     * @param key   Key to look for.
     * @return      True iff the exact property exists.
     */
    private boolean hasPhrase(String key) {
        return phrases.getProperty(key) != null;
    }

    private static final String RESOURCE_BASE = "text.planets.";
    private static final String FACILITY_BASE = "text.facilities.";
    private static final String SYSTEM_BASE = "text.systems.";

    private void readResource(String bundleName) {
        try {
            ResourceBundle bundle = ResourceBundle.getBundle(bundleName);
            Enumeration<String> e = bundle.getKeys();
            while (e.hasMoreElements()) {
                String key = e.nextElement();
                String value = bundle.getString(key);
                phrases.setProperty(key, value);
            }
        } catch (MissingResourceException e) {
            logger.warn(String.format("Unable to find text bundle for [%s]", bundleName));
        }

    }

    /**
     * Add a specific phrase to the list of phrases. Used for testing.
     *
     * @param key       Key for this phrase.
     * @param phrase    Text for the phrase.
     */
    protected void addPhrase(String key, String phrase) {
        phrases.setProperty(key, phrase);
    }

    /**
     * Finds all the resources for this type of planet generator. Resources are
     * named according to the planet type and group.
     */
    private void readResources() {
        String type = planet.getType().name();
        String group = planet.getType().getGroup().name();

        phrases = new Properties();

        readResource(RESOURCE_BASE + "all");
        readResource(RESOURCE_BASE + group);
        readResource(RESOURCE_BASE + group + "." + type);
    }

    public TextGenerator(final StarSystem system, final String resource) {
        this.system = system;
        phrases = new Properties();
        readResource(SYSTEM_BASE + resource);
    }

    public TextGenerator(final Planet planet) {
        this.planet = planet;
        if (this.planet == null) {
            throw new IllegalStateException(
                    "Planet builder has not been correctly initiated");
        }
        readResources();
    }

    public TextGenerator(final Planet planet, final Facility facility) {
        this.planet = planet;
        this.facility = facility;

        if (this.planet == null || this.facility == null) {
            throw new IllegalStateException(
                    "Planet builder has not been correctly initiated");
        }
        phrases = new Properties();

        String type = facility.getType().name().toLowerCase();
        String name = facility.getName();

        readResource(FACILITY_BASE + type + "." + name);
    }

    public void setGenerator(PlanetGenerator generator) {
        this.generator = generator;
    }

    /**
     * Get a random phrase from the text fragment. The text may be bounded by [
     * ] and will be delimited by '|'. For example, [hello|hi|greetings] will
     * return one of 'hello', 'hi' or 'greetings'.
     *
     * @param text
     *            Text to choose from.
     * @return One of the phrases from the list of phrases.
     */
    private String random(String text) {
        if (text == null) {
            return "";
        }
        text = text.replaceAll("^[\\[\\{]", "");
        text = text.replaceAll("[\\}\\]]$", "");
        StringTokenizer tokens = new StringTokenizer(text, "|");
        int count = tokens.countTokens();
        String token = null;
        int choice = (int) (Math.random() * count);

        for (int i = 0; i <= choice; i++) {
            token = tokens.nextToken();
        }

        return token;
    }

    /**
     * Parse any property getters in the text and expand them into human readable formats. These are prepended
     * with "$$". Numbers are converted to words, and camel cased enums are lowered and have spaces inserted.
     *
     * @param line  Line of text to be parsed.
     * @return      Modified line with properties expanded.
     */
    private String parseEnglishPropertyValues(String line) {
        if (line == null) {
            return "";
        }
        while (line.contains("$$")) {
            String prop = line.substring(line.indexOf("$$") + 2);
            prop = prop.replaceAll("[^A-Za-z0-9].*", "");

            // If there is a specialist 'Text' method for the property, we don't perform any other
            // post-processing on the result.
            String value = getProperty(prop + "Text");
            if (value.length() == 0) {
                value = getProperty(prop);
                if (value.matches("[0-9,]+")) {
                    try {
                        value = new NumberToWords(value.replaceAll(",", "")).format();
                    } catch (NumberFormatException e) {
                        // Failed to convert to words, so leave in original format.
                    }
                }

                value = value.replaceAll("([A-Z])", " $1").toLowerCase().trim();
            }
            line = line.replaceFirst("\\$\\$" + prop, value);
        }
        return line;
    }

    /**
     * Parse any trade code setters. These are defined by "@+", and add a trade code to the planet if the
     * code is not already present. The two letter trade code should directly follow the "@+", e.g. "@+Ag".
     * The trade code is removed from the text after being processed.
     *
     * It can also be used to add a resource. If the trade code is not recognised, then the CommodityName
     * enums will be checked. If a match is found, that resource is added/increased to the planet.
     *
     * @param line  Line of text to be parsed.
     * @return      Modified line with any trade code additions removed.
     */
    private String parseAddTradeCodes(String line) {
        if (line == null) {
            return "";
        }
        if (line.contains("@+AMBER")) {
            if (planet != null) {
                planet.setZone(Zone.AMBER);
            }
            line = line.replaceAll("@\\+AMBER ?", "");
        }

        if (line.contains("@+RED")) {
            if (planet != null) {
                planet.setZone(Zone.RED);
            }
            line = line.replaceAll("@\\+RED ?", "");
        }

        while (line.contains("@+")) {
            String code = line.substring(line.indexOf("@+") + 2);
            int    count = 1;
            // There's undoubtedly a better way of doing this.
            while (code.startsWith("+")) {
                count++;
                code = code.substring(1);
            }
            code = code.replaceAll("[^A-Za-z0-9].*", "");

            try {
                if (planet != null) {
                    TradeCode tradeCode = null;
                    try {
                        tradeCode = TradeCode.valueOf(code);
                        planet.addTradeCode(tradeCode);
                    } catch (Exception tce) {
                        // This is not a known trade code.
                    }
                    if (tradeCode == null && generator != null) {
                        CommodityName name = CommodityName.valueOf(code);
                        generator.addExtraResource(planet, name, count);
                    }
                }
            } catch (Throwable e) {
                logger.warn(String.format("Unrecognised code or commodity [%s]", code));
            }

            line = line.replaceFirst("@\\+*" + code + " ?", "");
        }

        return line;
    }

    private static final String ENUM_PREFIX = "uk.org.glendale.worldgen.astro.planets.generators.";

    // List of sections that should be added at the end of the description.
    List<String>    extraSections = new ArrayList<>();

    /**
     * Allow a PlanetFeature to be added to a planet. This allows the text to modify how the map is drawn.
     * Features are looked for on the current planet's generator class, by checking all its inner Enums
     * until a match is found. If no matches are found, move up to the super class and try again.
     *
     * @param line  Line to pass.
     * @return      Modified line, without the @=XXX inline code.
     */
    private String parseAddFeatures(String line) {
        if (line == null) {
            return "";
        }

        while (line.contains("@=")) {
            String code = line.substring(line.indexOf("@=") + 2);
            code = code.replaceAll("[^A-Za-z].*", "");
            line = line.replaceFirst("@=*" + code + " ?", "");

            if (hasPhrase(code + ".title")) {
                // This code has it's own section, so save it for later.
                extraSections.add(code);
            }

            try {
                // The code should be an enum value. Now it gets tricky.
                String type = planet.getType().toString();
                String group = planet.getType().getGroup().toString().toLowerCase();

                String prefix = ENUM_PREFIX + group + "." + type;
                Class planetClass = Class.forName(prefix);

                Enum  feature = null;
                while (planetClass != null && feature == null) {
                    Class[] inner = planetClass.getDeclaredClasses();
                    for (Class c : inner) {
                        try {
                            feature = Enum.valueOf(c, code);
                            break;
                        } catch (IllegalArgumentException e) {
                            // Not found anything here.
                        }
                    }
                    planetClass = planetClass.getSuperclass();
                }

                if (feature != null) {
                    planet.addFeature((PlanetFeature) feature);
                }
            } catch (Throwable e) {
                logger.warn(String.format("Failed to locate code [%s]", code));
            }
        }

        return line;
    }

    /**
     * Parse any trade code removers. These are defined by "@-", and remove a trade code from the planet
     * if the code is present. The two letter trade code should directly follow the "@-", e.g. "@-In".
     * The trade code is removed from the text after being processed.
     *
     * This does not work on resources.
     *
     * @param line  Line of text to be parsed.
     * @return      Modified line with any trade code removers removed.
     */
    private String parseRemoveTradeCodes(String line) {
        if (line == null) {
            return "";
        }
        while (line.contains("@-")) {
            String code = line.substring(line.indexOf("@-") + 2);
            code = code.replaceAll("[^A-Za-z0-9].*", "");

            try {
                System.out.println(code);
                if (code.length() == 2) {
                    if (planet != null) {
                        planet.removeTradeCode(TradeCode.valueOf(code));
                    }
                } else if (planet != null && generator != null) {
                    CommodityName name = CommodityName.valueOf(code);
                    System.out.println("Name: " + name);
                    generator.reduceResource(planet, name);
                    System.out.println("Reduced");
                }
            } catch (Throwable e) {
                logger.warn(String.format("Unrecognised code or commodity [%s]", code));
            }

            line = line.replaceFirst("@\\-" + code + " ?", "");
        }

        return line;
    }

    /**
     * Parses property getters and inserts their value into the text. Uses "$" as the control code.
     * Numbers are displayed with commas, enums are not expanded.
     *
     * @param line  Line of text to be parsed.
     * @return      Modified line with the property values replaced.
     */
    private String parsePropertyValues(String line) {
        if (line == null) {
            return "";
        }
        while (line.contains("$")) {
            String prop = line.substring(line.indexOf("$") + 1);
            prop = prop.replaceAll("[^A-Za-z0-9_:~!#].*", "");

            line = line.replaceFirst("\\$" + prop, getProperty(prop));
        }
        return line;
    }


    /**
     * Parses any feature switches in the text. A feature switch evaluates to a planetary feature if that
     * feature is present on the planet, otherwise it evaluates to a default value or empty string.
     *
     * "(?|Feature1|Feature2|Feature3)" - Returns the first matching feature, or an empty string.
     * "(?Default|Feature1|Feature2)" - Returns the first matching feature, or "Default".
     * "(?|Feature1=value1|Feature2=value2)" - Returns "value1" or "value2" if there is a match.
     *
     * @param line  Line of text to be parsed.
     * @return      Modified line with the features expanded if they match.
     */
    private String parseFeatureSwitches(String line) {
        if (line == null) {
            return "";
        }
        while (line.contains("(?") && line.contains(")")) {
            String options = line.substring(line.indexOf("(?") + 2,
                    line.indexOf(")"));
            String[] tokens = options.split("\\|");
            String option = "";
            String defaultValue = tokens[0];

            for (int i=1; i < tokens.length; i++) {
                String feature = tokens[i];
                String value = feature;
                if (feature.contains("=")) {
                    feature = feature.replaceAll("=.*", "");
                    value = value.replaceAll(".*=", "");
                }

                if (planet != null && feature.length() == 2) {
                    try {
                        TradeCode code = TradeCode.valueOf(feature);
                        if (planet.getTradeCodes().contains(code)) {
                            option = value;
                            break;
                        }
                    } catch (IllegalArgumentException e) {
                        // Just ignore if not a valid trade code.
                        logger.warn("Unrecognised trade code [" + feature + "]");
                    }
                }

                if (planet != null && planet.hasFeature(feature)) {
                    option = value;
                    break;
                } else if (civGenerator != null && civGenerator.hasFeature(feature)) {
                    option = value;
                    break;
                }
            }
            if (option.length() == 0) {
                option = defaultValue;
            }

            line = line.replaceFirst("\\(.*?\\)", option);
        }
        return line;
    }

    /**
     * Parse a list of Resources, and returns the one with the largest value. By default, the expanded name of the
     * resource will be returned. "!" can be used before the name to force the return of the raw value.
     *
     * "(%|Resource1|Resource2|Resource3)" - returns "Resource One" if that has highest value.
     * "(%|!Resource1|!Resource2|!Resource3)" - returns "Resource1" if that has highest value.
     * "(%default|Resource1|Resource2)" - returns "default" if none of the resources are higher than zero.
     * "(%min,default|Resource1|Resource2)" - ignores values lower than 'min', then calculates highest.
     *
     * @param line  Line of text to be parsed.
     * @return      Modified line with the switch expanded to the matching value.
     */
    private String parseResourceSwitches(String line) {
        if (line == null) {
            return "";
        }
        while (line.contains("(%") && line.contains(")")) {
            String options = line.substring(line.indexOf("(%") + 2, line.indexOf(")"));
            String[] tokens = options.split("\\|");
            String option = "";
            String defaultValue = tokens[0];
            int    minValue = 0;

            if (defaultValue.matches("[0-9].*")) {
                minValue = Integer.parseInt(defaultValue.replaceAll("[^0-9]", ""));
                if (defaultValue.contains(",")) {
                    defaultValue = defaultValue.replaceAll(".*,", "");
                } else {
                    defaultValue = "";
                }
            }

            if (planet != null) {
                int    highest = 0;

                for (int i=1; i < tokens.length; i++) {
                    String resourceName = tokens[i].replaceAll("!", "");

                    CommodityName commodity;
                    try {
                        commodity = CommodityName.valueOf(resourceName);
                    } catch (IllegalArgumentException e) {
                        // Not a valid resource name.
                        continue;
                    }

                    int value = planet.getResource(commodity);
                    if (value >= minValue && value > highest) {
                        highest = value;
                        if (tokens[i].startsWith("!")) {
                            option = resourceName;
                        } else {
                            option = commodity.getName();
                        }
                    }
                }
            }
            if (option.length() == 0) {
                option = defaultValue;
            }

            line = line.replaceFirst("\\(%.*?\\)", option);
        }
        return line;
    }

    // (VARIABLE|VALUE=a|VALUE=b|VALUE=c|DEFAULT)
    // If the VARIABLE is equal to a VALUE, display the option for that
    // value.
    // If VALUE> or VALUE< is used (instead of VALUE=), select the
    // option if the
    // VALUE is greater than or less than the VARIABLE.


    /**
     * Parses any switches in the text. Checks the variable and compares it against different values.
     *
     * "(VARIABLE|VALUE=a|VALUE=b|VALUE=c|DEFAULT)"
     *
     * If the variable matches the value, then the given text is returned. If there are no matches, then the
     * DEFAULT is returned. If no default is given, then nothing is returned. If there are multiple matches,
     * then the first match is returned.
     *
     * Other comparisons are possible:
     * "VALUE>text" - Returns text if the VALUE is greater than the variable.
     * "VALUE<text" - Returns text if the VALUE is less than the variable.
     * "V1,V2,V3=text" - Retuns text if the variable matches V1, V2 or V3.
     *
     * @param line  Line of text to be parsed.
     * @return      Modified line with the switch expanded to the matching value.
     */
    private String parseSwitches(String line) {
        if (line == null) {
            return "";
        }
        while (line.contains("(") && line.contains(")")) {
            String options = line.substring(line.indexOf("(") + 1,
                    line.indexOf(")") + 1);
            String[] tokens = options.replaceAll("\\)", "").split("\\|");
            String result = "";
            String value = tokens[0];

            TOKENS: for (int i = 1; i < tokens.length; i++) {
                String test = tokens[i].replaceFirst("[=<>].*", "");
                String option = tokens[i].replaceFirst(".*?[=<>]", "");

                if (tokens[i].contains("=")) {
                    if (test.contains(",")) {
                        String[] multi = test.split(",");
                        for (int j=0; j < multi.length; j++) {
                            if (multi[j].equals(value)) {
                                result = option;
                                break TOKENS;
                            }
                        }
                    } else if (test.equals(value)) {
                        result = option;
                        break;
                    }
                } else if (tokens[i].contains("<")) {
                    try {
                        Long v = Long.parseLong(value);
                        Long t = Long.parseLong(test);
                        if (t < v) {
                            result = option;
                            break;
                        }
                    } catch (NumberFormatException e) {
                        if (value.compareToIgnoreCase(test) < 0) {
                            result = option;
                            break;
                        }
                    }

                } else if (tokens[i].contains(">")) {
                    // Is the tested value greater than this case?
                    try {
                        Long v = Long.parseLong(value);
                        Long t = Long.parseLong(test);
                        if (t > v) {
                            result = option;
                            break;
                        }
                    } catch (NumberFormatException e) {
                        if (value.compareToIgnoreCase(test) > 0) {
                            result = option;
                            break;
                        }
                    }
                } else {
                    // Default value.
                    result = option;
                    break;
                }
            }

            line = line.replaceFirst("\\(.*?\\)", result);
        }
        return line;
    }

    private HashMap<String,String> savedPhrases = new HashMap<>();

    public String getSavedPhrase(String key) {
        if (savedPhrases.containsKey(key)) {
            return savedPhrases.get(key);
        }
        return null;
    }

    public void setSavedPhrase(String key, String value) {
        savedPhrases.put(key, value);
    }

    /**
     * Parses random phrases in the text. For a random block, a single phrase in that block is selected for
     * insertion. Uses "[v1|v2|v3]", will select one of v1, v2 or v3.
     *
     * Selections can be named and saved, to be re-used later. User NAME:: at the start to give a name to
     * a selection, e.g. "[GOV::governments|families|crime mobs]". Any future selection which has the same
     * start identifier, will use whatever was picked the first time. The identifier must be upper case
     * letters (A-Z) only.
     *
     * Because of this, it isn't necessary to list the options second and subsequent times. e.g. "[GOV::]"
     * will be good enough to pick up the previously matched government type. However, if they are listed
     * then they'll get used as a fallback in case the first set of phrases were missed.
     *
     * @param line  Line of text to be parsed.
     * @return      Modified line with any random elements selected.
     */
    private String parseRandomPhrases(String line) {
        while (line.contains("[") && line.contains("]")) {
            int start = line.indexOf("[");
            int end = line.indexOf("]") + 1;
            if (end < start) {
                // Malformed text string. Try to repair and continue.
                logger.warn(String.format("Text string <<%s>> is malformed, probably spurious ']'", line));
                line = line.replaceFirst("]", "");
                continue;
            }

            String  options = line.substring(start, end);
            String  option;
            String  save = null;
            boolean display = true;

            // Set for later recovery, but don't display.
            if (options.matches("\\[[A-Z]+==.*")) {
                save = options.replaceAll("==.*", "").substring(1);
                options = options.replaceAll("[A-Z]+==", "");
                display = false;
            }
            // Set for later recovery, and also display.
            if (options.matches("\\[[A-Z]+::.*")) {
                save = options.replaceAll("::.*", "").substring(1);
                options = options.replaceAll("[A-Z]+::", "");
            }
            if (savedPhrases.containsKey(save)) {
                option = savedPhrases.get(save);
            } else {
                option = random(options);
                if (save != null) {
                    setSavedPhrase(save, option);
                }
            }
            if (!display) {
                option = "";
            }

            line = line.replaceFirst("\\[.*?]", option);
        }
        return line;
    }

    /**
     * Expands substitution phrases in the text. For a block "{phrase}", will replace with the text from the
     * given line. For a random selection of lines, use "{phrase1|phrase2|phrase3}". Selects one and expands
     * that.
     *
     * @param line  Line of text to be parsed.
     * @return      Modified line with any random elements selected.
     */
    private String parseSubstitutedPhrases(String line) {
        while (line.contains("{") && line.contains("}")) {
            String options = line.substring(line.indexOf("{"),
                    line.indexOf("}") + 1);
            String option = random(options);
            option = parse(getPhrase(option));

            line = line.replaceFirst("\\{.*?}", option);
        }
        return line;
    }

    /**
     * Parse a line of text which has flow control sections. Currently supported
     * flow control consists of: [a|b|c] - Choose one of a, b or c and display
     * that. {a|b|c} - Choose one of a, b or c and look it up as a key phrase,
     * then parse that before displaying it.
     *
     * @param line
     *            Line to be parsed.
     * @return Result instance of the parsed line.
     */
    protected String parse(String line) {
        if (line == null || line.length() == 0) {
            return "";
        }

        try {
            // Replace any property variables, and change enums to lower case.
            line = parseEnglishPropertyValues(line);

            // Replace any property variables.
            line = parsePropertyValues(line);

            // Feature switches.
            line = parseFeatureSwitches(line);

            // Replace with highest resource.
            line = parseResourceSwitches(line);

            // Switch statements.
            line = parseSwitches(line);

            // Choosen a random phrase.
            line = parseRandomPhrases(line);

            // Replace a random option with the substituted phrase.
            line = parseSubstitutedPhrases(line);

            // Add a trade code or resource.
            line = parseAddTradeCodes(line);

            // Remove a trade code.
            line = parseRemoveTradeCodes(line);

            // Add any planetary features.
            line = parseAddFeatures(line);
        } catch (Throwable e) {
            System.out.println("Unable to parse [" + line + "] ("
                    + e.getMessage() + ")");
            e.printStackTrace();
        }

        // No clean up the text to remove unwanted whitespace.
        line = line.trim();
        line = line.replaceAll("  *", " ");
        line = line.replaceAll(" ([.,;])", "$1");

        return line;
    }

    /**
     * Gets the property method for a given object. Looks for getX(), isX() and hasX() in that order.
     * The first found method will be returned.
     *
     * @param object        Object to search on.
     * @param property      Name of property to look for.
     * @return              First found method, or null.
     */
    private Method getPropertyMethod(Object object, String property) {
        for (String prefix : new String[] { "has", "is", "get" }) {
            try {
                return object.getClass().getMethod(prefix + property);
            } catch (NoSuchMethodException e) {
                // Just keep on looking.
            }
        }

        return null;
    }

    /**
     * Get the named property from the Planet object. Uses reflection to call
     * the right getter on the Planet. If no such property is found, then the
     * empty string is returned. Result is always a string. If the contents
     * looks like a number, then it will be formatted and truncated to 1dp if
     * necessary.
     *
     * If the property name is of the form xDy, then a dice roll is actually
     * done instead, rolling x dice of size y. e.g., $3D6 translates as roll 3
     * six sided dice, and return the result.
     *
     * @param name
     *            Name of property to fetch.
     * @return Value of the property, or empty string.
     */
    private String getProperty(String name) {
        String value = "";
        if (planet == null && system == null) {
            return value;
        }

        if (name.startsWith("#")) {
            // This is a resource.
            if (planet != null) {
                return "" + planet.getResource(CommodityName.valueOf(name.substring(1)));
            } else {
                return "";
            }
        }

        boolean prettyPrint = true;
        boolean roundedPrint = false;
        int     precision = 0;
        boolean englishPrint = false;
        boolean distance = false;

        if (name.startsWith("!")) {
            // Force numbers to be printed without any special formatting.
            prettyPrint = false;
            name = name.substring(1);
        } else if (name.startsWith("~")) {
            // Number is printed in approximate English words rather than precisely.
            englishPrint = true;
            prettyPrint = false;
            name = name.substring(1);
        } else if (name.matches("[1-9]_.*")) {
            // Round number to the given precision, e.g. $4_Distance
            roundedPrint = true;
            precision = Integer.parseInt(name.substring(0, 1));
            name = name.substring(2);
        }
        if (name.matches("d_.*")) {
            distance = true;
            name = name.substring(2);
        }

        try {
            // Possible to make die rolls.
            if (Die.isRoll(name)) {
                value = "" + Die.roll(name);
            } else if (name.startsWith("Facility") && facility != null) {
                name = name.replaceAll("Facility", "");
                Method method = getPropertyMethod(facility, name);
                if (method != null) {
                    Object result = method.invoke(facility);
                    value = "" + result;
                } else {
                    value = facility.getExtras().get(name);
                    if (value == null) {
                        return "";
                    }
                }
            } else if (planet != null) {
                Method method = getPropertyMethod(planet, name);
                if (method != null) {
                    Object result = method.invoke(planet);
                    value = "" + result;
                } else {
                    value = planet.getExtras().get(name);
                    if (value == null) {
                        return "";
                    }
                }
            } else if (system != null) {
                if (name.contains(":")) {
                    int idx = Integer.parseInt(name.replaceAll(".*:", ""));
                    name = name.replaceAll(":.*", "");
                    Method method = getPropertyMethod(system.getStars().get(idx), name);
                    if (method != null) {
                        Object result = method.invoke(system.getStars().get(idx));
                        value = "" + result;
                    } else {
                        return "";
                    }
                } else {
                    Method method = getPropertyMethod(system, name);
                    if (method != null) {
                        Object result = method.invoke(system);
                        value = "" + result;
                    } else {
                        return "";
                    }
                }
            }

            try {
                String suffix = "";
                if (distance) {
                    double i = Double.parseDouble(value);
                    if (i > 1_000_000_000) {
                        suffix = " AU";
                        i /= Physics.AU;
                    } else if (i > 10_000_000) {
                        suffix = " Mkm";
                        i /= 1_000_000;
                    } else {
                        suffix = " km";
                    }
                    value = "" + i;
                }
                if (roundedPrint) {
                    double i = Double.parseDouble(value);
                    value = "" + Physics.round(i, precision);
                }
                if (prettyPrint) {
                    double i = Double.parseDouble(value);
                    DecimalFormat format = new DecimalFormat();
                    format.setMaximumFractionDigits(1);
                    value = format.format(i);
                }
                if (englishPrint) {
                    double i = Double.parseDouble(value);
                    value = getEnglishNumber(i);
                }
                value += suffix;
            } catch (NumberFormatException e) {
                // This isn't a number, so no parsing is needed.
            }
        } catch (Throwable e) {
            logger.warn("getProperty: Cannot find method for [" + name + "] (" + e.getMessage() + ")");
            value = "";
        }

        return value;
    }

    private String getEnglishNumber(double number) {
        DecimalFormat format = new DecimalFormat();
        format.setMaximumFractionDigits(1);
        String value = format.format(number);

        if (number >= 10_000_000_000_000L) {
            number = (long) (number + 500_000_000_000L) / 1_000_000_000_000L;
            format.setMaximumFractionDigits(0);
            value = format.format(number);
            value += " trillion";
        } else if (number >= 1_000_000_000_000L) {
            number = (long) (number + 50_000_000_000L) / 100_000_000_000L;
            value = (number / 10.0) + " trillion";
        } else if (number > 10_000_000_000L) {
            number = (long) (number + 500_000_000L) / 1_000_000_000L;
            value = (long) number + " billion";
        } else if (number > 1_000_000_000L) {
            number = (long) (number + 50_000_000L) / 100_000_000L;
            value = (number / 10.0) + " billion";
        } else if (number > 10_000_000L) {
            number = (long) (number + 500_000L) / 1_000_000L;
            value = (long) number + " million";
        } else if (number > 1_000_000L) {
            number = (long) (number + 50_000L) / 100_000L;
            value = (number / 10.0) + " million";
        } else if (number > 10_000L) {
            number = (long) (number + 500L) / 1_000L;
            value = (long) number + " thousand";
        }

        return value;
    }

    private StringBuffer addText(StringBuffer buffer, String key, int percentChance) {
        if (Die.d100() > percentChance)
            return buffer;

        String text = getPhrase(key);
        if (text == null || text.length() == 0) {
            return buffer;
        }

        if (buffer.length() > 0) {
            buffer.append(" ");
        }
        buffer.append(parse(text).trim());

        return buffer;
    }

    private int getNumber(String key, String subKey, int defaultValue) {
        if (subKey != null && subKey.length() > 0) {
            try {
                return Integer.parseInt(getPhrase(key + "." + subKey));
            } catch (Throwable e) {
                // Number isn't set, or isn't a valid number.
            }
        }
        try {
            return Integer.parseInt(getPhrase(key));
        } catch (Throwable e) {
            // Number isn't set, or isn't a valid number.
        }
        return defaultValue;
    }

    /**
     * Gets the full description for this planet. As well as getting a basic description based on the
     * type of the planet, it also returns text describing the planet's temperature, atmosphere and
     * biosphere if these are available.
     *
     * @return          String containing the full description of this planet.
     */
    public String getFullDescription() {
        return getFullDescription("planet");
    }

    /**
     * Gets the full description for this planet. As well as getting a basic description based on the
     * type of the planet, it also returns text describing the planet's temperature, atmosphere and
     * biosphere if these are available.
     *
     * There are certain planet features, that if defined, will override the normal descriptions.
     * If a planet has such a feature, the description for this feature will be returned instead.
     *
     * @param rootKey   This is the planet type.
     * @return          String containing the full description of this planet.
     */
    private String getFullDescription(String rootKey) {
        buffer = new StringBuffer();

        final int habChance = getNumber("HAB_CHANCE", "H"+planet.getHabitability(), 75);
        final int temperatureChance = getNumber("TEMPERATURE_CHANCE",
                Temperature.getTemperature(planet.getTemperature()).toString(), 75);
        final int atmosphereChance = getNumber("ATMOSPHERE_CHANCE", planet.getAtmosphere().toString(), 75);
        final int biosphereChance = getNumber("BIOSPHERE_CHANCE", planet.getLife().toString(), 75);
        final int specialChance = getNumber("SPECIAL_CHANCE", null,100);

        // Look for features definitions of form feature.<feature>
        for (PlanetFeature feature : planet.getFeatures()) {
            String key = String.format("feature.%s", feature.toString());
            if (hasPhrase(key)) {
                addText(buffer, key, feature.getPrimaryNotability());
                if (buffer.length() > 0) {
                    buffer.insert(0, "<p>").append("</p>");
                }
                break;
            }
        }

        if (buffer.length() == 0) {
            List<StringBuffer>  text = new ArrayList<>();

            for (PlanetFeature feature : planet.getFeatures()) {
                String key = String.format("%s.%s", rootKey, feature.toString());

                if (phrases.containsKey(key)) {
                    text.add(addText(new StringBuffer(), key, feature.getPrimaryNotability()));
                    if (phrases.containsKey(key + ".special")) {
                        text.add(addText(new StringBuffer(), key + ".special", 100));
                    }
                    break;
                }
            }

            if (text.size() == 0) {
                String habKey = rootKey + ".H" + planet.getHabitability();
                if (hasPhrase(habKey)) {
                    // Use text specific to this type of habitat, with a chance of falling back to default.
                    text.add(addText(new StringBuffer(), habKey, habChance));
                }
            }
            if (text.size() == 0) {
                // There must always be a default available. This is assumed.
                text.add(addText(new StringBuffer(), rootKey, 100));
            }
            text.add(addText(new StringBuffer(), rootKey + ".temperature." + planet.getTemperature(), temperatureChance));

            // Allow use of "Any" to match atmosphere type and pressure.
            String atmosKey = rootKey + ".atmosphere." + planet.getAtmosphere() + "." + planet.getPressureText();
            if (!hasPhrase(atmosKey)) {
                atmosKey = rootKey + ".atmosphere." + planet.getAtmosphere();
            }
            if (!hasPhrase(atmosKey)) {
                atmosKey = rootKey + ".atmosphere.Any." + planet.getPressureText();
            }
            if (hasPhrase(atmosKey)) {
                text.add(addText(new StringBuffer(), atmosKey, atmosphereChance));
            }
            text.add(addText(new StringBuffer(), rootKey + ".biosphere." + planet.getLife(), biosphereChance));

            boolean hasFeature = false;
            for (PlanetFeature feature : planet.getFeatures()) {
                String key = String.format("%s.feature.%s", rootKey, feature.toString());

                if (phrases.containsKey(key)) {
                    text.add(addText(new StringBuffer(), key, feature.getNotability()));
                    if (text.get(text.size() - 1).length() > 0) {
                        hasFeature = true;
                    }
                } else {
                    logger.error("MISSING KEY " + key);
                }
            }
            if (!hasFeature) {
                text.add(addText(new StringBuffer(), rootKey + ".special", specialChance));
            }

            for (String key : extraSections) {
                if (phrases.containsKey(key)) {
                    String titleKey = key + ".title";
                    text.add(addText(new StringBuffer(), key + ".title", 100));
                    text.get(text.size() - 1).insert(0, "<h5>").append("</h5>");
                    text.add(addText(new StringBuffer(), key, 100));
                }
            }

            // Now we need to piece together all the text into paragraphs. Whenever the text gets belong a
            // certain length, we create a new paragraph.
            buffer = getParagraphs(text);
        }

        // A biosphere. entry should not exist alongside a planet.type.biosphere. entry of the same type. The former
        // are meant for worlds with simple biospheres where it isn't that important. However, you might have a mix
        // of different types, as follows:
        //   planet.TYPE.biosphere.Organic
        //   planet.TYPE.biosphere.Archaean
        //   biosphere.Aerobic
        //   biosphere.ComplexOcean
        String bioKey = "biosphere." + planet.getLife();
        if (phrases.containsKey(bioKey)) {
            buffer.append("\n<h4>Ecology</h4>\n<p>");
            addText(buffer, bioKey, 100);
            buffer.append("</p>");
        }

        // Add description for any trade codes.
		/*
		 * for (String code : planet.getTradeCodes()) { String key =
		 * rootKey+".trade."+code; if (phrases.getProperty(key) != null) {
		 * addText(buffer, key, 100); } else { key = "trade."+code; if
		 * (phrases.getProperty(key) != null) { addText(buffer, key, 100); } } }
		 */
        return buffer.toString().replaceAll(" +", " ").trim();
    }

    final static int PARAGRAPH_LENGTH = 400;

    private StringBuffer getParagraphs(List<StringBuffer> text) {
        StringBuffer    buffer = new StringBuffer();
        int             length = 0;

        for (StringBuffer b : text) {
            if (b != null && b.length() > 0) {
                // If we have no text length recorded, this is a new paragraph.
                if (b.toString().startsWith("<h5>")) {
                    if (length != 0) {
                        buffer.append("</p>");
                        length = 0;
                    }
                    buffer.append(b.toString().trim());
                    continue;
                }
                if (length == 0) {
                    buffer.append("<p>");
                } else {
                    buffer.append(" ");
                }
                length += b.length();
                buffer.append(b.toString().trim());
            }
            if (length > PARAGRAPH_LENGTH) {
                buffer.append("</p>");
                length = 0;
            }
        }
        if (length > 0) {
            buffer.append("</p>");
        }
        return buffer;
    }

    public void setCivilisationGenerator(CivilisationGenerator generator) {
        this.civGenerator = generator;
    }

    /**
     * Gets a description for a Facility. Like for planets, there are rules in how the description is built up.
     *
     * feature.FEATURE
     *
     * or
     *
     * ( facility.FEATURE || facility.PLANETGROUP || facility )
     * facility.government.GOVERNMENT
     * ( facility.feature.FEATURE || facility.special )
     *
     * @return  String describing the facility.
     */
    public String getFacilityDescription() {
        buffer = new StringBuffer();

        for (CivilisationFeature feature: civGenerator.getFeatures()) {
            String key = String.format("feature.%s", feature.toString());
            if (hasPhrase(key)) {
                addText(buffer, key, 100);
                if (buffer.length() > 0) {
                    buffer.insert(0, "<p>").append("</p>");
                }
                break;
            }
        }

        if (buffer.length() == 0) {
            List<StringBuffer>  text = new ArrayList<>();

            // First look for a root key based on an existing feature.
            for (CivilisationFeature feature : facility.getFeatures()) {
                String key = String.format("facility.%s", feature.toString());

                if (phrases.containsKey(key)) {
                    text.add(addText(new StringBuffer(), key, 100));
                    break;
                }
            }

            // Next, look for one based on the planet classification type, then just the planet classification group.
            if (text.size() == 0) {
                String groupKey = "facility." + planet.getType().getClassification().getGroup();
                String typeKey = groupKey + "." + planet.getType();
                if (hasPhrase(typeKey)) {
                    text.add(addText(new StringBuffer(), typeKey, 100));
                } else if (hasPhrase(groupKey)) {
                    text.add(addText(new StringBuffer(), groupKey, 100));
                }
            }

            // Failing all the above, start with the default root.
            if (text.size() == 0) {
                text.add(addText(new StringBuffer(), "facility", 100));
            }
            for (String key : new String[] {
                    "facility.government." + planet.getGovernment(),
                    "facility.law." + planet.getLawLevel(),
                    "facility.port." + planet.getStarPort().getCode() }) {

                if (hasPhrase(key)) {
                    text.add(addText(new StringBuffer(), key, 75));
                }
            }

            boolean hasFeature = false;
            for (CivilisationFeature feature : facility.getFeatures()) {
                String key = "facility.feature." + feature;
                if (hasPhrase(key)) {
                    text.add(addText(new StringBuffer(), key, 100));
                }
            }

            if (!hasFeature) {
                text.add(addText(new StringBuffer(), "facility.special", 100));
            }

            for (String key : extraSections) {
                if (phrases.containsKey(key)) {
                    String titleKey = key + ".title";
                    text.add(addText(new StringBuffer(), key + ".title", 100));
                    text.get(text.size() - 1).insert(0, "<h5>").append("</h5>");
                    text.add(addText(new StringBuffer(), key, 100));
                }
            }
            buffer = getParagraphs(text);
        }

        return buffer.toString().replaceAll(" +", " ").trim();
    }

    public String getSystemDescription(String rootKey) {
        buffer = new StringBuffer();

        addText(buffer, "system." + rootKey, 100);

        for (StarSystemCode code : system.getTradeCodes()) {
            String key = "system." + rootKey + "." + code.name();
            if (!phrases.containsKey(key)) {
                key = "system." + code.name();
            }
            addText(buffer, key, code.getNotability());
        }

        return buffer.toString().replaceAll(" +", " ").trim();
    }

    /**
     * Get text for a specific key. This can be used to generate snippets of text rather than full
     * planetary or system descriptions.
     *
     * @param key   Key to generate text for.
     * @return      Generated text.
     */
    public String getText(String key) {
        buffer = new StringBuffer();
        addText(buffer, key, 100);

        return buffer.toString().replaceAll(" +", " ").trim();
    }

    public static void main(String[] args) {

        Planet p = new Planet();
        Facility f = new Facility();

        p.setGovernment(Government.Anarchy);
        p.setStarPort(StarPort.Do);
        f.setName("RamshackleDocks");
        f.setType(FacilityType.STARPORT);

        TextGenerator tg = new TextGenerator(p, f);

        System.out.println(tg.getFacilityDescription());
    }
}
