/*
 * Copyright (c) 2018, Samuel Penn (sam@glendale.org.uk).
 * See the file LICENSE at the root of the project.
 */

package uk.org.glendale.worldgen.civ.civilisation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.org.glendale.utils.rpg.Die;
import uk.org.glendale.worldgen.WorldGen;
import uk.org.glendale.worldgen.astro.Physics;
import uk.org.glendale.worldgen.astro.planets.Planet;
import uk.org.glendale.worldgen.astro.planets.codes.*;
import uk.org.glendale.worldgen.astro.systems.StarSystem;
import uk.org.glendale.worldgen.astro.systems.Zone;
import uk.org.glendale.worldgen.civ.CivilisationFeature;
import uk.org.glendale.worldgen.civ.CivilisationGenerator;
import uk.org.glendale.worldgen.civ.Facility;
import uk.org.glendale.worldgen.civ.facility.agriculture.FoodVats;
import uk.org.glendale.worldgen.civ.facility.residential.*;
import uk.org.glendale.worldgen.civ.facility.starport.AsteroidDocks;
import uk.org.glendale.worldgen.civ.facility.starport.LandingField;
import uk.org.glendale.worldgen.civ.facility.starport.RamshackleDocks;
import uk.org.glendale.worldgen.text.TextGenerator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import static uk.org.glendale.worldgen.astro.planets.codes.PlanetGroup.*;
import static uk.org.glendale.worldgen.astro.planets.codes.PlanetType.*;

/**
 * Hermits cover any type of settlement which actively avoids contact with the outside world. They may be
 * actual hermits, secluded religious groups or cults, or outposts deliberately located away from civilisation
 * for protection or due to social distaste, such as prisons or military outposts.
 */
public class Hermits extends CivilisationGenerator {
    private static final Logger logger = LoggerFactory.getLogger(Hermits.class);

    public Hermits(WorldGen worldGen, StarSystem system) {
        super(worldGen, system);
    }

    public void generate(String civ, CivilisationFeature... features) {
        logger.info("Adding Hermits to [" + system.getName() + "]");
        setFeatures(features);
        findPlanetsAvailable();

        List<Facility> facilities = new ArrayList<Facility>();
        Planet         bestPlanet = null;
        if (hasHabitableWorld) {
            logger.debug("hasHabitableWorld");
            // Not great for hermits, since such worlds are in high demand. There may be something
            // out in the outer ice rings though.
            if (hasIceBelt) {
                // Find the furthest icy belt and use that.
                for (Planet planet : system.getPlanets()) {
                    if (planet.getType().getGroup() == PlanetGroup.Belt) {
                        if (planet.getType() == PlanetType.IceBelt || planet.getType() == PlanetType.OortCloud) {
                            bestPlanet = planet;
                        }
                    }
                }
                if (bestPlanet != null) {
                    createIceFarmers(bestPlanet, facilities);
                    generateDescription(bestPlanet, facilities);
                }
            }
        } else if (hasRockWorld) {
            logger.debug("hasRockWorld");
            bestPlanet = getBestPlanet(4, Dwarf, Terrestrial, SmallBody);
            if (bestPlanet != null) {
                try {
                    switch (Die.d4()) {
                        case 1:
                            createMonasticOutpost(bestPlanet, facilities, features);
                            break;
                        case 2:
                            createOutlawOutpost(bestPlanet, facilities, features);
                            break;
                        case 3:
                            createPrisonOutpost(bestPlanet, facilities, features);
                            break;
                        case 4:
                            createEliteOutpost(bestPlanet, facilities, features);
                            break;
                    }
                } catch (IllegalStateException e) {
                    createMonasticOutpost(bestPlanet, facilities, features);
                }
                generateDescription(bestPlanet, facilities);
            }
        }
        if (bestPlanet == null && hasAsteroidBelt) {
            // Find a defined asteroid in an asteroid belt. Select the largest.
            for (Planet planet : system.getPlanets()) {
                if (planet.getType() == PlanetType.AsteroidBelt) {
                    for (Planet moon : getMoonsOf(planet)) {
                        if (moon.getType().getGroup() == SmallBody) {
                            if (Die.d4() == 1) {
                                createRockHermits(moon, facilities);
                                generateDescription(moon, facilities);
                            } else if (bestPlanet == null) {
                                bestPlanet = moon;
                            } else if (moon.getRadius() > bestPlanet.getRadius()) {
                                bestPlanet = moon;
                            }
                        }
                    }
                }
            }
            if (bestPlanet != null) {
                createRockHermits(bestPlanet, facilities);
                generateDescription(bestPlanet, facilities);
            }
        }
        if (bestPlanet == null && hasDustDisc) {
            for (Planet planet : system.getPlanets()) {
                if (planet.getType() == DustDisc) {
                    bestPlanet = planet;
                }
            }
            if (bestPlanet != null) {
                createDustFarmers(bestPlanet, facilities);
                generateDescription(bestPlanet, facilities);
            }
        }
    }

    private void addMinorBeltSettlements(CivilisationFeature... features) {
        var planets = new ArrayList<Planet>();
        for (Planet planet : system.getPlanets()) {
            if (planet.isA(Belt)) {
                planets.add(planet);
            }
        }
        if (planets.size() > 0) {
            features = CivilisationFeature.Secondary.addTo(features);
            List<Facility> facilities = new ArrayList<Facility>();
            Planet planet = planets.get(Die.rollZero(planets.size()));
            switch (planet.getType()) {
                case IceBelt:
                    createIceFarmers(planet, facilities, features);
                    break;
                case AsteroidBelt:
                    createRockFarmers(planet, facilities, features);
                    break;
                case DustDisc:
                    createDustFarmers(planet, facilities, features);
                    break;
            }
            planet.setStarPort(planet.getStarPort().getLocal());
            generateDescription(planet, facilities);
        }
    }

    public void createIceHermits(final Planet planet, final List<Facility> facilities, CivilisationFeature... features) {
        logger.info("Creating ice hermits at [" + planet.getName() + "]");
        setFeatures(features);
        planet.setTechLevel(4 + Die.d2());

        if (hasFeature(CivilisationFeature.LargePopulation)) {
            planet.setPopulation(Die.d12(10));
        } else if (hasFeature(CivilisationFeature.HugePopulation)) {
            planet.setPopulation(Die.d20(20));
            planet.setTechLevel(6);
        } else {
            planet.setPopulation(Die.d6(5));
        }

        Facility residential = new RockHermits(planet).getFacility();
        if (hasFeature(CivilisationFeature.Poor)) {
            residential.setRating(residential.getRating() - 25);
        } else if (hasFeature(CivilisationFeature.Rich)) {
            residential.setRating(residential.getRating() + 25);
        }
        facilities.add(residential);

        Facility starport = new AsteroidDocks(planet).getFacility();
        if (hasFeature(CivilisationFeature.Poor)) {
            starport.setRating(starport.getRating() - 15);
        } else if (hasFeature(CivilisationFeature.Rich)) {
            starport.setRating(starport.getRating() + 15);
        }
        facilities.add(starport);

    }

    public void createDustFarmers(final Planet planet, final List<Facility> facilities, CivilisationFeature... features) {
        logger.info("Creating dust farmers at [" + planet.getName() + "]");
        setFeatures(features);
        planet.setTechLevel(4 + Die.d2());

        long population = Die.d10(10);
        if (hasFeature(CivilisationFeature.SmallPopulation)) {
            population *= 10;
        } else if (hasFeature(CivilisationFeature.LargePopulation)) {
            population *= 100;
        } else if (hasFeature(CivilisationFeature.HugePopulation)) {
            population *= 300;
            planet.setTechLevel(6);
        } else {
            population *= 30;
        }
        population += Die.d100(); // Make the number look more natural.
        if (hasFeature(CivilisationFeature.Secondary)) {
            population /= ((Die.d3() == 1)?4:2);
        }
        planet.setPopulation(Physics.round(population));

        Facility residential = new DustFarmers(planet).getFacility();
        if (hasFeature(CivilisationFeature.Poor)) {
            residential.setRating(residential.getRating() - 10);
        } else if (hasFeature(CivilisationFeature.Rich)) {
            residential.setRating(residential.getRating() + 5);
        }

        logger.debug("Setting residential");
        worldGen.getPlanetFactory().setFacility(residential);
        facilities.add(residential);

        Facility port = new RamshackleDocks(planet).getFacility();
        worldGen.getPlanetFactory().setFacility(port);
        facilities.add(port);
    }

    /**
     * Graze on resources in the outer reaches of a solar system.
     *
     * @param planet
     * @param facilities
     * @param features
     */
    public void createIceFarmers(final Planet planet, final List<Facility> facilities, CivilisationFeature... features) {
        logger.info("Creating ice farmers at [" + planet.getName() + "]");
        setFeatures(features);
        planet.setTechLevel(4 + Die.d2());

        long population = Die.d10(10);
        if (hasFeature(CivilisationFeature.SmallPopulation)) {
            population *= 10 + Die.d10();
        } else if (hasFeature(CivilisationFeature.LargePopulation)) {
            population *= 100 + Die.d100();
        } else if (hasFeature(CivilisationFeature.HugePopulation)) {
            population *= 300 + Die.die(1000);
            planet.setTechLevel(6);
        } else {
            population *= 30 + Die.d100();
        }
        if (hasFeature(CivilisationFeature.Secondary)) {
            population /= ((Die.d3() == 1)?4:2);
        }
        planet.setPopulation(Physics.round(population));

        Facility residential = new IceFarmers(planet).getFacility();
        if (hasFeature(CivilisationFeature.Poor)) {
            residential.setRating(residential.getRating() - 25);
        } else if (hasFeature(CivilisationFeature.Rich)) {
            residential.setRating(residential.getRating() + 15);
        }

        logger.debug("Setting residential");
        worldGen.getPlanetFactory().setFacility(residential);
        facilities.add(residential);
    }

    public void createRockFarmers(final Planet planet, final List<Facility> facilities, CivilisationFeature... features) {
        logger.info("Creating rock farmers at [" + planet.getName() + "]");
        setFeatures(features);
        planet.setTechLevel(5 + Die.d2());

        long population = Die.d12(12);
        if (hasFeature(CivilisationFeature.SmallPopulation)) {
            population *= 10 + Die.d10();
        } else if (hasFeature(CivilisationFeature.LargePopulation)) {
            population *= 100 + Die.d100();
        } else if (hasFeature(CivilisationFeature.HugePopulation)) {
            population *= 300 + Die.die(1000);
            planet.setTechLevel(6);
        } else {
            population *= 30 + Die.die(30);
        }
        if (hasFeature(CivilisationFeature.Secondary)) {
            population /= ((Die.d3() == 1)?4:2);
        }
        planet.setPopulation(Physics.round(population));

        Facility residential = new RockFarmers(planet).getFacility();
        if (hasFeature(CivilisationFeature.Poor)) {
            residential.setRating(residential.getRating() - 15);
        } else if (hasFeature(CivilisationFeature.Rich)) {
            residential.setRating(residential.getRating() + 10);
        }

        logger.debug("Setting residential");
        worldGen.getPlanetFactory().setFacility(residential);
        facilities.add(residential);
    }


    private void spinUpAsteroid(Planet planet) {
        long desiredDay = Physics.getPeriodOfSpin(planet.getRadius() * 1000, Physics.g);

        planet.setDayLength((planet.getDayLength() + desiredDay * 4) / 5);
        if (hasFeature(CivilisationFeature.Poor)) {
            planet.setDayLength((planet.getDayLength() + desiredDay * 4) / 5);
        } else if (hasFeature(CivilisationFeature.Rich)) {
            planet.setDayLength((planet.getDayLength() + desiredDay * 7) / 8);
            planet.setDayLength((planet.getDayLength() + desiredDay * 7) / 8);
        } else {
            planet.setDayLength((planet.getDayLength() + desiredDay * 4) / 5);
            planet.setDayLength((planet.getDayLength() + desiredDay * 4) / 5);
        }
    }

    /**
     * Rock hermits are small groups of settlers who often take over an abandoned asteroid base and
     * use it for their own purposes. They are often seeking solitude and an escape from the rest of
     * society.
     *
     * @param planet        The planet to populate. Always a SmallBody.
     * @param facilities    List of facilities which will be added to.
     * @param features      Pre-defined features to modify this civilisation.
     */
    public void createRockHermits(final Planet planet, final List<Facility> facilities, CivilisationFeature... features) {
        if (!planet.isA(SmallBody)) {
            throw new IllegalStateException("Rock Hermits can't populate [" + planet.getType().getGroup() + "] worlds");
        }
        logger.info("Creating rock hermits at [" + planet.getName() + "]");
        setFeatures(features);

        planet.setTechLevel(4 + Die.d2());
        planet.setStarPort(StarPort.E);

        Set<TradeCode> codes = planet.getTradeCodes();
        if (codes.contains(TradeCode.Sg) || (!codes.contains(TradeCode.Lk) && planet.isSmallBody() && Die.d3() == 1)) {
            planet.addTradeCode(TradeCode.Sg);
            spinUpAsteroid(planet);
        }

        long population = Die.d6(6);
        if (hasFeature(CivilisationFeature.LargePopulation)) {
            population = population * 30 + Die.d100();
        } else if (hasFeature(CivilisationFeature.HugePopulation)) {
            population = population * 100 + Die.d100();
            planet.setTechLevel(6);
        } else if (hasFeature(CivilisationFeature.SmallPopulation)) {
            // No modification.
            population = population * 3 + Die.d3();
        } else {
            population = population * 10 + Die.d10();
        }
        planet.setPopulation(population);

        Facility residential = new RockHermits(planet).getFacility();
        if (hasFeature(CivilisationFeature.Poor)) {
            residential.setRating(residential.getRating() - 25);
        } else if (hasFeature(CivilisationFeature.Rich)) {
            residential.setRating(residential.getRating() + 15);
        }
        facilities.add(residential);

        Facility starport = new AsteroidDocks(planet).getFacility();
        if (hasFeature(CivilisationFeature.Poor)) {
            starport.setRating(starport.getRating() - 25);
        } else if (hasFeature(CivilisationFeature.Rich)) {
            starport.setRating(starport.getRating() + 15);
        }
        facilities.add(starport);

    }

    public void createMonasticOutpost(final Planet planet, final List<Facility> facilities, CivilisationFeature... features) {
        if (!planet.isA(Dwarf, Terrestrial, SmallBody)) {
            throw new IllegalStateException("Monastic Outposts can't populate [" + planet.getType().getGroup() + "] worlds");
        }

        logger.info("Creating Monastic Outpost at [" + planet.getName() + "]");
        setFeatures(features);

        planet.setTechLevel(5 + Die.d2());
        planet.setStarPort(StarPort.E);

        // Monastic enclaves always count as Poor.
        planet.addTradeCode(TradeCode.Po);

        long population = Die.d6(4);
        if (hasFeature(CivilisationFeature.HugePopulation)) {
            population = population * 30 + Die.d100();
        } else if (hasFeature(CivilisationFeature.SmallPopulation)) {
            // No modification.
            population = population * 3 + Die.d3();
        } else {
            population = population * 10 + Die.d10();
        }
        planet.setPopulation(population);

        Facility residential = new MonasticOutpost(planet).getFacility();
        worldGen.getPlanetFactory().setFacility(residential);
        facilities.add(residential);

        Facility starport = new LandingField(planet).getFacility();
        worldGen.getPlanetFactory().setFacility(starport);
        facilities.add(starport);

        Facility agriculture = new FoodVats(planet).getFacility();
        worldGen.getPlanetFactory().setFacility(agriculture);
        facilities.add(agriculture);
    }

    /**
     * Outlaws tend to be small, but can be larger in places further from civilisation. As such population
     * is actually inverse of the local average.
     *
     * @param planet
     * @param facilities
     * @param features
     */
    public void createOutlawOutpost(final Planet planet, final List<Facility> facilities, CivilisationFeature... features) {
        if (!planet.isA(Dwarf, Terrestrial, SmallBody)) {
            throw new IllegalStateException("Outlaw Outposts can't populate [" + planet.getType().getGroup() + "] worlds");
        }

        logger.info("Creating Outlaw Outpost at [" + planet.getName() + "]");
        setFeatures(features);

        planet.setTechLevel(6);
        planet.setStarPort(StarPort.E);

        planet.addTradeCode(TradeCode.Na);
        planet.addTradeCode(TradeCode.Ni);
        planet.setZone(Zone.RED);

        long population;

        if (hasFeature(CivilisationFeature.HugePopulation)) {
            population = Die.d6(2);
        } else if (hasFeature(CivilisationFeature.LargePopulation)) {
            population = Die.d6(3);
        } else if (hasFeature(CivilisationFeature.SmallPopulation)) {
            population = Die.d6(8);
        } else {
            population = Die.d6(5);
        }
        switch (Die.d6()) {
            case 1: case 2: case 3:
                population *= 10;
                break;
            case 4: case 5:
                population *= 30;
                break;
            case 6:
                planet.setStarPort(StarPort.D);
                population *= 100;
                break;
        }
        planet.setPopulation(population);

        Facility residential = new OutlawOutpost(planet).getFacility();
        worldGen.getPlanetFactory().setFacility(residential);
        facilities.add(residential);

        Facility starport = new LandingField(planet).getFacility();
        worldGen.getPlanetFactory().setFacility(starport);
        facilities.add(starport);
    }

    public void createPrisonOutpost(final Planet planet, final List<Facility> facilities, CivilisationFeature... features) {
        if (!planet.isA(Dwarf, Terrestrial, SmallBody)) {
            throw new IllegalStateException("Prison Outposts can't populate [" + planet.getType().getGroup() + "] worlds");
        }


        logger.info("Creating Prison Outpost at [" + planet.getName() + "]");
        setFeatures(features);

        planet.setTechLevel(5);
        planet.setStarPort(StarPort.D);

        planet.addTradeCode(TradeCode.Na);
        planet.addTradeCode(TradeCode.Ni);
        planet.setZone(Zone.AMBER);

        // Population is unrelated to the population density of the system.
        long population = Die.d20(5);
        switch (Die.d6()) {
            case 1: case 2: case 3:
                population *= 1_000;
                break;
            case 4: case 5:
                population *= 10_000;
                break;
            case 6:
                population *= 100_000;
                break;
        }
        planet.setPopulation(population);

        Facility residential = new PrisonOutpost(planet).getFacility();
        worldGen.getPlanetFactory().setFacility(residential);
        facilities.add(residential);

        Facility starport = new LandingField(planet).getFacility();
        worldGen.getPlanetFactory().setFacility(starport);
        facilities.add(starport);

        system.setZone(planet.getZone());

        TextGenerator t = new TextGenerator(planet, residential);
        t.setCivilisationGenerator(this);

        appendSystemDescription(t.getText("system.prison"));
    }

    public void createEliteOutpost(final Planet planet, final List<Facility> facilities, CivilisationFeature... features) {
        if (!planet.isA(Dwarf, Terrestrial)) {
            throw new IllegalStateException("Elite Outposts can't populate [" + planet.getType().getGroup() + "] worlds");
        }

        logger.info("Creating Elite Outpost at [" + planet.getName() + "]");
        setFeatures(features);

        planet.setTechLevel(6);
        planet.setStarPort(StarPort.D);

        planet.setGovernment(Government.Corporation);
        planet.addTradeCode(TradeCode.Na);
        planet.addTradeCode(TradeCode.Ni);
        planet.setZone(Zone.GREEN);

        long population = 12 + Die.d6(2);
        switch (Die.d6()) {
            case 1: case 2: case 3:
                population *= 100 + Die.d100();
                break;
            case 4: case 5:
                population *= 300 + Die.die(300);
                break;
            case 6:
                population *= 1_000 + Die.die(1000);
                break;
        }
        planet.setPopulation(Physics.round(population));
        planet.setPopulation(population);

        Facility residential = new EliteOutpost(planet).getFacility();
        worldGen.getPlanetFactory().setFacility(residential);
        facilities.add(residential);

        Facility starport = new LandingField(planet).getFacility();
        worldGen.getPlanetFactory().setFacility(starport);
        facilities.add(starport);
    }
}
