/*
 * Copyright (c) 2019, Samuel Penn (sam@glendale.org.uk).
 * See the file LICENSE at the root of the project.
 */
package uk.org.glendale.worldgen.civ.facility.residential;

import uk.org.glendale.utils.rpg.Die;
import uk.org.glendale.worldgen.astro.planets.Planet;
import uk.org.glendale.worldgen.astro.planets.codes.Government;
import uk.org.glendale.worldgen.civ.Facility;
import uk.org.glendale.worldgen.civ.facility.AbstractFacility;

import java.util.Set;

/**
 * Outlaw outposts tend to be very basic, and rely on imports for the bulk of their supplies, rather
 * than producing anything themselves.
 */
public class OutlawOutpost extends AbstractFacility {
    public OutlawOutpost(Planet planet) {
        super(planet);
    }

    public Facility getFacility() {
        Facility facility = super.getFacility();
        facility.setRating(80 + Die.die(40));
        planet.setTechLevel(planet.getTechLevel());
        planet.setLawLevel(Die.d2());
        switch (Die.d6()) {
            case 1: case 2: case 3:
                planet.setGovernment(Government.NonCharismaticLeader);
                break;
            case 4: case 5:
                planet.setGovernment(Government.SelfPerpetuatingOligarchy);
                break;
            case 6:
                planet.setGovernment(Government.Anarchy);
                break;
        }

        return facility;
    }
}
