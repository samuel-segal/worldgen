/*
 * Copyright (c) 2018, Samuel Penn (sam@glendale.org.uk).
 * See the file LICENSE at the root of the project.
 */
package uk.org.glendale.worldgen.civ.facility.residential;

import uk.org.glendale.utils.rpg.Die;
import uk.org.glendale.worldgen.astro.planets.Planet;
import uk.org.glendale.worldgen.astro.planets.codes.Government;
import uk.org.glendale.worldgen.astro.planets.codes.StarPort;
import uk.org.glendale.worldgen.astro.planets.codes.TradeCode;
import uk.org.glendale.worldgen.civ.CivilisationFeature;
import uk.org.glendale.worldgen.civ.Facility;
import uk.org.glendale.worldgen.civ.facility.AbstractFacility;

import java.util.Set;

/**
 * Farmsteads are a type of small colony found on class I or II worlds. They are often one of the first
 * stages of colony construction, aimed to make good use of any land suitable for agriculture, and using
 * extensive off-world resources to build and expand quickly.
 */
public class Farmsteads extends AbstractFacility {
    public Farmsteads(Planet planet) {
        super(planet);
        minTech = 7;
        maxTech = 10;
    }

    public Facility getFacility(Set<CivilisationFeature> features) {
        Facility facility = super.getFacility(features);
        facility.setRating(90 + Die.d12(2));

        switch (Die.d6()) {
            case 1:
            case 2:
            case 3:
                planet.setGovernment(Government.Corporation);
                planet.setLawLevel(2);
                facility.setTechLevel(9);
                facility.setRating(facility.getRating() + Die.d12());
                break;
            case 4:
            case 5:
                planet.setGovernment(Government.TheocraticOligarchy);
                planet.setLawLevel(3);
                facility.setTechLevel(8);
                break;
            case 6:
                planet.setGovernment(Government.ImpersonalBureaucracy);
                planet.setLawLevel(3 + Die.d2());
                facility.setTechLevel(9);
                facility.setRating(facility.getRating() - Die.d8());
                break;
        }
        if (planet.getPopulation() > 12_000) {
            facility.setTechLevel(planet.getTechLevel() + 2);
        } else if (planet.getPopulation() > 6_000) {
            facility.setTechLevel(planet.getTechLevel() + 1);
        }

        return facility;
    }
}
