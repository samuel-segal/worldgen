/*
 * Copyright (c) 2018, Samuel Penn (sam@glendale.org.uk).
 * See the file LICENSE at the root of the project.
 */

package uk.org.glendale.worldgen.civ;

import java.util.Arrays;

public enum CivilisationFeature {
    // Population modifier.
    SmallPopulation,
    LargePopulation,
    HugePopulation,
    // General wealth modifier.
    Rich,
    Poor,
    // Specific sizes.
    Tiny,
    Small,
    Medium,
    Large,
    Huge,
    // Misc.
    Colony,
    Blackmarket,
    Corrupt,
    Unrest,
    Capital,
    Infected,
    Secondary,
    Collapsed;

    public CivilisationFeature[] addTo(CivilisationFeature... features) {
        var list = Arrays.asList(features);
        list.add(this);

        return list.toArray(new CivilisationFeature[0]);
    }
}
