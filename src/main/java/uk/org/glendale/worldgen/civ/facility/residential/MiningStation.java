/*
 * Copyright (c) 2020, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */
package uk.org.glendale.worldgen.civ.facility.residential;

import uk.org.glendale.utils.rpg.Die;
import uk.org.glendale.worldgen.astro.planets.Planet;
import uk.org.glendale.worldgen.astro.planets.codes.Government;
import uk.org.glendale.worldgen.civ.Facility;
import uk.org.glendale.worldgen.civ.facility.AbstractFacility;

public class MiningStation extends AbstractFacility {
    public MiningStation(Planet planet) {
        super(planet);
    }

    public Facility getFacility() {
        Facility facility = super.getFacility();

        facility.setRating(100 + Die.die(40));

        // If the TL hasn't already been set, choose something.
        if (planet.getTechLevel() == 0) {
            switch (Die.d6()) {
                case 1: case 2: case 3:
                    facility.setTechLevel(8);
                    break;
                case 4: case 5:
                    facility.setTechLevel(9);
                    break;
                case 6:
                    facility.setTechLevel(10);
                    break;
            }
            planet.setTechLevel(facility.getTechLevel() - 1);
        }

        // If the government hasn't already been set, choose something.
        if (planet.getGovernment() == Government.None) {
            planet.setGovernment(Government.Corporation);
            planet.setLawLevel(Die.d2());
        }

        // If the population hasn't been set, generate one.
        if (planet.getPopulation() == 0) {
            planet.setPopulation(Die.d6(3) * 1_000);
        }

        return facility;
    }
}
