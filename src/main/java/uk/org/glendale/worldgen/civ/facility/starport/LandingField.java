/*
 * Copyright (c) 2018, Samuel Penn (sam@glendale.org.uk).
 * See the file LICENSE at the root of the project.
 */
package uk.org.glendale.worldgen.civ.facility.starport;

import uk.org.glendale.utils.rpg.Die;
import uk.org.glendale.worldgen.astro.planets.Planet;
import uk.org.glendale.worldgen.astro.planets.codes.StarPort;
import uk.org.glendale.worldgen.civ.Facility;
import uk.org.glendale.worldgen.civ.facility.AbstractFacility;

/**
 * This is a simple cleared area near a settlement, which has possibly some basic
 * buildings and rudimentary tools and facilities for refueling and loading/unloading.
 */
public class LandingField extends AbstractFacility {
    public LandingField(Planet planet) {
        super(planet);
    }

    public Facility getFacility() {
        Facility facility = super.getFacility();

        facility.setRating(90 + Die.d10(2));
        facility.setTechLevel(planet.getTechLevel());
        if (facility.getTechLevel() < 5) {
            facility.setTechLevel(5);
        }
        planet.setStarPort(StarPort.E);

        return facility;
    }
}
