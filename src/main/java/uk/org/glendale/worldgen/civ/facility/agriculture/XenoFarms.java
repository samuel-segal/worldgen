/*
 * Copyright (c) 2019, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */
package uk.org.glendale.worldgen.civ.facility.agriculture;

import uk.org.glendale.utils.rpg.Die;
import uk.org.glendale.worldgen.astro.planets.Planet;
import uk.org.glendale.worldgen.astro.planets.codes.TradeCode;
import uk.org.glendale.worldgen.civ.Facility;
import uk.org.glendale.worldgen.civ.facility.AbstractFacility;

/**
 * XenoFarms are surface farms on worlds with harsh conditions. Generally Habitability II/III worlds.
 * They require a lot of external support in the form of fertilizer and purification chemicals, as
 * well as genetic engineering for crops to grow well.
 *
 * The TL of such facilities will range from 7 to 9.
 */
public class XenoFarms extends AbstractFacility {
    public XenoFarms(Planet planet) {
        super(planet);
        minTech = 7;
        maxTech = 10;
    }

    /**
     * Generate a new facility based on the planet. Modifies the agricultural codes
     * for the world as well.
     *
     * @return  Generated facility.
     */
    public Facility getFacility() {
        Facility facility = super.getFacility();

        facility.setTechLevel(planet.getTechLevel());
        if (planet.getPopulation() > 10_000_000) {
            facility.modTechLevel(2);
        } else if (planet.getPopulation() > 1_000_000) {
            facility.modTechLevel(1);
        }

        if (planet.getHabitability() > 2) {
            facility.setRating(60 + Die.d20(2));
        } else if (planet.getHabitability() == 2) {
            facility.setRating(70 + Die.d20(2));
        } else {
            facility.setRating(80 + Die.d20(2));
        }
        if (planet.getTechLevel() >= facility.getTechLevel()) {
            facility.modRating(10 + (planet.getTechLevel() - facility.getTechLevel()) * 5);
        }

        planet.removeTradeCode(TradeCode.Na);
        if (facility.getRating() > 90) {
            planet.addTradeCode(TradeCode.Ag);
        }

        return facility;
    }
}
