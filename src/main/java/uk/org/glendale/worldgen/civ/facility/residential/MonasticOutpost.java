/*
 * Copyright (c) 2019, Samuel Penn (sam@glendale.org.uk).
 * See the file LICENSE at the root of the project.
 */
package uk.org.glendale.worldgen.civ.facility.residential;

import uk.org.glendale.utils.rpg.Die;
import uk.org.glendale.worldgen.astro.planets.Planet;
import uk.org.glendale.worldgen.astro.planets.codes.Government;
import uk.org.glendale.worldgen.civ.Facility;
import uk.org.glendale.worldgen.civ.facility.AbstractFacility;

/**
 * This outpost is a monastic retreat, populated entirely by a religious order of monks.
 *
 * Output will be religious texts.
 */
public class MonasticOutpost extends AbstractFacility {
    public MonasticOutpost(Planet planet) {
        super(planet);
    }

    public Facility getFacility() {
        Facility facility = super.getFacility();

        facility.setRating(89 + Die.d10(2));
        planet.setTechLevel(planet.getTechLevel());
        planet.setLawLevel(5);
        planet.setGovernment(Government.TheocraticOligarchy);

        return facility;
    }
}
