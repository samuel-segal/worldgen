/*
 * Copyright (c) 2018, Samuel Penn (sam@glendale.org.uk).
 * See the file LICENSE at the root of the project.
 */
package uk.org.glendale.worldgen.civ.facility.residential;

import uk.org.glendale.utils.rpg.Die;
import uk.org.glendale.worldgen.astro.planets.Planet;
import uk.org.glendale.worldgen.astro.planets.codes.Government;
import uk.org.glendale.worldgen.astro.planets.codes.StarPort;
import uk.org.glendale.worldgen.civ.Facility;
import uk.org.glendale.worldgen.civ.facility.AbstractFacility;

/**
 * Class to generate and manage Rock Farmers. Similar to Belters, but generally a lot less
 * organised and equipped, and seeking basic survival and solitude rather than get rich
 * quick schemes.
 */
public class RockFarmers extends AbstractFacility {
    public RockFarmers(Planet planet) {
        super(planet);
    }

    public Facility getFacility() {
        Facility facility = super.getFacility();

        facility.setRating(60 + Die.d20(3));
        planet.setTechLevel(4 + Die.d2());

        if (Die.d3() == 1) {
            planet.setGovernment(Government.Communist);
            planet.setLawLevel(Die.d2() + 1);
        } else {
            planet.setGovernment(Government.Anarchy);
            planet.setLawLevel(Die.d2() - 1);
        }

        if (planet.getPopulation() < 1000) {
            planet.setStarPort(StarPort.X);
        } else if (planet.getPopulation() < 10_000) {
            planet.setStarPort(StarPort.Eo);
        } else {
            planet.setStarPort(StarPort.Do);
        }

        return facility;
    }
}
