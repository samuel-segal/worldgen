/*
 * Copyright (c) 2019, Samuel Penn (sam@glendale.org.uk).
 * See the file LICENSE at the root of the project.
 */
package uk.org.glendale.worldgen.civ.facility.agriculture;

import uk.org.glendale.utils.rpg.Die;
import uk.org.glendale.worldgen.astro.planets.Planet;
import uk.org.glendale.worldgen.astro.planets.codes.TradeCode;
import uk.org.glendale.worldgen.civ.Facility;
import uk.org.glendale.worldgen.civ.facility.AbstractFacility;

/**
 * FoodVats are a means of artificially producing synthetic food.
 *
 * Regardless of the method used to produce the food, the resulting output commodity is always SynthFood.
 */
public class FoodVats extends AbstractFacility {
    public FoodVats(Planet planet) {
        super(planet);
    }

    /**
     * Generate a new facility based on the planet. Modifies the agricultural codes
     * for the world as well.
     *
     * @return  Generated facility.
     */
    public Facility getFacility() {
        Facility facility = super.getFacility();

        int tl = Math.min(planet.getTechLevel(), 8);
        if (planet.hasTradeCode(TradeCode.Lo)) {
            tl -= 1;
        } else if (planet.hasTradeCode(TradeCode.Hi)) {
            tl += 1;
        }
        facility.setTechLevel(tl);

        facility.setRating(70 + Die.d20(2) + (int) (Math.log10(planet.getPopulation()) * 3));

        // Worlds with Food Vats are generally considered non-agricultural.
        planet.removeTradeCode(TradeCode.Ag);
        planet.removeTradeCode(TradeCode.Na);
        if (facility.getRating() < 120) {
            planet.addTradeCode(TradeCode.Na);
        }

        return facility;
    }
}
