/*
 * Copyright (c) 2019, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */
package uk.org.glendale.worldgen.civ.facility.residential;

import uk.org.glendale.utils.rpg.Die;
import uk.org.glendale.worldgen.astro.planets.Planet;
import uk.org.glendale.worldgen.astro.planets.codes.Government;
import uk.org.glendale.worldgen.astro.planets.codes.StarPort;
import uk.org.glendale.worldgen.civ.CivilisationFeature;
import uk.org.glendale.worldgen.civ.Facility;
import uk.org.glendale.worldgen.civ.facility.AbstractFacility;

import java.util.Set;

public class DomedHabitats extends AbstractFacility {
    public DomedHabitats(Planet planet) {
        super(planet);
    }

    public Facility getFacility(Set<CivilisationFeature> features) {
        Facility facility = super.getFacility(features);

        facility.setRating(75 + Die.d20(2));
        planet.setTechLevel(6);

        // Larger populations have a chance of having a class D starport, but most
        // will be class D.
        if (250_000 + Die.die(200_000, 3) < planet.getPopulation()) {
            planet.setStarPort(StarPort.C);
            planet.setTechLevel(7);
        } else {
            planet.setStarPort(StarPort.D);
        }
        int modifier = 0;
        if (features.contains(CivilisationFeature.Poor) || features.contains(CivilisationFeature.SmallPopulation)) {
            modifier -= 1;
        }
        if (features.contains(CivilisationFeature.Rich)) {
            modifier += 1;
        }
        if (features.contains(CivilisationFeature.LargePopulation)) {
            modifier += 1;
        } else if (features.contains(CivilisationFeature.HugePopulation)) {
            modifier += 2;
        }
        switch (Die.d6() + modifier) {
            case 0: case 1:
                planet.setGovernment(Government.Anarchy);
                planet.setLawLevel(Die.d2() - 1);
                break;
            case 2:
                planet.setGovernment(Government.Communist);
                planet.setLawLevel(Die.d3() + 3);
                break;
            case 3:
                planet.setGovernment(Government.Corporation);
                planet.setLawLevel(Die.d2() + 2);
                break;
            case 4:
                planet.setGovernment(Government.ImpersonalBureaucracy);
                planet.setLawLevel(Die.d4() + 4);
                break;
            default:
                planet.setGovernment(Government.ParticipatingDemocracy);
                planet.setLawLevel(Die.d2() + 1);
                break;
        }
        facility.setTechLevel(9 + (int) (planet.getPopulation() / 250_000));

        return facility;
    }
}
