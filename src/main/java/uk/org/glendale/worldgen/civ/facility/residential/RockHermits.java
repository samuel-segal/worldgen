/*
 * Copyright (c) 2018, Samuel Penn (sam@glendale.org.uk).
 * See the file LICENSE at the root of the project.
 */

package uk.org.glendale.worldgen.civ.facility.residential;

import uk.org.glendale.utils.rpg.Die;
import uk.org.glendale.worldgen.astro.planets.Planet;
import uk.org.glendale.worldgen.astro.planets.codes.Government;
import uk.org.glendale.worldgen.astro.planets.codes.StarPort;
import uk.org.glendale.worldgen.civ.Facility;
import uk.org.glendale.worldgen.civ.FacilityType;
import uk.org.glendale.worldgen.civ.facility.AbstractFacility;

/**
 * Rock Hermits live inside hollowed out asteroids. They are generally religious, communist or anarchy.
 */
public class RockHermits extends AbstractFacility {

    public RockHermits(Planet planet) {
        super(planet);
    }

    public Facility getFacility() {
        planet.setTechLevel(4 + Die.d2());
        planet.setStarPort(StarPort.Ho);

        Facility facility = super.getFacility();
        facility.setRating(60 + Die.d20(2));

        switch (Die.d6()) {
            case 1: case 2: case 3:
                planet.setGovernment(Government.TheocraticDictatorship);
                planet.setLawLevel(Die.d3() + 3);
                facility.setTechLevel(planet.getTechLevel() + Die.d2());
                break;
            case 4: case 5:
                planet.setGovernment(Government.Communist);
                planet.setLawLevel(Die.d2());
                while (planet.getPopulation() > 1000) {
                    planet.setPopulation(planet.getPopulation() / 2);
                }
                if (Die.d20() == 1) {
                    planet.setStarPort(StarPort.Do);
                }
                facility.setTechLevel(planet.getTechLevel() + 2);
                break;
            case 6:
                planet.setGovernment(Government.Anarchy);
                planet.setPopulation( planet.getPopulation() * 10);
                facility.setRating( facility.getRating() + Die.d10(3));
                planet.setLawLevel(Die.d2() - 1);
                planet.setStarPort(StarPort.E);
                if (Die.d8() <= planet.getPopulation() / 1_000 + 1) {
                    planet.setStarPort(StarPort.D);
                    planet.setTechLevel( planet.getTechLevel() + 1);
                } else {
                    planet.setStarPort(StarPort.E);
                }
                facility.setTechLevel(planet.getTechLevel() + 5);
                break;
        }

        return facility;
    }
}
