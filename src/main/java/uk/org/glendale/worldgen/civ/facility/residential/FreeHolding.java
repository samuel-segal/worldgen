/*
 * Copyright (c) 2018, Samuel Penn (sam@glendale.org.uk).
 * See the file LICENSE at the root of the project.
 */
package uk.org.glendale.worldgen.civ.facility.residential;

import uk.org.glendale.utils.rpg.Die;
import uk.org.glendale.worldgen.astro.planets.Planet;
import uk.org.glendale.worldgen.astro.planets.codes.Government;
import uk.org.glendale.worldgen.astro.planets.codes.StarPort;
import uk.org.glendale.worldgen.astro.planets.codes.TradeCode;
import uk.org.glendale.worldgen.civ.CivilisationFeature;
import uk.org.glendale.worldgen.civ.Facility;
import uk.org.glendale.worldgen.civ.facility.AbstractFacility;

import java.util.Set;

/**
 * Free Holder settlements are generally found on Terrestrial or Dwarf Terrestrial worlds
 * which have a safe surface with at least a semi-breathable atmosphere.
 */
public class FreeHolding extends AbstractFacility {
    public FreeHolding(Planet planet) {
        super(planet);
    }

    public Facility getFacility(Set<CivilisationFeature> features) {
        Facility facility = super.getFacility(features);

        facility.setRating(75 + Die.d20(2));
        planet.setTechLevel(4 + Die.d2());

        planet.setStarPort(StarPort.E);
        int modifier = 0;
        if (features.contains(CivilisationFeature.Poor) || features.contains(CivilisationFeature.SmallPopulation)) {
            modifier -= 1;
        }
        if (features.contains(CivilisationFeature.Rich)) {
            modifier += 1;
        }
        if (features.contains(CivilisationFeature.LargePopulation)) {
            modifier += 1;
        } else if (features.contains(CivilisationFeature.HugePopulation)) {
            modifier += 2;
        }
        switch (Die.d6() + modifier) {
            case 0: case 1: case 2: case 3:
                planet.setGovernment(Government.Anarchy);
                planet.setLawLevel(Die.d2() - 1);
                break;
            case 4: case 5:
                planet.setGovernment(Government.Communist);
                planet.setLawLevel(Die.d2());
                if (planet.hasTradeCode(TradeCode.Ri)) {
                    planet.removeTradeCode(TradeCode.Ri);
                } else {
                    planet.addTradeCode(TradeCode.Po);
                }
                break;
            default:
                planet.setGovernment(Government.ParticipatingDemocracy);
                planet.setLawLevel(Die.d2());
                break;
        }
        if (modifier > 0) {
            facility.setTechLevel(planet.getTechLevel() + modifier);
            planet.setTechLevel(planet.getTechLevel() + modifier / 2);
        }

        return facility;
    }
}
