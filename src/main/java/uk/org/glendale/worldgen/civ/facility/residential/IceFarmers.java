/*
 * Copyright (c) 2018, Samuel Penn (sam@glendale.org.uk).
 * See the file LICENSE at the root of the project.
 */
package uk.org.glendale.worldgen.civ.facility.residential;

import uk.org.glendale.utils.rpg.Die;
import uk.org.glendale.worldgen.astro.planets.Planet;
import uk.org.glendale.worldgen.astro.planets.codes.Government;
import uk.org.glendale.worldgen.astro.planets.codes.StarPort;
import uk.org.glendale.worldgen.civ.Facility;
import uk.org.glendale.worldgen.civ.facility.AbstractFacility;

/**
 * Class to generate and manage Ice Farmers. Residential facility sometimes found around icy
 * belts far from a star, such as kuiper belts or oort clouds.
 */
public class IceFarmers extends AbstractFacility {
    public IceFarmers(Planet planet) {
        super(planet);
    }

    public Facility getFacility() {
        Facility facility = super.getFacility();

        facility.setRating(50 + Die.d20(2));
        planet.setTechLevel(4 + Die.d2());
        planet.setStarPort(StarPort.X);
        planet.setGovernment(Government.Anarchy);

        return facility;
    }
}
