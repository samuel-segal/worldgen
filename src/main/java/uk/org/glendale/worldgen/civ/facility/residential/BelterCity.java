/*
 * Copyright (c) 2020, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */
package uk.org.glendale.worldgen.civ.facility.residential;

import uk.org.glendale.utils.rpg.Die;
import uk.org.glendale.worldgen.astro.planets.Planet;
import uk.org.glendale.worldgen.astro.planets.codes.Government;
import uk.org.glendale.worldgen.civ.CivilisationFeature;
import uk.org.glendale.worldgen.civ.Facility;
import uk.org.glendale.worldgen.civ.facility.AbstractFacility;

import java.util.Set;

import static uk.org.glendale.worldgen.civ.CivilisationFeature.*;

/**
 * A Belter city is an asteroid
 */
public class BelterCity extends AbstractFacility {
    public BelterCity(Planet planet) {
        super(planet);
    }

    public Facility getFacility(Set<CivilisationFeature> features, CivilisationFeature... extra) {
        Facility facility = super.getFacility(features, extra);

        if (!facility.hasFeature(Rich) && !facility.hasFeature(Poor)) {
            switch (Die.d6(2) + (facility.hasFeature(Capital)?1:0)) {
                case 2: case 3:
                    facility.feature(Poor);
                    break;
                case 11: case 12: case 13:
                    facility.feature(Rich);
                    break;
            }
        }

        if (facility.hasFeature(Rich)) {
            facility.setRating(95 + Die.die(20, 2));
        } else if (facility.hasFeature(Poor)) {
            facility.setRating(65 + Die.die(20, 2));
        } else {
            facility.setRating(80 + Die.die(20, 2));
        }

        // If the TL hasn't already been set, choose something.
        if (planet.getTechLevel() == 0) {
            int mod = (facility.hasFeature(Capital)?1:0) +
                    (facility.hasFeature(Rich)?1:0) +
                    (facility.hasFeature(LargePopulation)?1:0) +
                    (facility.hasFeature(HugePopulation)?2:0) +
                    (facility.hasFeature(Poor)?-1:0) +
                    (facility.hasFeature(SmallPopulation)?-1:0);

            switch (Die.d6()) {
                case -1: case 0:
                    facility.setTechLevel(8);
                    break;
                case 1: case 2:
                    facility.setTechLevel(9);
                    break;
                case 3: case 4: case 5:
                    facility.setTechLevel(10);
                    break;
                case 6:
                    facility.setTechLevel(11);
                    break;
                case 7:
                    facility.setTechLevel(12);
                    break;
                default:
                    facility.setTechLevel(13);
                    break;
            }

            planet.setTechLevel(facility.getTechLevel() - 3);
        }

        // If the government hasn't already been set, choose something.
        if (planet.getGovernment() == Government.None) {
            planet.setGovernment(Government.SelfPerpetuatingOligarchy);
            planet.setLawLevel(Die.d2());
        }

        // If the population hasn't been set, generate one.
        if (planet.getPopulation() == 0) {
            planet.setPopulation(Die.d6(3) * 100_000);
        }

        return facility;
    }
}
