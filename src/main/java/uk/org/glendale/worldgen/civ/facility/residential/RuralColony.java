/*
 * Copyright (c) 2020, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */
package uk.org.glendale.worldgen.civ.facility.residential;

import uk.org.glendale.utils.rpg.Die;
import uk.org.glendale.worldgen.astro.planets.Planet;
import uk.org.glendale.worldgen.astro.planets.codes.Government;
import uk.org.glendale.worldgen.civ.CivilisationFeature;
import uk.org.glendale.worldgen.civ.Facility;
import uk.org.glendale.worldgen.civ.facility.AbstractFacility;

import java.util.Set;

/**
 * Rural Colonies are one up from Farmsteads, where the population has increased to the point where
 * it is possible to have towns and maybe even small cities. Most of the population is still rural,
 * and the social structures are similar to low population rural cultures.
 */
public class RuralColony extends AbstractFacility {
    public RuralColony(Planet planet) {
        super(planet);
        minTech = 8;
        maxTech = 12;
    }

    public Facility getFacility(Set<CivilisationFeature> features) {
        Facility facility = super.getFacility(features);
        facility.setRating(80 + Die.d20(2));

        switch (Die.d6(2)) {
            case 2:
                planet.setGovernment(Government.TheocraticDictatorship);
                planet.setLawLevel(4 + Die.d2());
                facility.setTechLevel(8);
                break;
            case 3:
                planet.setGovernment(Government.Balkanization);
                planet.setLawLevel(Die.d3());
                facility.setTechLevel(8);
                break;
            case 4: case 5: case 6:
                planet.setGovernment(Government.Corporation);
                planet.setLawLevel(1 + Die.d3());
                facility.setTechLevel(9);
                facility.setRating(facility.getRating() + Die.d12());
                break;
            case 7:
            case 8:
                planet.setGovernment(Government.ParticipatingDemocracy);
                planet.setLawLevel(3 + Die.d2());
                facility.setTechLevel(9);
                facility.setRating(facility.getRating() - Die.d8());
                break;
            case 9:
            case 10:
                planet.setGovernment(Government.RepresentativeDemocracy);
                planet.setLawLevel(3 + Die.d2());
                facility.setTechLevel(9);
                facility.setRating(facility.getRating() - Die.d8());
                break;
            case 11:
                planet.setGovernment(Government.TheocraticOligarchy);
                planet.setLawLevel(3 + Die.d2());
                facility.setTechLevel(9);
                facility.setRating(facility.getRating() - Die.d8());
                break;
            case 12:
                planet.setGovernment(Government.SelfPerpetuatingOligarchy);
                planet.setLawLevel(3 + Die.d2());
                planet.setTechLevel(4);
                facility.setTechLevel(4);
                break;
        }

        if (planet.getPopulation() > 12_000) {
            facility.setTechLevel(planet.getTechLevel() + 2);
        } else if (planet.getPopulation() > 6_000) {
            facility.setTechLevel(planet.getTechLevel() + 1);
        }

        return facility;
    }
}
