/*
 * Copyright (c) 2018, Samuel Penn (sam@glendale.org.uk).
 * See the file LICENSE at the root of the project.
 */

package uk.org.glendale.worldgen.civ;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.org.glendale.worldgen.WorldGen;
import uk.org.glendale.worldgen.astro.planets.Planet;
import uk.org.glendale.worldgen.astro.planets.codes.PlanetGroup;
import uk.org.glendale.worldgen.astro.planets.codes.PlanetType;
import uk.org.glendale.worldgen.astro.planets.codes.TradeCode;
import uk.org.glendale.worldgen.astro.systems.StarSystem;
import uk.org.glendale.worldgen.text.TextGenerator;

import java.util.*;

import static uk.org.glendale.worldgen.astro.planets.codes.PlanetType.*;

/**
 * Generates a new civilisation on a planet. A civilisation isn't recorded, but specifies
 * a list of facilities that are recorded against the planet.
 */
public abstract class CivilisationGenerator {
    private static final Logger logger = LoggerFactory.getLogger(CivilisationGenerator.class);
    protected WorldGen worldGen;
    protected StarSystem system;
    protected Set<CivilisationFeature> features;

    public CivilisationGenerator(WorldGen worldGen, StarSystem system) {
        this.worldGen = worldGen;
        this.system = system;

        this.features = new HashSet<>();
    }

    /**
     * Abstract generation class that must be extended.
     *
     * @param features  Optional list of features to apply to this civilisation.
     */
    public abstract void generate(String civ, CivilisationFeature... features);

    public void generate(CivilisationFeature... features) {
        generate(null, features);
    }

    public void setFeatures(CivilisationFeature... features) {
        if (features != null && features.length > 0) {
            this.features.addAll(Arrays.asList(features));
        }
    }

    public void addFeature(CivilisationFeature feature) {
        this.features.add(feature);
    }

    public void setFeatures(Set<CivilisationFeature> features) {
        this.features = features;
    }

    public Set<CivilisationFeature> getFeatures() {
        return features;
    }

    public boolean hasFeature(CivilisationFeature feature) {
        return this.features.contains(feature);
    }

    public boolean hasFeature(String... featureNames) {
        for (String featureName : featureNames) {
            for (CivilisationFeature feature : features) {
                if (feature.toString().equals(featureName)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Modify the quality rating of a facility based on whether the civilisation is considered
     * to be rich or poor. Poor civilisations have lower quality facilities than rich ones.
     *
     * @param facility  Facility to be modified.
     */
    public void qualityCheck(Facility facility) {
        if (hasFeature(CivilisationFeature.Poor)) {
            facility.setRating(facility.getRating() - 25);
        } else if (hasFeature(CivilisationFeature.Rich)) {
            facility.setRating(facility.getRating() + 15);
        }
    }

    public void tradeCheck(Planet planet) {
        planet.removeTradeCode(TradeCode.Hi);
        planet.removeTradeCode(TradeCode.Lo);
        planet.removeTradeCode(TradeCode.Ri);
        planet.removeTradeCode(TradeCode.Po);

        if (planet.getPopulation() == 0) {
            planet.addTradeCode(TradeCode.Ba);
            return;
        }
        planet.removeTradeCode(TradeCode.Ba);

        if (planet.getPopulation() > 1_000_000_000L) {
            planet.addTradeCode(TradeCode.Hi);
        } else if (planet.getPopulation() < 10_000) {
            planet.addTradeCode(TradeCode.Lo);
        }

        if (hasFeature(CivilisationFeature.Poor)) {
            planet.addTradeCode(TradeCode.Po);
        } else if (hasFeature(CivilisationFeature.Rich)) {
            planet.addTradeCode(TradeCode.Ri);
        }
    }

    /**
     * Append a description of the civilisation to the end of the planet's physical description.
     * Each facility gets a chance to add a description, which has its own heading. If a facility
     * does not have descriptions defined, then no heading is added.
     *
     * @param facilities    List of facilities to describe.
     */
    protected void generateDescription(Planet planet, List<Facility> facilities) {
        String text = planet.getDescription();
        boolean hasTitle = false;


        for (Facility f : facilities) {
            TextGenerator   t = new TextGenerator(planet, f);
            t.setCivilisationGenerator(this);
            String          description = t.getFacilityDescription();
            String          title = t.getSavedPhrase("TITLE");

            if (title != null) {
                f.setTitle(title);
            }

            if (t.getSavedPhrase("BONUS") != null) {
                int bonus = Integer.parseInt(t.getSavedPhrase("BONUS"));
                f.setRating(f.getRating() + bonus);
            }

            if (description.length() > 0) {
                if (!hasTitle) {
                    text += "\n<h4>Settlements</h4>\n";
                    hasTitle = true;
                }
                String type = f.getType().getTitle();
                if (type.length() > 0) {
                    type = " (" + type + ")";
                }
                text += "<h5>" + f.getTitle() + type + "</h5>";
                text += "<p>" + description + "</p>";
            }
        }
        planet.setDescription(text);
    }


    protected boolean hasAsteroidBelt = false;
    protected boolean hasIceBelt = false;
    protected boolean hasHabitableWorld = false;
    protected boolean hasRockWorld = false;
    protected boolean hasGardenWorld = false;
    protected boolean hasIdealWorld = false;
    protected boolean hasDustDisc = false;

    /**
     * Gets the best planet available in the given group, and limits the habitability to the
     * given tolerance.
     *
     * @param limit     Maximum habitation class of the planet. 1 is best, 6 is worst.
     * @param group     Optional list of world group to limit the result to.
     * @return          Planet if a match is found, otherwise null.
     */
    protected Planet getBestPlanet(int limit, PlanetGroup... group) {
        Planet best = null;

        for (Planet planet : system.getPlanets()) {
            logger.debug(String.format("Looking at [%s] of type [%s/%d]", planet.getName(), planet.getType(), planet.getHabitability()));
            if (planet.getHabitability() > limit) {
                logger.debug("Planet isn't habitable enough");
                continue;
            }
            if (group.length > 0 && !Arrays.stream(group).anyMatch(planet.getType().getGroup()::equals)) {
                logger.debug(String.format("Planet group [%s] does not match", planet.getType().getGroup()));
                continue;
            }
            if (best == null || planet.getHabitability() < best.getHabitability()) {
                best = planet;
                logger.debug("Selected best planet");
            }
        }

        return best;
    }

    protected void appendSystemDescription(String text) {
        String description = system.getDescription().replaceAll("</p>$", "");
        text = text.replaceAll("^<p>", "");
        system.setDescription(description + " " + text);
    }

    /**
     * Sets boolean variables which define what type of planets are available in this system.
     */
    protected void findPlanetsAvailable() {
        for (Planet planet : system.getPlanets()) {
            PlanetType type = planet.getType();
            if (type == AsteroidBelt || type == VulcanianBelt || type == MetallicBelt) {
                hasAsteroidBelt = true;
            } else if (type == IceBelt) {
                hasIceBelt = true;
            } else if (type.getGroup() == PlanetGroup.Dwarf || type.getGroup() == PlanetGroup.Terrestrial) {
                hasRockWorld = true;
                if (planet.getHabitability() < 4) {
                    hasHabitableWorld = true;
                }
                if (planet.getHabitability() < 3) {
                    hasGardenWorld = true;
                }
                if (planet.getHabitability() < 2) {
                    hasIdealWorld = true;
                }
            } else if (type == DustDisc || type == PlanetesimalDisc) {
                hasDustDisc = true;
            }
        }
    }

    protected List<Planet> getMoonsOf(Planet parent) {
        List<Planet> moons = new ArrayList<>();

        for (Planet moon : system.getPlanets()) {
            if (moon.getMoonOf() == parent.getId()) {
                moons.add(moon);
            }
        }
        return moons;
    }
}
