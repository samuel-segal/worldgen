/*
 * Copyright (c) 2020, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */
package uk.org.glendale.worldgen.civ.facility.agriculture;

import uk.org.glendale.utils.rpg.Die;
import uk.org.glendale.worldgen.astro.planets.Planet;
import uk.org.glendale.worldgen.astro.planets.codes.TradeCode;
import uk.org.glendale.worldgen.civ.Facility;
import uk.org.glendale.worldgen.civ.facility.AbstractFacility;

/**
 * Mechanised farms are high tech farms which make heavy use of robots to perform all the major work.
 * Such farms are almost free from human workers, except to handle the occasional emergency.
 *
 * The TL for these range from TL 10 to TL 13.
 *
 * The sort of things needed:
 *   Agricultural Robotics
 *   Advanced Fertilizer
 */
public class RoboticFarms extends AbstractFacility {
    public RoboticFarms(Planet planet) {
        super(planet);
        minTech = 10;
        maxTech = 13;
    }

    /**
     * Generate a new facility based on the planet. Modifies the agricultural codes
     * for the world as well.
     *
     * @return  Generated facility.
     */
    public Facility getFacility() {
        Facility facility = super.getFacility();

        facility.setTechLevel(Math.max(9 + Die.d4(), planet.getTechLevel()));
        facility.setRating(90 + Die.d20(2));
        if (planet.getTechLevel() >= facility.getTechLevel()) {
            facility.modRating(10 + (planet.getTechLevel() - facility.getTechLevel()) * 5);
        }

        planet.removeTradeCode(TradeCode.Na);
        planet.addTradeCode(TradeCode.Ag);

        return facility;
    }
}
