/*
 * Copyright (c) 2018, Samuel Penn (sam@glendale.org.uk).
 * See the file LICENSE at the root of the project.
 */
package uk.org.glendale.worldgen.civ.facility.residential;

import uk.org.glendale.utils.rpg.Die;
import uk.org.glendale.worldgen.astro.planets.Planet;
import uk.org.glendale.worldgen.astro.planets.codes.Government;
import uk.org.glendale.worldgen.astro.planets.codes.StarPort;
import uk.org.glendale.worldgen.civ.Facility;
import uk.org.glendale.worldgen.civ.facility.AbstractFacility;

/**
 * Class to generate and manage Belters. Live in zero-g environments such as asteroid belts.
 * Tend to be highly independent, and have their own unique cultures.
 */
public class Belters extends AbstractFacility {
    public Belters(Planet planet) {
        super(planet);
    }

    public Facility getFacility() {
        Facility facility = super.getFacility();

        facility.setRating(75 + Die.d20(2));
        planet.setTechLevel(6 + Die.d3());
        planet.setStarPort(StarPort.C);
        switch (Die.d6()) {
            case 1:
                planet.setGovernment(Government.Balkanization);
                facility.setRating(facility.getRating() - Die.d6(2));
                planet.setLawLevel(Die.d2());
                break;
            case 2:
                planet.setGovernment(Government.Communist);
                planet.setLawLevel(Die.d2());
                break;
            case 3: case 4:
                planet.setGovernment(Government.Anarchy);
                planet.setLawLevel(Die.d2() - 1);
                break;
            case 5: case 6:
                planet.setGovernment(Government.ParticipatingDemocracy);
                planet.setLawLevel(Die.d2());
                break;
        }
        facility.setTechLevel(planet.getTechLevel());

        return facility;
    }
}
