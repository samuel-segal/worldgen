/*
 * Copyright (c) 2020, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */
package uk.org.glendale.worldgen.civ.facility.agriculture;

import uk.org.glendale.utils.rpg.Die;
import uk.org.glendale.worldgen.astro.planets.Planet;
import uk.org.glendale.worldgen.astro.planets.codes.TradeCode;
import uk.org.glendale.worldgen.civ.Facility;
import uk.org.glendale.worldgen.civ.facility.AbstractFacility;

/**
 * This is a typical low technology farm, which uses manual labour rather than any form of
 * mechanisation. As such, they tend to be common on TL6 worlds or earlier.
 *
 * The sorts of things needed:
 *   Agricultural Tools
 *   Fertilizer
 *   Agricultural Machinery (if TL 7, minimal requirements)
 */
public class Farms extends AbstractFacility {
    public Farms(Planet planet) {
        super(planet);
        minTech = 2;
        maxTech = 7;
    }

    /**
     * Generate a new facility based on the planet. Modifies the agricultural codes
     * for the world as well.
     *
     * @return  Generated facility.
     */
    public Facility getFacility() {
        Facility facility = super.getFacility();

        facility.setTechLevel(planet.getTechLevel());
        if (planet.getPopulation() > 10_000_000) {
            facility.modTechLevel(2);
        } else if (planet.getPopulation() > 1_000_000) {
            facility.modTechLevel(1);
        }

        facility.setRating(80 + Die.d20(2));
        if (planet.getTechLevel() >= facility.getTechLevel()) {
            facility.modRating(5 + (planet.getTechLevel() - facility.getTechLevel()) * 5);
        }

        planet.removeTradeCode(TradeCode.Na);
        if (facility.getRating() > 80) {
            planet.addTradeCode(TradeCode.Ag);
        }

        return facility;
    }
}
