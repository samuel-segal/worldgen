/*
 * Copyright (c) 2018, Samuel Penn (sam@glendale.org.uk).
 * See the file LICENSE at the root of the project.
 */
package uk.org.glendale.utils.misc;

/**
 * Converts a number to words. Copes with numbers up to the size of a long,
 * so a few quintillion.
 */
public class NumberToWords {
    private final long number;

    private static final long QUINTILLION = 1_000_000_000_000_000_000L;
    private static final long QUADRILLION = 1_000_000_000_000_000L;
    private static final long TRILLION    = 1_000_000_000_000L;
    private static final long BILLION     = 1_000_000_000L;
    private static final long MILLION     = 1_000_000L;
    private static final long THOUSAND    = 1_000L;

    private static final String[] digits = {
        "",        "one",     "two",       "three",    "four",
        "five",    "six",     "seven",     "eight",    "nine",
        "ten",     "eleven",  "twelve",    "thirteen", "fourteen",
        "fifteen", "sixteen", "seventeen", "eighteen", "nineteen"
    };

    private static final String[] tens = {
         "",      "ten",   "twenty",  "thirty", "forty",
         "fifty", "sixty", "seventy", "eighty", "ninety"
    };

    public NumberToWords(final long number) {
        this.number = number;
    }

    public NumberToWords(final String number) throws NumberFormatException {
        this.number = Long.parseLong(number);
    }

    /**
     * Outputs words for numbers less than twenty. Zero will be output as an empty string.
     *
     * @param value     A number between 0 and 19 inclusive.
     * @return          The number as words.
     */
    private String lessThanTwenty(long value) {
        if (value < 0 || value > 19) {
            throw new IllegalArgumentException("lessThanTwenty called with a value of [" + value + "]");
        }
        return digits[(int) value];
    }

    /**
     * Outputs words for numbers less than one hundred. Zero will be output as an empty string.
     *
     * @param value     A number between 0 and 99 inclusive.
     * @return          The number as words.
     */
    private String lessThanOneHundred(long value) {
        if (value < 0 || value > 99) {
            throw new IllegalArgumentException("lessThanOneHundred called with a value of [" + value + "]");
        }
        if (value < 20) {
            return lessThanTwenty(value);
        } else {
            return tens[(int)(value / 10)] + " " + lessThanTwenty(value % 10);
        }
    }

    /**
     * Outputs words for numbers less than one thousand. Zero will be output as an empty string.
     *
     * @param value     A number between 0 and 999 inclusive.
     * @return          The number as words.
     */
    private String lessThanOneThousand(long value) {
        if (value < 0 || value > 999) {
            throw new IllegalArgumentException("lessThanOneThousand called with a value of [" + value + "]");
        }

        if (value < 100 && value % 100 > 0) {
            return " and " + lessThanOneHundred(value);
        }

        String words = digits[(int) (value / 100)] + " hundred";
        if (value % 100 > 0) {
            words += " and " + lessThanOneHundred(value % 100);
        }

        return words;
    }

    /**
     * Outputs words for numbers in thousands, millions, billions, trillions, quadrillions or
     * quintillions. To output the 'millions', ions should be 1_000_000, and the value should
     * be less than 1_000_000_000. Also handles thousands.
     *
     * @param value     A number no more than a thousand times larger than the 'ions' wanted.
     * @param ions      The size to three orders of magnitude.
     * @param word      Word to use to describe this number.
     * @return          The number as words.
     */
    private String ions(long value, long ions, String word) {
        if (value >= ions * 1_000L) {
            throw new IllegalArgumentException("ions [" + word + "] called with too large a number [" + value + "]");
        }
        if (value >= ions) {
            value /= ions;

            return lessThanOneThousand(value) + " " + word + " ";
        }
        return "";
    }

    /**
     * Format the number into words.
     *
     * @return  The number in words.
     */
    public String format() {
        if (number == 0) {
            // Simple case.
            return "zero";
        }
        boolean negative = number < 0;

        String words = "";
        long value = Math.abs(number);

        // Quintillions.
        words += ions(value, QUINTILLION, "quintillion");
        value %= QUINTILLION;

        // Quadrillions.
        words += ions(value, QUADRILLION, "quadrillion");
        value %= TRILLION;

        // Trillions.
        words += ions(value, TRILLION, "trillion");
        value %= QUADRILLION;

        // Billions.
        words += ions(value, BILLION, "billion");
        value %= BILLION;

        // Millions
        words += ions(value, MILLION, "million");
        value %= MILLION;

        // Thousands
        words += ions(value, THOUSAND, "thousand");
        value %= THOUSAND;

        if (value > 0) {
            words += lessThanOneThousand(value);
        }

        // We might have double spaces, so collapse them before returning result.
        // Removing any spurious 'and' from the start is easier than not putting it
        // there in the first place.
        return (negative?"minus ":"") + (words.trim().replaceAll(" +", " ").replaceAll("^and ", ""));
    }

    public static void main(String[] args) {
        System.out.println(new NumberToWords(42_300_163_314_610L).format());
        System.out.println(new NumberToWords(42_300_163_314_600L).format());
        System.out.println(new NumberToWords(42_000_314_600L).format());
        System.out.println(new NumberToWords(132L).format());
        System.out.println(new NumberToWords(42_300_000_000_004L).format());
        System.out.println(new NumberToWords(123_042_300_163_314_610L).format());
        System.out.println(new NumberToWords(3_123_042_300_163_314_610L).format());
        System.out.println(new NumberToWords(-19_123_042_314_610L).format());
    }
}
