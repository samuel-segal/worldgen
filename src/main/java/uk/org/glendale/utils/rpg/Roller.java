package uk.org.glendale.utils.rpg;

import java.util.ArrayList;

/**
 * Builds a list of objects and then returns a random one. Objects can be added multiple times
 * to allow them to be weighted.
 *
 * @param <T>
 */
public final class Roller<T> {
    ArrayList<T> list = new ArrayList<>();

    public Roller() {
    }

    public Roller(T... objects) {
        if (objects != null && objects.length > 0) {
            for (T o : objects) {
                list.add(o);
            }
        }
    }

    public Roller<T> add(T... objects) {
        if (objects != null && objects.length > 0) {
            for (T o : objects) {
                list.add(o);
            }
        }
        return this;
    }

    public final Roller<T> add(T object, int count) {
        for (int i=0; i < count; i++) {
            add(object);
        }
        return this;
    }

    /**
     * Randomly select one of the items are returns it.
     */
    public final T roll() {
        if (list.size() == 0) {
            return null;
        }
        return list.get(Die.rollZero(list.size()));
    }

}
