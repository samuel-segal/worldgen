
#CREATE DATABASE worldgen;
#GRANT ALL ON worldgen.* TO 'worldgen' IDENTIFIED BY 'worldgen';

/* Singleton table. */
DROP TABLE IF EXISTS universe;
CREATE TABLE universe (
  id INT NOT NULL DEFAULT 1,
  name VARCHAR(64) NOT NULL,
  created_date DATETIME NOT NULL,
  last_date DATETIME NOT NULL,
  sim_time BIGINT NOT NULL DEFAULT 0,
  running BOOLEAN NOT NULL DEFAULT FALSE,
  configured BOOLEAN NOT NULL DEFAULT FALSE,
  locked BOOLEAN NOT NULL DEFAULT FALSE,
  min_x INT NOT NULL DEFAULT 0,
  max_x INT NOT NULL DEFAULT 0,
  min_y INT NOT NULL DEFAULT 0,
  max_y INT NOT NULL DEFAULT 0,
  PRIMARY KEY(id)
);
INSERT INTO universe VALUES(1, "A Few Worlds", NOW(), NOW(), FALSE, FALSE, FALSE, 0, -2, +2, -1, +1);


/* List of numerical constants. */
DROP TABLE IF EXISTS constants;
CREATE TABLE constants (
  name VARCHAR(64) NOT NULL,
  value BIGINT DEFAULT 0,
  PRIMARY KEY (name)
);
INSERT INTO constants VALUES("speed", 10);


DROP TABLE IF EXISTS sectors;
CREATE TABLE sectors (
  id INT AUTO_INCREMENT,
  name VARCHAR(64) NOT NULL,
  x INT NOT NULL,
  y INT NOT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY (name),
  UNIQUE KEY (x, y)
);

DROP TABLE IF EXISTS blobs;
CREATE TABLE blobs (
  id INT AUTO_INCREMENT,
  name VARCHAR(64) NOT NULL,
  data LONGBLOB NOT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY (name)
);

DROP TABLE IF EXISTS stars;
CREATE TABLE stars (
  id INT AUTO_INCREMENT,
  system_id INT NOT NULL,
  name VARCHAR(32) NOT NULL,
  parent_id INT NOT NULL,
  distance BIGINT NOT NULL,
  luminosity VARCHAR(4) NOT NULL,
  type VARCHAR(4) NOT NULL,
  mass DOUBLE NOT NULL DEFAULT 1.0,
  radius BIGINT NOT NULL DEFAULT 0,
  period BIGINT NOT NULL DEFAULT 0,
  PRIMARY KEY(id),
  KEY(system_id),
  UNIQUE KEY(system_id, name)
);

DROP TABLE IF EXISTS systems;
CREATE TABLE systems (
  id INT AUTO_INCREMENT,
  sector_id INT NOT NULL,
  name VARCHAR(32) NOT NULL,
  x INT NOT NULL,
  y INT NOT NULL,
  type VARCHAR(24) NOT NULL,
  zone VARCHAR(8) NOT NULL,
  planets INT NOT NULL,
  moon_count INT NOT NULL DEFAULT 0,
  port VARCHAR(8) NOT NULL,
  tech INT NOT NULL,
  population BIGINT NOT NULL,
  codes VARCHAR(32) NOT NULL,
  description TEXT NOT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY(sector_id, x, y),
  UNIQUE KEY(sector_id, name)
);
ALTER TABLE systems AUTO_INCREMENT = 1;

DROP TABLE IF EXISTS survey;
CREATE TABLE survey (
    id INT AUTO_INCREMENT,
    system_id INT NOT NULL,
    ship_id INT NOT NULL DEFAULT 0,
    scan INT NOT NULL DEFAULT 0,
    PRIMARY KEY (id),
    UNIQUE KEY(system_id, ship_id)
);
ALTER TABLE survey AUTO_INCREMENT = 1;

DROP TABLE IF EXISTS planet_maps;
DROP TABLE IF EXISTS resources;
DROP TABLE IF EXISTS commodities;
DROP TABLE IF EXISTS facilities;
DROP VIEW IF EXISTS p;
DROP TABLE IF EXISTS planets;
CREATE TABLE planets (
  id INT AUTO_INCREMENT,
  system_id INT NOT NULL,
  name VARCHAR(64) NOT NULL,
  parent_id INT NOT NULL,
  moon_of INT NOT NULL DEFAULT 0,
  distance BIGINT NOT NULL DEFAULT 0,
  tilt INT NOT NULL DEFAULT 0,
  eccentricity INT NOT NULL DEFAULT 0,
  inclination INT NOT NULL DEFAULT 0,
  radius BIGINT NOT NULL DEFAULT 0,
  density INT NOT NULL DEFAULT 1000,
  period BIGINT NOT NULL DEFAULT 0,
  day BIGINT NOT NULL DEFAULT 0,
  type VARCHAR(24) NOT NULL,
  temperature INT NOT NULL DEFAULT 0,
  night INT NOT NULL DEFAULT 0,
  atmosphere VARCHAR(24) NOT NULL,
  pressure int NOT NULL DEFAULT 0,
  field VARCHAR(24) NOT NULL,
  hydro INT NOT NULL DEFAULT 0,
  habitability INT NOT NULL DEFAULT 4,
  life VARCHAR(16) NOT NULL DEFAULT 'None',
  port VARCHAR(4) NOT NULL DEFAULT 'X',
  population BIGINT NOT NULL,
  government VARCHAR(28) NOT NULL DEFAULT 'None',
  tech INT NOT NULL,
  law INT NOT NULL,
  codes VARCHAR(64) NOT NULL,
  zone VARCHAR(8) NOT NULL DEFAULT 'GREEN',
  description TEXT,
  PRIMARY KEY (id),
  KEY (system_id)
);
ALTER TABLE planets AUTO_INCREMENT = 1;


CREATE VIEW p (id, name, distance, type, radius, temperature, atmosphere, pressure, hydro) AS
  SELECT planets.id, planets.name, planets.distance, planets.type, planets.radius, planets.temperature,
         planets.atmosphere, planets.pressure, planets.hydro
  FROM planets WHERE planets.moon_of = 0
  ORDER BY planets.system_id, planets.distance;

DROP VIEW IF EXISTS sss;
create view sss (sector, system, x, y, luminosity, type) as
  select sectors.name, systems.name, systems.x, systems.y, stars.luminosity, stars.type
  from sectors, systems, stars
  where sectors.id = systems.sector_id and stars.system_id = systems.id
  order by x, y;

CREATE TABLE planet_maps (
  id INT AUTO_INCREMENT,
  planet_id INT NOT NULL,
  name VARCHAR(64) NOT NULL,
  data LONGBLOB NOT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY (planet_id, name)
);

CREATE TABLE commodities (
  id INT AUTO_INCREMENT,
  name VARCHAR(32) NOT NULL,
  frequency VARCHAR(16) NOT NULL,
  image VARCHAR(64) NOT NULL,
  PRIMARY KEY(id), UNIQUE (name)
);

INSERT INTO commodities VALUES(0, "Hydrogen", "COMMON", "chemicals/hydrogen");
INSERT INTO commodities VALUES(0, "Helium", "RARE", "chemicals/helium");
INSERT INTO commodities VALUES(0, "Organic Gases", "UNCOMMON", "chemicals/organic_gases");
INSERT INTO commodities VALUES(0, "Corrosive Gases", "RARE", "chemicals/corrosive_gases");
INSERT INTO commodities VALUES(0, "Exotic Gases", "VERYRARE", "chemicals/exotic_gases");
INSERT INTO commodities VALUES(0, "Silicate Ore", "COMMON", "minerals/silicate_ore");
INSERT INTO commodities VALUES(0, "Carbonic Ore", "UNCOMMON", "minerals/carbonic_ore");
INSERT INTO commodities VALUES(0, "Ferric Ore", "UNCOMMON", "minerals/ferric_ore");
INSERT INTO commodities VALUES(0, "Heavy Metals", "RARE", "minerals/heavy_metals");
INSERT INTO commodities VALUES(0, "Radioactives", "VERYRARE", "minerals/radioactives");
INSERT INTO commodities VALUES(0, "Rare Metals", "VERYRARE", "minerals/rare_metals");
INSERT INTO commodities VALUES(0, "Exotic Metals", "VERYRARE", "minerals/exotic_metals");
INSERT INTO commodities VALUES(0, "Silicate Crystals", "RARE", "minerals/silicate_crystals");
INSERT INTO commodities VALUES(0, "Carbonic Crystals", "RARE", "minerals/carbonic_crystals");
INSERT INTO commodities VALUES(0, "Exotic Crystals", "TRACE", "minerals/exotic_crystals");
INSERT INTO commodities VALUES(0, "Precious Metals", "TRACE", "minerals/precious_metals");
INSERT INTO commodities VALUES(0, "Water", "COMMON", "chemicals/water");
INSERT INTO commodities VALUES(0, "Oxygen", "COMMON", "chemicals/oxygen");
INSERT INTO commodities VALUES(0, "Organic Chemicals", "COMMON", "chemicals/organic_chemicals");
INSERT INTO commodities VALUES(0, "Organic Toxins", "UNCOMMON", "chemicals/organic_toxins");
INSERT INTO commodities VALUES(0, "Inorganic Toxins", "UNCOMMON", "chemicals/inorganic_toxins");
INSERT INTO commodities VALUES(0, "Prokaryotes", "COMMON", "organics/prokaryotes");
INSERT INTO commodities VALUES(0, "Protozoa", "COMMON", "organics/protozoa");
INSERT INTO commodities VALUES(0, "Algae", "COMMON", "organics/algae");
INSERT INTO commodities VALUES(0, "Slime Mold", "UNCOMMON", "organics/slimemold");
INSERT INTO commodities VALUES(0, "Metazoa", "COMMON", "organics/metazoa");
INSERT INTO commodities VALUES(0, "Echinoderms", "COMMON", "organics/echinoderms");
INSERT INTO commodities VALUES(0, "Lichen", "COMMON", "organics/lichen");
INSERT INTO commodities VALUES(0, "Fungus", "COMMON", "organics/fungus");
INSERT INTO commodities VALUES(0, "Molluscs", "COMMON", "organics/molluscs");
INSERT INTO commodities VALUES(0, "Large Molluscs", "COMMON", "organics/molluscs");
INSERT INTO commodities VALUES(0, "Huge Molluscs", "COMMON", "organics/molluscs");
INSERT INTO commodities VALUES(0, "Kelp", "COMMON", "organics/kelp");
INSERT INTO commodities VALUES(0, "Sponges", "COMMON", "organics/sponges");
INSERT INTO commodities VALUES(0, "Jellies", "COMMON", "organics/jellies");
INSERT INTO commodities VALUES(0, "Large Jellies", "COMMON", "organics/jellies");
INSERT INTO commodities VALUES(0, "Huge Jellies", "COMMON", "organics/jellies");
INSERT INTO commodities VALUES(0, "Fish", "COMMON", "organics/fish");
INSERT INTO commodities VALUES(0, "Large Fish", "COMMON", "organics/fish");
INSERT INTO commodities VALUES(0, "Huge Fish", "COMMON", "organics/fish");
INSERT INTO commodities VALUES(0, "Gigantic Fish", "COMMON", "organics/fish");

INSERT INTO commodities VALUES(0, "Scrubland", "COMMON", "organics/scrubland");
INSERT INTO commodities VALUES(0, "Woods", "COMMON", "organics/woods");
INSERT INTO commodities VALUES(0, "Grasses", "COMMON", "organics/grasses");

INSERT INTO commodities VALUES(0, "Insects", "COMMON", "animals/insects");
INSERT INTO commodities VALUES(0, "Rodents", "COMMON", "animals/rodents");
INSERT INTO commodities VALUES(0, "Large Rodents", "COMMON", "animals/rodents");

INSERT INTO commodities VALUES(0, "Small Amphibians", "COMMON", "animals/amphibians");
INSERT INTO commodities VALUES(0, "Amphibians", "COMMON", "animals/amphibians");
INSERT INTO commodities VALUES(0, "Large Amphibians", "COMMON", "animals/amphibians");

INSERT INTO commodities VALUES(0, "Small Burrowers", "COMMON", "animals/burrowers");
INSERT INTO commodities VALUES(0, "Burrowers", "COMMON", "animals/burrowers");
INSERT INTO commodities VALUES(0, "Large Burrowers", "COMMON", "animals/burrowers");

INSERT INTO commodities VALUES(0, "Small Grazers", "COMMON", "animals/grazers");
INSERT INTO commodities VALUES(0, "Grazers", "COMMON", "animals/grazers");
INSERT INTO commodities VALUES(0, "Large Grazers", "COMMON", "animals/grazers");
INSERT INTO commodities VALUES(0, "Huge Grazers", "COMMON", "animals/grazers");
INSERT INTO commodities VALUES(0, "Gigantic Grazers", "COMMON", "animals/grazers");

INSERT INTO commodities VALUES(0, "Small Hunters", "COMMON", "animals/hunters");
INSERT INTO commodities VALUES(0, "Hunters", "COMMON", "animals/hunters");
INSERT INTO commodities VALUES(0, "Large Hunters", "COMMON", "animals/hunters");
INSERT INTO commodities VALUES(0, "Huge Hunters", "COMMON", "animals/hunters");
INSERT INTO commodities VALUES(0, "Gigantic Hunters", "COMMON", "animals/hunters");

INSERT INTO commodities VALUES(0, "Salvage", "RARE", "items/salvage");
INSERT INTO commodities VALUES(0, "Historical Items", "RARE", "items/historical");
INSERT INTO commodities VALUES(0, "Alien Items", "RARE", "items/alien");


CREATE TABLE resources (
  id INT AUTO_INCREMENT,
  planet_id INT NOT NULL,
  commodity_id INT NOT NULL,
  density INT NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (planet_id) REFERENCES planets(id) ON DELETE CASCADE,
  FOREIGN KEY (commodity_id) REFERENCES commodities(id) ON DELETE CASCADE
);

CREATE TABLE facilities (
  id INT AUTO_INCREMENT,
  planet_id INT NOT NULL,
  name VARCHAR(24) NOT NULL,
  title VARCHAR(64) NOT NULL,
  type VARCHAR(24) NOT NULL,
  rating int NOT NULL,
  tech int NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (planet_id) REFERENCES planets(id) ON DELETE CASCADE
);
