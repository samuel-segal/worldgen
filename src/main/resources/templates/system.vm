#* @vtlvariable name="id" type="long" *#
#* @vtlvariable name="count" type="java.util.HashMap" *#
#* @vtlvariable name="minX" type="int" *#
#* @vtlvariable name="minY" type="int" *#
#* @vtlvariable name="maxX" type="int" *#
#* @vtlvariable name="maxY" type="int" *#
#* @vtlvariable name="system" type="uk.org.glendale.worldgen.astro.systems.StarSystem" *#
#* @vtlvariable name="systemType" type="java.lang.String" *#

<!DOCTYPE html>
<html lang="en">
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css"
        integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" crossorigin="anonymous">

  <link href="/css/worldgen.css" rel="stylesheet" type="text/css"/>

    #set ( $width = $maxX - $minX + 1 )
    #set ( $height = $maxY - $minY + 1 )

    #set ( $SECTOR_WIDTH = 96 )
    #set ( $SECTOR_HEIGHT = 120 )

    #set ( $mapWidth = $SECTOR_WIDTH * $width )
    #set ( $mapHeight = $SECTOR_HEIGHT * $height )

    #set ( $divWidth = $mapWidth + $SECTOR_WIDTH )
    #set ( $divHeight = $mapHeight + $SECTOR_HEIGHT )

  <style>
    span.counter {
      display: inline-block;
      border: 1px solid black;
      font-size: xx-large;
      font-weight: bold;
      color: red;
      width: 64px;
      height: 64px;
      border-radius: 4px;
      background-color: white;
      float: right;
      margin-top: -20px;
      margin-left: 8px;
    }

    span.counter span {
      display: inline-block;
      margin-left: 40px;
      margin-top: 24px;
    }

    span.startype {
      display: inline-block;
      border: 1px solid black;
      border-radius: 2px;
      padding: 0px 5px 0px 5px;
      margin-top: 2px;
    }

    h1 a.subtle {
      color: white;
    }

  </style>
</head>

<script>
  var systemId = ${id};
  var zoom = 0;
</script>

<body>
<div class="container-fluid">
  <h1><a id="home" href="/">&nbsp;</a> &gt; <a class="subtle" href="/sector/${sector.x},${sector.y}">$sector.name</a> &gt; $subsector &gt; $system.name [$x$y] <span>${systemZone} / ${systemType}</span></h1>

  <div>
    <span class="counter" style="background-image: url('/icons/systems/port_${system.starPort.code}.png');"></span>

    #if ($count['Jovian'])
      <span class="counter" style="background-image: url('/icons/systems/group_Jovian.png')"><span>${count['Jovian']}</span></span>
    #end
    #if ($count['Terrestrial'])
      <span class="counter" style="background-image: url('/icons/systems/group_Terrestrial.png')"><span>${count['Terrestrial']}</span></span>
    #end
    #if ($count['Dwarf'])
      <span class="counter" style="background-image: url('/icons/systems/group_Dwarf.png')"><span>${count['Dwarf']}</span></span>
    #end
    #if ($count['Belt'])
      <span class="counter" style="background-image: url('/icons/systems/group_Belt.png')"><span>${count['Belt']}</span></span>
    #end

    <p>
      #foreach ($star in $system.stars)
        <span class="startype"><b><big style="color: ${star.spectralType.getRGBColour()};">&#x25cf;</big> $star.name</b> $star.spectralType $star.luminosity</span>
      #end
    </p>

    $system.description
  </div>

  <div id="system" class="container-fluid">

    <ul id="star-menu" class="nav nav-tabs">
      <li class="nav-item active" id="system-map-menu">
        <a class="nav-link" data-toggle="tab" id="system-map-tab" href="#system-map">Overview</a>
      </li>

      #foreach ($star in $system.stars)
        <li class="nav-item">
          <a class="nav-link" data-toggle="tab" id="star-${star.id}-tab" href="#star-${star.id}">${star.name}</a>
        </li>
      #end
    </ul>

    <div class="tab-content" id="myTabContent">
      <div class="tab-pane show active" id="system-map">
        <div id="systemmap">
        </div>
        <div>
          <p>
          <button onclick="updateSystemMap(0)">System</button>
          <button onclick="updateSystemMap(1024)">Inner</button>
          <button onclick="updateSystemMap(2048)">Middle</button>
          <button onclick="updateSystemMap(4096)">Outer</button>
          <button onclick="updateSystemMap(8192)">Oort</button>
          </p>
          <p>
          <button onclick="updateSystemMap(0)">System</button>
          #foreach ($star in $system.stars)
              <button onclick="updateSystemMap($foreach.count)">${star.name}</button>
          #end
          </p>

          #foreach ($star in $stardata)
              <p>
                  <b>${star.name}</b><br/>
                  Constant = ${star.constant}<br/>
                  Luminosity = ${star.luminosity}<br/>
                  Diameter = ${star.diameter}<br/>
              </p>
          #end
        </div>
        <!--
        <dl class="data-block">
          <dt>Type</dt><dd>${system.type}</dd>
          <dt>Zone</dt><dd>${system.zone}</dd>
        </dl>
        -->
      </div>

      #foreach ($star in $system.stars)
        <div class="tab-pane" id="star-${star.id}">

        </div>
      #end
    </div>
  </div>

  <div id="footer">
    <span class="infolink"><a href="https://www.notasnark.net/worldgen/">WorldGen</a> by Samuel Penn</span>
    <span class="version"><strong>Version:</strong> <em>${version}</em></span>
  </div>
</div>
<script src="https://code.jquery.com/jquery-3.2.1.min.js"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js"
        integrity="sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4"
        crossorigin="anonymous"></script>

<script src="/js/system.js"></script>
<script src="/js/three.js"></script>

<script>

  function updateSystemMap(z) {
      if (z === 0) {
          zoom = 0;
      } else if (z < 1024) {
          zoom = zoom & (1024 + 2048 + 4096 + 8192);
          zoom += z;
      } else {
          zoom = zoom & 15;
          zoom += z;
      }
      $("#systemmap").html(`<img src="/api/system/${systemId}/map?width=1024&zoom=${zoom}" width="1024" height="1024"/>`);
  }

  $(document).ready(function() {

      starSystem.id = $system.id;
      starSystem.name = "$system.name";
      starSystem.stars = [];
      starSystem.planets = [];

      updateSystemMap(0);

      #foreach ($star in $system.stars)
        starSystem.stars[$star.id] = {};
        $.getJSON("/api/star/" + ${star.id} + "/planets", function(resp) {
          $("#star-${star.id}").html('<ul class="nav nav-tabs" id="planet-menu-${star.id}" role="tablist"></ul>');
          $("#star-${star.id}").append("<div class='tab-content' id='planet-content-${star.id}'></div>");

          starSystem.planets[${star.id}] = resp;

          if (resp.length > 0) {
            listPlanetsAroundStar(${star.id});
          } else {
              $("#star-${star.id}-tab").remove();
          }
        });
      #end

      // Look for any planets which are not directly orbiting a star.
      starSystem.stars[-1] = {};
      starSystem.planets[-1] = [];
      $.getJSON("/api/system/" + ${system.id} + "/planets", function(resp) {
         for (p in resp) {
             let planet = resp[p];
             if (planet.parentId == -1) {
                 starSystem.planets[-1].push(planet);
             }
         }
         if (starSystem.planets[-1].length > 0) {
             $("#myTabContent").append('<div class="tab-pane" id="star--1"></div>');
             $("#star-menu").append('<li class="nav-item"><a class="nav-link" data-toggle="tab" id="star--1-tab" href="#star--1">Planets</a></li>');
             $("#star--1").html('<ul class="nav nav-tabs" id="planet-menu--1" role="tablist"></ul>');
             $("#star--1").append("<div class='tab-content' id='planet-content--1'></div>");

             listPlanetsAroundStar(-1);
         }
      });

  });
</script>

</body>
</html>
