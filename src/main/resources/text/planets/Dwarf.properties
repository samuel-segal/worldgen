#
# Copyright (c) 2018, Samuel Penn (sam@notasnark.net).
# See the file LICENSE at the root of the project.
#

world=[dwarf world|dwarf planet]

# World names
name.intro=$Name
name.intro.1=[The world of $Name|The planet of $Name|The dwarf world of $Name]
name.intro.2=[This world|This planet|This]
name.intro.3=The [small|dwarf] [world|planet] of $Name

name.inline=$Name
name.inline.1=[$Name|this world|this planet]
name.inline.2=[$Name|the world of $Name|the planet of $Name|this world of $Name]


planet.feature.TideLocked=[This world|This planet|$Name] is tidally locked to its parent, always keeping the same \
  face towards it.

planet.feature.Resonance=[$Name|The world of $Name] is in orbital resonance with its parent, its rotation period \
  matching it in a common pattern.

planet.feature.ColdNight=The [night temperature|night time temperature|temperature at night] on [this world|$Name] \
  [drops|plummits] to below freezing [due to|because of] the [lack of atmosphere|absence of an atmosphere] \
  and [long days and nights|long nights|slow rotation].

planet.feature.ColdNight.1=[Because of|Due to] the combination of no regulating atmosphere and long \
  [day and night cycle|days and nights|rotation period], despite the hot daytime temperature \
  the nights of $Name drop [below freezing|considerably].



barren.surface.Common=The [surface within the|bottom of the] craters of {name.inline} are pitted and cracked, and \
  covered in [broken|shattered|fractured] [bits|chunks] of [glass|rock|crystalline formations].

barren.surface.Common.1=[The lack of|A complete lack of|The absence of] weather and hydrographics has left \
  the surface of {name.inline} sharp, with plenty of rock surfaces which can [rip|puncture|tear] suits or \
  damage tyres of ground exploration vehicles.

barren.surface.Common.2=The surface is covered in a fine dust that gets into mechanical objects and quickly \
  wears them down.

barren.surface.Uncommon=The surface of {name.inline} is covered in [small|broken] [shards|slivers|chips] of \
  glass, possibly the left overs of an ancient asteroid collision or surface heating.

barren.surface.Uncommon.1=Deep lava tubes can be found frequently across the surface.

barren.surface.Uncommon.2=Pebbles cover [most|much] of the surface of {name.inline}.

barren.surface.Rare=Long [scratches|markings] [cover|stretch across|run across] the surface, \
  [many|tens of|up to five] kilometres long. They are [several metres|tens of centimetres|about a metre] deep, and \
  it isn't known what caused them.

barren.surface.VeryRare=Strange columns of rock can be found around the equator.


#
# Common surface features for RedHot worlds.
#

RedHot=($3D6|3=<p>{RedHot.VeryRare}</p>|4,5=<p>{RedHot.Rare}</p>|6,7,8=<p>{RedHot.Uncommon}</p>|9,10,11,12=<p>{RedHot.Common}</p>)

RedHot.Common=[Streams|Rivelets] of [liquid|molten] [metal|metals] [run|flow] across the surface, bubbling up \
  from beneath the red hot surface of $Name.

RedHot.Common.1=[Thick|Thick layers of|A thin layer of] dust covers the surface of {name.inline}. \
  [Not only does it get|It not only gets] into [mechanical joints|machinary] and [clogs it up|wears it down], \
  but [its high temperature|it is red hot and] causes rapid [heat|temperature] overload.

RedHot.Common.2=[Much|Most] of the surface of {name.inline} is [cracked|covered in fine cracks], caused by \
  extreme heat stress.

RedHot.Common.3=Parts of the surface hides [dust bowls|bowls of dust|dust filled craters], which if not avoided \
  can [engulf|swallow] [explorers|vehicles] [completely|whole], smothering them in [hot|red hot] dust that \
  [quickly|fatally] overwhelms any cooling systems.

RedHot.Common.4=[Some parts|Parts] of the [ground|surface] cover hollowed out spaces just beneath the surface. \
  Travelling across such regions can cause collapse into sinkholes many [metres|tens of metres] deep.

RedHot.Common.5=[Bizarre|Strange|Unusual|Towering] metallic [ridges|formations|'ferns'] can be found across the \
  surface, [created|caused|formed] by the slow accretion of metallic ash.

RedHot.Uncommon=Scratches, many [kilometres|hundreds of metres] long, [cover|criss-cross] the surface of $Name. \
  It's not known what caused the scratches, but they appear to be [natural|natural|artificial] in origin.

RedHot.Uncommon.1=The surface of {name.inline} is covered in razor-sharp metal-tipped rocks, that have the \
  ability to shred anything unprotected.

RedHot.Uncommon.2=Plumes of metallic ash vent [sporadically|randomly|predictably] from cracks in the ground, \
  [covering|coating] the [surrounding|nearby] [area|region] with molten metal droplets.

RedHot.Uncommon.3=[Deep|Large|Crumbling|Unstable] sinkholes dot the surface of {name.inline}, dropping down \
  [hundreds of metres|kilometres|often over a hundred metres|several hundred metres|tens of metres] deep to \
  [boiling pools of metal|molten metallic pools|glowing pits of metal|white hot veins of rock].

RedHot.Uncommon.4=Planet wide [tremors|quakes] shake the surface of the world on an almost [daily|weekly|yearly] \
  basis, and can cause major upheaval.

RedHot.Rare=[Apparently|Seemingly] [unpredictable|random] magnetic hot spots move under the surface of $Name. \
  It is not known what causes them, but they seem to [strengthen and fade|come and go|appear] unpredictably. \
  They last [a few|several|one or two] [days|hours|hours|minutes], before fading. The region above them is \
  subject to strong magnetic interference, and increased heat.

RedHot.Rare.1=Radioactive hot spots of @+Radioactives [dangerous|dense|heavy|decaying|unstable] metals cover \
  the surface of $Name. They are poorly mapped, and [highly|very] dangerous.

RedHot.Rare.2=[Rivers|Streams|Flows] of molten metal [run|flow] just under the surface of {name.inline}, \
  heating the surface above to higher than [average|normal|usual] temperatures. The crust can be \
  [fragile|weak|unstable], and disturbing it can cause a [sinkhole to form|collapse].

RedHot.Rare.3=Electrical discharges [flow|flicker] across the surface of {name.inline}, following veins \
  of [molten metal|metal]. The discharges are [sporadic|random|unpredictable], and some think that they \
  are artifical in nature.

RedHot.VeryRare={name.intro} [vibrates|trembles] to a regular beat that [originates|seems to originate] from \
  [the core|near the core|inside the planet|deep inside the core]. The [source|origins|exact nature] \
  of the [sound waves|vibrations] aren't known, but they [repeat|regularly repeat|repeat roughly] every \
  [few days|few hours|$$2D4 hours|$$2D6 days|$$10D10 minutes].

RedHot.VeryRare.1=[Unsual|Unexplained|Improbable] masses move [beneath|under|below] the surface of {name.inline}. \
  They can be detected with gravity measurements and through the presence of tremors that reach the surface. There \
  are [dozens of|dozen of|hundreds of|several] such [anomolies|features], each [kilometres|tens of kilometres|\
  hundreds of metres|about a hundred kilometres|several kilometres|many kilometres] [across|in width|in length|big]. \
  Their [movement|motion] seems to be non-random, and there is a possibility that it is some form of \
  [silicate|non-carbon based|mineral based] [life|life-form] or at least artificial in nature.

#
# Common surface features for Hot worlds.
#

Hot=($3D6|3=<p>{Hot.VeryRare}</p>|4,5=<p>{Hot.Rare}</p>|6,7,8=<p>{Hot.Uncommon}</p>|9,10,11,12=<p>{Hot.Common}</p>)

Hot.Common=Small [fountains|streams] of [liquid|molten] metal can be found in the hottest [valleys|craters]. \
  They [cool|solidify] into silvery [surfaces|pools] of hot, solidified pure metal. 

Hot.Common.1=[Thick|Thick layers of|A thin layer of] dust covers the surface of {name.inline}. \
  [Not only does it get|It not only gets] into [mechanical joints|machinary] and [clogs it up|wears it down], \
  but [its high temperature|it is extremely hot] and can cause rapid [heat|temperature] overload.

Hot.Common.2=[Much|Most] of the surface of {name.inline} is [cracked|covered in fine cracks], caused by \
  extreme heat stress.

Hot.Common.3=Parts of the surface hides [dust bowls|bowls of dust|dust filled craters], which if not avoided \
  can [engulf|swallow] [explorers|vehicles] [completely|whole], smothering them in [hot|red hot] dust that \
  [quickly|fatally] overwhelms any cooling systems.

Hot.Common.4=[Some parts|Parts] of the [ground|surface] cover hollowed out spaces just beneath the surface. \
  Travelling across such regions can cause collapse into sinkholes many [metres|tens of metres] deep.

#
# Arean Specials
#

Arean.Common=The winds on $Name can be powerful, wipping up huge dust storms that last for days.

Arean.Uncommon=There are [scatterings|deposits|nodules] of precious metals on the [surface|ground], left overs \
  from a meteorite impact not too long in the past. @+PreciousMetals

Arean.Rare=

Arean.VeryRare=[@=StandingStones|@=AlienCube|@=Hexagons]


StandingStones.title=The Standing Stones of $Name

StandingStones=There are [several|a number] of [locations|sites] on $Name where [huge|massive|gigantic] standing \
  stones have been [erected|constructed]. Dating of them [estimates|suggests] that the are [at least|about|over|nearly] \
  [five|six|seven|eight|nine|ten|twelve|fifteen|twenty|twenty-five|thirty|forty|fifty] thousand years old. \
  This raises the question of who built them, why, and how they have [survived|lasted|resisted erosion for] so long.

AlienCube.title=The Black Cube of $Name

AlienCube=Sitting on the surface of $Name is a massive metal cube, [$3D100+100] metres on a side. It towers into \
  the sky, and sits perfectly [flat|aligned] to the local gravity. There is no visible erosion, and attempts to \
  estimate its age have failed.

AlienCube.1=Half buried in the sands of $Name is a [large|gigantic] cube, perfect in its dimensions. The material \
  it [has been|is] [built|made|constructed] from is unidentifiable, and it resists all erosion or attempts to take \
  samples or damage it. Each edge measures [$3D100+100] metres in length, and there have been suggestions that it \
  is a fracture in space-time rather than a physical entity.


Hexagons.title=The Hexagon Pits of $Name

Hexagons=Found across the surface of $Name are massive pits in the shape of hexagons. The hexagons are accurate to \
  within a few [millimetres|centimetres], and each edge is [$6D100+50] metres on a side, and about a kilometre deep. \
  It's not known who built them, but they seem to be at least 10,000 years old.


#
# The following descriptions are for worlds with at least ComplexSea level of life, principally SimpleLand or
# greater.
#

Arean.Chemicals=@+++OrganicChemicals @+Protozoa @+Metazoa @+++Algae @++Kelp
Arean.GeneralSea=@++Crawlers @+Jellies @+Sponges @+Molluscs

Arean.Sea=</p><p>The oceans of $Name are full of complex life. [Huge|Massive] shoals of [scaled|smooth skinned] \
  vertebrates are common, as are the hunters with large mouths full of teeth. There are also soft jellies, \
  and many limbed creatures that crawl and hunt the ocean floor. \
  {Arean.Chemicals} {Arean.GeneralSea} \
  @+++++Swimmers @+++LargeSwimmers @+HugeSwimmers @+++Crawlers @+LargeCrawlers @++Jellies

Arean.Sea.1=</p><p>The seas are home to long [snake-like|eel-like|worm-like] creatures that swim in the sunlight \
  upper layers. Some species can be over [3|4|5|7|10] metres long, and feed on anything smaller, including \
  [off-worlders|non-natives]. In the deeper ocean are massive \
  {Arean.Chemicals} {Arean.GeneralSea} \
  @++++Swimmers @+++++LargeSwimmers @+++HugeSwimmers @++LargeJellies @++HugeJellies

Arean.Sea.2=The [seas|oceans] are full of [advanced|highly evolved|diverse] [life|life forms|organisms], \
  which [exploit|exist in|live in] most niches and regions. There are [huge|massive|large] vertebrates \
  that are the apex aquatic predators, as well as sea dwelling shelled creatures. \

