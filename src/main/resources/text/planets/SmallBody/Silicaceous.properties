#
# Copyright (c) 2020, Samuel Penn (sam@notasnark.net).
# See the file LICENSE at the root of the project.
#


feature.Large=[$Name|This] is a large [asteroid|planetoid] about \
  [$Radius kilometres in radius|$Diameter kilometres in diameter]. It is [moderately|reasonably] \
  bright, being [made|composed] of [silicate|silicate and metallic] [materials|compounds].

feature.Huge=[$Name|This] is a [massive|huge] [asteroid|planetoid|planetoid] about \
  [$Radius kilometres in radius|$Diameter kilometres in diameter]. It is [moderately|reasonably] \
  bright, being [made|composed] of [silicate|silicate and metallic] [materials|compounds].

feature.Gigantic=[$Name|This] is a [giant|gigantic] planetoid about \
  [$Radius kilometres in radius|$Diameter kilometres in diameter]. It is [moderately|reasonably] \
  bright, being [made|composed] of [silicate|silicate and metallic] [materials|compounds].

planet=[Such asteroids|Asteroids such as this|Asteroids like $Name] are mostly \
  [made|formed] of [stony|rocky] silicate [materials|composition|minerals]. They \
  [contain|have|are mostly] iron and magnesium silicates, making them a [rich|good] source of \
  metallic resources. [This one|$Name] is [$Diameter]km [across|wide|in diameter].

#
# Special text is added to the end of the main description. Randomly select between VeryRare, Rare, UnCommon
# and Common additions, with about a 50% chance of not including anything. The first option from each goes off
# to use the generic descriptions defined in SmallBody.properties
#
planet.special=($3D6|3={VeryRare}|4,5={Rare}|6,7={Uncommon}|8,9,10={Common})


Common={SmallBody.Common}

Common.1=The surface of $Name is made up of [sharp|razor sharp|knife-like] [edges|ridges|corners] that can easily \
  [cut through|rip|tear] a [standard|light] space suit.

Common.2=Dust and [tiny|small] [broken|fractured|shattered] [rocks|pebbles] are scatted across the surface.

Common.3=Crystalline veins of rock are visible along the surface. @++SilicateCrystals

Uncommon={SmallBody.Uncommon}

Uncommon.1=Thick crystalline veins of rock are visible along the surface. @+++SilicateCrystals

Uncommon.2=[Much|Most|Large parts] of the [surface|ground] is covered in [loose|fractured|broken] [rubble|rocks|debris] \
  which sometimes shifts as the asteroid spins.

Uncommon.3=[Some|Several] parts of the surface look like it has melted in the recent past.

Rare={SmallBody.Rare}

Rare.1=The asteroid is heavily fractured, [probably|most likely] from a recent collision with a smaller body. \
  There are [several|a few] fissures that lead deep beneath the surface. @+ExoticCrystals

Rare.2=Several holes in the deeper craters look artificial, as if someone has tried to dig [tunnels|passages] down \
  into the rock, though there is no [record|evidence] of who might have done this.

VeryRare={SmallBody.VeryRare}

VeryRare.1=The core of [$Name|this asteroid] is unusually radioactive. @++Radioactives


