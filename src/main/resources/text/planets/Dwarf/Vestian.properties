#
# Copyright (c) 2020, Samuel Penn (sam@notasnark.net).
# See the file LICENSE at the root of the project.
#

planet.Flattened=This [dwarf|small] world is [clearly|obviously] [non-spherical|irregular|misshapen], flattened at \
  the poles and bulging around the [middle|equator]. The [silicate|rocky] surface is covered with craters, some of \
  which contain volatiles. @+Water

planet.Egg=$Name is an irregular egg-shaped world, spinning around it's long axis. It has an average radius of \
  [$Radius]km, but given [the planet's|its] shape and uneven surface this is difficult to measure. There are \
  [plenty of|many] craters, though there are signs of some parts of the surface having been repaved early in \
  its past.

planet.Dumbbell=[$Name|This] looks like two rocks collided and stuck together, and it is likely it was formed \
  after a slow collision between two [similar bodies|bodies of similar mass]. The poles are on either end \
  of the dumbbell shape, and its equator is around the thin middle.

planet.Irregular=An irregularly sharped chunk of rock, showing [clear|obvious] signs of having suffered collisions \
  in the past, and lacking sufficient gravity to pull itself back into a sphere. Craters and mountains are common \
  across the surface. It is [more massive|denser] than other bodies of its size due to a metallic core.

planet.Broken=$Name looks like it has been severely damaged in the past, as if chunks of the surface were \
  ripped away. There are signs of melting, so this was probably due to a collision. What is left consists of \
  silicates with a low amount of volatiles. @-Water


planet=This is an irregular world only barely [large|massive] enough to have [pulled|shaped|formed] itself into \
  something [resembling|almost like|similar to] a sphere. It is mostly silicates, with few volatiles or metals. \
  The barren surface is covered in craters, gullies and mountains.

planet.1=$Name is an irregularly shaped world that is [half way between asteroid and proto-planet|part asteroid \
  and part proto-planet|a small proto-planet|considered a small proto-planet]. It has a heavily cratered surface \
  and a metallic core.


planet.feature.MagneticField=$Name has a weak magnetic field, left over from its time of formation.


planet.special=($3D6|3={VeryRare}|4,5={Rare}|6,7={Uncommon}|8,9,10={Common})

Common=The rocky surface is dusty, and covered in [a multitude of|many] fragments from collisions with smaller asteroids.

Common.1=The surface [of $Name|] is made up of coarse [dust|grains of rocks] which can wreck equipment.

Common.2=The mantle of $Name is criss-crossed with lava tubes, many metres wide, which break the surface at \
  irregular intervals.

Common.3=The surface is covered with jagged spikes of rock, and sharp edges un-weathered by any atmosphere 

Uncommon=[The surface of $Name|$Name] has a reddish [colouring|tint] to it, caused by tholins on the surface. \
  @=Reddish @+OrganicChemicals

Rare=The world is rich in metals. @+PreciousMetals

VeryRare=$Name has a green tint to its surface, [due to|caused by] crystalline [dust|deposits|flakes] mixed into \
  the surface dust. @=Greenish @+SilicateCrystals
