#
# Copyright (c) 2018, Samuel Penn (sam@notasnark.net).
# See the file LICENSE at the root of the project.
#

facility={FreeHolding.Intro} {FreeHolding.$Type}

FreeHolding.Intro=There is a [simple|small|basic|free] colony of independent settlers [here|on $Name].

FreeHolding.Intro.1=An independent settlement of $~Population people is based [here|here on $Name].

FreeHolding.Intro.2=$Name is home to $~Population [settlers|colonists] who have built their own independent colony.


FreeHolding.EoGaian=The conditions [here|on $Name] don't allow for the colony to remove all dependencies. \
  Though basic edible matter can be obtained from the sea, it would be a relatively [harsh|hard] existance \
  if they didn't have access to external supplies. The [unpleasant|harsh|inhospitable] surface conditions \
  mean they are locked up in heavily sealed [domes|tunnels] for [most|the majority|much] of the time.

FreeHolding.MesoGaian=It is a [|relatively|quite|reasonably] [hard|harsh] environment [on $Name|here|on the surface \
  of $Name]. Though there are plenty of food resources in the sea, nothing can be grown on land. \
  {conditions.$$Habitability}

FreeHolding.EuArean=This is a cold, dry world with few natural resources. \
  {conditions.$$Habitability}

FreeHolding.MesoArean=$Name is a inhospitable world that has nonetheless been chosen by colonists. \
  {conditions.$$Habitability}


conditions.II=The main settlement consists of [clusters|a cluster] of buildings mostly [connected|joined] by tunnels \
  that [reduce|limit] the [necessity of going|need to go] outside. There is no need for airlocks though, and the air \
  can be filtered directly from the outside. A few smaller settlements [are located|have been built] elsewhere, but \
  all are within [a few kilometres|a few dozen kilometres|a hundred kilometres] [|or so] of the main settlement.

conditions.II.1=The largest [settlement|town] is located [near to|around the] spaceport, and [consists of|and is \
  comprised of] many small buildings [fashioned from local stone|built of plastic and metal|made from converted \
  cargo containers]. They are not airtight, being simply built on the hard rock of the land.

conditions.II.2=The [settlers|colonists] have built a small town of [stone|brick] buildings that mostly cluster \
  [along a single street|along a main street|along a couple of streets|at a crossroads] that sees mostly foot \
  traffic.

conditions.III=The settlements on $Name consist of domed buildings fully sealed against the environment. \
  [Inside|Within] them are individual buildings, with open areas for parks.

conditions.IV=The [primary|main] [settlement|town] of $Name is a domed crater several hundred metres across. The \
  lower levels are subterranean tunnels and rooms built into the rock, whilst better quality buildings sit \
  directly under the dome.

conditions.V=The settlements on $Name are underground complexes sealed completely off from the surface environment \
  with thick airlocks.


# Anarchic Governments. Often libertarian or criminal.
facility.government.Anarchy={anarchy.(?Standard|Rich|Poor)}

anarchy.standard=$Name has no unified government, instead society is [based around|built on] libertarian \
  values of personal [freedom|liberty] and responsibility. Shared responsibility is taken for any core services \
  ($Habitability|3<and life support systems) needed. The colonists have a strong sense of shared values and mutual \
  respect.

anarchy.Standard.1=$Name is a libertarian [outpost|settlement|colony] which was [created|founded] in order to \
  [escape|get away] from [big government|overbearing rules and regulations|restrictive government]. Overall say \
  in the [running|governance|direction] of each settlement is [controlled|dictated] by those who donate the \
  [most|largest amount of] resources into the settlement's funds.

anarchy.Standard.2=$Name was originally founded as a small colony of those fleeing [religious persecution| \
  legal sanction|a family feud|a clan feud|criminal gangs]. The [initial|original] [desire|intent] was to \
  [create|build|setup] a [communist|democratic|libertarian|workers] [republic|utopia|state|paradise], but it has \
  since collapsed into a [criminal|gang] run anarchic state, [dominated by|ruled through] violence and terror. A few \
  core families dominate $Name, and they control all aspects of life for everybody else.


anarchy.Rich=This is a smoothly running libertarian state, which was originally founded by [rich|wealthy] \
  [business people|philanthropists] who wanted somewhere of their own. $Name has since grown beyond the original \
  idea, but it continues to be [funded|supported] [from the outside|by external donors]. Society is divided by \
  family, and what [happens|occurs] within a family is considered up to the family to decide. \
  {anarchy.bad|anarchy.good|} Issues between families which can't be resolved [amicably|privately] are \
  [dealt with|handled] by a [court of peers|council of officials], who are generally quite harsh in their \
  rulings, in order to encourage private resolutions.

anarchy.Poor=This is a poor colony, which is low on [resources|funds|wealth] and is beginning to fall apart. \
  [Everybody|Individuals] are free to contribute to the [common good|upkeep of the colony], but not enough do \
  so shared facilities are [undergoing collapse|in a state of decay]. This has led to [friction|violence|disagreement] \
  between those who have, and those who don't. [Slavery|Indentured servitude] is common, with prostitution, begging \
  and theft [an accepted part of life here|on the increase] as people do what they can to survive.


anarchy.bad=This [tends to result in|has led to] some [unpleasant|abusive] situations, which [the settlements|society] \
  [ignores|turns a blind eye to|pretends to ignore], and accepts as a side effect of their freedom.

anarchy.bad.1=Abuse is [quite common|rampant|not unheard of|common|all too frequent], but as long as it doesn't \
  spill out to affect [the public|other people|the general public] it is [ignored|swept under the carpet|tolerated].

anarchy.good=[Issues|Problems] within families rarely become public.


facility.government.Communist={communist.(?Standard|Rich|Poor)}

communist.Standard=The settlements here work on a communist ethic, with all resources considered to \
  be equally shared amongst the [population|workers|people|populace]. The government consists of a randomly \
  [chosen|selected|determined] [body|parliament|board] of adults that rotate every [two|three|four|five] years who \
  make [overal|long term] decisions for the colony and act as [judges|enforcers] of the law. Individuals are assigned \
  work to which they are best suited.

communist.Poor=[This planet|$Name] is now home to a struggling group of colonists who are [trying|attempting] to \
  keep their [settlements|colony] together. What machinery they have has been repaired [too many|multiple] times, \
  and they barely have the skills to keep on doing this. They are still technically a communist collective, but it \
  is falling apart under the strain, with factionalism and rivalry common. 

communist.Rich=This is a [rich|wealthy|well off] communist settlement governed by a group of leaders that decide \
  policy and actions for everyone. Leaders are elected for life, so [are|tend to be] very conservative. Since the \
  colony is doing well, [most people are|everyone is] [happy|content] with the state of affairs, with everyone \
  doing the work assigned to them.





facility.government.ParticipatingDemocracy=This is a democratic settlement where agreements are reached through \
  popular votes on [specific|individual] issues. A [parliament|board] of [ministers|executives] are responsible \
  for the implementation of decisions, and are voted for every [three|four|five] years.

