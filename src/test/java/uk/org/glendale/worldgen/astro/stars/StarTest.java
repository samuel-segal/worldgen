/*
 * Copyright (c) 2018, Samuel Penn (sam@glendale.org.uk).
 * See the file LICENSE at the root of the project.
 */
package uk.org.glendale.worldgen.astro.stars;

import org.junit.Test;
import uk.org.glendale.worldgen.astro.Physics;
import uk.org.glendale.worldgen.astro.systems.CircumstellarZone;

import static junit.framework.TestCase.assertEquals;

import static uk.org.glendale.worldgen.astro.Physics.*;
import static uk.org.glendale.worldgen.astro.systems.CircumstellarZone.*;

public class StarTest {

    /**
     * Test some of the equations for Sol, to ensure that they give sensible results.
     * Need to allow some leeway in the results, since we rarely get exactly what we're
     * looking for. A 1% variance is generally considered good enough.
     */
    @Test
    public void sol() {
        Star star = new Star();
        star.setName("Sol");
        star.setLuminosity(Luminosity.V);
        star.setSpectralType(SpectralType.G2);
        star.setRadius(Physics.SOL_RADIUS);

        assertEquals("Luminosity of Sol", SOL_LUMINOSITY, getStellarLuminosity(star), SOL_LUMINOSITY * 0.01);
        assertEquals("Temperature at 1 AU", STANDARD_TEMPERATURE, getTemperatureOfOrbit(star, AU), STANDARD_TEMPERATURE * 0.01);

        assertEquals("Mercury Zone", Hot, getZone(getTemperatureOfOrbit(star, 57 * MKM)));
        assertEquals("Venus Zone", Inner, getZone(getTemperatureOfOrbit(star, 108 * MKM)));
        assertEquals("Earth Zone", Middle, getZone(getTemperatureOfOrbit(star, 150 * MKM)));
        assertEquals("Mars Zone", Outer, getZone(getTemperatureOfOrbit(star, 230 * MKM)));
        assertEquals("Jupiter Zone", Cold, getZone(getTemperatureOfOrbit(star, 780 * MKM)));
        assertEquals("Saturn Zone", Cold, getZone(getTemperatureOfOrbit(star, 1430 * MKM)));
        assertEquals("Uranus Zone", Stygia, getZone(getTemperatureOfOrbit(star, 2880 * MKM)));
        assertEquals("Neptune Zone", Stygia, getZone(getTemperatureOfOrbit(star, 4500 * MKM)));
        assertEquals("Pluto Zone", Tartarus, getZone(getTemperatureOfOrbit(star, 5910 * MKM)));
    }
}
