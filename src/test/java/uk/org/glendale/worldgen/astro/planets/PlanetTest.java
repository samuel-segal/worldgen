/**
 * PlanetTest.java
 *
 * Copyright (c) 2017, Samuel Penn.
 * See the file LICENSE at the root of the project.
 */
package uk.org.glendale.worldgen.astro.planets;

import org.junit.Test;
import uk.org.glendale.worldgen.astro.Physics;
import uk.org.glendale.worldgen.astro.planets.codes.*;

import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class PlanetTest {

    @Test
    public void basicPlanetValues() {
        Planet planet = new Planet();
        planet.setId(5);
        planet.setName("Test");
        planet.setType(PlanetType.Hermian);
        planet.setRadius(1000);
        planet.setDistance(100);

        assertEquals(planet.getId(), 5);
        assertEquals(planet.getName(), "Test");
        assertEquals(planet.getType(), PlanetType.Hermian);
        assertEquals(planet.getRadius(), 1000);
        assertEquals(planet.getDiameter(), 2000);
        assertEquals(planet.getDistance(), 100);

        assertTrue(planet.isDwarf());
        assertFalse(planet.isJovian());
        assertFalse(planet.isBelt());
        assertFalse(planet.isSmallBody());
        assertFalse(planet.isTerrestrial());

        planet.setAxialTilt(22);
        planet.setEccentricity(42);
        planet.setInclination(17);
        assertEquals(22, planet.getAxialTilt());
        assertEquals("22°", planet.getAxialTiltText());
        assertEquals(42, planet.getEccentricity());
        assertEquals("0.420", planet.getEccentricityText());
        assertEquals(17, planet.getInclination());
        assertEquals("17°", planet.getInclinationText());

        planet.setPressure(0);
        assertEquals(0, planet.getPressure());
        assertEquals(Pressure.None, planet.getPressureText());
        planet.setPressure(100_000);
        assertEquals(100_000, planet.getPressure());
        assertEquals(Pressure.Standard, planet.getPressureText());
    }

    @Test
    public void boundedTests() {
        Planet planet = new Planet();

        planet.setHydrographics(50);
        assertEquals(planet.getHydrographics(), 50);

        planet.setHydrographics(-5);
        assertEquals(planet.getHydrographics(), 0);

        planet.setHydrographics(150);
        assertEquals(planet.getHydrographics(), 100);
    }

    @Test
    public void physicsTests() {
        Planet earth = new Planet();
        earth.setDensity(5514);
        earth.setRadius(6378);
        assertEquals(5.972e24, earth.getMass(), 1e23);
        assertEquals(9.8, earth.getSurfaceGravity());
        assertEquals(11186, earth.getEscapeVelocity(), 20);

        Planet mars = new Planet();
        mars.setDensity(3933);
        mars.setRadius(3396);
        assertEquals(6.39e23, mars.getMass(), 1e22);
        assertEquals(3.7, mars.getSurfaceGravity());
        assertEquals(5027, mars.getEscapeVelocity(), 20);
    }

    @Test
    public void spinTests() {
        Planet asteroid = new Planet();
        asteroid.setRadius(250);

        final long dayLength = Physics.getPeriodOfSpin(250_000, Physics.g);
        asteroid.setDayLength(dayLength);

        assertEquals("Gravity due to spin", Physics.g, asteroid.getSpinGravity(), 0.01);
    }

    @Test
    public void orbitTests() {
        Planet earth = new Planet();
        earth.setDensity(5514);
        earth.setRadius(6378);

        System.out.println(Physics.getOrbitalPeriod(earth.getMass(), 100_000));
    }

    @Test
    public void tradeCodeTests() {
        Planet planet = new Planet();

        assertFalse(planet.getTradeCodes().contains(TradeCode.Ba));
        planet.addTradeCode(TradeCode.Ba);
        assertTrue(planet.getTradeCodes().contains(TradeCode.Ba));
        planet.addTradeCode(TradeCode.As);
        planet.addTradeCode(TradeCode.In);
        assertTrue(planet.getTradeCodes().contains(TradeCode.Ba));
        assertTrue(planet.getTradeCodes().contains(TradeCode.As));
        assertTrue(planet.getTradeCodes().contains(TradeCode.In));
        planet.removeTradeCode(TradeCode.As);
        assertFalse(planet.getTradeCodes().contains(TradeCode.As));
        assertTrue(planet.getTradeCodes().contains(TradeCode.Ba));
        assertTrue(planet.getTradeCodes().contains(TradeCode.In));
    }

    /**
     * Test the isA() methods on planets.
     */
    @Test
    public void isTests() {
        Planet planet = new Planet();
        planet.setType(PlanetType.EuGaian);

        assertTrue(planet.isA(PlanetType.EuGaian));
        assertTrue(planet.isA(PlanetClass.Tectonic));
        assertTrue(planet.isA(PlanetGroup.Terrestrial));

        assertFalse(planet.isA(PlanetType.Hermian));
        assertTrue(planet.isA(PlanetType.EoGaian, PlanetType.MesoGaian, PlanetType.EuGaian));

        assertFalse(planet.isA(PlanetClass.GeoCyclic));
        assertTrue(planet.isA(PlanetClass.GeoCyclic, PlanetClass.GeoPassive, PlanetClass.Tectonic));

        assertFalse(planet.isA(PlanetGroup.Belt));
        assertTrue(planet.isA(PlanetGroup.Dwarf, PlanetGroup.Terrestrial, PlanetGroup.Helian));
    }
}
