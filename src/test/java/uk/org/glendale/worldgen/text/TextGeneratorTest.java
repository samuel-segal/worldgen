/**
 * TextGeneratorTest.java
 *
 * Copyright (c) 2017, Samuel Penn.
 * See the file LICENSE at the root of the project.
 */
package uk.org.glendale.worldgen.text;

import org.junit.Test;
import uk.org.glendale.utils.rpg.Die;
import uk.org.glendale.worldgen.astro.commodities.Commodity;
import uk.org.glendale.worldgen.astro.commodities.CommodityName;
import uk.org.glendale.worldgen.astro.commodities.Frequency;
import uk.org.glendale.worldgen.astro.planets.Planet;
import uk.org.glendale.worldgen.astro.planets.PlanetFeature;
import uk.org.glendale.worldgen.astro.planets.codes.PlanetType;
import uk.org.glendale.worldgen.astro.planets.codes.StarPort;
import uk.org.glendale.worldgen.astro.planets.codes.TradeCode;
import uk.org.glendale.worldgen.astro.planets.generators.Dwarf;
import uk.org.glendale.worldgen.astro.planets.generators.Terrestrial;
import uk.org.glendale.worldgen.astro.planets.generators.dwarf.Arean;
import uk.org.glendale.worldgen.astro.planets.generators.dwarf.EoArean;
import uk.org.glendale.worldgen.astro.sectors.Sector;
import uk.org.glendale.worldgen.astro.stars.Luminosity;
import uk.org.glendale.worldgen.astro.stars.SpectralType;
import uk.org.glendale.worldgen.astro.stars.Star;
import uk.org.glendale.worldgen.astro.systems.StarSystem;
import uk.org.glendale.worldgen.astro.systems.StarSystemType;
import uk.org.glendale.worldgen.astro.systems.Zone;

import java.util.HashSet;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

public class TextGeneratorTest {

    /**
     * Test that the property file is found, text used and the variable correctly expanded.
     */
    @Test
    public void basicPlanetDescription() {
        TextGenerator text;
        Planet          planet = new Planet();
        planet.setType(PlanetType.Hermian);
        planet.setRadius(1000);

        text = new TextGenerator(planet);
        String d = text.getFullDescription();

        assertEquals( "<p>A small barren world 1,000km in radius.</p>", d);
    }

    /**
     * Now test that the feature is correctly picked up and included in the output text.
     */
    @Test
    public void featureDescription() {
        TextGenerator text;
        Planet          planet = new Planet();
        planet.setType(PlanetType.Hermian);
        planet.setRadius(1000);
        planet.addFeature(Dwarf.DwarfFeature.MetallicSea);

        text = new TextGenerator(planet);
        String d = text.getFullDescription();

        assertEquals("<p>A small barren world 1,000km in radius. It has a sea of molten metal.</p>", d);
    }

    @Test
    public void propertyValues() {
        TextGenerator text;
        Planet          planet = new Planet();
        planet.setName("Eo");
        planet.setType(PlanetType.EoGaian);
        planet.setRadius(6789);
        planet.setPopulation(17_000_000_000_000L);

        text = new TextGenerator(planet);

        planet.setPopulation(1_178_000_000_000_000L);
        assertEquals("<p>Eo is 6,800km in radius with a population of 1,178 trillion.</p>",
                text.getFullDescription());

        planet.setPopulation(17_000_000_000_000L);
        assertEquals("<p>Eo is 6,800km in radius with a population of 17 trillion.</p>", text.getFullDescription());

        planet.setPopulation(1_700_000_000_000L);
        assertEquals("<p>Eo is 6,800km in radius with a population of 1.7 trillion.</p>", text.getFullDescription());

        planet.setPopulation(1_780_000_000_000L);
        assertEquals("<p>Eo is 6,800km in radius with a population of 1.8 trillion.</p>", text.getFullDescription());

        planet.setPopulation(789_123_000_000L);
        assertEquals("<p>Eo is 6,800km in radius with a population of 789 billion.</p>", text.getFullDescription());

        planet.setPopulation(5_432_000_000L);
        assertEquals("<p>Eo is 6,800km in radius with a population of 5.4 billion.</p>", text.getFullDescription());

        planet.setPopulation(23_456_789L);
        assertEquals("<p>Eo is 6,800km in radius with a population of 23 million.</p>", text.getFullDescription());

        planet.setPopulation(3_456_789L);
        assertEquals("<p>Eo is 6,800km in radius with a population of 3.5 million.</p>", text.getFullDescription());

        planet.setPopulation(456_789L);
        assertEquals("<p>Eo is 6,800km in radius with a population of 457 thousand.</p>", text.getFullDescription());

        planet.setPopulation(56_789L);
        assertEquals("<p>Eo is 6,800km in radius with a population of 57 thousand.</p>", text.getFullDescription());

        planet.setPopulation(6_789L);
        assertEquals("<p>Eo is 6,800km in radius with a population of 6,789.</p>", text.getFullDescription());
    }

    @Test
    public void extendedProperties() {
        TextGenerator text;
        Planet          planet = new Planet();
        planet.setName("Eo");
        planet.setType(PlanetType.EoGaian);
        planet.setHabitability(3);
        planet.setRadius(6789);
        planet.setPopulation(17_000_000_000_000L);

        text = new TextGenerator(planet);
        assertEquals("This world is III", text.parse("This world is $$Habitability"));

        assertEquals("This world is nice.", text.parse("This world is ($Habitability|3<not) nice."));
        planet.setHabitability(4);
        assertEquals("This world is not nice.", text.parse("This world is ($Habitability|3<not) nice."));

        planet.setHydrographics(77);
        assertEquals("The water covers 77%", text.parse("The water covers $Hydrographics%"));
    }

    @Test
    public void dieRoller() {
        int result = Die.roll("12");
        assertTrue("Invalid dice must return zero", result == 0);

        result = Die.roll("1D6");
        assertTrue("1D6", result >= 1 && result <= 6);

        result = Die.roll("3D6");
        assertTrue("3D6", result >= 3 && result <= 18);

        result = Die.roll("1D6+12");
        assertTrue("1D6+12", result >= 13 && result <= 18);

        result = Die.roll("2D4-2");
        assertTrue("2D4-2", result >= 0 && result <= 6);

        result = Die.roll("20D10");
        assertTrue("20D10", result >= 20 && result <= 200);
    }

    @Test
    public void dieRollerInText() {
        TextGenerator text;
        Planet          planet = new Planet();
        planet.setName("Eo");
        planet.setType(PlanetType.EoGaian);

        text = new TextGenerator(planet);

        // Should output numbers as words, such as one or two.
        String result = text.parse("$$1D6");
        assertTrue("Die roll as words", result.matches("(one|two|three|four|five|six)"));

        // Should output a number with commas, such as 1,234
        result = text.parse("$1000D6");
        assertTrue("Die roll with comma", result.matches("[0-9],[0-9]{3}"));

        // Should output a pure number, such as 1234
        result = text.parse("$!1000D6");
        assertTrue("Die roll with pure digits", result.matches("[0-9]{4}"));

        // Should output rounded number as words, such as 50 thousand
        result = text.parse("$~1000D100");
        assertTrue("Die roll with simplified words", result.matches("[0-9]+ thousand"));
    }

    @Test
    public void selectors() {
        TextGenerator text;
        Planet          planet = new Planet();
        planet.setName("Eo");
        planet.setType(PlanetType.EoGaian);

        text = new TextGenerator(planet);
        text.addPhrase("testA", "Test Alpha");
        text.addPhrase("testB", "Test Beta");

        String result = text.parse("This is [large|medium|small].");
        assertTrue("Large, Medium or Small", result.matches("This is (large|medium|small)."));

        result = text.parse("This is [a test|{testA}].");
        assertTrue("Expand within selector", result.matches("This is (a test|Test Alpha)."));

        result = text.parse("This is {test[A|B]}.");
        assertTrue("Selector within expand", result.matches("This is Test (Alpha|Beta)."));
    }

    @Test
    public void switcher() {
        TextGenerator text;
        Planet          planet = new Planet();
        planet.setName("Eo");
        planet.setType(PlanetType.EoGaian);

        text = new TextGenerator(planet);
        text.addPhrase("test", "This world is ($!Radius|6000=perfect|6000>small|6000<large).");

        planet.setRadius(6000);
        assertEquals("Perfect", "This world is perfect.", text.parse("{test}"));

        planet.setRadius(7000);
        assertEquals("Large", "This world is large.", text.parse("{test}"));

        planet.setRadius(5000);
        assertEquals("Small", "This world is small.", text.parse("{test}"));

        text.addPhrase("test2", "Test is ($!Radius|1=small|2,3,4=perfect|5=large|bigger).");
        planet.setRadius(1);
        assertEquals("Switch 1", "Test is small.", text.parse("{test2}"));
        planet.setRadius(2);
        assertEquals("Switch 2", "Test is perfect.", text.parse("{test2}"));
        planet.setRadius(3);
        assertEquals("Switch 3", "Test is perfect.", text.parse("{test2}"));
        planet.setRadius(4);
        assertEquals("Switch 4", "Test is perfect.", text.parse("{test2}"));
        planet.setRadius(5);
        assertEquals("Switch 5", "Test is large.", text.parse("{test2}"));
        planet.setRadius(6);
        assertEquals("Switch 6", "Test is bigger.", text.parse("{test2}"));

        text.addPhrase("test3", "($!Radius|5>large|)");
        assertEquals("No output", "", text.parse("{test3}"));

        planet.setLawLevel(2);
        assertEquals("Low law", "This is a hippy place.", text.parse("{government.($LawLevel|0,1,2,3=Hippy|4,5,6=Strict)}"));
        planet.setLawLevel(5);
        assertEquals("High law", "This is a dystopia.", text.parse("{government.($LawLevel|0,1,2,3=Hippy|4,5,6=Strict)}"));
    }

    @Test
    public void featureSwitcher() {
        TextGenerator text;
        Planet          planet = new Planet();
        planet.setName("Eo");
        planet.setType(PlanetType.EoGaian);
        planet.addFeature(Terrestrial.TerrestrialFeature.Dry);

        text = new TextGenerator(planet);
        text.addPhrase("test.1", "It is (?|Hot|Cold|Dry) here.");
        text.addPhrase("test.2", "It is (?normal|Hot|Cold|Dry) here.");
        text.addPhrase("test.3", "It is (?normal|Hot=hot|Cold=too cold|Dry={desert}) here.");
        text.addPhrase("desert", "like a desert");

        assertEquals("Dry", "It is Dry here.", text.parse("{test.1}"));

        planet.addFeature(Terrestrial.TerrestrialFeature.Cold);
        assertEquals("Cold", "It is Cold here.", text.parse("{test.1}"));

        planet.setFeatures(new HashSet<PlanetFeature>());
        assertEquals("Empty", "It is here.", text.parse("{test.1}"));
        assertEquals("normal", "It is normal here.", text.parse("{test.2}"));

        planet.addFeature(Terrestrial.TerrestrialFeature.Dry);
        assertEquals("desert", "It is like a desert here.", text.parse("{test.3}"));

        planet.addFeature(Terrestrial.TerrestrialFeature.Cold);
        assertEquals("desert", "It is too cold here.", text.parse("{test.3}"));
    }

    @Test
    public void resourceSwitcher() {
        TextGenerator text;
        Planet          planet = new Planet();
        planet.setName("Eo");
        planet.setType(PlanetType.EoGaian);

        planet.addResource(new Commodity() {{
            setName(CommodityName.FerricOre.getName());
            setFrequency(Frequency.COMMON);
        }}, 500);
        planet.addResource(new Commodity() {{
            setName(CommodityName.SilicateOre.getName());
            setFrequency(Frequency.COMMON);
        }}, 400);
        planet.addResource(new Commodity() {{
            setName(CommodityName.CarbonicOre.getName());
            setFrequency(Frequency.COMMON);
        }}, 300);

        text = new TextGenerator(planet);
        text.addPhrase("test.1", "The most common is (%|CarbonicOre|FerricOre|SilicateOre).");
        text.addPhrase("test.2", "The most common is (%|!CarbonicOre|!SilicateOre|!FerricOre).");
        text.addPhrase("test.3", "The most common is (%nothing|CaronicOre|FericOre|SlicateOre).");
        text.addPhrase("test.4", "{test.(%|!FerricOre|!SilicateOre|!CarbonicOre)}");
        text.addPhrase("test.5", "(%450,SomethingElse|CarbonicOre|SilicateOre)");
        text.addPhrase("test.6", "(%150,SomethingElse|CarbonicOre|SilicateOre)");
        text.addPhrase("test.FerricOre", "This is Ferric.");
        text.addPhrase("test.SilicateOre", "This is Silicate.");
        text.addPhrase("test.CarbonicOre", "This is Carbonic.");

        assertEquals("Find Ferric expanded", "The most common is Ferric Ore.", text.parse("{test.1}"));
        assertEquals("Find Ferric raw", "The most common is FerricOre.", text.parse("{test.2}"));
        assertEquals("Find nothing", "The most common is nothing.", text.parse("{test.3}"));
        assertEquals("Get sub phrase", "This is Ferric.", text.parse("{test.4}"));
        assertEquals("Minimum 450", "SomethingElse", text.parse("{test.5}"));
        assertEquals("Minimum 150", "Silicate Ore", text.parse("{test.6}"));
    }

    /**
     * Test having multiple switches in the same text. This was causing a bug.
     */
    @Test
    public void multiSwitchers() {
        TextGenerator text;
        Planet          planet = new Planet();
        planet.setName("Eo");
        planet.setType(PlanetType.EoGaian);
        planet.addFeature(Terrestrial.TerrestrialFeature.Dry);

        text = new TextGenerator(planet);
        text.addPhrase("test.1", "It is ($!Radius|3000<large|small).");
        text.addPhrase("test.2", "It is ($!Radius|3000<large|small) and ($!Density|3000<dense|light).");

        planet.setRadius(4000);
        assertEquals("One switch", "It is large.", text.parse("{test.1}"));

        planet.setDensity(5000);
        assertEquals("Two switches", "It is large and dense.", text.parse("{test.2}"));
    }

    @Test
    public void tradeCodes() {
        TextGenerator text;
        Planet          planet = new Planet();
        planet.setName("Eo");
        planet.setType(PlanetType.EoGaian);
        planet.addTradeCode(TradeCode.Na);
        planet.addTradeCode(TradeCode.Lo);


        assertFalse(planet.getTradeCodes().contains(TradeCode.In));
        text = new TextGenerator(planet);
        text.addPhrase("test.1", "This world has industry. @+In Which is good.");
        assertEquals("Industry", "This world has industry. Which is good.", text.parse("{test.1}"));
        assertTrue(planet.getTradeCodes().contains(TradeCode.In));

        text.addPhrase("test.2", "This world has no industry@-In.");
        assertEquals("Industry", "This world has no industry.", text.parse("{test.2}"));
        assertFalse(planet.getTradeCodes().contains(TradeCode.In));

        text.addPhrase("test.3", "This world has a civil war.@+AMBER");
        assertEquals("Amber text", "This world has a civil war.", text.parse("{test.3}"));
        assertEquals("Amber Zone", Zone.AMBER, planet.getZone());

        text.addPhrase("test.4", "This world has a nano-plague. @+RED Do not land.");
        assertEquals("Red text", "This world has a nano-plague. Do not land.", text.parse("{test.4}"));
        assertEquals("Red Zone", Zone.RED, planet.getZone());

        text.addPhrase("test.5", "This world has a civil war @+AMBER and a nano-plague. @+RED");
        assertEquals("Red text", "This world has a civil war and a nano-plague.", text.parse("{test.5}"));
        assertEquals("Red Zone", Zone.RED, planet.getZone());
    }

    @Test
    public void commodities() {
        TextGenerator text;
        Planet          planet = new Planet();
        planet.setName("Eo");
        planet.setType(PlanetType.EoGaian);


    }

    @Test
    public void starProperties() {
        TextGenerator text;

        Sector     sector = new Sector("Sector", 2, 3);
        StarSystem system = new StarSystem(sector, "Test", 11, 13, StarSystemType.CLOSE_BINARY, Zone.AMBER);
        system.addStar(new Star() {{
            setName("Alpha");
            setSpectralType(SpectralType.G2);
            setLuminosity(Luminosity.V);
        }});
        system.addStar(new Star() {{
            setName("Beta");
            setSpectralType(SpectralType.M5);
            setLuminosity(Luminosity.VI);
        }});

        text = new TextGenerator(system, "Test.Example");
        assertEquals("Star properties", "This is system Test. The primary star is a G2 type star.",
                text.getSystemDescription("Example"));
    }

    @Test
    public void booleanProperties() {
        TextGenerator text;
        Sector     sector = new Sector("Sector", 2, 3);
        StarSystem system = new StarSystem(sector, "Test", 11, 13, StarSystemType.SINGLE, Zone.AMBER);
        system.addStar(new Star() {{
            setName("Alpha");
            setSpectralType(SpectralType.G2);
            setLuminosity(Luminosity.V);
        }});
        system.addPlanet(new Planet() {{
            setName("Test I");
            setStarPort(StarPort.A);
            setPopulation(500_000_000);
        }});

        text = new TextGenerator(system, "Test.Example");

        assertEquals("Test for true boolean", "This is true", text.parse("This is $MainWorld"));
        assertEquals("True switch", "The world of Test I has a population of 500,000,000.",
                text.getSystemDescription("Stats"));

        system.getPlanets().get(0).setPopulation(0);
        assertEquals("Test for false boolean", "This is false", text.parse("This is $MainWorld"));
        assertEquals("True switch", "There is no populated world here.",
                text.getSystemDescription("Stats"));
    }

    @Test
    public void randomSelector() {
        TextGenerator text;
        Planet          planet = new Planet();
        planet.setName("Eo");
        planet.setType(PlanetType.EoGaian);

        text = new TextGenerator(planet);

        String result = text.parse("This is [one|two|three].");
        assertTrue(result.matches("This is (one|two|three)."));

        result = text.parse("This is [NUM::one|two|three] and [NUM::].");
        assertTrue("Saved result", result.matches("This is (one and one|two and two|three and three)."));

        String result2 = text.parse("This is [NUM::one|two|three] and [NUM::].");
        assertEquals("Selections match between calls", result, result2);
    }

    @Test
    public void addFeature() {
        TextGenerator text;
        Planet planet = new Planet();
        planet.setName("EoArean Test");
        planet.setType(PlanetType.EoArean);

        text = new TextGenerator(planet);

        String result = text.parse("This is a white world. @=SaltPlains");
        assertTrue(result.matches("This is a white world."));
        assertTrue(planet.hasFeature(Arean.AreanFeature.SaltPlains));

        result = text.parse("This is a dark sea. @=MurkySea");
        assertTrue(planet.hasFeature(Arean.AreanFeature.MurkySea));
    }

    @Test
    public void cleanWhiteSpace() {
        TextGenerator text;
        Planet planet = new Planet();
        planet.setName("Space");
        planet.setType(PlanetType.EoArean);

        text = new TextGenerator(planet);

        String result = text.parse("This ,  is    my [world] .");
        assertEquals("This, is my world.", result);
    }
}
